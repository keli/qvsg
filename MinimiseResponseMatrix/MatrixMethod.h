
#include "utils/Utils.h"
#include "utils/AtlasStyle.C"
//#include "JES_ResponseFitter/JES_BalanceFitter.h"

#include <utility>

#include "TCanvas.h"
#include "TSystem.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TMatrixD.h"
#include "TVectorT.h"

//Eta bins combination
struct str_BinCombination
{
  int l1i=-1, l2i=-1, r1i=-1, r2i=-1;
  double N1=0, N2=0;
  double R=0, R_pe=0, dR=0;
};

bool _isMC;

bool _symmetrizeMatrixEmptyBins = false;

bool _subtractPtclAsym          = false;
TFile *truthFileForData;
Str inputTruthFileName          = "";

bool _doSystematics             = false;
Str _systematicShift            = "";
Str _systematicsFileNameSM      = "";
TFile *_systematicsFileSM;
Str _systematicsFileNameMM      = "";
TFile *_systematicsFileMM;

bool _doToys              = true;
bool _useWeights          = true;
bool _useFit              = true;
bool _rejectBasedOnBadFit = false;
bool _absEta              = false;
bool _isTrigOR            = true;
bool _defaultROOTMean 		= false;
bool _GetMeanFromNominal  = true;
bool _FoundMeanFromNominal;
bool _useAnalyticSolution = true;
//Eta bins combination
bool _doEtaBinsCombination = true;
double _doCombNeffMin = 10;

double Lambda = 0;

Str _etaBinning;

VecD _pTBINS;

TFile *inputFile, *outfile;
Str outfileName;
double lEtaMin, lEtaMax, rEtaMin, rEtaMax;
Str configFN;

Str MCgen;
StrV MCsamples;

StrV trigs;

//number of pseudoexperiments per algorithm to propagate the errors
Int_t n_pe       = 1000;
double maxNsigma = -1.0;
double NeffMin   = 45;

const Int_t N_BINS_MAX = 100;
double beta[N_BINS_MAX][N_BINS_MAX];
double beta_pe[N_BINS_MAX][N_BINS_MAX];
double err_pe[N_BINS_MAX][N_BINS_MAX];
double err[N_BINS_MAX][N_BINS_MAX];
int _Nbins;

TRandom3 trand3;
Str JNumb;

// variables read from TVectotT of root file
bool _isInclusionTrigComb;
bool _isSlimHistos; //SlimHistos


//variables that are read in from config file... (at least most of them)

StrV jetAlgos;
VecD _etaBinsSM, _ptBins;
TString histogramNamePrefix;
map<int,VecD> _etaBinMap;
VecD refRegion;

map<int,vector<double> > PtBinMap;

//helper variables
int current_ptRange;
Str ptRangeStr, etaBinning;

Str dataDesc, ptRangeDesc, currentTrig, currentTrigLong, currentTrigDesc;
Str jetAlgo, trig1, trig2, truthJetAlgo;
Str DirName;

double ptmin, ptmax;
int ptbin;

TH2F *PEc_vs_eta;
vector<float> *v_PEc_vs_eta;
vector<VecD> ptRanges;

TCanvas *Can;
TLatex *tex;
TLine *line;

////Initialize function///
void MinimizeData(VecD etaBins);
void MinimizeMC(VecD etaBins);
void MergeHistosAndMinimize(Str prefix, VecD etaBins, vector<TH3F*> histos, vector<TH1D*> etaSpectra, StrV desc, vector<TH3F*> histos_truth);
void Minimize(TH2F *matrix, vector<str_BinCombination> &v_CombinationResults);
TEnv *Init(Str config);
void NormalizeAndSaveResults(const double *xs, TH2F *PEc_vs_Eta, vector<float> *v_PEc_vs_eta);
void InvertAndAdd(TH1D *h, TH1D *h2);
void GetBinRange(TAxis *axis, double min, double max, int &minBin, int &maxBin);
void DrawEtaSpectra(vector<TH1D*> etaSpectra, bool scale=true, vector<int> stat=vector<int>(), Str comment="");
void PrintToPDF();
void ExtractPtEtaMean(vector<TH2F*> meanHistos, Str method, double pt_min, double pt_max);

std::pair<double,double> JudgeFit(TF1 *fit, double A, double dA, double Neff);

double * AnalyticSolution(bool &success, int NetaBins, vector<str_BinCombination> &v_CombinationResults);


TObject* Get(Str objName) {
  inputFile->cd( jetAlgo);
  TObject *obj = gDirectory->Get(objName);
  if (obj==NULL) error("Can't access "+objName+" in "+inputFile->GetName());
  return obj;
}


TH3F *GetTruth3DAsym(Str hname) {
  TH3F * h3d;
  h3d = (TH3F*)truthFileForData->Get(jetAlgo+"/"+hname);
  if(h3d==NULL) error("Cannot find histogram: "+jetAlgo+"/"+hname);

  return h3d;
}


TH1D *InvertHisto(TH1D *h);
TH3F *GetAsymmetry3DHisto(Str prefix, int lowPtBin, int highPtBin, Str j3desc);
TH3F *GetAsymmetry3DHistoTruth(Str prefix, int lowPtBin, int highPtBin, Str desc);
TH1D *GetHisto(Str hname)   { return (TH1D*)Get(hname); }
TH2F *Get2DHisto(Str hname) { return (TH2F*)Get(hname); }
TH3F *Get3DHisto(Str hname) {  TH3F* h3d = (TH3F*)Get(hname); if(h3d==NULL) error("Cannot find histogram: "+hname); return h3d; }
TProfile2D *Get2DProfile(Str hname) { return (TProfile2D*)Get(hname); }
TGraphErrors *MakeFinalGraph(TH2F *PEc_vs_eta);
TGraphErrors *MakeFinalGraph(TH2F *PEc_vs_eta, vector<float> *v_PEc_vs_eta);

double BetaMin(const double *xx);
double GetGaus();

double GetMean(TH1* h){
	if(_defaultROOTMean)
		return h->GetMean();
	static int cloneCount =0;cloneCount++;

	TH1* hClone = (	TH1* )h->Clone(Form("%s_%d",h->GetName(),cloneCount));cloneCount++;
	TH1* hClone2 = (	TH1* )h->Clone(Form("%s_%d",h->GetName(),cloneCount));
	TAxis *xaxis = hClone->GetXaxis();
	Int_t binLow = xaxis->FindBin(-0.7);
	Int_t binHigh = xaxis->FindBin(0.7);
	xaxis->SetRange(binLow, binHigh);
	double mean1 = hClone->GetMean();

	xaxis = hClone2->GetXaxis();
	binLow = xaxis->FindBin(mean1-0.5);
	binHigh = xaxis->FindBin(mean1+0.5);
	xaxis->SetRange(binLow, binHigh);
	double mean2 = hClone2->GetMean();

  h->GetXaxis()->SetRange(binLow, binHigh); //range changed so that when MeanErr() is executed, the histogram has the same range used in GetMean()

	return mean2;
}
/*

double GetMean(TH1* h){
	bool cleverShortMean = true;
	if(!cleverShortMean)
		return h->GetMean();

	TAxis *xaxis = h->GetXaxis();
	Int_t binLow = xaxis->FindBin(-0.5);
	Int_t binHigh = xaxis->FindBin(0.5);
	xaxis->SetRange(binLow, binHigh);
	double mean1 = h->GetMean();
	binLow = xaxis->FindBin(mean1-0.5);
	binHigh = xaxis->FindBin(mean1+0.5);
	xaxis->SetRange(binLow, binHigh);
	double mean2 = h->GetMean();
	xaxis->SetRange(1, ((TH1F*)h)->GetSize()-2);

	return mean2;
}
*/
double MeanErrShorterendRange(TH1 *h) {
	TAxis *xaxis = h->GetXaxis();
  Int_t binLow = xaxis->FindBin(-0.5);
  Int_t binHigh = xaxis->FindBin(0.5);

	xaxis->SetRange(binLow, binHigh);
	double err = h->GetRMS()/sqrt(h->GetEffectiveEntries()+1e-30);
	xaxis->SetRange(0,h->GetEntries());
}
double MeanErr(TH1 *h) {return h->GetRMS()/sqrt(h->GetEffectiveEntries()+1e-30); }

bool isRef(double eta) {
  double AbsEta=fabs(eta);
  if ( refRegion[0]<=AbsEta && AbsEta<refRegion[1] ) return true;
  return false;
}

int GetColor(int i) {
  if (i==0) return kBlack;
  if (i==1) return kRed; if (i==2) return kAzure;
  if (i==3) return kGreen+2; if (i==4) return kViolet+4;
  if (i==5) return kSpring; if (i==6) return kMagenta;
  if (i==7) return kAzure-4; if (i==8) return kOrange-4;
  if (i==9) return kViolet-4; if (i==10) return kRed-4;
  if (i==11) return kOrange-4; if (i==12) return kBlue-4;
  if (i==13) return kPink-4; if (i==14) return kSpring-4;
  if (i>13) return i;
  return kBlack;
}


void PrintText(Str txt, double x, double y, int align, int col) {
  tex->SetTextColor(GetColor(col));
  tex->SetTextAlign(align*10+2);
  tex->DrawLatex(x,y,txt);
  tex->SetTextAlign(12);
}

void PrintTextR(Str txt, double y, int col, double x=0.88) { PrintText(txt,x,y,3,col); }
void PrintTextL(Str txt, double y, int col, double x=0.20) { PrintText(txt,x,y,1,col); }


void PrintTrigAndPt(bool right) {
  if (right) {
    PrintTextR(currentTrigDesc,0.27,0);
    PrintTextR(ptRangeDesc,0.21,0);
  } else {
    PrintTextL(currentTrigDesc,0.90,0);
    PrintTextL(ptRangeDesc,0.84,0);
  }
}

void PrintData(int align=1) {
  if (align==1) {
    PrintTextL(dataDesc,0.90,0);
    PrintTextL(jetAlgo,0.84,0);
  } else if (align==2) { // center
    PrintText(dataDesc,0.5,0.9,0,0);
    PrintText(jetAlgo,0.5,0.85,0,0);
  } else { //right
    PrintTextR(dataDesc,0.90,0);
    PrintTextR(jetAlgo,0.85,0);
  }
}

/*
int OptimalRebin(TH1 *h)
{
  int method=2;

  // Get optimal bin withs using Scott's Choise
  double N=h->GetEffectiveEntries();
  //Neff(h);
  //double optWidth = _optBinParam*h->GetRMS()/pow(N,1.0/3);
  double optWidth = 3.5*h->GetRMS()/pow(N,1.0/3);
  int Nbins=h->GetNbinsX();
  double range=h->GetBinLowEdge(Nbins+1)-h->GetBinLowEdge(1);
  int rebin=1;
  double prevWidth=range/Nbins;
  for (int i=1;i<Nbins;++i) {
    if (Nbins%i!=0) continue;
    double binWidth=range/Nbins*i;
    //if (binWidth>maxWidth) continue;

    if (method==1) {
      // optimistic
      if (binWidth<optWidth) rebin=i;
    } else if (method==2) {
      if (TMath::Abs(binWidth-optWidth) <
          TMath::Abs(prevWidth-optWidth)) rebin=i;
    }
    else rebin=i; // method 3

    if (binWidth>optWidth) break;
    prevWidth=binWidth;
  }
  //printf("Optimal bin-width: %.3f\n",optWidth);
  //h->Rebin(rebin);
  //printf("Histo bin-width: %.3f\n",h->GetBinWidth(1));
  return rebin;
}
*/

int OptimalRebin(TH1 *h)
{
  int method=1;

  // Get optimal bin withs using Scott's Choise
  double N=h->GetEffectiveEntries();
  //Neff(h);
  //double optWidth = _optBinParam*h->GetRMS()/pow(N,1.0/3);
  double optWidth = 3.5*h->GetRMS()/TMath::Power(N,1.0/3);
  int Nbins=h->GetNbinsX();
  double range=h->GetBinLowEdge(Nbins+1)-h->GetBinLowEdge(1);
  int rebin=1;
  double prevWidth=range/Nbins;
  for (int i=1;i<Nbins;++i) {
    if (Nbins%i!=0) continue;
    double binWidth=range/Nbins*i;
    //if (binWidth>maxWidth) continue;

    if (method==1) {
      // optimistic
      if (binWidth<optWidth) rebin=i;
    } else if (method==2) {
      if (TMath::Abs(binWidth-optWidth) <
          TMath::Abs(prevWidth-optWidth)) rebin=i;
    }
    else rebin=i; // method 3

    if (binWidth>optWidth) break;
    prevWidth=binWidth;
  }
  bool _vebose=false;
  if (_vebose) {
    printf("\n%s\n  RMS: %.3f, Neff: %.3f\n",h->GetName(),h->GetRMS(),N);
    printf("  Opt width: %6.3f, histo binwidth: %6.3f => Rebin: %d\n",optWidth,range/Nbins,rebin);
  }
  return rebin;
}


/*
TF1* DrawAhistos(TH1D *hA, vector<TH1D*> Ahistos, StrV histDesc, bool isSM=false) {

  outfile->cd(jetAlgo+"_Histos");
  //int rebin  = OptimalRebin(hA);
  int fitCol = kOrange-3;

  double NsigmaForFit = 2.0;
  JES_BalanceFitter* m_BalFit = new JES_BalanceFitter(NsigmaForFit);
  m_BalFit->SetGaus();
  m_BalFit->SetFitOpt("RQ0");
  m_BalFit->Fit( hA , -3.0);

  TF1 *gaus = m_BalFit->GetFit();
  TH1D* hFit = (TH1D*)m_BalFit->GetHisto();
  int rebin = hA->GetNbinsX()/hFit->GetNbinsX();

  Str leta = Form("_etaL_%.1fto%.1f",lEtaMin,lEtaMax);
  Str reta = Form("_etaR_%.1fto%.1f",rEtaMin,rEtaMax);
  Str ptR  = Form("pT_%.0fto%.0f",ptmin,ptmax);
  Str TRIG = currentTrigDesc;
  TRIG = TRIG.ReplaceAll("EF_","").ReplaceAll("_OR","OR").ReplaceAll("a4tc_EFFS","");
  hFit->SetName(TRIG+ptR+leta+reta);
  //cout << hFit->GetName() << endl;
  hFit->Write();


  gaus->SetLineColor(fitCol);
  gaus->SetLineWidth(2);

  TH1D *hasym = hFit;
  hasym->SetMarkerStyle(1);
  double binWidth=hasym->GetBinWidth(1);

  hasym->SetXTitle("A = (p_{T}^{left} - p_{T}^{right})/p_{T}^{avg}");
  if (isSM) hasym->SetXTitle("A = (p_{T}^{probe} - p_{T}^{ref})/p_{T}^{avg}");
  hasym->Draw("e");
  double y=0.92, dy=0.05, x=0.21;
  tex->SetTextAlign(12);
  tex->DrawLatex(x,y-=dy,Form("N_{eff} = #font[62]{%.1f}",hasym->GetEffectiveEntries()));
  tex->DrawLatex(x,y-=dy,Form("Bin-width: %.3f",binWidth));
  tex->DrawLatex(x,y-=dy,Form("A: (#font[62]{%.1f} #pm %.1f)%%",hasym->GetMean()*100,MeanErr(hasym)*100));
  tex->DrawLatex(x,y-=dy,Form("#sigma: (%.1f #pm %.1f)%%",hasym->GetRMS()*100,hasym->GetRMSError()*100));

  gaus->Draw("same");
  //gaus2->SetLineColor(kMagenta);
  //gaus2->SetLineWidth(2);
  //gaus2->Draw("same");

  tex->SetTextColor(kOrange-6);
  tex->DrawLatex(x,y-=dy,Form("Fit: (#font[62]{%.1f} #pm %.1f)%%",gaus->GetParameter(1)*100,gaus->GetParError(1)*100));
  tex->DrawLatex(x,y-=dy,Form("#sigma: (%.1f #pm %.1f)%%",gaus->GetParameter(2)*100,gaus->GetParError(2)*100));
  tex->DrawLatex(x,y-=dy,Form("#chi^{2}/Ndof: %.2f",gaus->GetChisquare(),gaus->GetNDF()) );

  //tex->SetTextColor(kMagenta);
  //tex->DrawLatex(x,y-=dy,Form("Fit: (#font[62]{%.1f} #pm %.1f)%%",gaus2->GetParameter(1)*100,gaus2->GetParError(1)*100));
  //tex->DrawLatex(x,y-=dy,Form("#sigma: (%.1f #pm %.1f)%%",gaus2->GetParameter(2)*100,gaus2->GetParError(2)*100));
  //tex->DrawLatex(x,y-=dy,Form("#chi^{2}/Ndof: %.2f",gaus2->GetChisquare(),gaus2->GetNDF()) );

  tex->SetTextAlign(32);
  y=0.92-0.15;

  for (int hi=0;hi<Ahistos.size();++hi) {
    if (Ahistos[hi]->GetEntries()==0) continue;
    Ahistos[hi]->Rebin(rebin);
    int col=GetColor(hi+1);
    Ahistos[hi]->SetLineColor(col);
    Ahistos[hi]->SetMarkerStyle(1);
    if (hi==0) Ahistos[hi]->SetLineStyle(2);
    Ahistos[hi]->Draw("samehist");
    tex->SetTextColor(col);
    tex->DrawLatex(0.91,y-=dy,Form("%s: (%.1f #pm %.1f)%%", histDesc[hi].Data(),
				   Ahistos[hi]->GetMean()*100, MeanErr(Ahistos[hi])*100));
  }
  if (binWidth>0.25) return 0;
  return gaus;
}
*/


TF1* DrawAhistos(TH1D *hA, vector<TH1D*> Ahistos, StrV histDesc, bool isSM=false, bool isTruthSub=false) {
  hA->GetXaxis()->SetRange(0,0);//reset the range which was changed in GetMean()

  outfile->cd(jetAlgo+"_Histos");
  int rebin  = OptimalRebin(hA);

  if(isTruthSub) histDesc = MCsamples;


  hA->Rebin(rebin);

  static int fiti=0;
  TF1 *gaus = new TF1(Form("f%d",fiti),"gaus",-0.8,0.8);
  if (hA->GetEffectiveEntries()>NeffMin) {
    hA->Fit(gaus,"RQ0");
    double mid=gaus->GetParameter(1), sig=gaus->GetParameter(2);
    double Nsig=2.0;
    //the commented test is no more used, directly use the restricted range
		/*if(fabs(mid-sig*Nsig) >= 0.8 || fabs(mid+sig*Nsig) >= 0.8){
		  gaus->SetRange(hA->GetBinCenter(1),hA->GetBinCenter(hA->GetSize()-1));
		  hA->Fit(gaus,"RQ0");
		}else{*/
		  gaus->SetRange(mid-sig*Nsig,mid+sig*Nsig);
		  hA->Fit(gaus,"RQ0");
		  mid=gaus->GetParameter(1), sig=gaus->GetParameter(2);
		  gaus->SetRange(mid-sig*Nsig,mid+sig*Nsig);
		  hA->Fit(gaus,"RQ0");
		//}
  }


  Str leta = Form("_etaL_%.1fto%.1f",lEtaMin,lEtaMax);
  Str reta = Form("_etaR_%.1fto%.1f",rEtaMin,rEtaMax);
  Str ptR  = Form("pT_%.0fto%.0f",ptmin,ptmax);
  Str TRIG = currentTrigDesc;
  TRIG = TRIG.ReplaceAll("EF_","").ReplaceAll("_OR","OR").ReplaceAll("a4tc_EFFS","");
  hA->SetName(TRIG+ptR+leta+reta);
  //cout << hA->GetName() << endl;
  hA->Write();


  hA->SetMarkerStyle(1);
  double binWidth=hA->GetBinWidth(1);

  hA->SetXTitle("A = (p_{T}^{left} - p_{T}^{right})/p_{T}^{avg}");
  if (isSM) hA->SetXTitle("A = (p_{T}^{probe} - p_{T}^{ref})/p_{T}^{avg}");
  hA->DrawCopy("e");
  double y=0.92, dy=0.05, x=0.21;
  double A = GetMean(hA);//->GetMean();
  double R = (2.0+A)/(2.0-A);
  double dA = MeanErr(hA);
  hA->GetXaxis()->SetRange(0,0);//reset the range which was changed in GetMean()

  tex->SetTextAlign(12);
  tex->DrawLatex(x,y-=dy,Form("N_{eff} = #font[62]{%.1f}",hA->GetEffectiveEntries()));
  tex->DrawLatex(x,y-=dy,Form("Bin-width: %.3f",binWidth));
  tex->DrawLatex(x,y-=dy,Form("A: (#font[62]{%.1f} #pm %.1f)%%",A*100,dA*100));
  tex->DrawLatex(x,y-=dy,Form("R: (#font[62]{%.3f})",R));
  tex->DrawLatex(x,y-=dy,Form("#sigma: (%.1f #pm %.1f)%%",hA->GetRMS()*100,hA->GetRMSError()*100));


  tex->SetTextAlign(32);
  double y2=0.92-0.15;

  for (int hi=0;hi<Ahistos.size();++hi) {
    if (Ahistos[hi]->GetEntries()==0) continue;

    Ahistos[hi]->Rebin(rebin);
    int col=GetColor(hi+1);
    Ahistos[hi]->SetLineColor(col);
    Ahistos[hi]->SetMarkerStyle(1);
    if (hi==0) Ahistos[hi]->SetLineStyle(2);
    Ahistos[hi]->DrawCopy("samehist");

    tex->SetTextColor(col);
    tex->DrawLatex(0.91,y2-=dy,Form("%s(%.0f):(%.1f #pm %.1f)%%", histDesc[hi].Data(),Ahistos[hi]->GetEffectiveEntries(),
    Ahistos[hi]->GetMean()*100, MeanErr(Ahistos[hi])*100));
  }


  if (binWidth>0.25) return 0;

  //int fitCol = kOrange-3;
  int fitCol = kMagenta-3;
  tex->SetTextAlign(12);
  tex->SetTextColor(fitCol);
  double chi2NDF = gaus->GetChisquare()/gaus->GetNDF();
  tex->DrawLatex(x,y-=dy,Form("Fit: (#font[62]{%.1f} #pm %.1f)%%",gaus->GetParameter(1)*100,gaus->GetParError(1)*100));
  tex->DrawLatex(x,y-=dy,Form("#sigma: (%.1f #pm %.1f)%%",gaus->GetParameter(2)*100,gaus->GetParError(2)*100));
  tex->DrawLatex(x,y-=dy,Form("#chi^{2}/Ndof: %.2f",chi2NDF) );
  gaus->SetLineColor(fitCol);
  gaus->SetLineWidth(3);
  gaus->DrawCopy("same");

  return gaus;
}


void MakePDF(Str psName) {
  Can->Print(psName+"]");
  Str pdf(psName); pdf.ReplaceAll(".ps",".pdf");
  int stat = gSystem->Exec(Form("ps2pdf %s %s",psName.Data(),pdf.Data()));
  if (stat==0)  gSystem->Exec(Form("rm %s",psName.Data()));
  else { printf("\nWARNING: could not convert %s to %s\n",psName.Data(),pdf.Data()); }
  if (stat==0) printf("\nProduced\n  %s\n\n",pdf.Data());
}

void DrawGuideLines(double etaMax=4.5) {
  // Draw "guide-lines"
  double etaMin = -etaMax;
  if(_absEta) etaMin = 0.0;
  line->SetLineStyle(2);
  line->SetLineColor(kGray+1);
  line->DrawLine(etaMin,1.0,etaMax,1.0);
  line->SetLineColor(kRed);
  line->DrawLine(etaMin,1.05,etaMax,1.05);
  line->DrawLine(etaMin,0.95,etaMax,0.95);

  line->SetLineColor(kGreen+1);
  line->DrawLine(etaMin,1.02,etaMax,1.02);
  line->DrawLine(etaMin,0.98,etaMax,0.98);
}


void DrawGraph(TGraphErrors *graph, double ymin=0.75, double ymax=1.25) {
  double etaMax=4.5;
  TH1D *haxis = (TH1D*)gROOT->FindObject("graph_axis");
  if (haxis==0) {
    if(_absEta) haxis=new TH1D("graph_axis","",10,0.0,etaMax);
    else        haxis=new TH1D("graph_axis","",10,-etaMax,etaMax);
    haxis->SetXTitle("jet #eta_{det}");
    haxis->SetYTitle("Relative response, 1/c");
  }
  haxis->SetMinimum(ymin); haxis->SetMaximum(ymax);
  haxis->Draw();
  DrawGuideLines();

  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(0.6);
  graph->Draw("P");
}


void ScaleHisto(TH1D *h, double w=1.0) {
  int Nbins=h->GetNbinsX();
  for (int i=1;i<=Nbins;++i) {
    double width=h->GetBinWidth(i);
    double W=_useWeights?w/width:1.0/width;
    double y=h->GetBinContent(i);
    double err=h->GetBinError(i);
    h->SetBinContent(i,y*W);
    h->SetBinError(i,err*W);
  }
}

void DrawHisto(TH1 *h, bool logy=false, double xmin=-1, double xmax=-1) {
  if (logy) gPad->SetLogy();
  else { gPad->SetLogy(false); h->SetMinimum(0); }
  h->SetMarkerStyle(1);
  if (xmax>0) h->GetXaxis()->SetRangeUser(xmin,xmax);
  h->Draw("hist");
  h->Draw("same");
  Str tit(h->GetTitle());
  if(trig1!="") tit.ReplaceAll(Form(" %s from",trig1.Data()),"");
  //PrintText(tit,0.94,0.98,3);
}

void DrawEtaSpectra(vector<TH1D*> etaSpectra, bool scale, vector<int> stats, Str comment) {

    outfile->cd(jetAlgo+"_Histos");
  int Nhist=etaSpectra.size();
  bool drawStats=stats.size()==Nhist;

  if (scale)
    for (int i=0;i<Nhist;++i)
      ScaleHisto(etaSpectra[i]);

  Str hname=etaSpectra[0]->GetName();
  //hname.ReplaceAll(trig1,"Comb"); hname.ReplaceAll("CombOR",trig1+"OR");
  hname.ReplaceAll(trig2,"").ReplaceAll(trig1,""); hname.ReplaceAll("OR",""); hname.ReplaceAll("__","_");
  TH1D *etaDist = (TH1D*)gDirectory->FindObject(hname);
  if (etaDist==NULL) {
    etaDist =  (TH1D*)etaSpectra[0]->Clone(hname);
    for (int i=1;i<Nhist;++i) etaDist->Add(etaSpectra[i]);
    double max = etaDist->GetBinContent(etaDist->GetMaximumBin());
    etaDist->SetMinimum(0); etaDist->SetMaximum(1.25*max);
    etaDist->SetMarkerStyle(1);
    etaDist->SetYTitle("d#sigma / d#eta [pb]");
    etaDist->GetYaxis()->SetTitleOffset(1.5);
    etaDist->Write();
  }

  etaDist->Draw("hist");
  etaDist->Draw("samee");

  for (int i=0;i<Nhist;++i) {
    int col=GetColor(i+1);
    etaSpectra[i]->SetLineColor(col);
    if (i==0) etaSpectra[i]->SetLineStyle(2);
    etaSpectra[i]->Draw("samehist");
  }
  PrintTrigAndPt(false);
  tex->SetTextAlign(32);
  if (_isMC) {
    double y=0.92;
    for (int i=0;i<Nhist;++i)
      if (etaSpectra[i]->GetEffectiveEntries()>=1.0) {
	PrintTextR(MCsamples[i],y-=0.05,i+1,0.9);
      }
  } else {
    if (_isSlimHistos) { if(Nhist!=1) error("number of histos is wrong..."); }
    else if (_isInclusionTrigComb) { if(Nhist!=3) error("number of histos is wrong..."); }
    else if (Nhist!=2) error("number of histos is wrong...");
    //if (_doWeighting) tex->DrawLatex(0.92,0.9,"L weight applied");
    //else tex->DrawLatex(0.92,0.9,"L weight NOT applied");


    PrintTextR(comment+":",0.85,0);
    float position=0.85;
    for (size_t iS = 0; iS < stats.size(); iS++)
    {
      PrintTextR(Form("%d events",stats[iS]),position-=0.05,iS+1);
    }
  }

  tex->SetTextColor(kBlack); tex->SetTextAlign(12);
}


void CombineAndDrawHisto(Str hsuffix, bool logy=false, double xmin=-1, double xmax=-1) {

  vector<TH1D*> hvec;
  int N=0;

  Str prefix=jetAlgo+"_"+currentTrig+"_";
  if (trig1!="") {
    hvec.push_back(GetHisto(prefix+trig1+"_"+hsuffix));
    ScaleHisto(hvec[0],1);
    hvec.push_back(GetHisto(prefix+trig2+"_"+hsuffix));
    N=2;
  } else {
    hvec.push_back(GetHisto(prefix+trig1+hsuffix)); N=1;
  }

  if (N==0) error("No histos");
  Str hcname=jetAlgo+"_"+hsuffix; // combined name

  // combine
  TH1D *h =  (TH1D*)hvec[0]->Clone(); h->SetName(hcname);
  for (int mi=1;mi<N;++mi) h->Add(hvec[mi]);
  gROOT->cd();

  DrawHisto(h,logy,xmin,xmax);

  for (int i=0;i<N;++i) {
    int col=GetColor(i+1);
    hvec[i]->SetLineColor(col);
    if (i==0) hvec[i]->SetLineStyle(2);
    hvec[i]->Draw("samehist");
  }

  PrintTextR(trig1,0.75,1);
  PrintTextR(trig2,0.7,1);
}

void DrawMatrix(TH2F *matrix) {

  matrix->SetStats(0);
  matrix->SetMinimum(0.75);
  matrix->SetMaximum(1.25);

  matrix->Draw("colz");
  tex->SetTextSize(0.02);
  tex->SetNDC(0);
  tex->SetTextColor(kGray+1);
  for (int xi=1;xi<=matrix->GetNbinsX();++xi) {
    for (int yi=1;yi<=matrix->GetNbinsY();++yi) {
      double bc = matrix->GetBinContent(xi,yi);
      if (bc>0.1) {
	tex->SetTextColor(kBlack);
	PrintText(bc>1?Form("+%.1f%%",(bc-1.0)*100):
		  Form("%.1f%%",(bc-1.0)*100),
		  matrix->GetXaxis()->GetBinCenter(xi),
		  matrix->GetYaxis()->GetBinCenter(yi),2,
		  bc<0.75?2:0);
      }
    }
  }
  tex->SetNDC();
  tex->SetTextSize(0.04);
  tex->SetTextColor(kBlack);
  tex->SetTextAlign(12);
  tex->DrawLatex(0.16,0.97,"Response ratio matrix");
  PrintTrigAndPt(1);
}


/*
// calculate the weighted average
vector<double> GetCombinedMean(vector<TProfile2D*> Vp, int lowPtBin, int highPtBin, VecD etaBins) {
  vector<double> result; int verb=3;
  TAxis *etaAxis = Vp[0]->GetXaxis();

  // for each actual eta bin
  for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
    double SUMW=0, SUMWY=0;
    // for each histo
    for (int hi=0;hi<Vp.size();++hi) {
      // find the finer eta bins
      int lbin=-1, hbin=-1;
      GetBinRange(etaAxis,etaBins[ieta],etaBins[ieta+1],lbin,hbin);
      double sumw=0, sumwy=0, sumy2=0, sumw2=0, w=0, e=0, y=0;
      for (int ip=lowPtBin;ip<=highPtBin;++ip)
	for (int ie=lbin;ie<=hbin;++ie) {
	  e = Vp[hi]->GetBinError(ie,ip), y=Vp[hi]->GetBinContent(ie,ip);
	  if (e<1e-9) continue;
	  w = 1.0/e/e; sumw += w; sumwy += w*y, sumy2 += w*y*y; sumw2+=w*w;
	  if (verb>2) printf("  %.0f,%.2f : %.3f +/- %.3f, w=%.3f\n",Vp[hi]->GetYaxis()->GetBinCenter(ip),etaAxis->GetBinCenter(ie),y,e,w);
	}
      if (sumw<1e-9) continue;
      double avg=sumwy/sumw, neff=sumw*sumw/sumw2, rms=sqrt(sumy2/sumw-avg*avg), e_avg=rms/sqrt(neff), W=1.0/e_avg/e_avg;
      //double avg=sumwy/sumw, e_avg=1/sqrt(sumw);
      //double rms=sqrt((sumy2/sumw-avg*avg)); double W=1.0/e_avg/e_avg;
      if (verb>1)
	printf("== %d == pt: %d-%d, eta: (%5.2f,%.2f): avg: %.3f +/- %.3f, w=%.3f, W=%.3f rms: %.3f, Neff=%.2f\n",
	       hi,lowPtBin,highPtBin,etaAxis->GetBinLowEdge(lbin),etaAxis->GetBinLowEdge(hbin+1),avg,e_avg,w,W,rms,neff);
      if (e_avg<1e-9) continue;
      SUMW += W; SUMWY += W*avg;
    }
    if (verb>1) printf("== Average == %.3f\n",SUMWY/SUMW);
    result.push_back(SUMWY/SUMW);
  }
  if (verb>0) {
    printf("Bins: ");
    for (int ieta=0;ieta<etaBins.size();++ieta) printf("%6.3f ",etaBins[ieta]);
    printf("\nVals: ");
    for (int ieta=0;ieta<etaBins.size()-1;++ieta) if (result[ieta]>10) printf("%6.1f ",result[ieta]); else printf("%6.3f ",result[ieta]);
    printf("\n");
  }
  return result;
}
*/

Str AsymSM_2_psFile;


static int counter=0;

vector<TH1F*> GetSMResponseHistos( TEnv *settings ){

  int Nx=3, Ny=3; int cw=1;
  Can->Clear(); Can->Divide(Nx,Ny);

  int jetR=4; if (jetAlgo.Contains("Kt6")) jetR=6;

  TH3F *h3d_asym;
  vector<TH3F*> h3d_individual_asym;

  vector<TH3F*> h3d_data_asym;
  vector< vector<TH3F*> > h3d_data_individual_asym;


  if(_isMC){

    int N = MCsamples.size();


    TH3F* h3dasymTemp  =  Get3DHisto(jetAlgo+"_"+MCsamples[0].Data()+"_pTavg_etaprobe_Astandard");
    h3dasymTemp->SetName(Form("TEMP1_%d",counter++));
    if (h3dasymTemp->GetSumw2N()==0) { cout<<" GET SM RESPONSE 1 "<<endl; h3dasymTemp->Sumw2(); }
    h3d_asym = (TH3F*)h3dasymTemp->Clone();
    h3d_individual_asym.push_back((TH3F*)h3dasymTemp->Clone());


    for(int mci=1; mci<N; mci++){

      TH3F *hd3asymTemp2 =  Get3DHisto(jetAlgo+"_"+MCsamples[mci].Data()+"_pTavg_etaprobe_Astandard");
      h3dasymTemp->SetName(Form("TEMP_%d_%d",mci,counter++));
      if (hd3asymTemp2->GetSumw2N()==0) { cout<<" GET SM RESPONSE i :"<<mci<<endl; hd3asymTemp2->Sumw2(); }
      h3d_individual_asym.push_back((TH3F*)hd3asymTemp2->Clone());
      h3d_asym->Add(hd3asymTemp2);

    }

  }else{


    /// code needs to be updated to accommodate pTavg bin merging in data

    Str previousTrigKey = "";

    for (uint ri=0;ri<ptRanges.size();++ri){

      double ptMin=ptRanges[ri][0], ptMax=ptRanges[ri][1];


      Str trigKey = Form("AntiKt%d.%.0fto%.0f.TrigOR",jetR,ptMin,ptMax);
      Str trigSel = settings->GetValue(trigKey,""); trigSel.ReplaceAll(" ","");

      if(trigSel!="") previousTrigKey = trigKey;
      else{
	trigSel = settings->GetValue(previousTrigKey,""); trigSel.ReplaceAll(" ","");
	cout<<"using trigger: "<<trigSel<<" for pT range : "<<ptMin<<" - "<<ptMax<<endl;
      }
      //if(trigSel=="") error(trigKey+" not specified in "+configFN);

      trigSel.ReplaceAll("_AND_"," "); trigSel.ReplaceAll("_and_"," ");
      trigSel.ReplaceAll("_OR_"," "); trigSel.ReplaceAll("_or_"," ");
      trigSel.ReplaceAll("_","").ReplaceAll("EF","").ReplaceAll("a4tc","").ReplaceAll("FS","").ReplaceAll("L2","").ReplaceAll("had","");
      StrV trigs=Vectorize(trigSel);
      if (trigs.size()<2) error("Current trigger selection: "+trigSel+" is not a trigOR!?");


      Str SM2currentTrig="";
      if(trigs.size()>2){

	for( int trig=0; trig<trigs.size()-1; trig++){
	  if( trig%2 != 0 ){
	    SM2currentTrig+=trigs[trig]+"AND";
	  }else{
	    SM2currentTrig+=trigs[trig]+"OR";
	  }
	}
	SM2currentTrig+=trigs[trigs.size()-1];

      }else{
	SM2currentTrig=trigs[0]+"OR"+trigs[1];
      }


      Str prefixFULL=SM2currentTrig;

      Str tempTrigStr = SM2currentTrig.ReplaceAll("AND"," ");
      StrV currentTrigORs = Vectorize(tempTrigStr);
      int NcurrentTrigORs = currentTrigORs.size();


      TH3F *h3dasymTempHADD;
      TH3F *h3dasymTemp1HADD;
      TH3F *h3dasymTemp2HADD;

      Str prefix1;
      Str prefix2;
      Str suffix;

      Str TRIGSTR1="", TRIGSTR2="";


      for( int ior=0; ior<NcurrentTrigORs; ior++){


	cout<<" IOR = "<<ior<<endl;

	Str prefix=jetAlgo+"_"+currentTrigORs[ior]+"_";

	prefix1 = prefix+"RefFJ_J";
	prefix2 = prefix+"RefFJ_FJ";

	Str TRIG1 = trigs[2*ior];
	Str TRIG2 = trigs[2*ior+1];

	if(ior<NcurrentTrigORs-1){
	  TRIGSTR1+=TRIG1+" AND ";
	  TRIGSTR2+=TRIG2+" AND ";
	}else{
	  TRIGSTR1+=TRIG1;
	  TRIGSTR2+=TRIG2;
	}


	TH3F* h3dasymTemp  =  Get3DHisto(prefix1+"_pTavg_etaprobe_Astandard");
	h3dasymTemp->SetName(Form("TMEP1_%d",ior));
	if (h3dasymTemp->GetSumw2N()==0) h3dasymTemp->Sumw2();


	TH3F* h3dasymTemp2  =  Get3DHisto(prefix2+"_pTavg_etaprobe_Astandard");
	h3dasymTemp2->SetName(Form("TMEP2_%d",ior));
	if (h3dasymTemp2->GetSumw2N()==0) h3dasymTemp2->Sumw2();

	if(ior==0){
	  h3dasymTemp1HADD = (TH3F*)h3dasymTemp->Clone();
	  h3dasymTemp2HADD = (TH3F*)h3dasymTemp2->Clone();

	  //h3dasymTempHADD  = (TH3F*)h3dasymTemp->Clone();
	  //h3dasymTempHADD->Add((TH3F*)h3dasymTemp2->Clone());

	  h3dasymTempHADD =(TH3F*)h3dasymTemp1HADD->Clone();
	  h3dasymTempHADD->Add(h3dasymTemp2HADD);

	}else{
	  h3dasymTemp1HADD->Add(h3dasymTemp);
	  h3dasymTemp2HADD->Add(h3dasymTemp2);
	  h3dasymTempHADD->Add(h3dasymTemp);
	  h3dasymTempHADD->Add(h3dasymTemp2);

	}

      }

      vector<TH3F*> h3dIndTemp;
      h3dIndTemp.push_back(h3dasymTemp1HADD);
      h3dIndTemp.push_back(h3dasymTemp2HADD);
      h3d_data_individual_asym.push_back( h3dIndTemp );

      h3d_data_asym.push_back( h3dasymTempHADD );


    }


    /*

    Str previousTrigKey = "";

    for (uint ri=0;ri<ptRanges.size();++ri){

      double ptMin=ptRanges[ri][0], ptMax=ptRanges[ri][1];

      Str trigKey = Form("AntiKt%d.%.0fto%.0f.TrigOR",jetR,ptMin,ptMax);
      Str trigSel = settings->GetValue(trigKey,""); trigSel.ReplaceAll(" ","");

      if(trigSel!="") previousTrigKey = trigKey;
      else{
	trigSel = settings->GetValue(previousTrigKey,""); trigSel.ReplaceAll(" ","");
	cout<<"using trigger: "<<trigSel<<" for pT range : "<<ptMin<<" - "<<ptMax<<endl;
      }
      //if(trigSel=="") error(trigKey+" not specified in "+configFN);

      trigSel.ReplaceAll("_OR_"," "); trigSel.ReplaceAll("_or_"," ");
      trigSel.ReplaceAll("_","").ReplaceAll("EF","").ReplaceAll("a4tc","").ReplaceAll("FS","").ReplaceAll("L2","").ReplaceAll("had","");
      StrV trigs=Vectorize(trigSel);
      if (trigs.size()!=2) error("Current trigger selection: "+trigSel+" is not a trigOR!?");
      Str Trig1=trigs[0], Trig2=trigs[1];

      Str CurrentTrig=Trig1+"OR"+Trig2;
      Str prefix=jetAlgo+"_"+CurrentTrig+"_";

      TString prefix1 = prefix+"RefFJ_J";
      TString prefix2 = prefix+"RefFJ_FJ";

      prefix1.ReplaceAll("EF","").ReplaceAll("a4tc","").ReplaceAll("FS","").ReplaceAll("L2","").ReplaceAll("had","");
      prefix2.ReplaceAll("EF","").ReplaceAll("a4tc","").ReplaceAll("FS","").ReplaceAll("L2","").ReplaceAll("had","");

      Trig1.ReplaceAll("EF_",""); Trig1.ReplaceAll("_a4tchad",""); Trig1.ReplaceAll("_L2FS","");
      Trig2.ReplaceAll("EF_",""); Trig2.ReplaceAll("_a4tchad",""); Trig2.ReplaceAll("_L2FS","");

      TH3F* h3dasymTemp  =  Get3DHisto(prefix1+"_pTavg_etaprobe_Astandard");
      h3dasymTemp->SetName("TMEP1");
      if (h3dasymTemp->GetSumw2N()==0) h3dasymTemp->Sumw2();

      //if(RNDtrigOR[ri][0]==1){ h3dasymTemp->Scale(8.0/CentrallumiMap[Trig1]*1e6 ); }//cout<<" Ref_J lumi = "<<8.0/CentrallumiMap[Trig1]*1e6<<endl; }
      //else                   { h3dasymTemp->Scale( 1.0/CentrallumiMap[Trig1]*1e6 ); }//cout<<" Ref_J lumi = "<<1.0/CentrallumiMap[Trig1]*1e6<<endl; }


      h3dasymTemp->Scale( 1.0/CentrallumiMap[Trig1]*1e6 ); //cout<<" Ref_J lumi = "<<1.0/CentrallumiMap[Trig1]*1e6<<endl; }

      vector<TH3F*> h3dIndTemp;

      h3dIndTemp.push_back(h3dasymTemp);

      TH3F* h3dasymTemp2  =  Get3DHisto(prefix2+"_pTavg_etaprobe_Astandard");
      h3dasymTemp2->SetName("TMEP2");
      if (h3dasymTemp2->GetSumw2N()==0) h3dasymTemp2->Sumw2();

      //if(RNDtrigOR[ri][1]==1) { h3dasymTemp2->Scale(8.0/ForwardlumiMap[Trig2]*1e6 ); }//cout<<" Ref_FJ lumi = "<<8.0/ForwardlumiMap[Trig2]*1e6<<endl; }
      //else                    { h3dasymTemp2->Scale( 1.0/ForwardlumiMap[Trig2]*1e6 ); }//cout<<" Ref_FJ lumi = "<<1.0/ForwardlumiMap[Trig2]*1e6<<endl; }

      h3dasymTemp2->Scale( 1.0/ForwardlumiMap[Trig2]*1e6 ); //cout<<" Ref_FJ lumi = "<<1.0/ForwardlumiMap[Trig2]*1e6<<endl; }

      h3dIndTemp.push_back(h3dasymTemp2);

      h3d_data_individual_asym.push_back( h3dIndTemp );

      h3dasymTemp->Add( h3dasymTemp2 );

      h3d_data_asym.push_back( h3dasymTemp );


    }

    */

  }


  TAxis *fullEtaAxis;
  if(_isMC) fullEtaAxis = h3d_asym->GetYaxis();
  else      fullEtaAxis = h3d_data_asym[0]->GetYaxis();

  int NbinsSM = _etaBinsSM.size()-1;

  vector<TH1F*> returnHistos;

  int Aproj_i=0;

  cout<<"GETTING SM HISTOS 2"<<endl;

  for (uint ipt=0; ipt<_ptBins.size()-1; ipt++){

    double pTmin=_ptBins[ipt], pTmax=_ptBins[ipt+1];



    Str ptStr = Form("pt%.0fto%.0f",pTmin,pTmax);

    TH1F *h_responseSM = new TH1F(jetAlgo+"_"+ptStr+"Response2_SM","Standard method relative response "+ptStr,NbinsSM,&_etaBinsSM[0]);
    h_responseSM->SetMinimum(0.75); h_responseSM->SetMaximum(1.25);
    h_responseSM->SetYTitle("Relative response, 1/#font[52]{c}");


    for (uint etai=0;etai<NbinsSM;++etai) {

      int lowBin=-1, highBin=-1;
      double etaMin = _etaBinsSM[etai], etaMax = _etaBinsSM[etai+1];
      GetBinRange(fullEtaAxis, etaMin, etaMax, lowBin, highBin);

      int lowBinAbs=-1, highBinAbs=-1;

      TH1D* hA;
      if(_isMC) {
	hA = h3d_asym->ProjectionZ(Form("Aproj_sm_%d_%d",ipt,etai),ipt+1,ipt+1,lowBin,highBin);
	if (_absEta) {
	  GetBinRange(fullEtaAxis, -etaMax, -etaMin, lowBinAbs, highBinAbs);
	  TH1D *h2 = h3d_asym->ProjectionZ(Form("Aproj_sm2_%d_%d",ipt,etai),ipt+1,ipt+1,lowBinAbs,highBinAbs);
	  hA->Add(h2);
	}

      } else {

	hA = h3d_data_asym[ipt]->ProjectionZ(Form("Aproj_sm_%d_%d",ipt,etai),ipt+1,ipt+1,lowBin,highBin);

	if (_absEta) {
	  int lowBinAbs=-1, highBinAbs=-1;
	  GetBinRange(fullEtaAxis, -etaMax, -etaMin, lowBinAbs, highBinAbs);
	  TH1D *h2 = h3d_data_asym[ipt]->ProjectionZ(Form("Aproj_sm2_%d_%d",ipt,etai),ipt+1,ipt+1,lowBinAbs,highBinAbs);
	  hA->Add(h2);
	}

      }

      StrV desc;
      vector<TH1D*> Ahistos;
      int Hsize = h3d_individual_asym.size();
      if(!_isMC) Hsize = h3d_data_individual_asym[ipt].size();

      for(int i=0; i<Hsize; i++){
	if(_isMC) {
	  desc.push_back(MCsamples[i]);
	  TH1D *hist = h3d_individual_asym[i]->ProjectionZ(Form("Aproj2_sm_%d_%d_%d",ipt,etai,i),ipt+1,ipt+1,lowBin,highBin);
	  if (_absEta) {
	    TH1D *h2 = h3d_individual_asym[i]->ProjectionZ(Form("Aproj_sm3_%d_%d_%d",ipt,etai,i),ipt+1,ipt+1,lowBinAbs,highBinAbs);
	    hist->Add(h2);
	  }
	  Ahistos.push_back(hist);
	} else {
	  if(i==0) { desc.push_back("J"); desc.push_back("FJ"); }
	  TH1D *hist = h3d_data_individual_asym[ipt][i]->ProjectionZ(Form("Aproj2_sm_%d_%d_%d",ipt,etai,i),ipt+1,ipt+1,lowBin,highBin);
	  if (_absEta) {
	    TH1D *h2 = h3d_data_individual_asym[ipt][i]->ProjectionZ(Form("Aproj_sm4_%d_%d_%d",ipt,etai,i),ipt+1,ipt+1,lowBinAbs,highBinAbs);
	    hist->Add(h2);
	  }
	  Ahistos.push_back(hist);
	}

      }

      double Neff = hA->GetEffectiveEntries();
      if (Neff<NeffMin) continue;

      Can->cd(cw);
      if (cw==Nx*Ny+1) {
 	Can->Print(AsymSM_2_psFile);
	cw=1;
	Can->Clear(); Can->Divide(Nx,Ny); Can->cd(cw);
      }


      double A=hA->GetMean(), dA=MeanErr(hA);
      TF1 *fit = DrawAhistos(hA,Ahistos,desc,true);
      std::pair <double,double> AdA = JudgeFit(fit,A,dA,Neff);
      A = AdA.first; dA = AdA.second;

      tex->SetTextAlign(32); tex->SetTextColor(kBlack);
      tex->DrawLatex(0.91,0.88,ptStr);
      tex->DrawLatex(0.91,0.83,Form("%.1f #leq #eta_{ref} < %.1f",-refRegion[1],refRegion[1]));
      tex->DrawLatex(0.91,0.78,Form("%.1f #leq #eta_{probe} < %.1f",etaMin,etaMax));

      double R = (2.0+A)/(2.0-A), dR = 4.0/pow(2.0-A,2)*dA;

      h_responseSM->SetBinContent( etai+1, R );
      h_responseSM->SetBinError( etai+1, dR );

      cw++;

    }

    returnHistos.push_back(h_responseSM);

  }

  Can->Print(AsymSM_2_psFile);
  Can->Clear();

  return returnHistos;

}

void GetMeanAndRMS(vector<float> *v_PEc_vs_eta, double &c, double &cErr)
{
  c=0;
  cErr=0;
  int v_size = v_PEc_vs_eta->size();
  if(_FoundMeanFromNominal) c = (*v_PEc_vs_eta)[0];
  else
  {
    for (size_t iVec = 0; iVec < v_size; iVec++)
    {
      c += (*v_PEc_vs_eta)[iVec];
    }
    c = c/v_size;
  }
  if(v_size-1>0)
  {
    for (size_t iVec = 0; iVec < v_size; iVec++)
    {
      cErr += pow((*v_PEc_vs_eta)[iVec]-c,2); //even if the first value in the vector is the nominal, its variance is 0
    }
    cErr = sqrt(cErr/(v_size-1));
  }
}
