#!/bin/sh

gen="PowhegPythia"

exec=matrix_minimization.exe
configFile="utils/Dijet_xAOD.config"
prefix="syst_dphiUp_toy1"
jetAlgo="AntiKt4EMTopo"
DirName=${jetAlgo}
# in case of toys: +"_toy%d"

eta="Eta2016v12"
inDir=$1
outDir=$2
#"/afs/cern.ch/user/j/jrawling/workDir/EtaIntercalibration/Results/PowhegPythia8_mc15c/ResponseCurves/"
./$exec $configFile $jetAlgo $DirName ${eta} ${1} ${2}mc15_13TeV_${prefix}_${eta}_${jetAlgo}.root ${prefix} ${gen}
#&> ${outDir}Minimization.log &
