/*
 *  Utils.h -- a collection of helper functions for
 *             HEP analysis
 *
 *  Dag Gillberg
 *
 */

#ifndef Utils_h
#define Utils_h

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <time.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TChain.h"
#include "TFile.h"
#include "TEnv.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TH2.h"
#include "TStyle.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile2D.h"
#include "TLorentzVector.h"


using namespace std;

typedef vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector<float> VecF;
typedef vector<int> VecI;

bool _verbose=false;

///////////////////////////////////////////////////////////////////////////////////////

void error(Str msg) {
  printf("ERROR:\n\n  %s\n\n",msg.Data()); 
  abort();
}

TFile *InitOutputFile(Str ofName) {
  if (!gSystem->AccessPathName(ofName))
    error(Str("The intended output file already exist!\n")+
          "  If you want to replace it, please delete it manually:\n\n"+
          "    rm "+ofName);
  printf("\nHistograms will be written to %s\n",ofName.Data());
  return new TFile(ofName,"CREATE");
}



map<Str,TH1D*> histos;
map<Str,TH2F*> histos2d;
map<Str,TH3F*> histos3d;
map<Str,TProfile*> profiles;
map<Str,TProfile2D*> profiles2d;

//**************************  methods for vectorizing input data *******************************//

void add(VecD &vec, double a) { vec.push_back(a); };
void add(VecD &vec, double a, double b) { add(vec,a); add(vec,b); };
void add(VecD &vec, double a, double b, double c) { add(vec,a,b); add(vec,c); };
void add(StrV &vec, Str a) { vec.push_back(a); };
void add(StrV &vec, Str a, Str b) { add(vec,a); add(vec,b); };
void add(StrV &vec, Str a, Str b, Str c) { add(vec,a,b); add(vec,c); };

void add(VecD &vec, double a[]) { 
  uint n=sizeof(a)/sizeof(a[0]);
  for (uint i=0;i<n;++i) vec.push_back(a[i]);
}

StrV Vectorize(Str str, Str sep=" ") {
  StrV result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#') result.push_back(os->GetString());
    else break;
  return result;
}

VecD VectorizeD(Str str) {
  VecD result; StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i) 
    result.push_back(atof(vecS[i]));
  return result;
}

VecD MakeUniformVecD(int N, double min, double max) {
  VecD vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}


//********************* prepare histograms ***********//
 

void BookHisto(Str hname, Str tit, VecD bins, Str xtit="") {
  if (bins.size()==0) error("No bins specified for histo "+hname);
  TH1D *h = new TH1D(hname,tit,bins.size()-1,&bins[0]);
  h->SetXTitle(xtit); histos[hname]=h;
  if (_verbose) printf("Created 1D histo %s\n",hname.Data());
}

void BookHisto(Str hname, Str tit, int Nbins, double xmin, double xmax, Str xtit="") {
  BookHisto(hname,tit,MakeUniformVecD(Nbins,xmin,xmax),xtit);
}

void BookHisto2D(Str hname, Str tit, VecD xbins, VecD ybins, Str xtit="", Str ytit="") {
  if (xbins.size()==0) error("No xbins specified for histo "+hname);
  if (ybins.size()==0) error("No ybins specified for histo "+hname);
  TH2F *h = new TH2F(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  h->SetXTitle(xtit); h->SetYTitle(ytit); 
  histos2d[hname]=h;
  if (_verbose) printf("Created 2D histo %s\n",hname.Data());
}

void BookHisto3D(Str hname, Str tit, VecD xbins, VecD ybins, VecD zbins, Str xtit, Str ytit, Str ztit) {
  if (xbins.size()==0) error("No xbins specified for histo "+hname);
  if (ybins.size()==0) error("No ybins specified for histo "+hname);
  if (zbins.size()==0) error("No zbins specified for histo "+hname);
  TH3F *h = new TH3F(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0],zbins.size()-1,&zbins[0]);
  h->SetXTitle(xtit); h->SetYTitle(ytit); h->SetZTitle(ztit); 
  histos3d[hname]=h;
  if (_verbose) printf("Created 3D histo %s\n",hname.Data());
}


void BookProfile2D(Str hname, Str tit, VecD xbins, VecD ybins, Str xtit, Str ytit, Str ztit) {
  TProfile2D *p = new TProfile2D(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  p->SetXTitle(xtit); p->SetYTitle(ytit); p->SetZTitle(ztit); 
  profiles2d[hname]=p;
  if (_verbose) printf("Created 2D profile %s\n",hname.Data());
}


//************************** histo filling ********************************************//


void FillHisto(Str hname, double x, double w=1.0) {
  TH1D *h=histos[hname]; if (h==NULL) error("Cannot find histo "+hname);
  h->Fill(x,w);
}

void FillHisto2D(Str hname, double x, double y, double w=1.0) {
  TH2F *h=histos2d[hname]; if (h==NULL) error("Cannot find 2D histo "+hname);
  h->Fill(x,y,w);
}

void FillHisto3D(Str hname, double x, double y, double z, double w=1.0) {
  TH3F *h=histos3d[hname]; if (h==NULL) error("Cannot find 3D histo "+hname);
  h->Fill(x,y,z,w);
}

void FillProfile2D(Str hname, double x, double y, double w=1.0) {
  TProfile2D *p=profiles2d[hname]; if (p==NULL) error("Cannot find 2D profile "+hname);
  p->Fill(x,y,w);
}

void FillHistoTwice(Str hname, double x1, double x2, double w=1.0) {
  FillHisto(hname,x1,w); FillHisto(hname,x2,w);
}


//********************* methods for opening and reading the config file as a TEnv ************//

TEnv *OpenSettingsFile(Str fileName) {
  if (fileName=="") error("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status=settings->ReadFile(fileName.Data(),EEnvLevel(0));
  if (status!=0) error(Form("Cannot read file %s",fileName.Data()));
  return settings;
}

StrV ReadFile(TString fileName) {
  StrV lines;
  ifstream file(fileName.Data());
  string line, lastline="weeee";
  while (getline(file,line)) {
    if (line==lastline) continue;
    if (line[0]==' ') continue;
    StrV subLines=Vectorize(line,",");
    for (uint i=0;i<subLines.size();++i) { cout << subLines[i] << endl;
      lines.push_back(subLines[i]);
    }
  }
  return lines;
}

//**************** cool method to keep track of runtimes ****************************//

Str getTime()
{
  time_t aclock;
  ::time( &aclock );
  return Str(asctime( localtime( &aclock )));
}



void PrintTime()
{
  static bool first=true;
  static time_t start;
  if(first) { first=false; ::time(&start); }
  time_t aclock; ::time( &aclock );
  char tbuf[25]; ::strncpy(tbuf, asctime( localtime( &aclock ) ),24);
  tbuf[24]=0;
  cout <<  "Current time: " << tbuf
       << " ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}




#endif // #ifdef Utils_h

