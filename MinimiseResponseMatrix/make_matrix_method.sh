#!/bin/sh

includes=""
pkgs=""
for pkg in `cat scripts/packages | grep -v RootCore` ; do
    p=`basename $pkg | cut -f1 -d'-'`
    pkgs+=" $p"
    includes+=" -I$p"
done
echo "Packages: "
echo "Includes: $includes"
#mylibs=`$ROOTCOREDIR/scripts/get_ldflags.sh $pkgs`
mylibs=""
echo "Libraries: $mylibs"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore -lMathMore  -lXMLParser $mylibs"

code=MatrixMethod.C
exec=matrix_minimization.exe
rm -f $exec

g++ $flagsNlibs $includes -o $exec $code || {
    echo
    echo "Failed complingin & linking using g++, will try gcc instead"
    echo
    gcc $flagsNlibs $includes -o $exec $code && echo "Successful!"

}
