#!/bin/sh

exec=matrix_minimization.exe
#configFile="utils/Dijet_xAOD.config"
configFile="utils/Dijet_xAOD.config"
syst=""
eta="Eta2016v12"
eta="Eta0.3"
inDir=${1}
#"/pc2013-data5/jrawling/data16_13TeV/periodBreakDown/periodsIG/Data_nominal.root"
outDir=${2}
jetAlgo="AntiKt4LCTopo"
DirName=${jetAlgo}
# in case of toys: +"_toy%d"

nohup ./$exec $configFile $jetAlgo $DirName ${eta} ${inDir} ${outDir}data16_13TeV_${syst}_${eta}_${jetAlgo}.root nominal &> ClosureStudy.log &
