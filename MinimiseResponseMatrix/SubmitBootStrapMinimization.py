import os
from subprocess import call,Popen
import datetime

Minimizer = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/matrix_minimization.exe"
SettingsPath = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/utils/Dijet_xAOD.config"

triggerLumiSettings = "25nsFinal"
jetAlgo = "AntiKt4LCTopo"
etaBinning = "Eta2016v12"
#etaBinning ="Eta0.3"
eventGenerator=""

##comment the following two if on MC
#eventGenerator = "PowhegPythia"
#triggerLumiSettings = "p1562"

##pc2012
MinimizationOuptutSubfolder = "MMOut/"
#inputPath = "/tony-data2/jrawling/data16_13TeV/DphiUp/"
#inputPath = "/pc2012-data2/jrawling/data16_13TeV/JVTTight/"
#inputPath = "/pc2013-data5/jrawling/data16_13TeV/Nominal/"
inputPath = "/higgs-data4/jrawling/data16_13TeV_Closure/LCClosure/"
#inputPath = "/tony-data2/jrawling/mc15_13TeV/DphiUp/"
#inputPath = "/hepgpu1-data3/jrawling/data16_13TeV_Closure/LCTopo/nominal/"
#inputPath = "/pc2014-data5/jrawling/data16_13TeV_Closure/nominal/"
#inputPath = "/pc2013-data5/jrawling/data16_13TeV_Closure/nominal/"
#inputPath = "/pc2012-data2/jrawling/data16_13TeV_Closure/nominal/"

MinimizationOuptutPath = inputPath + MinimizationOuptutSubfolder
#histoName = "mc15_13TeV_PowhegPythia_bootStrapJVT.root"
systName="nominal"
#systName="syst_dphiDown"
#systName="syst_dphiUp"
#systName="syst_j3up"
#systName="syst_j3down"
#systName="syst_jvtTight"


current_time = datetime.datetime.now().time()
print current_time.isoformat()
ps = {}

###### Set the number to be submitted at a time and how many jobs are allowed to run simultaneously ####
maxSimulataneousJobs = 42
nStartToy = 150
#150
# 512  
#630
nLastToy = 358
#357
# 634
# 768
for i in range(nStartToy,nLastToy):
    toyNum = i

    inputFile = inputPath + "Data_"+ systName +"_toy"+`toyNum`+".root"
         #inputFile = inputPath + histoName

    fullPrefix = systName+"_toy"+`toyNum`

    logFile=MinimizationOuptutPath +"minimization"+`toyNum`+".log"
    print "Logfile: " + logFile

    outputFile = MinimizationOuptutPath + "mc15_13TeV_" + fullPrefix + "_" + etaBinning+ "_" + triggerLumiSettings + "_"+jetAlgo + ".root"

    print "nice "+" -n "+" 10 "+Minimizer + " "+  SettingsPath + " " + jetAlgo  + " "+ triggerLumiSettings  + " "+ etaBinning  + " "+ inputFile  + " "+ outputFile  + " "+fullPrefix;
    if eventGenerator != "":
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix, eventGenerator]
    else:
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix]
    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p

    while len(ps) >= maxSimulataneousJobs:
        pid, status = os.wait()
        if pid in ps:
            del ps[pid]
            print "Starting next job: " + `i+1`

current_time = datetime.datetime.now().time()
print current_time.isoformat()
print "done"
