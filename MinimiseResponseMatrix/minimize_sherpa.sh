#!/bin/sh

jetAlgo=$1
exec=matrix_minimization.exe
configFile="utils/Dijet_xAOD.config"

jetAlgo="AntiKt4EMTopo"
DirName=${jetAlgo}
# in case of toys: +"_toy%d"

outDir="~/2015FinestBinningTest/"
gen="Sherpa"
ieta="Eta2015V13"
inDir="/pc2013-data5/jrawling/EtaIntercal/mc15_13TeV/SherpaFinal/mc15_13TeV_sherpa_final.root"

jetAlgo="AntiKt4EMTopo"
./$exec ${configFile} ${jetAlgo} ${DirName} ${ieta} ${inDir} ${outDir}${gen}_${ieta}_${jetAlgo}.root ${gen}
jetAlgo="AntiKt4LCTopo"
# nohup ./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_${ieta}_${jetAlgo}.root ${gen}
jetAlgo="AntiKt4EMPFlow"
# `nohup ./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_${ieta}_${jetAlgo}.root ${gen}
