  #!/bin/sh

code=plotCurves.C
exec=drawTwoCurves.exe

etaBinning="0.3"
jetAlgos="AntiKt4EMTopo"
methods="MM"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter"

rm -f $exec
g++ $flagsNlibs -o $exec $code && {

    echo "*********************"
    echo "Compilation successful"
    echo "*********************"

    for meth in $methods ; do
		method=$meth
		for jA in $jetAlgos ; do

	    echo $jA

      dataFile=~/triggerEmulationResults/data_13TeV_final_periodsDEFGHJ_Eta0.3_AntiKt4EMTopo.root
      nom_mc_File=~/MCFinalCubed/PowhegPythia_finalFinal_Eta0.3_AntiKt4EMTopo.root
	    syst_mc_File=~/MCFinalCubed/Sherpa_finalFinal_Eta0.3_AntiKt4EMTopo.root


      dataFile2=/afs/hep.man.ac.uk/u/jrawling/2015FinalBinning/data_13TeV_periodsDEFGHJ_V3_Eta2015V13_AntiKt4EMTopo.root
      nom_mc_File2=/afs/hep.man.ac.uk/u/jrawling/2015FinalBinning/PowhegPythia_V3_Eta2015V13_AntiKt4EMTopo.root
      syst_mc_File2=/afs/hep.man.ac.uk/u/jrawling/2015FinalBinning/Sherpa_Eta2015V13_AntiKt4EMTopo.root


      dataFile2=/afs/hep.man.ac.uk/u/jrawling/2015FinestBinningTest/data_13TeV_periodsDEFGHJ_V3_Eta2015V13_AntiKt4EMTopo.root
      nom_mc_File2=/afs/hep.man.ac.uk/u/jrawling/2015FinestBinningTest/PowhegPythia_V3_Eta2015V13_AntiKt4EMTopo.root
      syst_mc_File2=/afs/hep.man.ac.uk/u/jrawling/2015FinestBinningTest/PowhegPythia_V3_Eta2015V13_AntiKt4EMTopo.root

      calibFile=Before21/Feb16_EtaIntercalibration13TeV25ns_ETA03_MM_AntiKt4EMTopo_Calibration.root
      uncertFile=Before21/Feb16_EtaIntercalibration13TeV25ns_ETA03_MM_AntiKt4EMTopo_Uncertainties.root

      calibFile2=After21/Feb16_EtaIntercalibration13TeV25nsMM_AntiKt4EMTopo_Calibration.root
      uncertFile2=After21/Feb16_EtaIntercalibration13TeV25nsMM_AntiKt4EMTopo_Uncertainties.root
#	    dataFile=/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV/TestCombination/results/data_13TeV_periodsD__AntiKt4EMTopo.root
#	    nom_mc_File=/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/mc15_13TeV/AntiKt4EMTopo/results/PowhegPythia_Eta0.3_AntiKt4EMTopotest.root
#     syst_mc_File=/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/mc15_13TeV/AntiKt4EMTopo/results/PowhegPythia_Eta0.3_AntiKt4EMTopotest.roo



	    echo "***************************************************************"
	    echo "   method: $meth -- jetalgo: $jA  "
	    echo "***************************************************************"
	    echo "*********************   inputs   ******************************"
	    echo "***************************************************************"

	    echo $dataFile
	    echo $nom_mc_File
	    ./$exec $dataFile $nom_mc_File $syst_mc_File $dataFile2 $nom_mc_File2 $syst_mc_File2 $calibFile $uncertFile $calibFile2 $uncertFile2 $jA


	    echo "**********************************************************"
	    echo "**********************************************************"
	    echo "*********************** done *****************************"
	done
    done

}
