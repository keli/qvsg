/*
 *  Utils.h -- a collection of helper functions for
 *             HEP analysis
 */

#ifndef Utils_h
#define Utils_h

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <time.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TChain.h"
#include "TFile.h"
#include "TEnv.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TROOT.h"
#include "TApplication.h"

#include "TH2.h"
#include "TH3.h"

#include <TGraphErrors.h>
#include <TGraph2DErrors.h>
#include "TProfile2D.h"
#include "TLorentzVector.h"

using namespace std;

typedef vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector<float> VecF;
typedef vector<int> VecI;
typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

bool _verbose=false;


///////////////////////////////////////////////////////////////////////////////////////

void error(Str msg) {
  printf("ERROR:\n\n  %s\n\n",msg.Data());
  abort();
}


//******************************************************************
//**************** vectorization methods/functions
//******************************************************************




void add(VecD &vec, double a) { vec.push_back(a); };
void add(VecD &vec, double a, double b) { add(vec,a); add(vec,b); };
void add(VecD &vec, double a, double b, double c) { add(vec,a,b); add(vec,c); };
void add(StrV &vec, Str a) { vec.push_back(a); };
void add(StrV &vec, Str a, Str b) { add(vec,a); add(vec,b); };
void add(StrV &vec, Str a, Str b, Str c) { add(vec,a,b); add(vec,c); };

void add(VecD &vec, double a[]) {
  uint n=sizeof(a)/sizeof(a[0]);
  for (uint i=0;i<n;++i) vec.push_back(a[i]);
}

StrV Vectorize(Str str, Str sep=" ") {
  StrV result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#') result.push_back(os->GetString());
    else break;
  return result;
}

VecD VectorizeD(Str str) {
  VecD result; StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

VecD MakeUniformVecD(int N, double min, double max) {
  VecD vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}


VecD MakeLogVector(int N, double min, double max) {
  VecD vec; double dx=(log(max)-log(min))/N;
  for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
  return vec;
}


//******************************************************************
//**************** functions for obtaining histos ect.
//******************************************************************

TObject* Get(TFile *f, Str jetAlgo, Str objName) {
  f->cd(jetAlgo);
  TObject *obj = gDirectory->Get(objName);
  if (obj==NULL) error("Can't access "+objName+" in "+f->GetName());
  return obj;
}

TH1D *GetHisto(TFile *f, Str jetAlgo, Str hname)
{ return (TH1D*)Get( f, jetAlgo, hname ); }
TH1D *GetHisto2(TFile *f, Str hn)
{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH1D*)f->Get(hn); }

TH2D *Get2DHisto(TFile *f, Str jetAlgo, Str hname)
{ return (TH2D*)Get( f, jetAlgo, hname ); }
TH2D *Get2DHisto(TFile *f, Str hn)
{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH2D*)f->Get(hn); }

//TH2F *GetTH2F(TFile *f, Str hn)
//{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH2F*)f->Get(hn); }
//TH1F *GetTH1F(TFile *f, Str hn)
//{ if (f->Get(hn)==NULL) error("Cannot access: "+hn+" in file "+f->GetName()); return (TH1F*)f->Get(hn); }


//******************************************************************
//**************** functions to retrive calibration input points
//******************************************************************

Graph2D *GetCalibrationPoints(Graph2D *d, Graph2D *mcPrediction) {
  Graph2D *calib = new Graph2D();
  for (int i=0;i<d->GetN();++i) {
    double x=d->GetX()[i], y=d->GetY()[i];
    double ex=d->GetEX()[i], ey=d->GetEY()[i];
    double R_d = d->GetZ()[i], dR_d = d->GetEZ()[i];
    double R_mc = mcPrediction->GetZ()[i], dR_mc = mcPrediction->GetEZ()[i];
    double c=R_mc/R_d;
    double err_c=c*sqrt(dR_d*dR_d*pow(R_mc/R_d/R_d,2) + dR_mc*dR_mc/(R_d*R_d) );
    calib->SetPoint(i,x,y,c);
    calib->SetPointError(i,ex,ey,err_c);
  }
  return calib;
}



Graph2D* TrimGraph(Graph2D *g1, Graph2D *g2, Graph2D *g3, double maxPt=1500) {
  double maxErr =  0.04;
  int n=g1->GetN(), nn=0; Graph2D *g = new Graph2D();
  if (n!=g2->GetN()||n!=g3->GetN()){
    cout<<" Npoints g1 : g2 : g3 == "<<n<<" : "<<g2->GetN()<<" : "<<g3->GetN()<<endl;
    error("Trimming issue!");
  }
  for (int i=0;i<n;++i) {
    if (g1->GetZ()[i]<=0||g2->GetZ()[i]<=0||g3->GetZ()[i]<=0) continue;
    bool high_pT = ( g1->GetX()[i]>100 || g2->GetX()[i]>100 || g3->GetX()[i]>100 );
    if ( high_pT &&(g1->GetEZ()[i]>maxErr||g2->GetEZ()[i]>maxErr||g3->GetEZ()[i]>maxErr)) continue;
    g->SetPoint(nn,g1->GetX()[i],g1->GetY()[i],g1->GetZ()[i]);
    g->SetPointError(nn,g1->GetEX()[i],g1->GetEY()[i],g1->GetEZ()[i]);
    nn++;
  }
  return g;
}



Graph2D* GetCalibration2DGraph(TFile *f, Str jetAlgo, Str method, bool numInv=true, double pTmax=0 ){

  Graph2D *calibGraph = new Graph2D();
  TH2D *avg_pt  = Get2DHisto(f,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+method);
  TH2D *avg_eta = Get2DHisto(f,jetAlgo+"_AvgEta_vs_EtaPtAvg_"+method);


  int Npt=avg_pt->GetNbinsY(), Neta=avg_pt->GetNbinsX();

  for (int ipt=1;ipt<=Npt;++ipt) {

    double ptmin = avg_pt->GetYaxis()->GetBinLowEdge(ipt);
    double ptmax = avg_pt->GetYaxis()->GetBinLowEdge(ipt+1);

    if(pTmax>0 && pTmax<=ptmin) continue;

    Str Rhname=jetAlgo+"/"+jetAlgo+"_"+Form("pt%.0fto%.0f_R_",ptmin,ptmax)+method;
    TH1D *hR = GetHisto2(f,Rhname);


    if (hR->GetNbinsX()!=Neta) error("Binning problem");
    for (int ieta=1;ieta<=Neta;++ieta) {
      int n=calibGraph->GetN();
      double R=hR->GetBinContent(ieta);
      if (R < 0) {
	calibGraph->SetPoint(n,-99,-99,-99);
	continue;
      }

      double dR       = hR->GetBinError(ieta);
      double pt       = avg_pt->GetBinContent(ieta,ipt);
      double err_pt   = avg_pt->GetBinError(ieta,ipt);
      double eta      = avg_eta->GetBinContent(ieta,ipt);
      double err_eta  = avg_eta->GetBinError(ieta,ipt);

      double etamin = avg_pt->GetXaxis()->GetBinLowEdge(ieta);
      double etamax = avg_pt->GetXaxis()->GetBinLowEdge(ieta+1);
      //printf("Pt,Eta bins: (%2d,%2d), (%6.2f +/- %5.2f)%%\n",ipt,ieta,R*100-100,dR*100);
      //printf("%.2f < %.2f\n",avg_pt->GetXaxis()->GetBinLowEdge(ieta),eta);
      //printf("%.2f < %.2f\n",avg_pt->GetYaxis()->GetBinLowEdge(ipt),pt);
      if (numInv) {
      	// pTavg = (pTprobe+pTref)/2  and  R = pTprobe/pTref   => pTprobe = 2R/(R+1)pTavg
      	pt *= 2.0*R/(R+1); // pTavg -> pTprobe
      	// from error propagation:
      	//printf("Err befor: %.4f\n",err_pt);
      	err_pt = sqrt( pow(err_pt*2.0*R/(R+1),2) + pow(2.0*pt/(R+1)/(R+1)*dR,2 ) );
      	//printf("Err after: %.4f\n",err_pt);
      }
      //printf("( ptmin : ptmax :n : pt : eta : R )  = ( %.0f : %.0f : %d : %.0f : %.1f : %.2f )\n",ptmin, ptmax, n, pt,eta,R);
      calibGraph->SetPoint(n,pt,eta,R);
      calibGraph->SetPointError(n,err_pt,err_eta,dR);
    }
  }
  return calibGraph;
}


void PrintPoint(Graph2D *g, int i) {
  printf("Point %3d (x,y,z) = (%6.1f,%6.2f,%7.4f ) +/- (%6.1f,%6.2f,%7.4f)\n",
	 i,g->GetX()[i],g->GetY()[i],g->GetZ()[i],g->GetEX()[i],g->GetEY()[i],g->GetEZ()[i]);
}



Graph2D *GetAvg(Graph2D *g1, Graph2D *g2) {
  if (g1->GetN()!=g2->GetN()) error("GetAvg error.");
  Graph2D *g = new Graph2D();
  for (int i=0;i<g1->GetN();++i) {
    double x1=g1->GetX()[i], y1=g1->GetY()[i];
    double ex1=g1->GetEX()[i], ey1=g1->GetEY()[i];
    double z1=g1->GetZ()[i], z2=g2->GetZ()[i];
    double e1=g1->GetEZ()[i], e2=g2->GetEZ()[i];
    g->SetPoint(i,x1,y1,(z1+z2)/2);
    g->SetPointError(i,ex1,ey1,0.5*sqrt(e1*e1+e2*e2));
  }
  return g;
}


Graph2D *GetWeightedAvg(Graph2D *g1, Graph2D *g2) {
  if (g1->GetN()!=g2->GetN()) error("GetAvg error.");
  Graph2D *g = new Graph2D();
  for (int i=0;i<g1->GetN();++i) {
    double x1=g1->GetX()[i], y1=g1->GetY()[i];
    double ex1=g1->GetEX()[i], ey1=g1->GetEY()[i];
    double z1=g1->GetZ()[i], z2=g2->GetZ()[i];
    double e1=g1->GetEZ()[i], e2=g2->GetEZ()[i];
    g->SetPoint(i,x1,y1,(z1*e1+z2*e2)/(e1+e2));
    g->SetPointError(i,ex1,ey1,0.5*sqrt(e1*e1+e2*e2));
  }
  return g;
}


//******************************************************************
//**************** functions to retrive calibration pointshistos and values
//******************************************************************

void PrintVecI(VecI vecToPrint, TString startMessage="Printing vector of integers: "){
  cout <<startMessage;
  for(int i =0; i < vecToPrint.size(); ++i)
    cout <<vecToPrint[i] << ",";
  cout << endl;
}
VecI GetRvsPtPoints(Graph2D *g2d, double etamin, double etamax, double maxPt=1500) {
  VecI res;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double eta=g2d->GetY()[pi];
    if(g2d->GetX()[pi]>maxPt) continue;
    if (etamin<eta&&eta<etamax) res.push_back(pi);
  }
  return res;
}

VecI GetRvsEtaPoints(Graph2D *g2d, double ptmin, double ptmax) {
  VecI res;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double pt=g2d->GetX()[pi], R=g2d->GetZ()[pi];
    double sf=2.0*R/(R+1);
    if (ptmin<pt/sf&&pt/sf<ptmax) res.push_back(pi);
  }
  return res;
}

Graph *GetRvsEtaGraph(Graph2D *g2d, VecI points) {
  Graph *res = new Graph(); double sumpt=0;
  for (int pi=0;pi<points.size();++pi) {
    int n=res->GetN();
    int index=points[pi];
    res->SetPoint(n,g2d->GetY()[index],g2d->GetZ()[index]);
    res->SetPointError(n,g2d->GetEY()[index],g2d->GetEZ()[index]);
  }
  return res;
}


Graph *GetRvsEtaGraph(Graph2D *g2d, double ptmin, double ptmax) {
  // fix the pt range
  Graph *res = new Graph(); double sumpt=0;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double pt=g2d->GetX()[pi];
    double R=g2d->GetZ()[pi];
    double sf=2.0*R/(R+1);
    if (ptmin<pt/sf&&pt/sf<ptmax) {
      int n=res->GetN(); sumpt+=pt;
      res->SetPoint(n,g2d->GetY()[pi],g2d->GetZ()[pi]);
      res->SetPointError(n,g2d->GetEY()[pi],g2d->GetEZ()[pi]);
    }
  }
  int n=res->GetN();
  //  avg_pt = n>0?sumpt/n:0;
  return res;
}

Graph *GetRvsPtGraph(Graph2D *g2d, double etamin, double etamax) {
  Graph *res = new Graph(); double sumeta=0;
  for (int pi=0;pi<g2d->GetN();++pi) {
    double eta=g2d->GetY()[pi];
    if (etamin<eta&&eta<etamax) {
      int n=res->GetN(); sumeta+=eta;
      res->SetPoint(n,g2d->GetX()[pi],g2d->GetZ()[pi]);
      res->SetPointError(n,g2d->GetEX()[pi],g2d->GetEZ()[pi]);
    }
  }
  int n=res->GetN();
  //avg_eta = n>0 ? sumeta/n : 0;
  return res;
}


Graph *GetRvsPtGraph(Graph2D *g2d, VecI points) {
  Graph *res = new Graph(); double sumeta=0;
  for (int pi=0;pi<points.size();++pi) {
    int n=res->GetN();
    int index=points[pi];
    res->SetPoint(n,g2d->GetX()[index],g2d->GetZ()[index]);
    res->SetPointError(n,g2d->GetEX()[index],g2d->GetEZ()[index]);
  }
  return res;
}



double GetAvgEta(Graph2D *g2d, VecI points) {
  double sumw=0, sumwx=0;
  for (int pi=0;pi<points.size();++pi) {
    int i=points[pi];
    double e=g2d->GetEY()[i]; if (e<=0) continue;
    double w=1.0/e/e; sumw+=w; sumwx+=w*g2d->GetY()[i];
  }
  return sumw==0?0:sumwx/sumw;
}

double GetAvgPt(Graph2D *g2d, VecI points) {
  double sumw=0, sumwx=0;
  for (int pi=0;pi<points.size();++pi) {
    int i=points[pi]; double e=g2d->GetEX()[i]; if (e<=0) continue;
    double w=1.0/e/e; sumw+=w; sumwx+=w*g2d->GetX()[i];
  }
  return sumw==0?0:sumwx/sumw;
}



TH1D *GetSmoothVsEta(TH2D *h2d, double avg, VecD bins, double maxEta=4.5 ) {
  static int si=0;
  TH1D *h = new TH1D(Form("vseta%d",++si),"",bins.size()-1,&bins[0]);
  for (int i=1;i<=h->GetNbinsX();++i) {
    double eta=h->GetBinCenter(i);
    if(fabs(eta)>maxEta) continue;
    h->SetBinContent(i,h2d->Interpolate(avg,eta));
    double eta_edge=h->GetXaxis()->GetBinLowEdge(i);
    if(eta_edge<=0 && eta_edge >= -0.8 ) h->SetBinContent(i,0.0);
    if(eta_edge>=0 && eta_edge <= 0.7 )  h->SetBinContent(i,0.0);
  }
  return h;
}






TH1D *GetSmoothVsEta(TH2D *h2d, double avg, double min, double max) {
  static int si=0;
  TH1D *h = new TH1D(Form("vseta%d",++si),"",300,min,max);
  for (int i=1;i<=h->GetNbinsX();++i) {
    double eta=h->GetBinCenter(i);
    h->SetBinContent(i,h2d->Interpolate(avg,eta));
  }
  return h;
}

TH1D *GetSmoothVsPt(TH2D *h2d, double avg, double min, double max) {
  static int si=0;
  VecD bins=MakeLogVector(200,min,max);
  TH1D *h = new TH1D(Form("vspt%d",++si),"",200,&bins[0]);
  for (int i=1;i<=h->GetNbinsX();++i) {
    double pt=h->GetBinCenter(i);
    h->SetBinContent(i,h2d->Interpolate(pt,avg));
  }
  return h;

}

//************** errors etc.
void RemoveRefRegionPoints(Graph *g)
{
  for( int i=0; i<g->GetN(); i++){
    if(g->GetX()[i] >= -0.8 && g->GetX()[i] <= 0.8) { g->SetPoint(i,g->GetX()[i],0); g->SetPointError(i,0); }
  }
}



TH1D* AddError(TH1D *h, TH1D *herr) {
  static int a=0; TH1D *hh = (TH1D*)herr->Clone(Form("cerr%d",a++));
  for (int i=1;i<=h->GetNbinsX();++i) {
    hh->SetBinContent(i,h->GetBinContent(i));
    hh->SetBinError(i,herr->GetBinContent(i)*h->GetBinContent(i));
  }
  return hh;
}


TH1D *AddInQuad(TH1D *h1, TH1D *h2, Str hname="clone") {
  TH1D *total = (TH1D*)h1->Clone(h2->GetName()+hname);
  total->Reset();
  for (int i=1;i<=total->GetNbinsX();++i) {

    double x1 = h1->GetBinContent(i);
    double x2 = h2->GetBinContent(i);
    double sumx2 = x1*x1 + x2*x2;
    total->SetBinContent(i,sqrt(sumx2));
  }
  return total;
}

TH2D *AddInQuad(vector<TH2D*> hvec, Str hname) {
  TH2D *total = (TH2D*)hvec[0]->Clone(hname);
  total->Reset();
  for (int ipt=0;ipt<=total->GetNbinsX()+1;++ipt) {
    for (int ieta=0;ieta<=total->GetNbinsY()+1;++ieta) {
      double z=0, sumz2=0;
      for (int hi=0;hi<hvec.size();++hi) {
	z=hvec[hi]->GetBinContent(ipt,ieta);
	sumz2+=z*z;
      }
      total->SetBinContent(ipt,ieta,sqrt(sumz2));
    }
  }
  return total;
}


TH1D* AddError(TH1D *h, TH1D *herr, int color) {
  static int a=0;
  TH1D *hh = (TH1D*)herr->Clone(Form("cerr%d",a++));
  for (int i=1;i<=h->GetNbinsX();++i) {

    double binctr = h->GetXaxis()->GetBinCenter(i);
    int errbin = herr->GetXaxis()->FindBin(binctr);
    hh->SetBinContent(i,h->GetBinContent(i));
    hh->SetBinError(i,herr->GetBinContent(errbin)*h->GetBinContent(i));
  }
  hh->SetFillColor(color);
  hh->SetMarkerStyle(1);
  return hh;
}


TH1D* AddErrorToUnity(TH1D *herr, int color) {
  static int a=0;
  TH1D *hh = (TH1D*)herr->Clone(Form("cerr%d",a++));
  for (int i=1;i<=herr->GetNbinsX();++i) {
    hh->SetBinContent(i,1.0);
    hh->SetBinError(i,herr->GetBinContent(i));
  }

  hh->SetFillColor(color);
  hh->SetMarkerStyle(1);
  return hh;
}


TH2D *GetCalibRatio(TH2D *h1, TH2D *h2, Str hname="", TH2D *h3=NULL)
{
  TH2D *hratio = (TH2D*)h1->Clone(hname);
  hratio->Reset();
  for(int ax=1;ax<=h1->GetNbinsX();ax++)
    for(int ay=1;ay<=h1->GetNbinsY();ay++){
      double Sys1=-999, Sys2=-999;
      Sys1    =  h2->GetBinContent(ax,ay)/h1->GetBinContent(ax,ay);
      if(h3!=NULL) Sys2 = h3->GetBinContent(ax,ay)/h1->GetBinContent(ax,ay);
      double sys_max =  Sys1;
      if ( Sys1<Sys2  ) sys_max = Sys2;
      double SYST = fabs(1-sys_max);
      hratio->SetBinContent(ax,ay,SYST);
    }
  return hratio;
}


//**************** cool method to keep track of runtimes ****************************//

Str getTime()
{
  time_t aclock;
  ::time( &aclock );
  return Str(asctime( localtime( &aclock )));
}



void PrintTime()
{
  static bool first=true;
  static time_t start;
  if(first) { first=false; ::time(&start); }
  time_t aclock; ::time( &aclock );
  char tbuf[25]; ::strncpy(tbuf, asctime( localtime( &aclock ) ),24);
  tbuf[24]=0;
  cout <<  "Current time: " << tbuf
       << " ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}




#endif // #ifdef Utils_h
