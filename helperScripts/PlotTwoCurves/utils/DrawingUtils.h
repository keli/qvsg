#include <TCanvas.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TGraph2DErrors.h>
#include "AtlasLabels.C"

typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

TCanvas *_can;
TLatex *_tex;
double TEXTSIZE=0.05;

//int cols[] = {kRed, kBlue, kGreen+1, kOrange+7, kViolet+2, kGray+1};

int cols[] = {kBlack,kRed,kBlue,kViolet+1,kGreen+3,kOrange+2,kCyan+1,kMagenta+2,kYellow+2,kBlue-2,kCyan+3,kPink+3,kMagenta+2,kBlue-10};
int mark[] = {20,21,22,23,26,25,24,27,28,29,30,20,2,3,5,31,32,33,34};
int mark2[] = {24,25,26,27,28,29,30,2,3,5,31,32,33,34,20,21,22,23,24};


int _jetR;
Str _calibration, _methodDesc, _name;

TFile *Open(Str fn) { TFile *f=TFile::Open(fn); if (f==NULL) error("Can't open "+fn); return f; }
void FormatHisto(TH1 *h, int mstyle, int col, double msize);
void FormatHisto(TH1 *h, int style, double msize = 1.2);
void FormatGraph(Graph *h, int mstyle, int col, double msize);
void FormatGraph(Graph *h, int style, double msize = 1.2);

void DrawText(TString txt, int col=kBlack, double y=0.88, double x=0.145);
void DrawTextR(TString txt, int col=kBlack, double y=0.88, double x=0.92);
void DrawLabel(TString txt, int style, double x, double y);
void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize);


void FormatHisto(TH1 *h, int style, double msize)
{
  if      (style==1)  FormatHisto(h,20,kBlack,msize);
  else if (style==2)  FormatHisto(h,22,kRed,msize);
  else if (style==3)  FormatHisto(h,23,kBlue,msize);
  else if (style==4)  FormatHisto(h,24,kOrange+7,msize);
  else if (style==5)  FormatHisto(h,25,kGreen+2,msize);
  else if (style==6)  FormatHisto(h,26,kGray+2,msize);
  else if (style==7)  FormatHisto(h,27,kBlue-2+1,msize);
  else if (style==8)  FormatHisto(h,28,kCyan+1,msize);
  else if (style==9)  FormatHisto(h,29,kMagenta+2,msize);
  else if (style==10) FormatHisto(h,30,kPink+1,msize);

  h->SetXTitle("");
  h->SetYTitle("Relative jet response, 1/c");
  h->GetXaxis()->SetTitleOffset(0.95);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetYaxis()->SetNdivisions(505);
}

void FormatHisto(TH1 *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle);
  h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col);
}

void FormatGraph(Graph *h, int style, double msize)
{

  if      (style==1)  FormatGraph(h,20,kBlack,msize);
  else if (style==2)  FormatGraph(h,21,kRed,msize);
  else if (style==3)  FormatGraph(h,23,kBlue,msize);
  else if (style==4)  FormatGraph(h,26,kOrange+7,msize);
  else if (style==5)  FormatGraph(h,33,kGreen+2,msize);
  else if (style==6)  FormatGraph(h,28,kGray+2,msize);
  else if (style==10) FormatGraph(h,24,kBlack,msize);
  else if (style==11) FormatGraph(h,24,kBlack,msize);
  else if (style==12) FormatGraph(h,25,kRed,msize);
  else if (style==13) FormatGraph(h,27,kBlue,msize);

}

void FormatGraph(Graph *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle);
  h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col);
  h->SetLineWidth(2);
}

void DrawATLASLabel(double x=0.145, double y=0.93){

  ATLASLabel(x,y,(char*)"Internal",kBlack);
}


void PrintInfo() {
  DrawText(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration+", "+_methodDesc,kBlack,0.97,0.12);
  DrawTextR(_name,kBlack,0.97);
}

void PrintEtaBinInfo(double ptmin, double ptmax, Str Calibration = _calibration) {
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+Calibration,kBlack,0.93);
  DrawTextR(Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),1,0.86);
  //DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void PrintPtBinInfo(double etamin, double etamax, Str Calibration = _calibration) {

  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+Calibration,kBlack,0.93);
  DrawTextR(Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),1,0.86);
  //DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void DrawText(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(11); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt); }

void DrawTextR(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(31); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt);
  _tex->SetTextAlign(11); _tex->SetTextColor(kBlack); }

void DrawGuideLineSyst(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2);
  line->SetLineWidth(1); line->SetLineColor(kRed);
  line->DrawLine(min,0.05,max,0.05);
  line->SetLineColor(kBlue);
  line->DrawLine(min,0.02,max,0.02);
}

void DrawGuideLines(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2);
  line->SetLineWidth(1);
  line->SetLineColor(kGray+1);
  line->DrawLine(-1,0.95,-1,1.05);
  line->DrawLine(1,0.95,1,1.05);
  line->SetLineWidth(1); line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
  line->SetLineWidth(1);
  line->SetLineColor(kRed);
  line->DrawLine(min,1.05,max,1.05);
  line->DrawLine(min,0.95,max,0.95);
  line->SetLineColor(kBlue);
  line->DrawLine(min,1.02,max,1.02);
  line->DrawLine(min,0.98,max,0.98);
}

void DrawCenterLine(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2); line->SetLineWidth(1);
  line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
}

void DrawGuideLinesVsEta(double etaMin=-4.5, double etaMax=4.5) {
  DrawGuideLines(etaMin,etaMax);
}

void DrawGuideLinesVsPt() {
  DrawGuideLines(20,2500);
}

void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize) {
  TMarker *m = new TMarker(x,y,mstyle);
  m->SetNDC();
  m->SetMarkerSize(msize);
  m->SetMarkerColor(col);
  TLine *l = new TLine();
  l->SetLineWidth(2);
  l->SetLineColor(col);
  l->DrawLineNDC(x-0.02,y,x-0.005,y);
  l->DrawLineNDC(x+0.005,y,x+0.02,y);
  m->Draw();
  //_tex->SetTextSize(0.04);
  _tex->SetTextSize(TEXTSIZE);
  _tex->SetTextAlign(12);
  _tex->SetTextColor(kBlack);
  _tex->DrawLatex(x+0.04,y,txt);
  _tex->SetTextSize(TEXTSIZE);
}

void DrawLabel(TString txt, int style, double x, double y) {
  double size = 1.2;
  if (style==1)       DrawLabel(txt,x,y,20,kBlack,size);
  else if (style==2)  DrawLabel(txt,x,y,21,kRed,size);
  else if (style==3)  DrawLabel(txt,x,y,23,kBlue,size);
  else if (style==4)  DrawLabel(txt,x,y,26,kOrange+7,size);
  else if (style==5)  DrawLabel(txt,x,y,33,kGreen+2,size);
  else if (style==6)  DrawLabel(txt,x,y,28,kGray+2,size);
  else if (style==10) DrawLabel(txt,x,y,24,kBlack,size);
  else if (style==11) DrawLabel(txt,x,y,24,kBlack,size);
  else if (style==12) DrawLabel(txt,x,y,25,kRed,size);
  else if (style==13) DrawLabel(txt,x,y,27,kBlue,size);
}


void PrintIntegratedLumi(){
  DrawText("#int #it{L dt} = 3.3 fb^{-1}",kBlack,0.86,0.15);
}

void PrintDataMC(Str mc1, Str mc2) {
  float dy = 0.07;
  DrawLabel("Data 2015, #sqrt{s} = 13 TeV",1,0.38,0.5);
  DrawLabel(mc1,2,0.3,0.43);
  DrawLabel(mc2,3,0.64,0.43);
}



void DrawPtEtaPoints(Graph2D *graph, VecD ptBins, VecD etaBins, double freezeCalibEta = 4.5, double minPt = 20.0,  double maxPt = 1500.0 ) {

  TLine *line = new TLine();
  line->SetLineColor(kGray);
  Graph *bin_xy = new Graph(), *avg_xy = new Graph();
  static TH2F *h = new TH2F("a","",1,17.5,2000,1,etaBins[0],etaBins[etaBins.size()-1]);
  h->GetXaxis()->SetMoreLogLabels();
  h->GetYaxis()->SetTitleOffset(0.8);
  h->SetXTitle("p_{T}^{avg}");
  h->SetYTitle("#eta_{det}");
  h->SetXTitle("p_{T}^{probe} [GeV]");
  h->Draw();

  for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
    double etal = etaBins[ieta];
    double eta = 0.5*(etal+etaBins[ieta+1]);
    line->DrawLine(25,etal,1500,etal);
    if (ieta==etaBins.size()-2) line->DrawLine(minPt,etaBins[ieta+1],1500,etaBins[ieta+1]);
    for (int ipt=0;ipt<ptBins.size()-1;++ipt) {
      double ptl = ptBins[ipt];
      double pt  = 0.5*(ptl+ptBins[ipt+1]);
      line->DrawLine(ptl,etaBins[0],ptl,etaBins[etaBins.size()-1]);
      if (ipt==ptBins.size()-2) line->DrawLine(ptBins[ipt+1],etaBins[0],ptBins[ipt+1],4.5);
      int n=bin_xy->GetN();
      bin_xy->SetPoint(n,pt,eta);
    }
  }
  for (int i=0;i<graph->GetN();++i) {
    avg_xy->SetPoint(i,graph->GetX()[i],graph->GetY()[i]);
    avg_xy->SetPointError(i,graph->GetEX()[i],graph->GetEY()[i]);
  }
  avg_xy->SetMarkerSize(0.4);
  avg_xy->Draw("P");


  float minEta = -freezeCalibEta;
  if(etaBins[0]>=0) minEta = 0;

  line->SetLineColor(kRed+1);
  line->SetLineWidth(2);

  line->DrawLine(minPt,minEta,minPt,freezeCalibEta);
  line->DrawLine(minPt,minEta,maxPt,minEta);
  line->DrawLine(maxPt,minEta,maxPt,freezeCalibEta);
  line->DrawLine(minPt,freezeCalibEta,maxPt,freezeCalibEta);
  h->Draw("sameaxis");

}
