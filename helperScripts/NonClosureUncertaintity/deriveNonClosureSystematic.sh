#!/bin/sh

code=DeriveNonClosureSystematicBS.C
exec=finalize_calibration.exe

etaBinning="0.3"
jetAlgos="AntiKt4LCTopo"
methods="MM"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter"

rm -f $exec
g++ $flagsNlibs -o $exec $code && {

    echo "*********************"
    echo "Compilation successful"
    echo "*********************"

    for meth in $methods ; do
		method=$meth
		for jA in $jetAlgos ; do

	    echo $jA

      # dataFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/EMTopo/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root
      # #/pc2014-data5/jrawling/data16_13TeV/ClosurePartial/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root
      # nom_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4EMTopo.root
      # syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/Sherpa_Eta2016v12_AntiKt4EMTopo.root
      # outputFilePath=Test/

      # nonClosureFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/Closure/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root
      # nonClosureMCFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4EMTopo.root
      # totalSystFile=/afs/hep.man.ac.uk/u/jrawling/LEGACY/CleanEtaIntercal/CalibrationAndUncertainties/Test/Jul16_EtaIntercalibration13TeV25ns_Eta2016v12_MM_AntiKt4EMTopo_Uncertainties.root

	    dataFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/LCTopo/data16_13TeV__Eta2016v12_AntiKt4LCTopo.root
      nom_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4LCTopo.root
      syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4LCTopo.root
      outputFilePath=LCtopo/

      nonClosureFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/LCTopo/data16_13TeV__Eta2016v12_AntiKt4LCTopo.root
      nonClosureMCFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4LCTopo.root
      totalSystFile=/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/CalibrationAndUncertainties/Test/Nov16_EtaIntercalibration13TeV25ns_Eta03_MM_AntiKt4LCTopo_Uncertainties.root

      /afs/hep.man.ac.uk/u/jrawling/LEGACY/CleanEtaIntercal/CalibrationAndUncertainties/Test/Jul16_EtaIntercalibration13TeV25ns_Eta2016v12_MM_AntiKt4EMTopo_Uncertainties.root
#''      Symmetrized_15_EtaIntercalibration13TeV25nsPreliminary_MM_AntiKt4EMTopo_Uncertainties.root

	    echo "***************************************************************"
	    echo "   method: $meth -- jetalgo: $jA  "
	    echo "***************************************************************"
	    echo "*********************   inputs   ******************************"
	    echo "***************************************************************"

	    echo $dataFile
	    echo $nom_mc_File
	    ./$exec $jA $meth $dataFile $nom_mc_File $syst_mc_File $nonClosureFile $nonClosureMCFile 


	    echo "**********************************************************"
	    echo "**********************************************************"
	    echo "*********************** done *****************************"
	done
    done

}
