# setupATLAS and AnalysisBase
# this will create the CMakeLists.txt file for your analysis locally
printf "\n===================================\n"
printf "SETUP EtaInterCalxAODAnalysis : Setup of ATLAS Environment"
printf "\n====================================\n"
setupATLAS
asetup AnalysisBase,21.2.124,here
# create the build directory
printf "\n====================================\n"
printf "SETUP EtaInterCalxAODAnalysis : Making build directory for CMake building"
printf "\n====================================\n"
mkdir build
cd build
# configure and make with CMake
printf "\n====================================\n"
printf "SETUP EtaInterCalxAODAnalysis : Building/Compiling with CMake"
printf "\n====================================\n"
cmake ../
make -j5
# set the environment paths as described
# here : https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialCMake#Runtime_Environment
printf "\n====================================\n"
printf "SETUP EtaInterCalxAODAnalysis : Loading the environment after building with CMake"
printf "\n====================================\n"
cd ../
source build/x86*/setup.sh
#source build/${CMTCONFIG}/setup.sh

#variable to tell the script cmake has been used
export EtaInterCal_cmake="yes"
