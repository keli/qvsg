#include "PhiInterCalibration/PhiInterCalibration.h"
#include "util/atlasstyle/AtlasStyle.C"

int main(int argc, char** argv) {

  if(argc<2){
    std::cout << "ERROR: Too few arguments. Missing intput file?" << std::endl;
    abort();
  }

  SetAtlasStyle(); gErrorIgnoreLevel=2001; gStyle->SetPalette(1);
  TH1::SetDefaultSumw2(kTRUE);

  // Phi-intercalibration
  // Takes as input mini-trees from EtaInterCalxAODAnalysis
  // Loop over arguments, read input files, parse options
  std::string inFileName;
  std::string outFileName="PhiInterCal";
  std::string plotFileName="";
  std::string config="settings.config";
  //  std::string config="central_phiintercal_settings.config"
  bool isMC = true;
  double offset=0.0;
  std::string sample;
  // loop over arguments
  for(int i=1; i<argc; ++i){
    TString arg = TString(argv[i]);
    if(arg.BeginsWith("-")){
      arg.ReplaceAll("-","");
      if(arg=="inFile"||arg=="f"){ inFileName = argv[i+1]; ++i; continue; }
      if(arg=="outFile"||arg=="o"){ outFileName = argv[i+1]; ++i; continue; }
      if(arg=="plotFile"||arg=="p"){ plotFileName = argv[i+1]; ++i; continue; }
      if(arg=="settings"||arg=="s"){ config = argv[i+1]; ++i; continue; }
      if(arg=="offset"){ offset = atof(argv[i+1]); ++i; continue; }
      if(arg=="isData"){ isMC=false; continue; }
      if(arg=="isMC"){ isMC=true; continue; }
    }

    std::cout << "Don't understand input argument " << arg << std::endl;
    abort();
  }

  if ( plotFileName=="" )
  plotFileName = outFileName;

  // open settings file
  TEnv* settings = new TEnv();
  settings->ReadFile(Form("PhiInterCalibration/share/%s",config.c_str()),EEnvLevel(0));
  settings->ReadFile("EtaInterCalxAODAnalysis/share/settings.config",EEnvLevel(0));
  settings->ReadFile("EtaInterCalxAODAnalysis/share/MC_metadata.config",EEnvLevel(0));
  std::vector<double> phi_bins = vectoriseD(settings->GetValue("PhiBins",""));
  std::vector<double> pTavg_bins = vectoriseD(settings->GetValue("pTavgBins",""));

  // read input file and ttree
  TFile *infile = TFile::Open(inFileName.c_str(),"read");
  if ( !infile->IsOpen() ) {
    std::cout << "Failed to open file " << inFileName << std::endl;
    abort();
  }

  std::cout << "Initialising phi-intercalibration..." << std::endl;

  PhiInterCalibration phi_intercalibration(infile,settings,isMC,offset);
  std::vector<double> eta_bins = phi_intercalibration.GetEtaBins();

  TCanvas *can = new TCanvas();
  can->SetTicks(1,1);
  can->Draw();

  TString ofn = "plots/"+plotFileName+"_Asym.pdf";
  TString ofn_R = "plots/"+plotFileName+"_R.pdf";
  can->Print(ofn+"[");
  can->Print(ofn_R+"[");

  TLatex *tex = new TLatex();
  tex->SetNDC(); tex->SetTextSize(0.035);

  TFile *fout = TFile::Open((outFileName+".root").c_str(),"recreate");

  for (uint ipTavg=1; ipTavg<pTavg_bins.size(); ++ipTavg) {
    for (uint ieta=1; ieta<eta_bins.size(); ++ieta) {
      //    for (int ieta=1; ieta<=3; ++ieta) {
      //      if(ieta==2) continue;
      TH1F c(Form("R_pTavg%d_eta%d",ipTavg,ieta),"",phi_bins.size()-1,&phi_bins[0]);
      c.SetMarkerSize(0.6);
      c.GetYaxis()->SetTitle("Relative response, 1/#font[52]{c}");
      c.GetXaxis()->SetTitle("probe jet #phi_{det}");
      can->Clear(); can->Divide(4,4);
      for (uint iphi=1; iphi<phi_bins.size(); ++iphi) {
        can->cd(iphi > 14 ? iphi-14 : iphi);
        TH1F* h = phi_intercalibration.AsymHist(ipTavg, ieta, iphi);
        int rebin = OptimalRebin(h);
        h->Rebin(rebin);
        h->SetTitle(";(p_{T}^{probe}-p_{T}^{ref})/p_{T}^{avg};Entries");
        h->SetMarkerSize(0);
        h->SetLineWidth(2);
        TF1* fit = phi_intercalibration.FitAsymHist(h);
        fit->SetLineColor(kRed+1);
        fit->SetLineWidth(1);
        h->Draw();
        fit->Draw("same");
        tex->DrawLatex(0.2,0.85,Form(" %1.0f < p_{T}^{avg} < %1.0f",pTavg_bins[ipTavg-1],pTavg_bins[ipTavg]));
        tex->DrawLatex(0.2,0.80,Form(" %1.1f < #eta_{probe} < %1.1f",eta_bins[ieta-1],eta_bins[ieta]));
        tex->DrawLatex(0.2,0.75,Form(" %1.3f < #phi_{probe} < %1.3f",phi_bins[iphi-1],phi_bins[iphi] ));

        double A = fit->GetParameter(1), dA = fit->GetParError(1);
        double R = (2.0+A)/(2.0-A);
        double dR = 4.0/pow(2.0-A,2)*dA;

        //	c.SetBinContent(iphi,1/R);
        //	c.SetBinError(iphi,dR/R/R);
        c.SetBinContent(iphi,R);
        c.SetBinError(iphi,dR);

        if(phi_bins.size()==29 && (iphi==14||iphi==28)){
          can->Print(ofn);
          can->Clear(); can->Divide(4,4);
        }
        if(phi_bins.size()==15 && (iphi==7||iphi==14)){
          can->Print(ofn);
          can->Clear(); can->Divide(4,4);
        }
      }
      can->Clear();
      c.GetYaxis()->SetRangeUser(0.8,1.2);
      c.Draw();

      tex->DrawLatex(0.2,0.85,Form(" %1.0f < p_{T}^{avg} < %1.0f",pTavg_bins[ipTavg-1],pTavg_bins[ipTavg]));
      tex->DrawLatex(0.2,0.80,Form(" %1.1f < #eta_{probe} < %1.1f",eta_bins[ieta-1],eta_bins[ieta]));
      if(settings->GetValue("InjectBias",0)) {
        TF1* func = phi_intercalibration.drawBias();
        tex->DrawLatex(0.2,0.75,"#color[9]{Injected bias}");
        if(ipTavg==1&&ieta==1){ fout->cd(); func->Write(); }
      }
      fout->cd();
      c.Write();
      can->Print(ofn_R);
    }
  }

  phi_intercalibration.WriteAvgPtEtaPhiGraph(fout);

  can->Print(ofn+"]");
  can->Print(ofn_R+"]");

  fout->Close();
  delete fout;

  return 0;
}
