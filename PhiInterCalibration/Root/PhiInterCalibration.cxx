#ifndef PhiInterCalibration_cxx
#define PhiInterCalibration_cxx

#include "PhiInterCalibration/PhiInterCalibration.h"

Double_t pos_step_func(Double_t *x, Double_t *par) {
  Float_t xx=x[0];
  Double_t f = (1+par[0]*sin((par[1]*xx) + par[2]*TMath::Pi()));
  //  return f >= 1 ? (1-par[0]) : 1.0;
  return f >= 1 ? (1-par[0]) : (1+par[0]);
}

Double_t pos_sine_func(Double_t *x, Double_t *par)
{
  Float_t xx =x[0];
  Double_t f = (1+par[0]*sin((par[1]*xx) + par[2]*TMath::Pi()));
  return f >= 1 ? 1.0 : f;
}

// constructor
PhiInterCalibration::PhiInterCalibration(TFile* infile, TEnv* settings, bool isMC, double offset){

  m_isMC = isMC;
  if ( infile->GetListOfKeys()->Contains("dijet_insitu") ) {
    m_tree = (TTree*)infile->Get("dijet_insitu");
    if ( m_tree->GetEntries()<1 ) {
      m_tree = (TTree*)infile->Get("AntiKt4EMTopo_dijet_insitu");
    }
  }
  else if ( infile->GetListOfKeys()->Contains("AntiKt4EMTopo_dijet_insitu") )
  m_tree = (TTree*)infile->Get("AntiKt4EMTopo_dijet_insitu");
  else {
    std::cout << "Can't find ttree in file!" << std::endl;
    abort();
  }

  m_settings = settings;

  if ( m_isMC ) {
    if ( TString(infile->GetName()).Contains("PowhegPythia") )
    m_mcName = "PowhegPythia8";
    else if ( TString(infile->GetName()).Contains("Pythia") )
    m_mcName = "Pythia8";
    else if ( TString(infile->GetName()).Contains("Sherpa") )
    m_mcName = "Sherpa";
    else {
      std::cout << "Can't find MC name!" << std::endl;
      abort();
    }
  }

  if ( offset > -1 ) {
    m_doOffset = true;
    m_offset = offset;
  }
  else {
    m_doOffset = false;
    m_offset = -1;
  }

  phi_bins = vectoriseD(settings->GetValue("PhiBins",""));
  phi_bins[0] = -TMath::Pi(); phi_bins[phi_bins.size()-1] = TMath::Pi();

  pTavg_bins = vectoriseD(settings->GetValue("pTavgBins",""));
  m_refRegion = settings->GetValue("RefRegion",3.2);
  m_probeRegion_min = vectoriseD(settings->GetValue("ProbeRegion","")).at(0);
  m_probeRegion_max = vectoriseD(settings->GetValue("ProbeRegion","")).at(1);
  std::vector<double> probe_regions = vectoriseD(settings->GetValue("ProbeRegion",""));
  eta_bins.clear();

  for ( int i=probe_regions.size()-1; i>=0; i-- ) {
    eta_bins.push_back(-probe_regions[i]);
  }

  for ( uint i=0; i<probe_regions.size(); ++i )
  eta_bins.push_back(probe_regions[i]);

  // std::cout << "Eta bins are: " << std::endl;
  // for ( auto bin : eta_bins )  std::cout << bin << std::endl;

  int n_asym_bins = settings->GetValue("NAsymBins",150);
  std::vector<double> asym_range = vectoriseD(settings->GetValue("AsymBinsRange",""));
  asym_bins = makeUniformVec(n_asym_bins,asym_range[0],asym_range[1]);

  eta_bins_plots = vectoriseD(settings->GetValue("EtaBins",""));

  m_injectBias = settings->GetValue("InjectBias",0);
  m_useJXweight = settings->GetValue("UseJXWeight",0);

  if(m_injectBias) {
    double par0 = settings->GetValue("Bias",1.0);
    double par1 = settings->GetValue("BiasPeriod",1.0);
    double par2 = settings->GetValue("BiasOffset",0.0);

    TString func = settings->GetValue("BiasFunc","sin");

    std::cout << "\n";
    std::cout << "Injecting bias into jet response" << std::endl;
    std::cout << "  shape:     " << func << std::endl;
    std::cout << "  amplitude: " << par0 << std::endl;
    std::cout << "  period:    " << par1/TMath::Pi() << "pi" << std::endl;
    std::cout << "  offset:    " << par2 << std::endl;
    std::cout << "\n";

    if(func=="sin")
    m_biasFunc = new TF1("bias","1+[0]*sin(([1]*x) + [2]*TMath::Pi())",-TMath::Pi(), TMath::Pi());
    else if(func=="pos_sin")
    m_biasFunc = new TF1("bias",pos_sine_func,-TMath::Pi(), TMath::Pi(),3);
    else if(func=="step")
    m_biasFunc = new TF1("bias",pos_step_func,-TMath::Pi(), TMath::Pi(),3);
    else {
      std::cout << "Didn't recognise bias functional form." << std::endl;
      abort();
    }

    m_biasFunc->SetParameter(0,par0);
    m_biasFunc->SetParameter(1,par1);
    m_biasFunc->SetParameter(2,par2);
    m_biasFunc->SetNpx(1000);

  }

  for ( int i=0; i<9; ++i) {
    m_cutFlow_Tot.push_back(0);
    m_cutFlow_pTavg.push_back(0);
    m_cutFlow_LeadRef.push_back(0);
    m_cutFlow_LeadRefSubProbe.push_back(0);
    m_cutFlow_SubRef.push_back(0);
    m_cutFlow_SubRefLeadProbe.push_back(0);
  }

  InitTree();

  m_pTavgHist = new TH1F("pTavg","",300,0,300);
  for (uint i=0; i<pTavg_bins.size()-1; ++i) {
    m_3DAsymHists.push_back( new TH3F(Form("3DAsymHists_%d",i+1),"",eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0],n_asym_bins,&asym_bins[0]) );
    m_PtProbeHists.push_back( new TH1F(Form("pTprobe_pTavgBin%d",i+1),"",300,0,300) );
    m_PtRefHists.push_back( new TH1F(Form("pTref_pTavgBin%d",i+1),"",300,0,300) );
    m_PtAvgHists.push_back( new TH1F(Form("pTavg_pTavgBin%d",i+1),"",300,0,300) );

    for ( uint j=0; j<eta_bins_plots.size()-1; ++j) {
      m_phiHists.insert( std::pair<std::string,TH1F*>(Form("phi_%1.0fpTavg%1.0f_%1.1feta%1.1f",pTavg_bins[i],pTavg_bins[i+1],eta_bins_plots[j],eta_bins_plots[j+1]), new TH1F(Form("phi_%1.0fpTavg%1.0f_%1.1feta%1.1f",pTavg_bins[i],pTavg_bins[i+1],eta_bins_plots[j],eta_bins_plots[j+1]),";Probe jet #phi;",phi_bins.size()-1,&phi_bins[0])) );
    }
  }
  for(uint i=0; i<=8; ++i) {
    m_pTprobe_JX.push_back( new TH1F(Form("pTprobe_JX%d",i),"",300,0,300) );
    m_pTavg_JX.push_back( new TH1F(Form("pTavg_JX%d",i),"",300,0,300) );
  }

  Fill3DHists();

}

PhiInterCalibration::~PhiInterCalibration(){}


TH1F* PhiInterCalibration::AsymHist(int pTavg_bin, int eta_bin, int phi_bin) {
  return (TH1F*)m_3DAsymHists[pTavg_bin-1]->ProjectionZ(Form("Asym_%d_%d_%d",pTavg_bin,eta_bin,phi_bin),eta_bin,eta_bin,phi_bin,phi_bin);
}

double PhiInterCalibration::weightJX(int JX){

  float eff=1.0;
  float N=1.0;
  float xs=1.0;

  if ( m_mcName=="PowhegPythia8" ) {
    eff = m_settings->GetValue(Form("PowhegPythia8.42600%d.FilterEff",JX),-99.0);
    N = m_settings->GetValue(Form("PowhegPythia8.42600%d.SumW",JX),-99.0);
    xs = m_settings->GetValue(Form("PowhegPythia8.42600%d.XSec",JX),-99.0);
  }
  else if ( m_mcName=="Pythia8" ) {
    eff = m_settings->GetValue(Form("Pythia8.14791%d.FilterEff",JX),-99.0);
    N = m_settings->GetValue(Form("Pythia8.14791%d.NEvents",JX),-99.0);
    xs = m_settings->GetValue(Form("Pythia8.14791%d.XSec",JX),-99.0);
  }
  else if ( m_mcName="Sherpa" ) {
    eff = m_settings->GetValue(Form("Sherpa.00000%d.FilterEff",JX),-99.0);
    N = m_settings->GetValue(Form("Sherpa.00000%d.NEvents",JX),-99.0);
    xs = m_settings->GetValue(Form("Sherpa.00000%d.XSec",JX),-99.0);
  }
  else {
    std::cout << "Couldn't get MC JX weight!" << std::endl;
    abort();
  }

  return (eff*xs)/N;
}

int PhiInterCalibration::pTavgBin(double pTavg){
  int pTavg_bin=-1;
  for(uint i=1; i<pTavg_bins.size(); ++i) {
    if(pTavg>pTavg_bins[i-1] && pTavg<pTavg_bins[i]){
      pTavg_bin=i;
    }
  }
  return pTavg_bin;
}

int PhiInterCalibration::etaBin(double eta){
  int eta_bin=-1;
  for(uint i=1; i<eta_bins_plots.size(); ++i) {
    if(eta>eta_bins_plots[i-1] && eta<eta_bins_plots[i]){
      eta_bin=i;
    }
  }
  return eta_bin;
}

double PhiInterCalibration::bias(double phi) {
  return m_biasFunc->Eval(phi);
}

TF1* PhiInterCalibration::drawBias(){
  m_biasFunc->SetLineColor(9);
  m_biasFunc->SetLineWidth(2);
  m_biasFunc->Draw("same");
  return m_biasFunc;
}

TString PhiInterCalibration::triggerClassification(int ptbin, float pTavg){

  int itrig = -1;

  if ( m_pTbinTrigs[ptbin].Contains("j15orj25") ) {
    itrig=0;
    if ( pTavg>25 && (m_pass_HLT_j15 || m_pass_HLT_j15_320eta490) ) {
      bool passEmulFwd = m_trigBitsFwd & int(pow(2,itrig));
      if ( m_pass_HLT_j15_320eta490 )
      return "FJ15";
      else if ( !passEmulFwd )
      return "J15";
      else
      return "";
    }
    else if ( pTavg>40 && (m_pass_HLT_j25 || m_pass_HLT_j25_320eta490) ) {
      bool passEmulFwd = m_trigBitsFwd & int(pow(2,itrig));
      if ( m_pass_HLT_j25_320eta490 )
      return "FJ25";
      else if ( !passEmulFwd )
      return "J25";
      else
      return "";
    }
  }
  else if ( m_pTbinTrigs[ptbin].Contains("j15") ) {
    itrig=0;
    if ( m_pass_HLT_j15 || m_pass_HLT_j15_320eta490 ) {
      bool passEmulFwd = m_trigBitsFwd & int(pow(2,itrig));
      if ( m_pass_HLT_j15_320eta490 )
      return "FJ";
      else if ( !passEmulFwd )
      return "J";
      else
      return "";
    }
  }
  else if ( m_pTbinTrigs[ptbin].Contains("j25") ) {
    itrig=1;
    if ( m_pass_HLT_j25 || m_pass_HLT_j25_320eta490 ) {
      bool passEmulFwd = m_trigBitsFwd & int(pow(2,itrig));
      if ( m_pass_HLT_j25_320eta490 )
      return "FJ";
      else if ( !passEmulFwd )
      return "J";
      else
      return "";
    }
  }
  else if ( m_pTbinTrigs[ptbin].Contains("j60") ) {
    itrig=1;
    if ( m_pass_HLT_j60 || m_pass_HLT_j60_320eta490 ) {
      bool passEmulFwd = m_trigBitsFwd & int(pow(2,itrig));
      if ( m_pass_HLT_j60_320eta490 )
      return "FJ";
      else if ( !passEmulFwd )
      return "J";
      else
      return "";
    }
  }
  else
  return "";

  return "";
}

float PhiInterCalibration::lumiWeight(int ptbin, TString trig_class) {

  if ( trig_class=="J" ) {
    if ( m_pTbinTrigs[ptbin].Contains("j15") )
    return 1.0/m_ctrl_lumis[0]*1e6;
    if ( m_pTbinTrigs[ptbin].Contains("j25") )
    return 1.0/m_ctrl_lumis[1]*1e6;
    if ( m_pTbinTrigs[ptbin].Contains("j60") )
    return 1.0/m_ctrl_lumis[2]*1e6;
  }
  else if ( trig_class=="FJ" ) {
    if ( m_pTbinTrigs[ptbin].Contains("j15") )
    return 1.0/m_fwd_lumis[0]*1e6;
    if ( m_pTbinTrigs[ptbin].Contains("j25") )
    return 1.0/m_fwd_lumis[1]*1e6;
    if ( m_pTbinTrigs[ptbin].Contains("j60") )
    return 1.0/m_fwd_lumis[2]*1e6;
  }
  if ( trig_class=="J15" ) {
    return 1.0/m_ctrl_lumis[0]*1e6;
  }
  else if ( trig_class=="FJ15" ) {
    return 1.0/m_fwd_lumis[0]*1e6;
  }
  if ( trig_class=="J25" ) {
    return 1.0/m_ctrl_lumis[1]*1e6;
  }
  else if ( trig_class=="FJ25" ) {
    return 1.0/m_fwd_lumis[1]*1e6;
  }
  else {
    std::cout << "ERROR: can't understand lumi weight!" << std::endl;
    abort();
  }
}

void PhiInterCalibration::applyPhiOffset(float &phi) {

  phi -= m_offset;
  if ( phi < -TMath::Pi() )
  phi += 2*TMath::Pi();

  return;
}

void PhiInterCalibration::Fill3DHists() {

  std::vector<double> pTavg;
  std::vector<double> pTavg_w;
  std::vector<double> pTprobe;
  std::vector<double> pTprobe_w;

  double pTavg_for_bins;

  // create temporary histograms for calculating average pTavg, eta and phi
  TH3F* pTavgEtaPhi_sumw = new TH3F("sumw","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumw2 = new TH3F("sumw2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwpt = new TH3F("sumwpt","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwpt2 = new TH3F("sumwpt2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwptprobe = new TH3F("sumwptprobe","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwptprobe2 = new TH3F("sumwptprobe2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwptref = new TH3F("sumwptref","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwptref2 = new TH3F("sumwptref2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumweta = new TH3F("sumweta","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumweta2 = new TH3F("sumweta2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwphi = new TH3F("sumwphi","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  TH3F* pTavgEtaPhi_sumwphi2 = new TH3F("sumwphi2","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);

  m_pTavgEtaPhi = new TGraph2DErrors();
  m_pTavgEtaPhi->SetName("avg_pTavgEtaPhi");

  m_pTavg_pTavgEtaPhi = new TH3F("avg_pTavg","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  m_pTprobe_pTavgEtaPhi = new TH3F("avg_pTprobe","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  m_pTref_pTavgEtaPhi = new TH3F("avg_pTref","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  m_eta_pTavgEtaPhi = new TH3F("avg_eta","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);
  m_phi_pTavgEtaPhi = new TH3F("avg_phi","",pTavg_bins.size()-1,&pTavg_bins[0],eta_bins.size()-1,&eta_bins[0],phi_bins.size()-1,&phi_bins[0]);

  // use biased pTavg for binning? (when injecting bias)
  int binInNewPtAvg = m_settings->GetValue("BinInNewPtAvg",0);
  if(m_injectBias){
    std::cout << "Bin in new pTavg? " << binInNewPtAvg << std::endl;
  }

  std::cout << "Data or MC:       " << (m_isMC ? "MC" : "Data") << std::endl;
  if ( m_isMC ) std::cout << "Use JX weights? " << m_useJXweight << std::endl;
  if ( m_isMC ) std::cout << "MC generator: " << m_mcName << std::endl;

  Long64_t nentries = m_tree->GetEntries();
  for (Long64_t i = 0; i<nentries; i++) {
    m_tree->GetEntry(i);

    if(i%500000 == 0) std::cout << Form(" %5dk / %6.2fk events ( %7.1f%% )",int(i)/1000,double(nentries)/1e3,100.0*i/nentries) << std::endl;

    // convert to GeV
    m_pTavg /= 1000.;
    if ( !(TString(m_tree->GetName()).Contains("AntiKt4EMTopo")) ) {
      m_j1_pT /= 1000.; m_j2_pT /= 1000.;
    }

    // inject bias into jet response if outside reference region
    if(m_injectBias) {

      if(fabs(m_j1_eta)>m_refRegion)
      m_j1_pT *= bias(m_j1_phi);
      if(fabs(m_j2_eta)>m_refRegion)
      m_j2_pT *= bias(m_j2_phi);

    }

    // multiply by JX weight
    if ( m_isMC && m_useJXweight )
    m_weight *= weightJX(m_JX);

    m_cutFlow_Tot[m_JX-1]+=1;

    // pTavg bin
    int pTavg_bin = -1;
    if(m_injectBias) {
      if(!binInNewPtAvg) {
        pTavg_bin = pTavgBin(m_pTavg);
        pTavg_for_bins = m_pTavg;
        m_pTavg = (m_j1_pT+m_j2_pT)/2.0;
      }
      else{
        m_pTavg = (m_j1_pT+m_j2_pT)/2.0;
        pTavg_bin = pTavgBin(m_pTavg);
        pTavg_for_bins = m_pTavg;
      }
    }
    else {
      pTavg_bin = pTavgBin(m_pTavg);
      pTavg_for_bins = m_pTavg;
    }
    if(pTavg_bin<0) continue;

    m_cutFlow_pTavg[m_JX-1]+=1;

    // Apply an offset to phi
    if ( m_doOffset ) {
      applyPhiOffset(m_j1_phi);
      applyPhiOffset(m_j2_phi);
    }

    if(pTavg_bin==2) pTavg.push_back(m_pTavg);
    if(pTavg_bin==2) pTavg_w.push_back(m_weight);

    // data trigger selection
    if ( !m_isMC ) {
      std::cout << "classifying trigger" << std::endl;
      TString trig_class = triggerClassification(pTavg_bin-1,m_pTavg);

      // fail trigger classification
      if ( trig_class == "" ) continue;

      m_weight *= lumiWeight(pTavg_bin-1,trig_class);

    }

    // check eta bin for each jet and (if within valid eta range), fill phi histograms
    int ieta=0;
    ieta = etaBin(m_j1_eta);
    if (ieta>0)
    m_phiHists[ Form("phi_%1.0fpTavg%1.0f_%1.1feta%1.1f",pTavg_bins[pTavg_bin-1],pTavg_bins[pTavg_bin],eta_bins_plots[ieta-1],eta_bins_plots[ieta]) ]->Fill(m_j1_phi,m_weight);
    ieta = etaBin(m_j2_eta);
    if (ieta>0)
    m_phiHists[ Form("phi_%1.0fpTavg%1.0f_%1.1feta%1.1f",pTavg_bins[pTavg_bin-1],pTavg_bins[pTavg_bin],eta_bins_plots[ieta-1],eta_bins_plots[ieta]) ]->Fill(m_j2_phi,m_weight);

    m_pTavgHist->Fill(m_pTavg,m_weight);
    if ( m_isMC )
    m_pTavg_JX[m_JX]->Fill(m_pTavg,m_weight);

    // leading jet in reference region
    if(fabs(m_j1_eta)<m_refRegion){

      m_cutFlow_LeadRef[m_JX-1]+=1;
      if(fabs(m_j2_eta)>3.2)
      m_cutFlow_LeadRefSubProbe[m_JX-1]+=1;

      float asym = (m_j2_pT-m_j1_pT)/m_pTavg;
      m_3DAsymHists[pTavg_bin-1]->Fill(m_j2_eta,m_j2_phi,asym,m_weight);

      if(pTavg_bin==2 && m_j2_eta>3.2) pTprobe.push_back(m_j2_pT);
      if(pTavg_bin==2 && m_j2_eta>3.2) pTprobe_w.push_back(m_weight);

      if(m_j2_eta>3.2){
        if ( m_isMC ) m_pTprobe_JX[m_JX]->Fill(m_j2_pT,m_weight);
        m_PtProbeHists[pTavg_bin-1]->Fill(m_j2_pT,m_weight);
        m_PtAvgHists[pTavg_bin-1]->Fill(m_pTavg,m_weight);
        m_PtRefHists[pTavg_bin-1]->Fill(m_j1_pT,m_weight);
      }

      pTavgEtaPhi_sumw->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight);
      pTavgEtaPhi_sumw2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_weight);
      pTavgEtaPhi_sumwpt->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_pTavg);
      pTavgEtaPhi_sumwpt2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_pTavg*m_pTavg);
      pTavgEtaPhi_sumwptprobe->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_pT);
      pTavgEtaPhi_sumwptprobe2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_pT*m_j2_pT);
      pTavgEtaPhi_sumwptref->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j1_pT);
      pTavgEtaPhi_sumwptref2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j1_pT*m_j1_pT);
      pTavgEtaPhi_sumweta->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_eta);
      pTavgEtaPhi_sumweta2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_eta*m_j2_eta);
      pTavgEtaPhi_sumwphi->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_phi);
      pTavgEtaPhi_sumwphi2->Fill(pTavg_for_bins,m_j2_eta,m_j2_phi,m_weight*m_j2_phi*m_j2_phi);

      // subleading jet also in reference region
      if(fabs(m_j2_eta)<m_refRegion){

        m_3DAsymHists[pTavg_bin-1]->Fill(m_j2_eta,m_j2_phi,-asym,m_weight);

        if(pTavg_bin==2 && m_j1_eta>3.2) pTprobe.push_back(m_j1_pT);
        if(pTavg_bin==2 && m_j1_eta>3.2) pTprobe_w.push_back(m_weight);

        if(m_j1_eta>3.2){
          if ( m_isMC ) m_pTprobe_JX[m_JX]->Fill(m_j1_pT,m_weight);
          m_PtProbeHists[pTavg_bin-1]->Fill(m_j1_pT,m_weight);
          m_PtAvgHists[pTavg_bin-1]->Fill(m_pTavg,m_weight);
          m_PtRefHists[pTavg_bin-1]->Fill(m_j2_pT,m_weight);
        }

        pTavgEtaPhi_sumw->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight);
        pTavgEtaPhi_sumw2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_weight);
        pTavgEtaPhi_sumwpt->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_pTavg);
        pTavgEtaPhi_sumwpt2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_pTavg*m_pTavg);
        pTavgEtaPhi_sumwptprobe->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_pT);
        pTavgEtaPhi_sumwptprobe2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_pT*m_j1_pT);
        pTavgEtaPhi_sumwptref->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j2_pT);
        pTavgEtaPhi_sumwptref2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j2_pT*m_j2_pT);
        pTavgEtaPhi_sumweta->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_eta);
        pTavgEtaPhi_sumweta2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_eta*m_j1_eta);
        pTavgEtaPhi_sumwphi->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_phi);
        pTavgEtaPhi_sumwphi2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_phi*m_j1_phi);

      }
    }
    // subleading jet in reference region
    else if(fabs(m_j2_eta)<m_refRegion) {

      m_cutFlow_SubRef[m_JX-1]+=1;
      if(fabs(m_j1_eta)>3.2)
      m_cutFlow_SubRefLeadProbe[m_JX-1]+=1;

      float asym = (m_j1_pT-m_j2_pT)/m_pTavg;
      m_3DAsymHists[pTavg_bin-1]->Fill(m_j1_eta,m_j1_phi,asym,m_weight);

      if(m_j1_eta>3.2){
        if ( m_isMC ) m_pTprobe_JX[m_JX]->Fill(m_j1_pT,m_weight);
        m_PtProbeHists[pTavg_bin-1]->Fill(m_j1_pT,m_weight);
        m_PtAvgHists[pTavg_bin-1]->Fill(m_pTavg,m_weight);
        m_PtRefHists[pTavg_bin-1]->Fill(m_j2_pT,m_weight);
      }

      if(pTavg_bin==2 && m_j1_eta>3.2) pTprobe.push_back(m_j1_pT);
      if(pTavg_bin==2 && m_j1_eta>3.2) pTprobe_w.push_back(m_weight);

      pTavgEtaPhi_sumw->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight);
      pTavgEtaPhi_sumw2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_weight);
      pTavgEtaPhi_sumwpt->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_pTavg);
      pTavgEtaPhi_sumwpt2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_pTavg*m_pTavg);
      pTavgEtaPhi_sumwptprobe->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_pT);
      pTavgEtaPhi_sumwptprobe2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_pT*m_j1_pT);
      pTavgEtaPhi_sumwptref->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j2_pT);
      pTavgEtaPhi_sumwptref2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j2_pT*m_j2_pT);
      pTavgEtaPhi_sumweta->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_eta);
      pTavgEtaPhi_sumweta2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_eta*m_j1_eta);
      pTavgEtaPhi_sumwphi->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_phi);
      pTavgEtaPhi_sumwphi2->Fill(pTavg_for_bins,m_j1_eta,m_j1_phi,m_weight*m_j1_phi*m_j1_phi);

    }
  }

  int n=0;
  for (uint ipTavg=1; ipTavg<pTavg_bins.size(); ++ipTavg) {
    for (int ieta=1; ieta<=3; ++ieta) {
      for (uint iphi=1; iphi<phi_bins.size(); ++iphi) {
        double sumw = pTavgEtaPhi_sumw->GetBinContent(ipTavg,ieta,iphi), sumw2 = pTavgEtaPhi_sumw2->GetBinContent(ipTavg,ieta,iphi);
        double sumwpt = pTavgEtaPhi_sumwpt->GetBinContent(ipTavg,ieta,iphi), sumwpt2 = pTavgEtaPhi_sumwpt2->GetBinContent(ipTavg,ieta,iphi);
        double sumweta = pTavgEtaPhi_sumweta->GetBinContent(ipTavg,ieta,iphi), sumweta2 = pTavgEtaPhi_sumweta2->GetBinContent(ipTavg,ieta,iphi);
        double sumwphi = pTavgEtaPhi_sumwphi->GetBinContent(ipTavg,ieta,iphi), sumwphi2 = pTavgEtaPhi_sumwphi2->GetBinContent(ipTavg,ieta,iphi);
        double sumwptprobe = pTavgEtaPhi_sumwptprobe->GetBinContent(ipTavg,ieta,iphi), sumwptprobe2 = pTavgEtaPhi_sumwptprobe2->GetBinContent(ipTavg,ieta,iphi);
        double sumwptref = pTavgEtaPhi_sumwptref->GetBinContent(ipTavg,ieta,iphi), sumwptref2 = pTavgEtaPhi_sumwptref2->GetBinContent(ipTavg,ieta,iphi);

        double Neff=sumw*sumw/sumw2;

        double avgPt=sumwpt/sumw, avgEta=sumweta/sumw, avgPhi=sumwphi/sumw;
        double avgPtProbe=sumwptprobe/sumw; double errPtProbe = sqrt((sumwptprobe2/sumw-avgPtProbe*avgPtProbe)/Neff);
        double avgPtRef=sumwptref/sumw; double errPtRef = sqrt((sumwptref2/sumw-avgPtRef*avgPtRef)/Neff);
        double errPt=sqrt((sumwpt2/sumw-avgPt*avgPt)/Neff);
        double errEta=sqrt((sumweta2/sumw-avgEta*avgEta)/Neff);
        double errPhi=sqrt((sumwphi2/sumw-avgPhi*avgPhi)/Neff);

        // if(ipTavg==2){
        //   std::cout << "avg pTavg (hist) = " << avgPt << std::endl;
        //   Long64_t n = Long64_t(pTavg.size());
        //   std::cout << "avg pTavg (tmath mean) = " << TMath::Mean(n,&pTavg[0],&pTavg_w[0]) << std::endl;
        //
        //   std::cout << "avg pTprobe (hist) = " << avgPtProbe << std::endl;
        //   n = Long64_t(pTprobe.size());
        //   std::cout << "avg pTprobe (tmath mean) = " << TMath::Mean(n,&pTprobe[0],&pTprobe_w[0]) << std::endl;
        //
        //   std::cout << pTavg.size() << "\t" << pTprobe.size() << std::endl;
        // }

        m_pTavg_pTavgEtaPhi->SetBinContent(ipTavg,ieta,iphi,avgPt);
        m_pTavg_pTavgEtaPhi->SetBinError(ipTavg,ieta,iphi,errPt);
        m_pTprobe_pTavgEtaPhi->SetBinContent(ipTavg,ieta,iphi,avgPtProbe);
        m_pTprobe_pTavgEtaPhi->SetBinError(ipTavg,ieta,iphi,errPtProbe);
        m_pTref_pTavgEtaPhi->SetBinContent(ipTavg,ieta,iphi,avgPtRef);
        m_pTref_pTavgEtaPhi->SetBinError(ipTavg,ieta,iphi,errPtRef);
        m_eta_pTavgEtaPhi->SetBinContent(ipTavg,ieta,iphi,avgEta);
        m_eta_pTavgEtaPhi->SetBinError(ipTavg,ieta,iphi,errEta);
        m_phi_pTavgEtaPhi->SetBinContent(ipTavg,ieta,iphi,avgPhi);
        m_phi_pTavgEtaPhi->SetBinError(ipTavg,ieta,iphi,errPhi);

        m_pTavgEtaPhi->SetPoint(n,avgPt,avgEta,avgPhi);
        m_pTavgEtaPhi->SetPointError(n,errPt,errEta,errPhi);
        n++;
      }
    }
  }

  // for ( int i=0; i<9; ++i) {
  //
  //   std::cout << "========== MC cutflow ==========" << std::endl;
  //   std::cout << "JX: " << i+1 << std::endl;
  //   std::cout << "Total:                            " << m_cutFlow_Tot[i] << std::endl;
  //   std::cout << "pTavg bin:                        " << m_cutFlow_pTavg[i] << std::endl;
  //   std::cout << "Ref lead jet:                     " << m_cutFlow_LeadRef[i] << std::endl;
  //   std::cout << "Ref lead jet + probe sublead jet: " << m_cutFlow_LeadRefSubProbe[i] << std::endl;
  //   std::cout << "Ref sublead jet:                  " << m_cutFlow_SubRef[i] << std::endl;
  //   std::cout << "Ref sublead jet + probe lead jet: " << m_cutFlow_SubRefLeadProbe[i] << std::endl;
  // }


}

TF1* PhiInterCalibration::FitAsymHist(TH1* hA){
  static int fiti=0;
  float NeffMin=20.0;
  double A = hA->GetMean();
  double dA = hA->GetRMS()/sqrt(hA->GetEffectiveEntries()+1e-30);
  TF1 *gaus = new TF1(Form("f%d",fiti),"gaus",-0.8,0.8);
  if (hA->GetEffectiveEntries()>NeffMin) {
    hA->Fit(gaus,"RQ0");
    double mid=gaus->GetParameter(1), sig=gaus->GetParameter(2);
    double Nsig=2.0;
    gaus->SetRange(mid-sig*Nsig,mid+sig*Nsig);
    hA->Fit(gaus,"RQ0");
    mid=gaus->GetParameter(1), sig=gaus->GetParameter(2);
    gaus->SetRange(mid-sig*Nsig,mid+sig*Nsig);
    hA->Fit(gaus,"RQ0");
  }
  else {
    // reject point if less than number of desired entries
    gaus->SetParameter(1,-99); gaus->SetParError(1,-99);
    return gaus;
  }

  // reject point if fit failed
  if ( gaus==NULL ) {
    gaus->SetParameter(1,A); gaus->SetParError(1,dA);
    return gaus;
  }

  double Afit = gaus->GetParameter(1), dAfit = gaus->GetParError(1);
  double chi2 = gaus->GetChisquare(),  NDF = gaus->GetNDF();

  bool badFit = dAfit/dA > 3 || dAfit > 0.1 || ( dAfit > 0.05 && ( fabs(A-Afit) > 2.0*dAfit ) ) || chi2/NDF>15 || fabs(Afit)>2.5*fabs(A);

  if ( badFit ) {
    gaus->SetParameter(1,A); gaus->SetParError(1,dA);
    return gaus;
  }

  return gaus;
}

void PhiInterCalibration::InitTree(){
  m_tree->SetBranchAddress("weight",&m_weight);
  m_tree->SetBranchAddress("JX",&m_JX);
  m_tree->SetBranchAddress("pass_HLT_j15",&m_pass_HLT_j15);
  m_tree->SetBranchAddress("pass_HLT_j25",&m_pass_HLT_j25);
  m_tree->SetBranchAddress("pass_HLT_j60",&m_pass_HLT_j60);
  m_tree->SetBranchAddress("pass_HLT_j15_320eta490",&m_pass_HLT_j15_320eta490);
  m_tree->SetBranchAddress("pass_HLT_j25_320eta490",&m_pass_HLT_j25_320eta490);
  m_tree->SetBranchAddress("pass_HLT_j60_320eta490",&m_pass_HLT_j60_320eta490);
  m_tree->SetBranchAddress("trigBitsFwd",&m_trigBitsFwd);
  m_tree->SetBranchAddress("pTavg",&m_pTavg);
  m_tree->SetBranchAddress("j1_pT",&m_j1_pT);
  m_tree->SetBranchAddress("j1_eta",&m_j1_eta);
  m_tree->SetBranchAddress("j1_phi",&m_j1_phi);
  m_tree->SetBranchAddress("j2_pT",&m_j2_pT);
  m_tree->SetBranchAddress("j2_eta",&m_j2_eta);
  m_tree->SetBranchAddress("j2_phi",&m_j2_phi);
}

void PhiInterCalibration::WriteAvgPtEtaPhiGraph(TFile* f){
  f->cd();
  m_pTavgEtaPhi->Write();
  m_pTavg_pTavgEtaPhi->Write();
  m_pTprobe_pTavgEtaPhi->Write();
  m_pTref_pTavgEtaPhi->Write();
  m_eta_pTavgEtaPhi->Write();
  m_phi_pTavgEtaPhi->Write();

  m_pTavgHist->Write();

  for ( const auto& h : m_phiHists )
  h.second->Write();

  for(const auto& hist : m_pTprobe_JX)
  hist->Write();
  for(const auto& hist : m_pTavg_JX)
  hist->Write();

  for(const auto& hist : m_PtProbeHists)
  hist->Write();
  for(const auto& hist : m_PtRefHists)
  hist->Write();
  for(const auto& hist : m_PtAvgHists)
  hist->Write();
}

#endif
