#ifndef TRIGGEREFFICIENCYEVALUATOR_H
#define TRIGGEREFFICIENCYEVALUATOR_H

#include "utils/Utils.h"
#include "utils/AtlasStyle.h"

#include <utility>
#include "TEnv.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TFile.h"

extern float GeV;
///
/// A tool that creates a batch of 3D histogram root files given a certain se
class TriggerEfficiencyEvaluator{
public:
  TriggerEfficiencyEvaluator(std::string configFileName, std::string inputFileName, std::string outputFilePath);
  ~TriggerEfficiencyEvaluator();
  void Evaluate();
  void WriteOutput();
  void DrawOutput();
private:
  TEnv *m_settings;

  vector<TString> m_jetCollections;
  vector<TString> m_triggers;
  vector<vector<TString>> m_triggerCombinations;
  vector<TString> m_fullTriggerCombination;
  vector<double> m_etaPlotsMinPTAvg; //i.e for each combination what is the minimum pT we require for an event to enter in the Nevents plots
  map<TString, float> m_turnOnPT;

  TFile* m_inputFile;
  std::string m_inputFileName;
  std::string m_outputFilePath;
  std::string m_configFileName;
  TTree *m_dijetInsituTree;

  void DisplayWelcomeMessage();
  void ReadSettings();
  void InitialiseInputFile();
  void InitialiseHistograms();
  void CreatePlots();
  void WritePlots();
  void InitBranches();
  void ProcessTree();
  void SetTrigMap();
  void FillHistograms();
  //
  bool   PassCuts();
  bool   PassJVTCut();
  bool   PassPt3Cut();
  bool   PassDeltaPhiCut();
  bool   PassTriggerCombination(vector<TString> triggerComb, bool emulate = true);
  bool   PassEmulation(TString trigger);
  float  GetTurnOnPT(TGraphAsymmErrors* efficiencyCurve);
  //Drawing fuunctions
  float GetMinPt(vector<int> graphIndices);
  float GetMaxPt(vector<int> graphIndices);
  void DrawEfficiencyCurve(int index,int counter, bool drawEta);
  void DrawEfficiencyCurves(vector<int> graphIndices, bool drawEta);

  //Tree Stuff
  bool m_pass_HLT_j15;
  bool m_pass_HLT_j15_320eta490;
  bool m_pass_HLT_j25;
  bool m_pass_HLT_j25_320eta490;
  bool m_pass_HLT_j35;
  bool m_pass_HLT_j35_320eta490;
  bool m_pass_HLT_j45;
  bool m_pass_HLT_j45_320eta490;
  bool m_pass_HLT_j60;
  bool m_pass_HLT_j60_320eta490;
  bool m_pass_HLT_j110;
  bool m_pass_HLT_j110_320eta490;
  bool m_pass_HLT_j175;
  bool m_pass_HLT_j175_320eta490;
  bool m_pass_HLT_j260;
  bool m_pass_HLT_j260_320eta490;
  bool m_pass_HLT_j360;
  bool m_pass_HLT_j360_320eta490;
  bool m_doTriggerEmulation;
  bool m_atLeasteOneForwardJet;
  float m_asymMM;
  float m_asymSM;

  float m_left_deta;
  float m_right_deta;
  float m_ref_eta;
  float m_probe_eta;
  float m_weight;
  float m_pTavg;
  float m_j1_eta;
  float m_j2_eta;
  float m_j1_phi;
  float m_j2_phi;
  float m_j3_eta;
  float m_j3_phi;
  float m_j3_pT;
  float m_j1_pT;
  float m_j2_pT;
  float m_j1_E, m_j2_E;
  float m_j1_JVT, m_j2_JVT;


  float m_Dphijj;
  float m_jvtCut;
  float m_dphijjCut;


  int m_trigBitsCtrl;
  int m_trigBitsFwd;
  TString  ctrl_trig;
  TString  fwd_trig;
  std::map<TString, bool> trigMap;

  std::map<TString,TH1F*> m_hists1D;
  vector<double> m_pTMin;
  vector<double> m_pTMax;
  vector<vector<int>> m_graphsToDraw;

  std::map<TString,TGraphAsymmErrors*> m_efficiencyPlots;
  vector<int> m_referenceTriggerIndex;
  int m_JX;
  bool m_isMC=false;
  bool m_passTrigger;

};

#endif
