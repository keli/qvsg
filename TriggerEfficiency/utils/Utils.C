#include "Utils.h"

void error(Str msg) {
  printf("ERROR:\n\n  %s\n\n",msg.Data());
  abort();
}

TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh) {
  TH1F* h = new TH1F(hname,title,nbins,xlow,xhigh);
  return h;
}

TH1F* createHist1D(TString hname, TString title, std::vector<double> bins) {
  TH1F* h = new TH1F(hname,title,bins.size()-1,&bins[0]);
  return h;
}

TH2F* createHist2D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh) {
  TH2F* h = new TH2F(hname,title,xnbins,xlow,xhigh,ynbins,ylow,yhigh);
  return h;
}

TH2F* createHist2D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins) {
  TH2F* h = new TH2F(hname,title,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  return h;
}

TH3F* createHist3D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh, int znbins, float zlow, float zhigh) {
  TH3F* h = new TH3F(hname,title,xnbins,xlow,xhigh,ynbins,ylow,yhigh,znbins,zlow,zhigh);
  return h;
}

TH3F* createHist3D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins, std::vector<double> zbins) {
  TH3F* h = new TH3F(hname,title,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0],zbins.size()-1,&zbins[0]);
  return h;
}
std::vector<TString> vectorise(TString str, TString sep) {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}
std::vector<double> vectoriseD(TString str, TString sep) {
  std::vector<double> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}
std::vector<int> vectoriseI(TString str, TString sep) {
  std::vector<int> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}


 std::vector<double> makeUniformVec(int N, double min, double max) {
  std::vector<double> vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}

void PrintTime()
{
  static bool first=true;
  static time_t start;
  if(first) { cout << "Timer stated" << std::endl; first=false; ::time(&start); }
  time_t aclock; ::time( &aclock );
  char tbuf[25]; ::strncpy(tbuf, asctime( localtime( &aclock ) ),24);
  tbuf[24]=0;
  cout << " ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}

TEnv *OpenSettingsFile(Str fileName) {
  if (fileName=="") error("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status=settings->ReadFile(fileName.Data(),EEnvLevel(0));
  if (status!=0) error(Form("Cannot read file %s",fileName.Data()));
  return settings;
}

TH1F *GetTH1F(TFile *f, Str hn) {
  if (f->Get(hn)==NULL)
  error("Cannot access: "+hn+" in file "+f->GetName());
  return (TH1F*)f->Get(hn);
 }

 TH2F *GetTH2F(TFile *f, Str hn) {
   if (f->Get(hn)==NULL)
   error("Cannot access: "+hn+" in file "+f->GetName());
   return (TH2F*)f->Get(hn);
  }


TFile *InitOutputFile(Str ofName) {
  /*
  if (!gSystem->AccessPathName(ofName))
    error(Str("The intended output file already exist!\n")+
          "  If you want to replace it, please delete it manually:\n\n"+
          "    rm "+ofName);*/
  printf("\nHistograms will be written to %s\n",ofName.Data());
  return new TFile(ofName,"RECREATE");
}
