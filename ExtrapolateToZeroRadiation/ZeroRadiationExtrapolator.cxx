#include "ZeroRadiationExtrapolator.h"
using namespace std;

ZeroRadiationExtrapolator::ZeroRadiationExtrapolator(TString settingsFilePath)
  : m_configFileName(settingsFilePath)
{
  DisplayWelcomeMessage();

  InitialiseSettings();
  DisplaySettings();

  // Turn off all those "Info in <TCanvas::Print>: Current canvas added ..."
  gErrorIgnoreLevel=2000;
  InitialiseFiles();

}
ZeroRadiationExtrapolator::~ZeroRadiationExtrapolator(){
  delete m_settings;
  for(auto& inputFile : m_inputFiles){
    inputFile->Close();
    delete inputFile;
  }
}
void ZeroRadiationExtrapolator::DisplayWelcomeMessage(){
  cout << "=============================================" << endl
       << "========Zero radiation Extrapolator==========" << endl
       << "\t Settings file: " << m_configFileName << endl
       << "=============================================" << endl;
}

void ZeroRadiationExtrapolator::DisplaySettings(){

  cout << "\t Jet Colleciton: " << m_jetAlgorithm << endl
       << "\t Input Files: " << endl;
       for(const auto& inputFilePath : m_inputFilePaths)
          cout << "\t\t " << inputFilePath << endl;

  cout << "\t Third Jet Variable: " << m_thirdJetVariable << endl;
  cout << "\t " << m_thirdJetVariable << " ranges: " << endl << "\t\t";
       for(const auto& bin : m_thirdJetVariableBins)
         cout << bin << "\t";
  cout <<endl;

  cout << "\t pT Avg ranges: " << endl << "\t\t";
      for(const auto& bin : m_ptBins)
        cout << bin << "\t";
  cout <<endl;

  cout << "=============================================" << endl;
}
void ZeroRadiationExtrapolator::WriteOutput(){
  /*
  //OPEN OUTPUTFILE

  //SETUP DIRECTORIES TO APPROIATE DIRECTORY
  TString extrapolationPlotName = m_thirdJetVariable + "_extrapolationPlots.pdf";
  TH1F* responseHist =  GetTH1F(m_inputFiles[0], "/"+m_jetAlgorithm+"/"+m_jetAlgorithm+Form("_pt%.0fto%.0f_R_MM",m_ptBins[0] ,m_ptBins[1])  );
  for(unsigned int i = 0; i < m_ptBins.size()-1; ++i){
    /
    for(unsigned int j = 1;  j < responseHist->GetSize();++j){
      float etaMax = responseHist->GetBinLowEdge(j+1);
      float eta = (etaMax+etaMin)/2.0f;
      TString histName = Form("Eta%.2f_pt%.0fto%.0f",eta,m_ptBins[i] ,m_ptBins[i+1]);
      if(m_responsePointForGivenEta[histName].size() == 0) continue;
        m_responseVsThirdJetVariable[histName]->Write();
    }
  }

  //CLOSE FILE
  */
}
void ZeroRadiationExtrapolator::PrintOutput(){
  TCanvas* can = new TCanvas("Extrapolation to Zero Radiation Plots" ,"Extrapolation to Zero Radiation", 900,900 );
  TCanvas* can2 = new TCanvas("Extrapolation to Zero Radiation" ,"Extrapolation to Zero Radiation", 900,900 );
  can->Divide(3,3);
  TString extrapolationPlotName = m_thirdJetVariable + "_extrapolationPlots.pdf";
  can->Print(extrapolationPlotName+"[");
  int drawCount = 0;


  TH1F* responseHist =  GetTH1F(m_inputFiles[0], "/"+m_jetAlgorithm+"/"+m_jetAlgorithm+Form("_pt%.0fto%.0f_R_MM",m_ptBins[0] ,m_ptBins[1])  );

  TLatex tex;
  tex.SetTextFont(43);
  tex.SetTextSize(9);
  tex.SetNDC();
  for(unsigned int i = 0; i < m_ptBins.size()-1; ++i){
    for(unsigned int j = 1;  j < responseHist->GetSize();++j){
      float etaMin = responseHist->GetBinLowEdge(j);
      float etaMax = responseHist->GetBinLowEdge(j+1);
      float eta = (etaMax+etaMin)/2.0f;
      TString histName = Form("Eta%.2f_pt%.0fto%.0f",eta,m_ptBins[i] ,m_ptBins[i+1]);
      if(m_responsePointForGivenEta[histName].size() == 0) continue;
      can->cd(drawCount%9+1);
      drawCount++;


      //Fromat the Graph
      m_responseVsThirdJetVariable[histName]->SetTitle("");
      m_responseVsThirdJetVariable[histName]->Draw("ALP*");

      m_responseVsThirdJetVariable[histName]->GetYaxis()->SetRangeUser(0.85,1.15);
      m_responseVsThirdJetVariable[histName]->GetYaxis()->SetTitle("Relative Response, #frac{1}{c}");
      m_responseVsThirdJetVariable[histName]->GetYaxis()->SetTitleOffset(1.2);
      m_responseVsThirdJetVariable[histName]->GetXaxis()->SetRangeUser(0,m_thirdJetVariableBins[m_thirdJetVariableBins.size()-1]);
      m_responseVsThirdJetVariable[histName]->Draw("ALP*");


      if(m_thirdJetVariable.Contains( "pt3OverpTAvg"))
            m_responseVsThirdJetVariable[histName]->GetXaxis()->SetTitle("#frac{p_{T}^{3}}{p_{T}^{Avg}}");
      else if(m_thirdJetVariable.Contains( "pt3") )
        m_responseVsThirdJetVariable[histName]->GetXaxis()->SetTitle("p_{T}^{3} /GeV");
      else if(m_thirdJetVariable.Contains( "Dphijj") )
        m_responseVsThirdJetVariable[histName]->GetXaxis()->SetTitle("#pi - #Delta #phi_{jj}");
      else if (m_thirdJetVariable.Contains( "PhiStar")){
        m_responseVsThirdJetVariable[histName]->GetXaxis()->SetTitle("#phi^{*}");
      }

      float xPos = 0.14, yPos = 0.88, dY = 0.05;

      tex.DrawLatex(xPos,yPos-=dY, Form("Intercept: %.4f",m_responseVsThirdJetVariableFit[histName]->GetParameter(0)));
      tex.DrawLatex(xPos,yPos-=dY, Form("Gradient: %.4f",m_responseVsThirdJetVariableFit[histName]->GetParameter(1)));
      tex.DrawLatex(xPos,yPos-=dY, Form("#color[2]{#chi^{2}/N_{DoF} = %.1f/%.1f}",m_responseVsThirdJetVariableFit[histName]->GetChisquare(), m_responseVsThirdJetVariableFit[histName]->GetNDF()));
      xPos = 0.7; yPos = 0.88;
      tex.DrawLatex(xPos,yPos-=dY, Form("%.1f < #eta < %.1f",etaMin,etaMax));
      tex.DrawLatex(xPos,yPos-=dY, Form("%.0f < p_{T}^{Avg} < %.0f",m_ptBins[i] ,m_ptBins[i+1]));
      can->Update();

      can->Print("plots/" + histName+"_"+m_thirdJetVariable+".pdf");
      can->Print("Cfiles/" + histName+"_"+m_thirdJetVariable+".C");
        if(j==responseHist->GetSize()-1 || drawCount%9 == 8 ){
          can->Print(extrapolationPlotName);
        }

    }
    //Print the curve
    can2->cd();
    can2->Clear();

    TString histName = Form("pt%.0fto%.0f",m_ptBins[i] ,m_ptBins[i+1]);
    m_responseVsEtaAtZeroRadiation[histName]->Draw();
    can2->Print("plots/" + histName+"_AtZeroRad.pdf");
    can2->Print("Cfiles/" + histName+"_AtZeroRad.C");
  }

  can->Print(extrapolationPlotName+"]");
  delete responseHist;
  delete can;
  delete can2;
}

void ZeroRadiationExtrapolator::InitialiseSettings(){
    m_settings = OpenSettingsFile(m_configFileName);

    //read in the basic stuff we will need to perform the extrapolation
    m_jetAlgorithm            = m_settings->GetValue("JetCollection","AntiKt4EMTopo");
    m_inputFilePaths          = vectorise(m_settings->GetValue("InputFilePaths",""));
    m_thirdJetVariable        = m_settings->GetValue("thirdJetVariable","pt3");
    m_thirdJetVariableBins    = vectoriseD(m_settings->GetValue(TString(m_thirdJetVariable) + ".Bins",""));
    m_ptBins                  = vectoriseD(m_settings->GetValue("PtBins"," "));
    m_thirdJetVariablePoints  = vectoriseD(m_settings->GetValue(TString(m_thirdJetVariable) +".points",""));
    if(m_thirdJetVariablePoints.size() != m_thirdJetVariableBins.size()-1)
      error(Form("Number of points (%d) and bins (%d) for your specificed third jet variable do not match. Please check configuration file",m_thirdJetVariablePoints.size() ,m_thirdJetVariableBins.size()-1));
}
void ZeroRadiationExtrapolator::InitialiseFiles(){
  for(const auto& inputFilePath : m_inputFilePaths){
      m_inputFiles.push_back(TFile::Open(inputFilePath));
      if(!m_inputFiles.back())
        error("Failed to open file: " + inputFilePath);
  }

}
void ZeroRadiationExtrapolator::GenerateResponseAtZeroRadiation(float ptMin, float ptMax){
//
  TH1F* responseHist =  GetTH1F(m_inputFiles[0], "/"+m_jetAlgorithm+"/"+m_jetAlgorithm+Form("_pt%.0fto%.0f_R_MM",ptMin ,ptMax)  );
  TString responseHitsName = Form("pt%.0fto%.0f",ptMin,ptMax);
  //cycle over the eta bins and add the points to the graphs.
  for(unsigned int j = 1;  j < responseHist->GetSize();++j){
    float etaMin = responseHist->GetBinLowEdge(j);
    float etaMax = responseHist->GetBinLowEdge(j+1);
    float eta = (etaMax+etaMin)/2.0f;
    TString hname = Form("Eta%.2f_pt%.0fto%.0f",eta,ptMin,ptMax);

    m_responseVsEtaAtZeroRadiation[responseHitsName]->SetBinContent(j,(float)m_responseVsThirdJetVariableFit[hname]->GetParameter(0));
  }

  delete responseHist;
}

void ZeroRadiationExtrapolator::CreateResponseVsThirdJetVarCurves(map<TString, TH1F*> responseVsEtaCurves, float ptMin, float ptMax){
  //Now we need to go through the third Jet variable names


  TString thirdJetVariableName = Form("%.3fTo%.3f",m_thirdJetVariableBins[0],m_thirdJetVariableBins[1]);
  //cycle over the eta bins and add the points to the graphs.
  for(unsigned int j = 1;  j < responseVsEtaCurves[thirdJetVariableName]->GetSize();++j){
    float etaMin = responseVsEtaCurves[thirdJetVariableName]->GetBinLowEdge(j);
    float etaMax = responseVsEtaCurves[thirdJetVariableName]->GetBinLowEdge(j+1);
    float eta = (etaMax+etaMin)/2.0f;
    TString histName = Form("Eta%.2f_pt%.0fto%.0f",eta,ptMin,ptMax);

    if(m_responsePointForGivenEta[histName].size() == 0) continue;
    if(m_responsePointForGivenEta[histName].size() != m_thirdJetVariablePoints.size()){
        cout << "For " << histName << "  Mis-match of relative response points across all third jet variable points." << endl;
        cout << "Sizes: "<< m_responsePointForGivenEta[histName].size() << " and " << m_thirdJetVariablePoints.size() << endl;
        error("Did you run all input files with the same eta binning?");
    }
    m_responseVsThirdJetVariable[histName] = new TGraph(m_thirdJetVariablePoints.size(),
                                                &m_thirdJetVariablePoints[0],
                                                &m_responsePointForGivenEta[histName][0]);

    m_responseVsThirdJetVariable[histName]->GetXaxis()->SetRangeUser(0,m_thirdJetVariableBins[m_thirdJetVariableBins.size()-1]);
    //Do the fit
    m_responseVsThirdJetVariable[histName]->Fit("pol1","q");
    m_responseVsThirdJetVariableFit[histName]  = m_responseVsThirdJetVariable[histName]->GetFunction("pol1");
    if(!m_responseVsThirdJetVariableFit[histName])
      cout << "FAILED TO FIT " << histName  << endl;
  }

}
void ZeroRadiationExtrapolator::FitResponseVsThirdJetVarCurves(float ptMin, float ptMax){

}
map<TString, TH1F*>  ZeroRadiationExtrapolator::GetResponseCurves(float ptMin, float ptMax){
  map<TString, TH1F*>  responseVsEtaCurves;
  TString responseHitsName = Form("pt%.0fto%.0f",ptMin,ptMax);
  //loop over all input Files
  for(unsigned int i = 0; i < m_thirdJetVariableBins.size()-1;++i){
    TFile* inputFile = m_inputFiles[i];
    TString thirdJetVariableName = Form("%.3fTo%.3f",m_thirdJetVariableBins[i],m_thirdJetVariableBins[i+1]);

    //get the response curves
    responseVsEtaCurves[thirdJetVariableName] =  GetTH1F(inputFile, "/"+m_jetAlgorithm+"/"+m_jetAlgorithm+Form("_pt%.0fto%.0f_R_MM",ptMin ,ptMax)  );
    m_responseVsEtaAtZeroRadiation[responseHitsName] = (TH1F*)responseVsEtaCurves[thirdJetVariableName]->Clone();

    //cycle over the eta bins and add the points to the graphs.
    for(unsigned int j = 1;  j < responseVsEtaCurves[thirdJetVariableName]->GetSize();++j){
      float etaMin = responseVsEtaCurves[thirdJetVariableName]->GetBinLowEdge(j);
      float etaMax = responseVsEtaCurves[thirdJetVariableName]->GetBinLowEdge(j+1);
      float eta = (etaMax+etaMin)/2.0f;
      float response   = responseVsEtaCurves[thirdJetVariableName]->GetBinContent(j);
      TString histName = Form("Eta%.2f_pt%.0fto%.0f",eta,ptMin,ptMax);
      if(response != -99){
        m_responsePointForGivenEta[histName].push_back(response);

      //  cout << "For bin " << j << " found R = " << response << " eta = " << eta << " etaMin  = " << etaMin << " etaMax = " << etaMax <<  endl;
      }

    }
  }

  //create the the TGraphs of R(eta)
  return responseVsEtaCurves;
}
void ZeroRadiationExtrapolator::PerformExtrapolation(){
  //For all pT  ranges
  for(unsigned int i = 0; i < m_ptBins.size()-1; ++i){
    cout << "Performing extrapolation on pT bin: " << m_ptBins[i] << "-" << m_ptBins[i+1] << endl;

    //Get a list of all R(eta) curves mapped to the correct third jet variable range
    map<TString, TH1F*> responseVsEtaCurves = GetResponseCurves(m_ptBins[i],m_ptBins[i+1]);

    //Create the R(pt3, eta_i, pTAvg_j) graphs for all eta ranges
    CreateResponseVsThirdJetVarCurves( responseVsEtaCurves,m_ptBins[i],m_ptBins[i+1]);

  }//for m_ptBins
}

/*
TH2D *ZeroRadiationExtrapolator::GetChi2Map( vector<double> PTBINS,vector<double> ETABINS, double freezePt, double freezeEta){

  TH2D *Chi2 = new TH2D("Chi2","",PTBINS.size()-1,&PTBINS[0],ETABINS.size()-1,&ETABINS[0]);

  double sumChi2=0, sumChi2freeze=0;
  int nbinsTot=0, nbinsFreezeCalib=0;

  for( int i=0; i<calibPoints->GetN(); i++ ){
    double pT, eta, R, dR;
    pT = calibPoints->GetX()[i];
    eta = calibPoints->GetY()[i];
    R = calibPoints->GetZ()[i];
    dR = calibPoints->GetEZ()[i];
    int histPtBin, histEtaBin;
    histPtBin     = Chi2->GetXaxis()->FindBin( pT );
    histEtaBin    = Chi2->GetYaxis()->FindBin( eta );
    double chi2 = pow( (R - calibSmooth->Interpolate(pT,eta))/dR , 2.0 );
    sumChi2 += chi2;
    nbinsTot++;
    if(fabs(eta)<freezeEta && pT<freezePt) { sumChi2freeze += chi2; nbinsFreezeCalib++;}
    Chi2->SetBinContent( histPtBin, histEtaBin, chi2 );
  }

  Chi2->SetXTitle("p_{T}^{probe}");
  Chi2->GetYaxis()->SetTitleOffset(0.6);
  Chi2->GetZaxis()->SetRangeUser(0,10);
  Chi2->GetXaxis()->SetMoreLogLabels();
  Chi2->SetYTitle("#eta");
  Chi2->Draw("COLZ");
  _tex->SetTextAlign(12);
  _tex->SetTextSize(0.03);
  _tex->DrawLatex(0.1,0.96, Form("#chi^{2} = ( point - calib )^{2}/#sigma_{point}^{2}; Nbins=%d : #sum#chi^{2}=%.1f : Nbins=%d; #sum#chi^{2}(|#eta|<%.1f, p_{T}<%0.1f)=%.1f",
				 nbinsTot,sumChi2,nbinsFreezeCalib,freezeEta,freezePt,sumChi2freeze) );
  _tex->SetTextAlign(22);

  _tex->SetNDC(0);
  _tex->SetTextColor(kBlack);
  for (uint ri=0;ri<Chi2->GetNbinsX();++ri) {
    for(int ieta=0; ieta<Chi2->GetNbinsY(); ieta++){
      double bc = Chi2->GetBinContent(ri+1,ieta+1);
      _tex->SetTextColor(kBlack);
      if( Chi2->GetBinContent(ri+1,ieta+1) > 0 )
	_tex->DrawLatex( Chi2->GetXaxis()->GetBinCenter(ri+1), Chi2->GetYaxis()->GetBinCenter(ieta+1), Form("%.2f",bc ) );

    }
  }

  TLine line;
  line.SetLineColor(kGray);
  for (Int_t bin=1; bin<=Chi2->GetNbinsX(); bin++)
    line.DrawLine( Chi2->GetXaxis()->GetBinLowEdge(bin), Chi2->GetYaxis()->GetBinLowEdge(1),
		    Chi2->GetXaxis()->GetBinLowEdge(bin), Chi2->GetYaxis()->GetBinLowEdge( Chi2->GetNbinsY()+1 ) );

  for (Int_t bin=1; bin<=Chi2->GetNbinsY(); bin++)
    line.DrawLine( Chi2->GetXaxis()->GetBinLowEdge(1), Chi2->GetYaxis()->GetBinLowEdge(bin),
		    Chi2->GetXaxis()->GetBinLowEdge(Chi2->GetNbinsX()+1), Chi2->GetYaxis()->GetBinLowEdge(bin) );

  return Chi2;

}
*/
