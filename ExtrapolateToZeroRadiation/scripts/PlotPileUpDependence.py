import os
from subprocess import call,Popen
import datetime

HistogramGenerator = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/histogramGenerator.exe"
HistogramGeneratorSettings = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/utils/settings.config"

inputNTuple="/pc2012-data2/jrawling/data16_13TeV/data16_13TeV_periodsABCDEFGI.root"
HistogramOutPath = "/pc2012-data2/jrawling/PileUpDependence/NPV/"
HistogramOutPath = "/pc2012-data2/jrawling/PileUpDependence/mu/"

###Don't forget to check the settings files for the binnings of this variable in GenerateHistograms/utils/settings.config!!

##NPV##
thirdJetVariableBins="5 8 10 12 14 16 18 20 25 30"
##Mu##
thirdJetVariableBins="7 12 15 18 20 22 24 26 29 32 40"

thirdJetVariableBinList = [float(k) for k in thirdJetVariableBins.split(' ')]
nBins=len(thirdJetVariableBinList) - 1

Minimizer = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/matrix_minimization.exe"
SettingsPath = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/MinimiseResponseMatrix/utils/Dijet_xAOD_forExtrapolation.config"

triggerLumiSettings = "25nsFinal"
jetAlgo = "AntiKt4LCTopo"
etaBinning = "Eta2016v12"
eventGenerator=""
##Set to 1 and it'll only run over the first 10000 events
testRun=0
##pc2012
MinimizationOuptutPath =HistogramOutPath + "MMOutput/"
inputPath = HistogramOutPath

current_time = datetime.datetime.now().time()
print current_time.isoformat()

###### Set the number to be submitted at a time and how many to submit per batch ####
'''
ps = {}
for j in range(0,nBins):
    onTheFlySettings="thirdJetBin="+`j`+",doMuBinning=1,doNPVBinning=0,doThirdJetExtrapolation=0,doBootStrap=0,doNominal=0,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0,testRun="+`testRun`
    logFile=HistogramOutPath+"generation_Mu_Bin"+"{0:.3f}".format(thirdJetVariableBinList[j]) + "To"+ "{0:.3f}".format(thirdJetVariableBinList[j+1])+".log"
    print "Logfile: " + logFile
    print "nice "+" -n "+" 10 "+HistogramGenerator+ " " +HistogramGeneratorSettings+ " " +inputNTuple+ " " +HistogramOutPath+ " " +onTheFlySettings
    args = ["nice","-n","10",HistogramGenerator,HistogramGeneratorSettings,inputNTuple,HistogramOutPath,onTheFlySettings]

    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p
while ps:
    pid, status = os.wait()
    if pid in ps:
        del ps[pid]
        print "Waiting for %d processes..." % len(ps)
print "done."
'''
current_time = datetime.datetime.now().time()
print current_time.isoformat()

ps = {}
for j in range(0,nBins):
    inputFile = inputPath + "Data_3DHistograms_mu_Bin"+`j`+".root"
    fullPrefix = "mu_"+ "{0:.3f}".format(thirdJetVariableBinList[j]) + "To"+ "{0:.3f}".format(thirdJetVariableBinList[j+1])
    logFile=MinimizationOuptutPath +"minimization"+ "{0:.3f}".format(thirdJetVariableBinList[j]) + "To"+ "{0:.3f}".format(thirdJetVariableBinList[j+1])+".log"
    print "Logfile: " + logFile

    outputFile = MinimizationOuptutPath + "data16_13TeV_" + fullPrefix + "_" + etaBinning+ "_" + triggerLumiSettings + "_"+jetAlgo + ".root"

    print "nice "+" -n "+" 10 "+Minimizer + " "+  SettingsPath + " " + jetAlgo  + " "+ triggerLumiSettings  + " "+ etaBinning  + " "+ inputFile  + " "+ outputFile  + " "+fullPrefix;
    if eventGenerator != "":
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix, eventGenerator]
    else:
        args = ["nice","-n","10",Minimizer, SettingsPath,jetAlgo, triggerLumiSettings, etaBinning, inputFile,outputFile, fullPrefix]
    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p

while ps:
    pid, status = os.wait()
    if pid in ps:
        del ps[pid]
        print "Waiting for %d processes..." % len(ps)


current_time = datetime.datetime.now().time()
print current_time.isoformat()

print "done"
