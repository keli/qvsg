#include "ZeroRadiationExtrapolator.h"

int main(int argc, char **argv) {
  //Read in setting names etc. from the given parameters of this programme.
	TString configFileName(argv[1]);

//  PrintTime();
  ZeroRadiationExtrapolator zeroRadiationExtrapolator(configFileName);

  zeroRadiationExtrapolator.PerformExtrapolation();
  zeroRadiationExtrapolator.WriteOutput();
  zeroRadiationExtrapolator.PrintOutput();
//  PrintTime();

  return 0;
} // main
