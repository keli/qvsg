# Specify the image from which you are working
FROM atlas/analysisbase:21.2.76

# Put the current repo (the one in which this Dockerfile resides) in the directory specified here
# Note that this directory is created on the fly and does not need to reside in the repo already
ADD . /qgtag/dijet

# Go into the directory specified here (again, it will create the directory if it doesn't already exist)
WORKDIR /qgtag/build

# Execute this comment in the directory specified just above
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /qgtag && \
    cmake ../dijet/ && \
    make -j4
