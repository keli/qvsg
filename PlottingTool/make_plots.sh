#!/bin/sh

#code=DrawDijetBalance.C
#code=DijetResponsePlotting.C
code=DrawDijetResponse.C
exec=draw_dijet_response.exe
config=${1:-"plots.config"}
plotType=${2:-"Closure"} #MCvsData SMvsMM

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore"
rm -f $exec
gcc -std=c++11 $flagsNlibs -o $exec $code && {

    echo ; echo "Compilation successful" ; echo
    ./$exec $config $plotType

}
#rm -f $exec
