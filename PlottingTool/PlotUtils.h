#include <cmath>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>
#include <utility>

#include <TROOT.h>
#include <TLatex.h>
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TH3F.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TString.h>
#include "TSystem.h"
#include "TLine.h"
#include "TMarker.h"

using namespace std;

void fatal(Str msg) {
  printf("FATAL: %s\n",msg.Data());
  abort();
}

TObject* Get(TFile *f, Str jetAlgo, Str objName) {
  f->cd(jetAlgo);
  TObject *obj = gDirectory->Get(objName);
  if (obj==NULL) error("Can't access "+objName+" in "+f->GetName());
  return obj;
}

TH1D *GetHisto(TFile *f, Str jetAlgo, Str hname) { return (TH1D*)Get( f, jetAlgo, hname ); }
