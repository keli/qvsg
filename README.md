# Dijet calibration for qg tagging

service account:

login:qgtag
password:QuarkGluon123




## Quick Start

$ setupATLAS

Or if elsewhere:

$ export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
$ alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'  
$ setupATLAS


## EtaIntercalxAODAnalysis 
EtaIntercalxAODAnalysis - Generates an NTuple, storing relevant kinematics, asymmetry variables and variables for systematics
To setup environment:

$ source setup.sh

This will make/build the package.

### Configuration file

EtaInterCalxAODAnalysis/share/settings.config

Important things to specify in the config file are:

* if you are running on R21 samples (isR21:1) or R20 samples (isR21:0)
* GRL xml file: can be downloaded from [here](http://atlasdqm.web.cern.ch/atlasdqm/grlgen/All_Good/), and should be put in EtaInterCalxAODAnalysis/GRLs directory
* JetCalibTool.*.config: the latest recommended config file for jet calibration (note that it's different for R21 or R20 samples)
* JetCollections: AntiKt4EMTopo AntiKt4EMPFlow AntiKt4LCTopo
* Cuts/calibration/selection settings

### Running locally

For now the executable expects as an argument a comma-separated list of input files, for example:

$EtaInterCalAnalysis path/to/your/local/input (usually DAOD root file)

The output is a single ROOT file called EtaInterCal_3DHistos.root.

### Running on the grid

1. Setup voms:  
$  voms-proxy-init -voms atlas

2. Then panda:  
$  lsetup panda

3. Modify the submission script's user setting USERNAME to be your CERN USERNAME, file:  
$  EtaInterCalxAODAnalysis/scripts/mygridSubmit_mc16a_Sherpa_dijet.sh


4. Files can then be retrived through rucio in the following way:  
$  voms-proxy-init -voms atlas  
$  lsetup rucio  
$  cd <FolderToDownloadDataTo>  
$  rucio download user.<CERNUSERNAME>.*EtaInterCal_3DHistos*.root  


