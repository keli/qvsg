#include <TCanvas.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TGraph2DErrors.h>

typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

double TEXTSIZE=0.04;
double FIGURE2_RATIO = 0.36;
double SUBFIGURE_MARGIN = 0.06; // white space between figures
double _maxDev=0.25;

int cols[] = {kRed, kBlue, kGreen+1, kOrange+7, kViolet+2, kGray+1};

TLatex *_tex;
TCanvas *_can;

int _jetR;
Str _calibration, _methodDesc, _name;


// min/max of the histos
double FIGURE1MIN=0.775;
double FIGURE1MAX=1.2;
double FIGURE2MAX=1.2;
double FIGURE2MIN=0.8;

// Helper functions
TFile *Open(Str fn) { TFile *f=TFile::Open(fn); if (f==NULL) error("Can't open "+fn); return f; } 
void FormatHisto(TH1 *h, int mstyle, int col, double msize);
void FormatHisto(TH1 *h, int style);
void FormatGraph(Graph *h, int mstyle, int col, double msize);
void FormatGraph(Graph *h, int style);

void PrintEtaBinInfo(double ptmin, double ptmax);
void DrawText(TString txt, int col=kBlack, double y=0.88, double x=0.145);
void DrawTextR(TString txt, int col=kBlack, double y=0.88, double x=0.92);
void DrawLabel(TString txt, int style, double x, double y);
void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize);

VecD MakeLogVector(int N, double min, double max) {
  VecD vec; double dx=(log(max)-log(min))/N;
  for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
  return vec;
}


void FormatHisto(TH1 *h, int style)
{
  if      (style==1) FormatHisto(h,20,kBlack,0.8); // data
  else if (style==2) FormatHisto(h,22,kRed,0.6);
  else if (style==3) FormatHisto(h,23,kBlue,0.8);
  else if (style==4) FormatHisto(h,24,kOrange+7,1.2);
  else if (style==5) FormatHisto(h,25,kGreen+2,1.6);
  else if (style==6) FormatHisto(h,26,kGray+2,0.8);
  else if (style==7) FormatHisto(h,27,kBlue-2+1,0.8);
  else if (style==8) FormatHisto(h,28,kCyan+1,0.8);
  else if (style==9) FormatHisto(h,29,kMagenta+2,0.8);
  else if (style==10) FormatHisto(h,30,kPink+1,1.2);

  h->SetXTitle("");
  h->SetYTitle("Relative jet response, 1/c");
  h->GetXaxis()->SetTitleOffset(0.95);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetYaxis()->SetNdivisions(505);
}

void FormatHisto(TH1 *h, int mstyle, int col, double msize) {
  //h->SetMinimum(FIGURE1MIN); h->SetMaximum(FIGURE1MAX);
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col);
}

void FormatGraph(Graph *h, int style)
{
  if      (style==1) FormatGraph(h,20,kBlack,0.8); // data
  else if (style==2) FormatGraph(h,21,kRed,0.6);
  else if (style==3) FormatGraph(h,23,kBlue,0.8);
  else if (style==4) FormatGraph(h,26,kOrange+7,1.2);
  else if (style==5) FormatGraph(h,33,kGreen+2,1.6);
  else if (style==6) FormatGraph(h,28,kGray+2,1.2);
  else if (style==10) FormatGraph(h,24,kBlack,1.2);
  else if (style==11) FormatGraph(h,24,kBlack,1.2);
  else if (style==12) FormatGraph(h,25,kRed,0.8);
  else if (style==13) FormatGraph(h,27,kBlue,0.8);

}

void FormatGraph(Graph *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col); h->SetLineWidth(2);
}

void PrintIntegratedLumi(){
  DrawText(Form("#int #it{L dt} = %.0f fb^{-1}", 20.0 ),kBlack,0.94,0.14);
}

void PrintInfo() {
  DrawText(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration+", "+_methodDesc,kBlack,0.97,0.12);
  DrawTextR(_name,kBlack,0.97);
}

void PrintEtaBinInfo(double ptmin, double ptmax) {
  DrawText(Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void PrintPtBinInfo(double etamin, double etamax) {
  DrawText(Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

// Draw text to the left
void DrawText(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(11); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt); }

// Draw text in the upper right corner
void DrawTextR(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(31); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt); 
  _tex->SetTextAlign(11); _tex->SetTextColor(kBlack); }


void DrawGuideLineSyst(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2); 
  line->SetLineWidth(1); line->SetLineColor(kRed);
  line->DrawLine(min,0.05,max,0.05);
  line->SetLineColor(kBlue);
  line->DrawLine(min,0.02,max,0.02);
}

void DrawGuideLines(double min, double max) {
  static TLine *line = new TLine();
  // Draw "guide-lines"
  line->SetLineStyle(2); 
  line->SetLineWidth(1);
  line->SetLineColor(kGray+1);
  line->DrawLine(-1,0.95,-1,1.05);
  line->DrawLine(1,0.95,1,1.05);

  line->SetLineWidth(1); line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
  line->SetLineWidth(1);
  line->SetLineColor(kRed);
  line->DrawLine(min,1.05,max,1.05);
  line->DrawLine(min,0.95,max,0.95);

  line->SetLineColor(kBlue);
  line->DrawLine(min,1.02,max,1.02);
  line->DrawLine(min,0.98,max,0.98);
}

void DrawCenterLine(double min, double max) {
  static TLine *line = new TLine();
  line->SetLineStyle(2); line->SetLineWidth(1);
  line->SetLineColor(kBlack);
  line->DrawLine(min,1.0,max,1.0);
}

void DrawGuideLinesVsEta(double etaMin=-4.5, double etaMax=4.5) {
  DrawGuideLines(etaMin,etaMax);
}

void DrawGuideLinesVsPt() {
  DrawGuideLines(30,1500);
}

void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize) {
  TMarker *m = new TMarker(x,y,mstyle);
  m->SetNDC(); m->SetMarkerSize(msize); m->SetMarkerColor(col);
  TLine *l = new TLine(); l->SetLineWidth(2); l->SetLineColor(col);
  l->DrawLineNDC(x-0.02,y,x-0.005,y); l->DrawLineNDC(x+0.005,y,x+0.02,y); m->Draw();
  _tex->SetTextSize(0.04); _tex->SetTextAlign(12); _tex->SetTextColor(col);
  _tex->DrawLatex(x+0.04,y,txt);
  _tex->SetTextSize(TEXTSIZE);
}

void DrawLabel(TString txt, int style, double x, double y) {
  if (style==1) DrawLabel(txt,x,y,20,kBlack,1.2);
  else if (style==2) DrawLabel(txt,x,y,21,kRed,1.0);
  else if (style==3) DrawLabel(txt,x,y,23,kBlue,1.2);
  else if (style==4) DrawLabel(txt,x,y,26,kOrange+7,1.2);
  else if (style==5) DrawLabel(txt,x,y,33,kGreen+2,1.6);
  else if (style==6) DrawLabel(txt,x,y,28,kGray+2,1.2);
  else if (style==10) DrawLabel(txt,x,y,24,kBlack,1.2);
  else if (style==11) DrawLabel(txt,x,y,24,kBlack,1.2);
  else if (style==12) DrawLabel(txt,x,y,25,kRed,0.8);
  else if (style==13) DrawLabel(txt,x,y,27,kBlue,0.8);
}

void PrintDataMCPyHw(bool isTruth=false) {
  if (!isTruth) DrawLabel("Data 2012",1,0.4,0.47); DrawLabel("Pythia",2,0.4,0.42); 
  DrawLabel("Herwig++",3,0.6,0.47); DrawLabel("MC baseline",6,0.6,0.42);
  if (isTruth) DrawLabel("MC average",1,0.4,0.47);
}

void PrintDataMCPowPySh(bool isTruth=false) {
  if (!isTruth) DrawLabel("Data 2012",1,0.37,0.47); DrawLabel("PowhegPythia",2,0.37,0.42); 
  DrawLabel("Sherpa",3,0.6,0.47); DrawLabel("MC baseline",6,0.6,0.42);
  if (isTruth) DrawLabel("MC average",1,0.4,0.47);
}



void DrawSubPad() {
  _can->SetBottomMargin(FIGURE2_RATIO);
  // create new pad, fullsize to have equal font-sizes in both plots
  TPad *p = new TPad( "p_test", "", 0, 0, 1, 1.0 - SUBFIGURE_MARGIN, 0, 0, 0);
  p->SetTopMargin(1.0 - FIGURE2_RATIO); p->SetFillStyle(0);
  p->SetMargin(0.12,0.04,0.1,1.0 - FIGURE2_RATIO);
  //p->SetMargin(0.12,0.08,0.15,1.0 - FIGURE2_RATIO);
  p->Draw(); p->cd(); //p->SetGridy(kTRUE);
}

void DrawPtEtaPoints(Graph2D *graph, VecD ptBins, VecD etaBins, int jetR, double freezeCalib = 2.8) {
  TLine *line = new TLine(); line->SetLineColor(kGray);
  Graph *bin_xy = new Graph(), *avg_xy = new Graph();
  //static TH2F *h = new TH2F("a","",1,17.5,2000,1,-4.5,4.5);
  static TH2F *h = new TH2F("a","",1,17.5,2000,1,etaBins[0],etaBins[etaBins.size()-1]);
  h->GetXaxis()->SetMoreLogLabels(); h->GetYaxis()->SetTitleOffset(0.8);
  h->SetXTitle("p_{T}^{avg}"); h->SetYTitle("#eta_{det}");
  h->SetXTitle("p_{T}^{probe} [GeV]"); 
  h->Draw();

  for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
    double etal = etaBins[ieta];
    double eta = 0.5*(etal+etaBins[ieta+1]);
    line->DrawLine(25,etal,1500,etal);
    if (ieta==etaBins.size()-2) line->DrawLine(30,etaBins[ieta+1],1500,etaBins[ieta+1]);
    for (int ipt=0;ipt<ptBins.size()-1;++ipt) {
      double ptl = ptBins[ipt];
      double pt  = 0.5*(ptl+ptBins[ipt+1]);
      line->DrawLine(ptl,etaBins[0],ptl,etaBins[etaBins.size()-1]);
      if (ipt==ptBins.size()-2) line->DrawLine(ptBins[ipt+1],etaBins[0],ptBins[ipt+1],4.5);
      int n=bin_xy->GetN();
      bin_xy->SetPoint(n,pt,eta);
    }
  }
  for (int i=0;i<graph->GetN();++i) {
    avg_xy->SetPoint(i,graph->GetX()[i],graph->GetY()[i]);
    avg_xy->SetPointError(i,graph->GetEX()[i],graph->GetEY()[i]);
  }
  //bin_xy->SetMarkerColor(kOrange); bin_xy->Draw("P");
  avg_xy->SetMarkerSize(0.4); avg_xy->Draw("P");

  float minEta = -freezeCalib;
  if(etaBins[0]>=0) minEta = 0;


  line->SetLineColor(kRed+1); line->SetLineWidth(2);
  double minPt=jetR==4?25:40;
  line->DrawLine(minPt,minEta,minPt,freezeCalib);
  //double pt1=jetR==4?100:105;
  line->DrawLine(minPt,minEta,1500,minEta);
  line->DrawLine(minPt,freezeCalib,1500,freezeCalib);
  h->Draw("sameaxis");
  //PrintInfo();
}
