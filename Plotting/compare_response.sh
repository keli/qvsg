#!/bin/sh

code=CompareResponse.C
exec=response_comparison.exe


flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter" 
rm -f $exec
gcc $flagsNlibs -o $exec $code && {
    echo ; echo "Compilation successful" ; echo
    ./$exec
}
rm -f $exec

