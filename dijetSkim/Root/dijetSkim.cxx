#include "dijetSkim/dijetSkim.h"	
#include "TFile.h"
#include "TH1F.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"
//#include "xAODTriggerCnv/TriggerMenuMetaDataTool.h"


const char* APP_NAME = "dijetSkim";

dijetSkim::dijetSkim() {}

dijetSkim::~dijetSkim() {}

int dijetSkim::Initialize(xAOD::TEvent& event) {

  const xAOD::EventInfo* eventInfo = 0;
  CHECK(event.retrieve(eventInfo, "EventInfo"));
    m_isMC = false;
  if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    m_isMC = true; 
  }   
    std::cout<< "is mc = " << m_isMC <<std::endl;
  m_triggertool = new xAODMaker::TriggerMenuMetaDataTool();
  m_triggertool->initialize();
  
  fOut = TFile::Open("mini-xAOD.root", "RECREATE");
  CHECK(event.writeTo(fOut));

  //event.setAuxItemList("AntiKt4EMTopoJetsAux.", "pt.eta.phi.m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.btaggingLink.GhostMuonSegment.GhostMuonSegmentCount.GhostTrack.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.NumTrkPt500.SumPtTrkPt500.Timing.TrackWidthPt1000.TruthLabelID.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.OriginCorrected.PileupCorrected.DetectorEta.JetPileupScaleMomentum_pt.JetPileupScaleMomentum_eta.JetPileupScaleMomentum_phi.JetPileupScaleMomentum_m.JetEtaJESScaleMomentum_pt.JetEtaJESScaleMomentum_eta.JetEtaJESScaleMomentum_phi.JetEtaJESScaleMomentum_m"); 
  if(m_isMC)event.setAuxItemList("AntiKt4EMTopoJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.PartonTruthLabelID.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt");
else event.setAuxItemList("AntiKt4EMTopoJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt"); 
if(m_isMC)event.setAuxItemList("AntiKt4LCTopoJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.PartonTruthLabelID.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt");
else event.setAuxItemList("AntiKt4LCTopoJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt");
if(m_isMC)event.setAuxItemList("AntiKt4EMPFlowJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.PartonTruthLabelID.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt.SumPtChargedPFOPt500");
else event.setAuxItemList("AntiKt4EMPFlowJetsAux.", "pt.eta.phi.m.JetOriginConstitScaleMomentum_pt.JetOriginConstitScaleMomentum_eta.JetOriginConstitScaleMomentum_phi.JetOriginConstitScaleMomentum_m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.GhostMuonSegment.GhostMuonSegmentCount.HECFrac.HECQuality.JVF.LArQuality.NegativeE.NumTrkPt1000.SumPtTrkPt500.Timing.TrackWidthPt1000.Width.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.AverageLArQF.EMFrac.FracSamplingMax.FracSamplingMaxIndex.DetectorEta.JvtJvfcorr.JvtRpt.SumPtChargedPFOPt500");

 event.setAuxItemList("Kt4LCTopoEventShapeAux.", "Density"); 
 event.setAuxItemList("Kt4EMTopoEventShapeAux.", "Density");
 event.setAuxItemList("Kt4EMPFlowEventShapeAux.", "Density");
  //event.setAuxItemList("EventInfoAux.", "mcEventWeights.averageInteractionsPerCrossing.mcChannelNumber.eventTypeBitmask");
  //event.setAuxItemList("PrimaryVerticesAux.", "trackParticleLinks");
    if(m_isMC)(event.setAuxItemList("AntiKt4TruthJetsAux.", "pt.eta.phi.m"));
  //event.setAuxItemList("TruthEventAux.", "truthParticleLinks");
  //event.setAuxItemList("TruthVertex.", "incomingParticleLinks");
  //event.setAuxItemList("TruthParticleAux.", "prodVtxLink.decayVtxLink.m.px.py.pz.status.pdgId.barcode.e.polarizationPhi.polarizationTheta");
  //event.setAuxItemList("MET_RefFinalFixAux.", "mpx.mpy.name");
  //event.setAuxItemList("MET_TruthAux.", "mpx.mpy.name");

    event.setAuxItemList("AntiKt2PV0TrackJetsAux.", "pt.eta.phi.m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m");


  sumOfWeights = new TH1F("sumOfWeights", "sumOfWeights", 5, -0.5, 4.5);
  sumOfWeights->Sumw2();
  sumOfWeights->GetXaxis()->SetBinLabel(1, "mc only");
  sumOfWeights->GetXaxis()->SetBinLabel(2, "mc*pileup");
  sumOfWeights->GetXaxis()->SetBinLabel(3, "mc*pileupUp");
  sumOfWeights->GetXaxis()->SetBinLabel(4, "mc*pileupDn");
  sumOfWeights->GetXaxis()->SetBinLabel(5, "mc*pileupNo");

  return 0;
}


int dijetSkim::Execute(xAOD::TEvent& event) {

  const xAOD::EventInfo* eventInfo = 0;
  CHECK(event.retrieve(eventInfo, "EventInfo"));
  mcEventWeight = m_isMC? eventInfo->mcEventWeight() : 1.0;

  //const xAOD::TruthEventContainer* truthEvents = 0;
  //CHECK(event.retrieve(truthEvents, "TruthEvent"));

  if (m_isMC) {   
    sumOfWeights->Fill(0., mcEventWeight);
    sumOfWeights->Fill(1., mcEventWeight);
    //sumOfWeights->Fill(1., mcEventWeight*pileupWeight  );
    //sumOfWeights->Fill(2., mcEventWeight*pileupWeightUp);
    //sumOfWeights->Fill(3., mcEventWeight*pileupWeightDn);
    //sumOfWeights->Fill(4., mcEventWeight*pileupWeightNo);
  }

  else {
    sumOfWeights->Fill(1., 1.0);
  }  
  
  /*
  TLorentzVector truthNeutrinoMET;

  for (const auto &truthEvent : *truthEvents) {
    int nParticles = truthEvent->nTruthParticles();
    for (int i = 0; i < nParticles; i++) {
      const xAOD::TruthParticle* particle = truthEvent->truthParticle(i);
      int barcode = 0, status = 0, pdgId = 0, parentPdgId = 0;
      if (particle) {
        barcode = particle->barcode();
        status = particle->status();
        pdgId = particle->pdgId();
        parentPdgId = particle->nParents() > 0? particle->parent(0)->pdgId() : -999;
      }
      if (status == 1 && barcode < 200000 && std::abs(parentPdgId) < 111 && std::abs(parentPdgId) != 15) {
        if (std::abs(pdgId) == 12 || std::abs(pdgId) == 14 || std::abs(pdgId) == 16 ) {
          truthNeutrinoMET += particle->p4();
        }
        //Info("execute()", " number of parents  = %i", particle->nParents());
        //Info("execute()", " pdg ID = %i", pdgId);
        //Info("execute()", " parent pdg ID = %i", parentPdgId);
      }
    }
  }*/
//This is an event filtering...have to check size reduction first
/*

  TLorentzVector truthNeutrinoMET;

  for (const auto &truthEvent : *truthEvents) {
    int nParticles = truthEvent->nTruthParticles();
    for (int i = 0; i < nParticles; i++) {
      const xAOD::TruthParticle* particle = truthEvent->truthParticle(i);
      int barcode = 0, status = 0, pdgId = 0, parentPdgId = 0;
      if (particle) {
        barcode = particle->barcode();
        status = particle->status();
        pdgId = particle->pdgId();
        parentPdgId = particle->nParents() > 0? particle->parent(0)->pdgId() : -999;
      }
      if (status == 1 && barcode < 200000 && std::abs(parentPdgId) < 111 && std::abs(parentPdgId) != 15) {
        if (std::abs(pdgId) == 12 || std::abs(pdgId) == 14 || std::abs(pdgId) == 16 ) {
          truthNeutrinoMET += particle->p4();
        }
        //Info("execute()", " number of parents  = %i", particle->nParents());
        //Info("execute()", " pdg ID = %i", pdgId);
        //Info("execute()", " parent pdg ID = %i", parentPdgId);
      }
    }
  }

  float truthNuMET = truthNeutrinoMET.Pt();
  //Info("execute()", " Truth MET is %.2f", tTruthMET);
  //Info("execute()", " Truth neutrino MET is %.2f", truthNuMET);
  //if (tMET < 100000. && truthNuMET < 100000.) return 0;
*/
  CHECK(event.copy("EventInfo"));
  CHECK(event.copy("PrimaryVertices"));
  CHECK(event.copy("AntiKt4EMTopoJets"));
  CHECK(event.copy("AntiKt4LCTopoJets"));
  //CHECK(event.copy("AntiKt2PV0TrackJets"));
  CHECK(event.copy("AntiKt4EMPFlowJets"));
  CHECK(event.copy("Kt4EMPFlowEventShape"));
  CHECK(event.copy("Kt4EMTopoEventShape"));
  CHECK(event.copy("Kt4LCTopoEventShape"));
  //CHECK(event.copy("GSFTrackParticles"));
  //CHECK(event.copy("InDetTrackParticles"));
  //CHECK(event.copy("CombinedMuonTrackParticles"));
  //CHECK(event.copy("ExtrapolatedMuonTrackParticles"));
  CHECK(event.copy("xTrigDecision"));
  CHECK(event.copy("TrigConfKeys"));
  //CHECK(event.copy("TrigNavigation"));
  
  //CHECK(event.copy("HLT_xAOD__JetContainer_EFJet"));
  //CHECK(event.copy("HLT_xAOD__JetContainer_SplitJet"));
  CHECK(event.copy("HLT_xAOD__JetContainer_a10tcemsubjesFS"));
  CHECK(event.copy("HLT_xAOD__JetContainer_a4tcemsubjesFS"));
  //CHECK(event.copy("HLT_xAOD__JetContainer_a4tclcwsubjesFS"));
  if(!m_isMC)CHECK(event.copy("LVL1JetRoIs"));
  if (m_isMC) {
    //CHECK(event.copy("TruthParticle"));
    //CHECK(event.copy("TruthVertex"));
    //CHECK(event.copy("TruthEvent"));
    CHECK(event.copy("AntiKt4TruthJets"));
  }
  CHECK(event.fill());

  return 0;
}


int dijetSkim::Finalize(xAOD::TEvent& event) {

  fOut->cd();
  sumOfWeights->Write();
  CHECK(event.finishWritingTo(fOut));
  fOut->Write();
  fOut->Close();
  delete m_triggertool;
  return 0;
}
