#!/bin/bash

USERNAME=fballi
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=mini-xAOD.root

export tagout=`echo $j | cut -d '_' -f 4`
echo $tagout
#MC

#25ns
#Pythia
'
for i in 0 1 2 3 4 5 6 7 8 9; do
    INDS=mc15_13TeV.36102${i}.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ${i}W.merge.DAOD_JETM1.*_r6765_r6282_p2375/
    OUTDS=mc15_13TeV.Pythia8EvtGen_A14NNPDF23LO_JZ${i}W_mini.r6765_r6282_p2375_v02
    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput
done
#Powheg+Pythia
for i in 1 2 3 4 5 6 7 8 9; do

    INDS=mc15_13TeV.42600${i}.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6765_r6282_p2352/
    OUTDS=mc15_13TeV.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_JZ${i}W_mini.r6765_r6282_p2375_v02
    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput
done
#50ns


for i in 0 1 2 3 4 5 6 7 8 9
#for i in 5

do
    INDS=mc15_13TeV.36102${i}.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ${i}W.merge.DAOD_JETM1.*_r6630_r6264_p2352/
    #export tagout=`echo $j | cut -d '_' -f 4`
    #echo $tagout
    OUTDS=mc15_13TeV.Pythia8EvtGen_A14NNPDF23LO_JZ${i}W_mini.r6630_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done                  


for i in 1 2 3 4 5 6 8
#for i in 5
do
    INDS=mc15_13TeV.42600${i}.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6630_r6264_p2352/
    #export tagout=`echo $j | cut -d '_' -f 4`
    #echo $tagout
    OUTDS=mc15_13TeV.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_JZ${i}W_mini.r6630_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done   

for i in 1 2 3; do
    INDS=mc15_13TeV.42600${i}.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6633_r6264_p2352/
    OUTDS=mc15_13TeV.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_JZ${i}W_mini.r6633_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done 



#Powheg+Herwig, week 1 config
for i in 1 2; do
    INDS=mc15_13TeV.42610${i}.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6633_r6264_p2352/
    OUTDS=mc15_13TeV.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}W_mini.r6633_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done 

#Powheg+Herwig, 50 ns
#for i in 4 5 6 8 9; do
for i in 1 2 3; do
    INDS=mc15_13TeV.42610${i}.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6630_r6264_p2352/
    OUTDS=mc15_13TeV.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}W_mini.r6630_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done 

for i in 7; do
    INDS=mc15_13TeV.42610${i}.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}.merge.DAOD_JETM1.*_r6630_r6264_p2352_tid06117600_00
    OUTDS=mc15_13TeV.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ${i}W_mini.r6630_r6264_p2352_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15\
         --mergeOutput

done 



#data

#for i in 00267152 00267073; do
#00266904 00266919; do
#for i in 00267167 00267162; do
#for i in 00267638; do
#for i in 00267639; do
    INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.f594_m1435_p2361/
#    INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.f594_m1441_p2361/
    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.f594_m1435_p2361_v02c




##High mu, 639
#    INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.r6818_p2358_p2361/
#    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.r6818_p2358_p2361_v02c
#INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.f598_m1441_p2361_tid05735976_00
#    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.f598_m1441_p2361_v02c
#    
#    
##High mu, 638
#INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.r6818_p2358_p2361_tid05714274_00
#    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.r6818_p2358_p2361_v02c
#
#INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.f598_m1441_p2361_tid05734460_00
#    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.f598_m1441_p2361_v02c


for i in 00267152 00267073 00267162 00267167 00267638 00267639; do

INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.r6848_p2358_p2361_tid*_00
    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.r6848_p2358_p2361_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15

done


for i in 00266904 00266919; do

INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.r6847_p2358_p2361/
    OUTDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1_mini.r6847_p2358_p2361_v02c

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15

done



#reproduced period C with higher triggers
#for i in 00270441 00270448 00270588 00270806 00270816 00270949; do
#for i in 00270953 00271048 00271298 00271388 00271421 00271516 00271595; do
for i in 00270441; do
INDS=group.perf-jets.data15_13TeV.${i}.physics_Main.DAOD_JETM1.f*addtrig.v01_EXT0/
    OUTDS=data15_13TeV.${i}.physics_Main.DAOD_JETM1_mini.periodC.addtrig_v00

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15

done


'
#reprocessed data 50ns 21/08/2015

#periodA
for i in 00267152 00267073 00267162 00267167 00267638 00267639; do
#periodC
#for i in 00270441 00270448 00270588 00270806 00270816 00270949; do
#for i in 00270953 00271048 00271298 00271388 00271421 00271516 00271595; do
    INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.r694*_p2410_p2411/
    OUTDS=data15_13TeV.${i}.physics_Main.DAOD_JETM1_mini.r6943_p2410_p2411_v00

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15

done
'
#25ns data

#not in the GRLs
#for i in 00276176 00276166 00276073 00276147; do
#in the GRLs
#for i in 00276262 00276511 00276416 00276329 00276336 00276147; do
#
#for i in 00276790 00276689 00276778 00276952 00276954; do
for i in 00276731; do
#will come later
#00276731 

    INDS=data15_13TeV.${i}.physics_Main.merge.DAOD_JETM1.f*_m1480_p2411/
    OUTDS=data15_13TeV.${i}.physics_Main.DAOD_JETM1_mini.f618_m1480_p2411

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15

done

'
