printf "\n===================================\n"
printf "Cloning Bootstrap package from git repository"
printf "\n===================================\n"
lsetup git
git clone https://gitlab.cern.ch/cjmeyer/BootstrapGenerator.git && {
  printf "\n===================================\n"
  printf "Building/Compiling"
  printf "\n===================================\n"
  cd BootstrapGenerator
  mkdir StandAlone
  cd cmt
  make -f Makefile.Standalone
  cd ../..
  printf "\n===================================\n"
  printf "Success"
  printf "\n===================================\n"
} || printf "error: couldn't clone the git package\naborting\n"
