#!/bin/bash
echo "RUNNING TEST OF NTUPLING : Signal"

echo "START1"
ls .
echo "START2"
ls ..

echo ${CI_PROJECT_DIR}

echo "CHECKING BUILD DIR ABOVE"
pwd
ls ../*

##############################
# Setup                      #
##############################
echo "SETUPRelease"
source ~/release_setup.sh
echo "SETUPExecutables"
source ../build/${AnalysisBase_PLATFORM}/setup.sh

# definition of folder for storing test results
TESTDIR=test
TESTFILE_ORIGIN="root://eoshome.cern.ch//eos/user/q/qgtag/dijet_data/DAOD_JETM1.14783442._000291.pool.root.1"
TESTFILE_LOCAL=test_signal.root
TESTRESULT=testResult_signal.root

##############################
# Process test sample        #
##############################

# create directory for results
pwd
mkdir -p ${TESTDIR}
cd ${TESTDIR}
pwd
# copy file with xrdcp
if [ ! -f ${TESTFILE_LOCAL} ]; then
    echo "File not found! Copying it from EOS"
    # get kerberos token with service account monojet to access central test samples on EOS
    if [ -z ${SERVICE_PASS} ]
    then
      CERN_USER=monojet
      echo "Please enter the password for the service account: user ${CERN_USER} (can be obtained in the Gitlab/Settings/CICD/Variables menu)"
      echo "If you belong to the analysis team and already have your CERN USER kerberos token by entering kinit ${USER}@CERN.CH you can skip by entering ctrl + c"
      kinit ${CERN_USER}@CERN.CH
    else
      echo "Setting up kerberos"
      echo "CERN_USER - "${CERN_USER}
      echo "SERVICE_PASS - "${SERVICE_PASS}
      echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
    fi
    echo xrdcp ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
    xrdcp  ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
fi
 
# clean up old job result
if [ -f ${TESTRESULT} ]; then
    rm ${TESTRESULT} 
fi

# put the test input file in a proper place
echo "Moving test file to appropriate place"
mkdir -p TestFiles/mc16_13TeV.dijet
mv ${TESTFILE_LOCAL} TestFiles/mc16_13TeV.dijet/.

# run a test job
echo "Running a test job"
echo "Current Dir : "
pwd
echo "Place where executable lives : "
#ls ../source/*
echo "Running job now : "
EtaInterCalAnalysis TestFiles/mc16_13TeV.dijet/${TESTFILE_LOCAL} 

echo "DONE"
