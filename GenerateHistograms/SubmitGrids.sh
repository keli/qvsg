#!/bin/bash

USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=Data_3DHistograms.root

##
## Contains all good 25ns runs from 2015
##
now=$(date +"%T")
echo "Current time : $now"

INDS=user.jrawling:user.jrawling.data15_13TeV.r7600_p2521_p2566.miniTree.20160512.2314_EtaInterCal_miniTree_data.root.77665643
OUTDS=bootStrap.Test

echo "Submitting to "${INDS}
echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

prun --inDS=${INDS}\
     --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
     --outputs=${OUTPUTS}\
     --exec="GenerateHistograms/generateHistograms.sh %IN"\
     --useRootCore\
     --excludedSite=${BLACKLIST}\
     --nGBPerJob=10\
     --tmpDir=/tmp\
     --extFile=JetCalibTools/*,EventShapeTools/*CalibrationAndUncertainties/*,ExtrapolateToZeroRadiation/*,helperScripts/*, PhiInterCalibration/*,PreliminaryEtaIntercalUncertainties13TeV/*,TriggerEfficiency/*,DijetJER/*,EtaInterCalxAODAnalysis/*,FillSystAsymmetry3dToys/*,IterativeEtaIntercalibration/*,Plotting/*,dijetSkim/*,EtaInterCalxAODAnalysis_3DHist/*.MinimiseResponseMatrix/*,PlottingTool/*,SystematicShiftStatSignificance/*\
     --mergeOutput



 now=$(date +"%T")
 echo "Current time : $now"
