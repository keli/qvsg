import os
from subprocess import call,Popen

HistogramGenerator = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/histogramGenerator.exe"
HistogramGeneratorSettings = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/utils/settings.config"



inputNTuple="/tony-data2/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath = "/tony-data2/jrawling/data16_13TeV/DphiUp/"

inputNTuple="/higgs-data4/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath = "/higgs-data4/jrawling/data16_13TeV/pt3Down/"

inputNTuple="/pc2013-data5/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath="/pc2013-data5/jrawling/data16_13TeV/Nominal/"

inputNTuple="/hepgpu1-data3/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath = "/hepgpu1-data3/jrawling/data16_13TeV/pt3Up/"

inputNTuple="/pc2014-data5/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath="/pc2014-data5/jrawling/data16_13TeV/DphiDown/"

inputNTuple="/pc2012-data2/jrawling/data16_13TeV//data16_13TeV_25fb.root"
HistogramOutPath = "/pc2012-data2/jrawling/data16_13TeV/JVTTight/"
###### Set the number to be submitted at a time and how many to submit per batch ####
nBatches=23
nBatchSize=32

nBatches=48
nBatchSize=16

nBatches=32
nBatchSize=24
ps = {}
for i in range(19,nBatches):
    for j in range(1,nBatchSize+1):
        toyNum = j+i*nBatchSize
        onTheFlySettings="toy="+`toyNum`+",lastToy="+`toyNum+1`+",doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=1,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"
        logFile=HistogramOutPath+"generation_"+`toyNum`+".log"
        print "Logfile: " + logFile
        print "nice "+" -n "+" 10 "+HistogramGenerator+ " " +HistogramGeneratorSettings+ " " +inputNTuple+ " " +HistogramOutPath+ " " +onTheFlySettings
        args = ["nice","-n","10",HistogramGenerator,HistogramGeneratorSettings,inputNTuple,HistogramOutPath,onTheFlySettings]

        log_file = open(logFile,"wb")
        p = Popen(args,stdout=log_file,stderr=log_file)
        ps[p.pid] = p
    print "Waiting for %d processes..." % len(ps)
    while ps:
        pid, status = os.wait()
        if pid in ps:
            del ps[pid]
            print "Waiting for %d processes..." % len(ps)

print "done."
