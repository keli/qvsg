#ifndef HISTOGRAMFACTORY_H
#define HISTOGRAMFACTORY_H

#include "utils/Utils.h"
#include "utils/AtlasStyle.h"
#include "Histogram3DTool.h"

#include <utility>
#include "TEnv.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TFile.h"

extern float GeV;
///
/// A tool that creates a batch of 3D histogram root files given a certain se
class Histogram3DFactory{
public:
  Histogram3DFactory(std::string configFileName, std::string inputFileName, std::string outputFilePath, std::string onTheFlySettings);
  void Create3DHistograms();


private:
  TEnv *m_settings;

  vector<TString> m_jetCollections;
  vector<double>  m_thirdJetVariableBins;
  vector<TString> m_thirdJetVariableRanges;
  std::string     m_thirdJetVariable;
  bool            m_doNominal;
  bool            m_doBootStrap;
  bool            m_doSystematics;
  bool            m_doThirdJetExtrapolation;
  bool            m_isMC;
  int             m_nbToys; //Bootstrap
  int             m_thirdJetBin;

  bool m_doMuBinning, m_doNPVBinning;
  TFile* m_inputFile;
  std::string m_inputFileName;
  std::string m_outputFilePath;
  std::string m_configFileName;
  std::string m_onTheFlySettings;
  TTree *m_dijetInsituTree;

  void DisplayWelcomeMessage();
  void ReadSettings();
  void InitialiseInputFile();
  TString EvaluateName();
};

#endif
