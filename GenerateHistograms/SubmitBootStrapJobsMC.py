import os
from subprocess import call,Popen

### Localtions of the generate histogram exes ###
HistogramGenerator = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/histogramGenerator.exe"
HistogramGeneratorSettings = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/GenerateHistograms/utils/settings.config"

### settings for the histogram generator###

inputNTuple="/hepgpu1-data3/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath = "/hepgpu1-data3/jrawling/data16_13TeV/pt3Up/"
onTheFlySettings="doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=1,doJet3PtFracShiftDown=0,doJVTCutTight=0"



inputNTuple="/higgs-data4/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath = "/higgs-data4/jrawling/data16_13TeV/pt3Down/"
onTheFlySettings="doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=1,doJVTCutTight=0"

inputNTuple="/pc2013-data5/jrawling/data16_13TeV/data16_13TeV_25fb.root"
HistogramOutPath="/pc2013-data5/jrawling/data16_13TeV/Nominal/"
onTheFlySettings="doBootStrap=1,doNominal=1,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"

inputNTuple="/pc2012-data2/jrawling/mc15_13TeV/mc15_13TeV_PowhegPythia.root"
HistogramOutPath = "/pc2012-data2/jrawling/mc15_13TeV/JVTTight/"
#HistogramOutPath = "/pc2012-data2/jrawling/data16_13TeV/test/"
onTheFlySettings="doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=1"


inputNTuple="/pc2012-data2/jrawling/mc15_13TeV/mc15_13TeV_PowhegPythia.root"
HistogramOutPath = "/pc2012-data2/jrawling/mc15_13TeV/pt3Down/"
#HistogramOutPath = "/pc2012-data2/jrawling/data16_13TeV/test/"
onTheFlySettings="doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=1,doJVTCutTight=0"

inputNTuple="/tony-data2/jrawling/mc15_13TeV/mc15_13TeV_PowhegPythia.root"
HistogramOutPath = "/tony-data2/jrawling/mc15_13TeV/DphiDown/"
onTheFlySettings="doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=1,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"

inputNTuple="/pc2014-data5/jrawling/jrawling/mc/LCTopo/mc15_13Tev_lctopo_powhegpythia.root"
HistogramOutPath="/pc2014-data5/jrawling/jrawling/mc/LCTopo/"

# inputNTuple="/higgs-data4/jrawling/mc/LCTopo/mc15_13Tev_lctopo_powhegpythia.root"
# HistogramOutPath="/higgs-data4/jrawling/mc/LCTopo/nominal/"
onTheFlySettings="doBootStrap=1,doNominal=1,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"

###### Set the number to be submitted at a time and how many to submit per batch ####
nStartToy = 384
##553
nLastToy = 768
#750
maxSimulataneousJobs = 40
ps = {}
for i in range(nStartToy,nLastToy):
    logFile=HistogramOutPath+"generation_"+`i`+".log"
    FullOnTheFlySettings = "toy="+`i`+",lastToy="+`i+1`+","+onTheFlySettings
    print "Logfile: " + logFile
    print "nice "+" -n "+" 10 "+HistogramGenerator+ " " +HistogramGeneratorSettings+ " " +inputNTuple+ " " +HistogramOutPath+ " " +FullOnTheFlySettings
    args = ["nice","-n","10",HistogramGenerator,HistogramGeneratorSettings,inputNTuple,HistogramOutPath,FullOnTheFlySettings]

    log_file = open(logFile,"wb")
    p = Popen(args,stdout=log_file,stderr=log_file)
    ps[p.pid] = p

    while len(ps) >= maxSimulataneousJobs:
        pid, status = os.wait()
        if pid in ps:
            del ps[pid]
            print "Starting next job: " + `i+1`

print "done."
