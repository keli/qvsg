#/bin/sh

inFile="/higgs-data4/jrawling/Micro-xAODs/user.jrawling.Dijet_micro-xAOD.20160308.1546_EtaInterCal_miniTree_data.root/data15_13TeV_all25ns.root"
inFile="/higgs-data4/jrawling/Micro-xAODs/data15_13TeV_partialMerge.root"
#inFile="/higgs-data4/jrawling/Micro-xAODs/user.jrawling.Dijet_micro-xAOD.20160315.1015_EtaInterCal_miniTree_data.root/user.jrawling.7933355._000001.EtaInterCal_miniTree_data.root"
inFile="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/MiniTrees/EMTopo/data15_13TeV_periodsDFGHJ_microXAOD.root"
inFile="/pc2012-data2/jrawling/3DHistograms/Nominal1/nominal_3DHistograms.root"
inFile="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/MiniTrees/Attempt2/data15_13TeV_periodsDFGHJ_micro-xAOD.root"
#inFile="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/MiniTrees/EMTopo/periodD/00276952/EtaInterCal_3DHists_0.root"
inFile="/higgs-data4/jrawling/Micro-xAODs/user.jrawling.dijet_micro-xAOD.20160319.2324_EtaInterCal_micro-xAOD_data.root/data15_13TeV_partial.root"
inFile="/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/data15_13TeV_periodsDEFGHJ.root"
inFile="/hepgpu1-data3/jrawling/Micro-xAODs/OnlineJets/data15_13TeV_periodsDEFGHJ_onLineJets_NoCalib.root"
configFile="utils/settings.config"
outPath="/higgs-data4/jrawling/3DHistograms/160319"
#outPath="/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/3DHistograms/4Calib"
#outPath="/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/3DHistograms/InjectedBias/2Step"
#outPath="/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/3DHistograms/0CalibLCtopo"
#outPath="/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/3DHistograms/InjectedBias/Nominal"
outPath="/hepgpu1-data3/jrawling/Micro-xAODs/OnlineJets/3DHistograms/JVTCorrected"
exec=histogramGenerator.exe
nohup ./$exec ${configFile} ${inFile} ${outPath} &> ${outPath}/generation.log &
