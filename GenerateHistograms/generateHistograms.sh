#!/bin/sh

inFile=$1
configFile="utils/settings.config"
outPath="/hepgpu1-data3/jrawling/data15_13TeV_Closure/"
onTheFlySettings="doBootStrap=0,doNominal=1,doSystematics=0,doDphiShiftDown=0,doDphiShiftUp=0,doJet3PtFracShiftUp=0,doJet3PtFracShiftDown=0,doJVTCutTight=0"
gen="PowhegPytiha8"
productionTag="p2666"
exec=histogramGenerator2.exe

#nohup
./$exec ${configFile} ${inFile} ${outPath} ${gen} ${productionTag} ${onTheFlySettings}
#&> ${outPath}/generation.log &
