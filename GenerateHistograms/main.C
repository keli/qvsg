#include "Histogram3DFactory.h"


int main(int argc, char **argv) {
  //Read in setting names etc. from the given parameters of this programme.
	std::string configFN           = argv[1];
  std::string inputFileName      = argv[2];
  std::string outputFilePath     = argv[3];
  std::string dataSet            = argv[4];
  std::string productionTag      = argv[5];
  std::string onTheFlySettings   = (argc > 6 ? argv[6] : " ") ;
  onTheFlySettings = "dataSet="+dataSet + ",productionTag="+productionTag + ","+onTheFlySettings;
  PrintTime();
	if(outputFilePath=="null")outputFilePath="";
  Histogram3DFactory histogramFactory(configFN,inputFileName,outputFilePath,onTheFlySettings);

  histogramFactory.Create3DHistograms();

  PrintTime();
  return 0;
} // main
