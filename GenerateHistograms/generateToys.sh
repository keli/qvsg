#!/bin/sh

inFile=$1
configFile="utils/settings.config"
outPath=$2

exec=histogramGenerator.exe
nBatches=40;
batchSize=20;


for ((i=0; i<$nBatches; i++)); do
  pids=()
  for ((j=1; j<=$batchSize; j++));  do
  toy=$(($i*10 + $j))
  nice -n 10 nohup ./$exec ${configFile} ${inFile} ${outPath} toy=${toy},doBootStrap=1,doNominal=0,doSystematics=1,doDphiShiftDown=1 &> ${outPath}/generation_${toy}.log & pids+=$!
  echo ${toy} " pid=" $!;
  done

  for i in "${arrayName[@]}";  do
    while [ -d /proc/$pid ] ; do
      sleep 1
    done
  done
done
