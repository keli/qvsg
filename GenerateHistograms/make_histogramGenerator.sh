#!/bin/sh

includes=""
pkgs=""

echo "Includes: $includes"
#mylibs=`$ROOTCOREDIR/scripts/get_ldflags.sh $pkgs`
mylibs=""
echo "Libraries: $mylibs"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore -lMathMore  -lXMLParser $mylibs"

BootstrapLib="-I../BootstrapGenerator/ -l:../BootstrapGenerator/StandAlone/libBootstrapGenerator.so"

code="utils/Utils.C Histogram3DTool.C Histogram3DFactory.C main.C utils/AtlasStyle.C "
exec=histogramGenerator.exe
rm -f $exec

g++ -g $flagsNlibs $includes $BootstrapLib -o $exec $code
