#ifndef HISTOGRAMTOOL_H
#define HISTOGRAMTOOL_H

#include "utils/Utils.h"
#include "utils/AtlasStyle.h"

#include <utility>
#include "TEnv.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TFile.h"

//Bootstrap
#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH2DBootstrap.h"
#include "BootstrapGenerator/TH3DBootstrap.h"

extern float GeV;

/// A tool to take an opened input fill and produce a root file with all the necessary 3D histograms to run MinimizeMatrixMethod
class Histogram3DTool{
public:
  Histogram3DTool(TTree* dijetTree, bool isMC, int JX,std::string configFileName, TFile* outputFile, std::string thirdJetVariable, std::string thirdJetVariableRange, std::string jetAlgorithm, std::string outputFilePath , std::string onTheFlySettings);
  ~Histogram3DTool();

  void InitBranches();
  void InitLumiWeights();
  void ProcessTree();
  void InitialiseHistograms(std::string histogramNamePrefix);
  void FillHistograms(int pTbin);
  void FillHistograms(int pTbin, std::string prefix);
  void WriteOutput();

  void CreateHistograms(std::string prefix);
  std::string TriggerClassification(TString trig);
  std::string TriggerClassificationOldMethod(TString trig);
  std::string TriggerClassificationInclusionMethod(TString trig);

  //SlimHistos
  bool PassSlimHistos(TString prefix, int pTbin);
  bool PassSlimHistos(TString prefix);

private:
  TFile *m_outfile;
  TTree *m_dijetInsituTree;
  Str m_outfileName;
  std::string m_outputFilePath;
  Str m_thirdJetVariable;
  Str m_thirdJetVariableRange;
  Str m_configFileName;
  std::string m_name;

  TEnv *m_settings;


  // dijet selection
  int m_testRun;
  float m_jvtCut;
  float m_dphijjCut;
  float m_j3pTCut;
  int m_filled;
  int m_seed;
  bool m_injectBias;
  bool m_stressTest;
  bool m_doNominal;
  bool m_doThirdJetExtrapolation;
  bool m_doSystematics;
  bool m_doDphiShiftUp;
  bool m_doDphiShiftDown;
  bool m_doJet3PtFracShiftUp;
  bool m_doJet3PtFracShiftDown;
  bool m_doJVTCutTight;
  bool m_makeControlPlots;
  bool m_drawControlPlots;
  bool m_doBootStrap;
  bool m_madeControlHists;
  bool m_useCentralTrigger;
  bool m_useForwardTrigger;
  bool m_createTruthHists;
  bool m_createMCasym;
  bool m_inclusionTrigComb;

  std::vector<TString> m_systVariations;
  std::vector<double> m_eta_bins;
  std::vector<double> m_pTavg_bins;
  std::string m_onTheFlySettings;
  std::string m_jetAlgo;
  TString m_trigMenu;
  std::vector<TString> m_trigORs;
  std::vector<double>  m_trigThresholds;
  bool m_pass_HLT_j15;
  bool m_pass_HLT_j15_320eta490;
  bool m_pass_HLT_j25;
  bool m_pass_HLT_j25_320eta490;
  bool m_pass_HLT_j35;
  bool m_pass_HLT_j35_320eta490;
  bool m_pass_HLT_j45;
  bool m_pass_HLT_j45_320eta490;
  bool m_pass_HLT_j60;
  bool m_pass_HLT_j60_320eta490;
  bool m_pass_HLT_j110;
  bool m_pass_HLT_j110_320eta490;
  bool m_pass_HLT_j175;
  bool m_pass_HLT_j175_320eta490;
  bool m_pass_HLT_j260;
  bool m_pass_HLT_j260_320eta490;
  bool m_pass_HLT_j360;
  bool m_pass_HLT_j360_320eta490;
  bool m_doTriggerEmulation;
  bool m_atLeasteOneForwardJet;

	bool m_pass_Nominal;
	bool m_pass_DphiUp;
	bool m_pass_DphiDown;
	bool m_pass_JVTThight;
	bool m_pass_J3pTCutUp;
	bool m_pass_J3pTCutDown;
  float m_left_deta;
  float m_left_pT;
  float m_right_deta;
  float m_right_pT;
  float m_ref_eta;
  float m_ref_pT;
  float m_probe_eta;
  float m_probe_pT;
  float m_asymMM;
  float m_asymSM;

  float m_left_deta_truth;
  float m_left_pT_truth;
  float m_right_deta_truth;
  float m_right_pT_truth;

  float m_weight;
  float m_pTavg;
  float m_j1_eta;
  float m_j2_eta;
  float m_j1_phi;
  float m_j2_phi;
  float m_j3_eta;
  float m_j3_phi;
  float m_j3_pT;
  float m_j1_pT;
  float m_j2_pT;
  float m_j1_JVT;
  float m_j2_JVT;
  float m_NPV,m_mu;

  float m_pTavg_truth;
  float m_j1_eta_truth;
  float m_j2_eta_truth;
  float m_j1_phi_truth;
  float m_j2_phi_truth;
  float m_j3_eta_truth;
  float m_j3_phi_truth;
  float m_j3_pT_truth;
  float m_j1_pT_truth;
  float m_j2_pT_truth;
  float m_j1_JVT_truth;
  float m_j2_JVT_truth;

  //Bootstrap
  int m_nbToys;
  uint m_mcChannelNumber;
	uint m_runNumber;
  unsigned long long int m_eventNumber;
  BootstrapGenerator *m_generator;
  std::map<std::string,TH3DBootstrap*> m_hists3D_BS;
  std::map<std::string,TH2DBootstrap*> m_hists2D_BS;

  //SlimHistos
  bool m_doSlimHistos;
  std::vector<std::vector<TString>> m_Slim_pTbins;

  //lumi weight
  double LumiWeight;
  Str productionTag;
  //data :
  VecD Central_lumis, Forward_lumis;
  StrV CentrallumiTrigs, ForwardlumiTrigs;
  map<Str,double> CentrallumiMap;
  map<Str,double> ForwardlumiMap;
  double Deliveredlumi;
  //StrV trig_rndBit;
  //vector<VecD> RNDtrigOR;
  //MC :
  Str MCgen;
  StrV MCsamples;
  Str MCweightStyle;
  VecD MCxsec, MCNevts, MCFiltEff;
  double MClumi;
  map<Str,double> MClumiWeight;

  int m_trigBitsCtrl;
  int m_trigBitsFwd;
  TString  ctrl_trig;
  TString  fwd_trig;
  std::map<std::string, bool> trigMap;
  std::map<std::string,TH3F*> m_hists3D;
  std::map<std::string,TH2F*> m_hists2D;
  std::map<std::string,TH1F*> m_controlHists;
  int m_JX;
  bool m_isMC=false;
  bool m_passTrigger;
  bool m_doMuBinning, m_doNPVBinning;
  double m_minPileUpVar, m_maxPileUpVar;
  double m_minThirdJetVariable, m_maxThirdJetVariable;
  bool   PassCuts();
  bool   PassJVTCut();
  bool   PassPt3Cut();
  bool   PassDeltaPhiCut();
  double FindThirdJetVariable();
  bool   passThirdJetVaraible();
  void   CleanUpHistograms();
  void   SetTrigMap();
  void   DisplayWelcomeMessage();
  bool   passTrigger(TString triggerCombination);
  void   FillControlPlot(std::string prefix);
  void   DrawControlPlot();
  void   CalibrateJets();
  void EvaluateAsymmetry();
  bool   m_applyCalibration;
  bool PassPileUpVarBinning();
  TString m_calibrationFilePath;
  TFile*  m_calibraitonFile;
  TH2F*   m_calibrationHistogram;
  void   InitialiseCalibration();
  float GetCalibrationFactor(float pT, float eta);
  void InitOutputFile();

};
#endif
