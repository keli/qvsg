# Cuts/calibration/selection settings
applyPileupReweighting: 0
applyGRL:               1
applyTrigger:           1
applyJetCalibration:    1
EMscale:                0
applyJetCleaning:       1
applyJ3Cleaning:        0
applyJVF:               0
applyJVT:               1
applyDphijj:            1
applyJet3pT:            1
applyMCCleaning:        1
calibDataAsMC:          0
applyDijetSelection:    1
vetoJetsInBadEta:       0
vetoEventsInBadEta:     0
applyTriggerEmulation   1
badEtaRange:            0.8 1.5
forceMCTrig:						0
enforceRefFJ_FJEta: 		0
centralAsDefaultTriggerPrefrence: 0
requireOneJetForward: 0
# Systematics and corresponding systematic shifts

doNominal:				0
applyCalibration: 0
#calibrationFilePath: /afs/hep.man.ac.uk/u/jrawling/CLEAN_INTERCALAREA/CalibrationAndUncertainties/Feb16_EtaIntercalibration13TeV25ns_ETA03_MM_AntiKt4EMTopo_Calibration.root
calibrationFilePath: /afs/hep.man.ac.uk/u/jrawling/CLEAN_INTERCALAREA/helperScripts/MultiplyCalibrations/level2Calib.root
doSystematics		1
doDphiShiftUp		1
doDphiShiftDown		1
doJet3PtFracShiftUp     1
doJet3PtFracShiftDown   1
doJVTCutTight           1

DphijjUpCut	       2.8
DphijjDownCut 	       2.2
Jet3PtFracUpCut        0.5
Jet3PtFracDownCut      0.3
JVFCutTight            0.92

# Veto pathalogical MC events
vetoPathalogicalEvents: 1
discardTriggerOverlapEvents: 0
# Dijet selection
JVFCut:        0.25
JVTCut:        0.64
DphijjCut:     2.5
Jet3PtFracCut: 0.4

RefRegion:     0.8
CentralRegion: 2.4

# Jet collections
###################
JetCollections: AntiKt4EMTopo
#AntiKt4LCTopo AntiKt4EMPFlow
#AntiKt4LCTopo AntiKt4EMPFlow  AntiKt4EMTopo

# AntiKt4LCTopo AntiKt6EMTopo AntiKt6LCTopo AntiKt4EMTopo AntiKt4LCTopo AntiKt4EMPFlow AntiKt4LCTopo

TruthJetCollections: AntiKt4Truth


# pTavg and |eta| bins
########################

#AntiKt4LCTopo.pTavg.Bins: 25 40 55 65 85 115 145 175 220 270 330 400 525 760 1200 1500
AntiKt4EMTopo.pTavg.Bins:  25 40 55 70 85 115 145 175 220 270 330 400 525 760 1200 1500
PhiInterCal.pTavg.Bins: 25 40 55 65 85 115

#AntiKt6LCTopo.pTavg.Bins: 40 55 70 85 100 135 180 200 240 290 380 480 600 760 1200 1500
#AntiKt6EMTopo.pTavg.Bins: 40 55 70 85 100 135 180 200 240 290 380 480 600 760 1200 1500

EtaBins.3Dhists:        -4.5 -4.0 -3.8 -3.6 -3.4 -3.2 -3.1 -3.0 -2.9 -2.8 -2.7 -2.6 -2.5 -2.4 -2.3 -2.2 -2.1 -2.0
+EtaBins.3Dhists:       -1.9 -1.8 -1.7 -1.6 -1.5 -1.4 -1.3 -1.2 -1.1 -1.0 -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2
+EtaBins.3Dhists:       -0.1 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0
+EtaBins.3Dhists:       2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.4 3.6 3.8 4.0 4.5

# Triggers
############

TriggerMenu: bestpT
#options are: week1, periodC, highpT

UseCentralTriggersOnly: 0
UseForwardTriggersOnly: 0

CentralTriggers2015.week1: HLT_j15 HLT_j25
ForwardTriggers2015.week1: HLT_j15_320eta490 HLT_j25_320eta490

CentralTriggers2015.periodC: HLT_j15 HLT_j25 HLT_j60
ForwardTriggers2015.periodC: HLT_j15_320eta490 HLT_j25_320eta490 HLT_j60_320eta490

CentralTriggers2015.highpT: HLT_j15 HLT_j25 HLT_j60 HLT_j110 HLT_j175 HLT_j260 HLT_j360
ForwardTriggers2015.highpT: HLT_j15_320eta490 HLT_j25_320eta490 HLT_j60_320eta490 HLT_j110_320eta490 HLT_j175_320eta490 HLT_j260_320eta490 HLT_j360_320eta490

CentralTriggers2015.highestpT: HLT_j15 HLT_j25 HLT_j45 HLT_j60 HLT_j110 HLT_j175 HLT_j260 HLT_j360
ForwardTriggers2015.highestpT: HLT_j15_320eta490 HLT_j25_320eta490 HLT_j45_320eta490 HLT_j60_320eta490 HLT_j110_320eta490 HLT_j175_320eta490 HLT_j260_320eta490 HLT_j360_320eta490
CentralTriggers2015.bestpT: HLT_j15 HLT_j25 HLT_j35 HLT_j45 HLT_j60 HLT_j110 HLT_j175 HLT_j260 HLT_j360
ForwardTriggers2015.bestpT: HLT_j15_320eta490 HLT_j25_320eta490 HLT_j35_320eta490 HLT_j45_320eta490 HLT_j60_320eta490 HLT_j110_320eta490 HLT_j175_320eta490 HLT_j260_320eta490 HLT_j360_320eta490

Triggers2015.week1:  HLT_j15
+Triggers2015.week1: HLT_j15_320eta490
+Triggers2015.week1: HLT_j25
+Triggers2015.week1: HLT_j25_320eta490

Triggers2015.periodC:  HLT_j15
+Triggers2015.periodC: HLT_j15_320eta490
+Triggers2015.periodC: HLT_j25
+Triggers2015.periodC: HLT_j25_320eta490
+Triggers2015.periodC: HLT_j60
+Triggers2015.periodC: HLT_j60_320eta490

Triggers2015.highpT:  HLT_j15
+Triggers2015.highpT: HLT_j15_320eta490
+Triggers2015.highpT: HLT_j25
+Triggers2015.highpT: HLT_j25_320eta490
+Triggers2015.highpT: HLT_j60
+Triggers2015.highpT: HLT_j60_320eta490
+Triggers2015.highpT: HLT_j110
+Triggers2015.highpT: HLT_j110_320eta490
+Triggers2015.highpT: HLT_j175
+Triggers2015.highpT: HLT_j175_320eta490
+Triggers2015.highpT: HLT_j260
+Triggers2015.highpT: HLT_j260_320eta490
+Triggers2015.highpT: HLT_j360
+Triggers2015.highpT: HLT_j360_320eta490

+Triggers2015.test45: HLT_j45
+Triggers2015.test45: HLT_j45_320eta490

Triggers2015.bestpT:  HLT_j15
+Triggers2015.bestpT: HLT_j15_320eta490
+Triggers2015.bestpT: HLT_j25
+Triggers2015.bestpT: HLT_j25_320eta490
+Triggers2015.bestpT: HLT_j35
+Triggers2015.bestpT: HLT_j35_320eta490
+Triggers2015.bestpT: HLT_j45
+Triggers2015.bestpT: HLT_j45_320eta490
+Triggers2015.bestpT: HLT_j60
+Triggers2015.bestpT: HLT_j60_320eta490
+Triggers2015.bestpT: HLT_j110
+Triggers2015.bestpT: HLT_j110_320eta490
+Triggers2015.bestpT: HLT_j175
+Triggers2015.bestpT: HLT_j175_320eta490
+Triggers2015.bestpT: HLT_j260
+Triggers2015.bestpT: HLT_j260_320eta490
+Triggers2015.bestpT: HLT_j360
+Triggers2015.bestpT: HLT_j360_320eta490

Trig_OR.week1:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.week1: HLT_j25_OR_HLT_j25_320eta490

Trig_OR.periodC:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.periodC: HLT_j25_OR_HLT_j25_320eta490
+Trig_OR.periodC: HLT_j60_OR_HLT_j60_320eta490

Trig_OR.highpT:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.highpT: HLT_j25_OR_HLT_j25_320eta490
+Trig_OR.highpT: HLT_j60_OR_HLT_j60_320eta490
+Trig_OR.highpT: HLT_j110_OR_HLT_j110_320eta490
+Trig_OR.highpT: HLT_j175_OR_HLT_j175_320eta490
+Trig_OR.highpT: HLT_j260_OR_HLT_j260_320eta490
+Trig_OR.highpT: HLT_j360_OR_HLT_j360_320eta490

Trig_OR.highestpT:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.highestpT: HLT_j25_OR_HLT_j25_320eta490
+Trig_OR.highestpT: HLT_j35_OR_HLT_j35_320eta490
+Trig_OR.highestpT: HLT_j35_OR_HLT_j45_320eta490
+Trig_OR.highestpT: HLT_j45_OR_HLT_j45_320eta490
+Trig_OR.highestpT: HLT_j60_OR_HLT_j60_320eta490
+Trig_OR.highestpT: HLT_j110_OR_HLT_j110_320eta490
+Trig_OR.highestpT: HLT_j175_OR_HLT_j175_320eta490
+Trig_OR.highestpT: HLT_j260_OR_HLT_j260_320eta490
+Trig_OR.highestpT: HLT_j360_OR_HLT_j360_320eta490

Trig_OR.bestpT:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.bestpT: HLT_j25_OR_HLT_j25_320eta490
+Trig_OR.bestpT: HLT_j35_OR_HLT_j35_320eta490
+Trig_OR.bestpT: HLT_j35_OR_HLT_j45_320eta490
+Trig_OR.bestpT: HLT_j45_OR_HLT_j45_320eta490
+Trig_OR.bestpT: HLT_j60_OR_HLT_j60_320eta490
+Trig_OR.bestpT: HLT_j110_OR_HLT_j110_320eta490
+Trig_OR.bestpT: HLT_j175_OR_HLT_j175_320eta490
+Trig_OR.bestpT: HLT_j260_OR_HLT_j260_320eta490
+Trig_OR.bestpT: HLT_j360_OR_HLT_j360_320eta490

# lower threshold (in pTavg) for each trigger combination
TrigPtThreshold.highpT:   25 40 85 175 220 330 400
TrigPtThreshold.highestpT:   25 40 65 85 175 220 330 400
TrigPtThreshold.bestpT:   25 40 55 65 65 85 175 220 330 400
TrigPtThreshold.periodC:  25 40 85
TrigPtThreshold.week1:    25 40


TriggersForOverlap:  HLT_j15 HLT_j15_320eta490
+TriggersForOverlap: HLT_j25 HLT_j25_320eta490
+TriggersForOverlap: HLT_j35 HLT_j35_320eta490
+TriggersForOverlap: HLT_j35 HLT_j45_320eta490
+TriggersForOverlap: HLT_j45 HLT_j45_320eta490
+TriggersForOverlap: HLT_j55 HLT_j55_320eta490
+TriggersForOverlap: HLT_j60 HLT_j60_320eta490
+TriggersForOverlap: HLT_j85 HLT_j85_320eta490
+TriggersForOverlap: HLT_j110 HLT_j110_320eta490
+TriggersForOverlap: HLT_j175 HLT_j175_320eta490
+TriggersForOverlap: HLT_j260 HLT_j260_320eta490
+TriggersForOverlap: HLT_j360 HLT_j360_320eta490
+TriggersForOverlap: HLT_j15_j15_320eta490
+TriggersForOverlap: HLT_j25_j25_320eta490
+TriggersForOverlap: HLT_j35_j35_320eta490
+TriggersForOverlap: HLT_j45_j45_320eta490
+TriggersForOverlap: HLT_j55_j55_320eta490
+TriggersForOverlap: HLT_j60_j60_320eta490
+TriggersForOverlap: HLT_j85_j85_320eta490

# other week 1 triggers
#HLT_j35
#HLT_j35_320eta490
#HLT_j45_L1RDO
#HLT_j45_320eta490
#HLT_j55_L1RDO
#HLT_j55
#HLT_j55_320eta490
#HLT_j85_L1RDO
#HLT_j85
#HLT_j85_320eta490
#HLT_j110
#HLT_j110_320eta490

# dijet triggers
#HLT_j15_j15_320eta490
#HLT_j25_j25_320eta490
#HLT_j35_j35_320eta490
#HLT_j45_j45_320eta490
#HLT_j55_j55_320eta490
#HLT_j60_j60_320eta490
#HLT_j85_j85_320eta490

# 2012 triggers
#Triggers2012:  EF_j15_a4tchad EF_fj15_a4tchad
#+Triggers2012: EF_j25_a4tchad EF_fj25_a4tchad
#+Triggers2012: EF_j35_a4tchad EF_fj35_a4tchad
#+Triggers2012: EF_j45_a4tchad EF_j45_a4tchad_L2FS_L1J15 EF_fj45_a4tchad_L2FS
#+Triggers2012: EF_j55_a4tchad EF_fj55_a4tchad_L2FS
#+Triggers2012: EF_j80_a4tchad EF_fj80_a4tchad
#+Triggers2012: EF_j110_a4tchad EF_fj110_a4tchad
#+Triggers2012: EF_j145_a4tchad EF_fj145_a4tchad
#+Triggers2012: EF_j180_a4tchad EF_fj180_a4tchad
#+Triggers2012: EF_j220_a4tchad EF_fj220_a4tchad

#CentralTriggers2012: EF_j15_a4tchad EF_j25_a4tchad EF_j35_a4tchad EF_j45_a4tchad EF_j45_a4tchad_L2FS_L1J15 EF_j55_a4tchad EF_j80_a4tchad EF_j110_a4tchad EF_j145_a4tchad EF_j180_a4tchad EF_j220_a4tchad EF_j280_a4tchad EF_j360_a4tchad
#ForwardTriggers2012: EF_fj15_a4tchad EF_fj25_a4tchad EF_fj35_a4tchad EF_fj45_a4tchad_L2FS EF_fj55_a4tchad_L2FS EF_fj80_a4tchad EF_fj110_a4tchad EF_fj145_a4tchad EF_fj180_a4tchad EF_fj220_a4tchad

####


#pt3 Dphijj phiStar pt3OverpTAvg
thirdJetVariable: Dphijj

pt3.Bins: 15 25 35 55 80 300
Dphijj.Bins: 0 0.15 0.35 0.5 4.0
phiStar.Bins: 0 0.025 0.5 0.1 0.2 1.0
pt3OverpTAvg.Bins: 0 0.1 0.3 0.2 0.4 0.6 0.8 1.0
