makeControlPlots: 1
drawControlPlots: 0


##inject a bias into the 85-115GeV, 3.0,3.6 bin of A=0.4##
injectBias: 0

# Cuts/calibration/selection settings
applyTriggerEmulation   1
inclusionTrigComb:    1

# Systematics and corresponding systematic shifts
doNominal:				1
applyCalibration: 0
calibrationFilePath: /afs/hep.man.ac.uk/u/jrawling/EtaIntercalWorkingBranch/CalibrationAndUncertainties/Feb16_EtaIntercalibration13TeV25ns_Eta2016_MM_AntiKt4EMTopo_Calibration.root

JVFCut:        0.25
JVTCut:        0.59
# 0.59 for EM and LC, 0.2 for PFlow
DphijjCut:     2.5
Jet3PtFracCut: 0.4
RefRegion:     0.8
CentralRegion: 2.4

# Jet collections
###################
JetCollections: AntiKt4EMTopo
#AntiKt4LCTopo AntiKt4EMPFlow  AntiKt4EMTopo

TruthJetCollections: AntiKt4Truth
createTruthHists: 0

# pTavg and |eta| bins
########################

AntiKt4EMTopo.pTavg.Bins:  25 40 60 85 115 145 175 220 270 330 400 525 760 1200 1500
AntiKt6LCTopo.pTavg.Bins:  25 40 60 85 115 145 175 220 270 330 400 525 760 1200 1500
AntiKt6EMTopo.pTavg.Bins:  25 40 60 85 115 145 175 220 270 330 400 525 760 1200 1500

PhiInterCal.pTavg.Bins: 25 40 55 65 85 115

EtaBins.3Dhists:        -4.5 -4.0 -3.8 -3.6 -3.4 -3.2 -3.1 -3.0 -2.9 -2.8 -2.7 -2.6 -2.5 -2.4 -2.3 -2.2 -2.1 -2.0
+EtaBins.3Dhists:       -1.9 -1.8 -1.7 -1.6 -1.5 -1.4 -1.3 -1.2 -1.1 -1.0 -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2
+EtaBins.3Dhists:       -0.1 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0
+EtaBins.3Dhists:       2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.4 3.6 3.8 4.0 4.5

# Triggers
############

TriggerMenu: bestpT

Trig_OR.bestpT:  HLT_j15_OR_HLT_j15_320eta490
+Trig_OR.bestpT: HLT_j25_OR_HLT_j25_320eta490
+Trig_OR.bestpT: HLT_j35_OR_HLT_j35_320eta490
+Trig_OR.bestpT: HLT_j45_OR_HLT_j45_320eta490
+Trig_OR.bestpT: HLT_j60_OR_HLT_j60_320eta490
+Trig_OR.bestpT: HLT_j110_OR_HLT_j110_320eta490
+Trig_OR.bestpT: HLT_j175_OR_HLT_j175_320eta490
+Trig_OR.bestpT: HLT_j260_OR_HLT_j260_320eta490
+Trig_OR.bestpT: HLT_j360_OR_HLT_j360_320eta490

AntiKt4.25to40.TrigOR:     HLT_j15_OR_HLT_j15_320eta490
AntiKt4.40to60.TrigOR:     HLT_j25_OR_HLT_j25_320eta490
AntiKt4.60to85.TrigOR:     HLT_j35_OR_HLT_j35_320eta490
AntiKt4.85to115.TrigOR:    HLT_j60_OR_HLT_j60_320eta490
AntiKt4.115to145.TrigOR:   HLT_j60_OR_HLT_j60_320eta490
AntiKt4.145to175.TrigOR:   HLT_j110_OR_HLT_j110_320eta490
AntiKt4.175to220.TrigOR:   HLT_j110_OR_HLT_j110_320eta490
AntiKt4.220to270.TrigOR:   HLT_j175_OR_HLT_j175_320eta490
AntiKt4.270to330.TrigOR:   HLT_j175_OR_HLT_j175_320eta490
AntiKt4.330to400.TrigOR:   HLT_j260_OR_HLT_j260_320eta490
AntiKt4.400to525.TrigOR:   HLT_j360_OR_HLT_j360_320eta490
AntiKt4.525to760.TrigOR:   HLT_j360_OR_HLT_j360_320eta490
AntiKt4.760to1200.TrigOR:  HLT_j360_OR_HLT_j360_320eta490
AntiKt4.1200to1500.TrigOR: HLT_j360_OR_HLT_j360_320eta490


# lower threshold (in pTavg) for each trigger combination
TrigPtThreshold.bestpT:   25 40 55 65 65 85 175 220 330 400

#########For Third Jet Extrapolation###############
thirdJetVariable: pt3
#pt3 Dphijj phiStar pt3OverpTAvg

pt3.Bins: 15 25 35 55 80 300
#50
# 60 100 150 300 500
Dphijj.Bins: 0 20 40 60 80 100 150
phiStar.Bins: 0 20 40 60 80 100 150
pt3OverpTAvg.Bins: 0 0.1 0.3 0.2 0.4 0.6 0.8 1.0

NPV.Bins: 5 8 10 12 14 16 18 20 25 30
mu.Bins: 7 12 15 18 20 22 24 26 29 32 40


#########################################################################
#                  MC: num events & cross-sections
#########################################################################
#J8: pTtruth>2500, J9: pTtruth>3200, J10: pTtruth>3900,

# MC lumi in nb-1
MCLumi:  1

# Nevents = total sum of events(weighted if Sherpa or Powheg) in each sample

# PowhegPythia88 MC15c 13 TeV
PowhegPythia8.SampleNames:         J1               J2             J3            J4          J5         J6         J7        J8         J9
PowhegPythia8.NEvents.p2666:       5376591995600896 69019264614400 1080493998080 19661285376 636408768  48449536   6400130   127377     13417.1
PowhegPythia8.CrossSections.p2666: 421630000.0      48024000.0     1908400.0     173620.0    5259.4     276.12     26.41     0.077298   2.7470E-03
PowhegPythia8.FilterEff.p2666:     0.0034956        0.0004107      0.00014988    1.8654e-05  1.1846e-05 8.3645e-06 6.671e-06 8.3211e-05 9.0501E-05

# Sherpa MC15c 13 TeV
# J7: no eff info (N=955010.5, xsec=4.3735E-02)
Sherpa.SampleNames:         J1          J2        J3        J4        J5        J6        J8         J9
Sherpa.NEvents.p2666:       2930076     1970766   1892493   1984024   1961526   1963836   977439.25  465105
Sherpa.CrossSections.p2666: 20595000.0  107290.0  13075.0   96.075    2.7252    0.20862   0.00033371 0.000058948
Sherpa.FilterEff.p2666:     0.08573     0.1417    0.018529  0.027780  0.018385  0.0086905 0.014507   0.0030686

#########################################################################
#                  Data: GRL, Triggers and Luminosities
#########################################################################
# data trigger lumi in ub-1, in script divide by 1e3 to change it to nb-1

CentralLumiTriggers: HLT_j15 HLT_j25 HLT_j35 HLT_j45 HLT_j60 HLT_j110 HLT_j175 HLT_j260 HLT_j360
ForwardLumiTriggers: HLT_j15_320eta490 HLT_j25_320eta490 HLT_j35_320eta490 HLT_j45_320eta490  HLT_j60_320eta490 HLT_j110_320eta490 HLT_j175_320eta490 HLT_j260_320eta490 HLT_j360_320eta490

# all 2017 runs with standard fill
CentralLumis.2016_v82: 6673.86 14829.4 48640  136107  1881430  15265700  109860000  684644000   6228730000
ForwardLumis.2016_v82: 25205.3 139472  475623 1986940 10064000 193875000 3999230000 25178700000 25178700000
Deliveredlumi.2016_v82: 25178700000
