#!/bin/sh

code=EtaIncerCalibrationAndUncertaintiesBS.C
exec=finalize_calibration.exe

etaBinning="0.3"
jetAlgos="AntiKt4EMPFlow"
jetAlgos="AntiKt4EMTopo"
jetAlgos="AntiKt4LCTopo"
methods="MM"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter"

rm -f $exec
g++ $flagsNlibs -o $exec $code && {

    echo "*********************"
    echo "Compilation successful"
    echo "*********************"

    for meth in $methods ; do
		method=$meth
		for jA in $jetAlgos ; do

	    echo $jA

      ########## November 2015 Claibraiton Need to make backup of this ####
      dataFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/EMTopo/MM/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root
      #/pc2014-data5/jrawling/data16_13TeV/ClosurePartial/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root
      nom_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4EMTopo.root
	    syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/Sherpa_Eta2016v12_AntiKt4EMTopo.root
      #syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/Sherpa_Eta2016v12_AntiKt4EMTopo.root
      outputFilePath=Test/
      #~/2016EtaIntercal/FinalCalibration/EMTopo/CalibrationFiles/
      ########## November 2015 Claibraiton Need to make backup of this ####


     #  	dataFile=/hepgpu1-data3/jrawling/data16_13TeV/NominalLC/data16_13TeV__Eta2016v12_AntiKt4LCTopo.root
     #  	nom_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4LCTopo.root
	    # syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4LCTopo.root
	    dataFile=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/LCTopo/data16_13TeV__Eta0.3_AntiKt4LCTopo.root
      	nom_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_Eta0.3_AntiKt4LCTopo.root
	    syst_mc_File=/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/Sherpa_mc15c/mc15_13TeV_null_Eta0.3_AntiKt4LCTopo.root


	    echo "***************************************************************"
	    echo "   method: $meth -- jetalgo: $jA  "
	    echo "***************************************************************"
	    echo "*********************   inputs   ******************************"
	    echo "***************************************************************"
      mkdir $outputFilePath/plots
	    echo $dataFile
	    echo $nom_mc_File
	    ./$exec $jA $meth $dataFile $nom_mc_File $syst_mc_File $outputFilePath


	    echo "**********************************************************"
	    echo "**********************************************************"
	    echo "*********************** done *****************************"
	done
    done

}
