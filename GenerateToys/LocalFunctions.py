import sys
from ROOT import TLatex,TFile, TDatime, TH1F, TH1D, TH1, TGraph, TGraphErrors, TTree, TCanvas, gROOT, gStyle, gDirectory,TIter, Double, TLegend, THStack, TColor,kAzure,kMagenta,kGreen,kGray,kOrange,kGray,kPink,kBlack,kRed, kWhite,kCyan,TPaletteAxis, gPad, kBlue,kViolet
from numpy import log,arange
from math import sqrt
import array

# from config import verbosity

gROOT.SetBatch()
TH1.SetDefaultSumw2()
def get_hist_in_range(hist, lower_bin,upper_bin, name = ""):
    if name == "":
        name = hist.GetName() + "_clone"
    temp = hist.Clone(name)

    for i in range(1,hist.GetSize()-1):
        if i > lower_bin and i <= upper_bin:
            temp.SetBinContent(i,hist.GetBinContent(i))
        else:
            temp.SetBinContent(i,0)
    return temp

def get_tree(name,root_file):
    htemp = root_file.Get( name )
    if not isinstance(htemp,TTree):
        print "ERROR: failed to get tree ",name," from ", root_file.GetName()
        sys.exit()
    return htemp

def get_hist(name,root_file):
    htemp = root_file.Get( name )
    if not isinstance(htemp,TH1F):
        print "ERROR: failed to get histogram ",name," from ", root_file.GetName()
        sys.exit()
    return htemp

def drawText(x, y, color, tsize, text,angle):
    l = TLatex()
    l.SetTextAngle(angle)
    l.SetTextColor(color)
    l.SetTextSize(tsize)
    l.DrawLatex(x,y,text)

def compare(hist1, hist2):
    return int(hist1.Integral()) - int(hist2.Integral())
def deltaR(eta1,phi1,eta2,phi2):
    return sqrt( pow(eta1-eta2,2) + pow(phi1-phi2,2))
def GetQuantiles(hist,quants):
    quants = array.array('d', [ quant for quant in quants])
    q = array.array('d', [0.0]*len(quants))
    hist.GetQuantiles(len(quants), q, quants)
    return q
def get_sorted_stack(stack_name, hist_list):
    hist_list.sort()
    hist_stack = THStack(stack_name,"")
    for hist in hist_list:
        hist_stack.Add(hist)
    return hist_stack

def format_hist(hist,index ):
    colors = [kRed,kBlue,kMagenta,kGreen-2,kGray,kOrange, kBlack, kWhite,kCyan,kAzure-2,kOrange-4,kViolet-6,kCyan+4,kMagenta+3,kGreen+3,kGray,kOrange+3]
    hist.SetFillColor(colors[index] )
    hist.SetLineColor(colors[index] + 3 )
    return hist

def get_hist_1d(file_name, ntupname, var, rangex, histWeight,scale=1.):
    # print "DRAWING: ",var, "in range: ", rangex," with weight: ",histWeight,"\n"
    TH1.SetDefaultSumw2()
    ftemp    = open_file(file_name)
    ntuptemp = ftemp.Get(ntupname)

    # print " From ",file_name," Finding hist: ",var , " with weight: ", histWeight
    ntuptemp.Draw(var+">>htemp("+rangex+")", histWeight, "e")
    htemp = gDirectory.Get("htemp")
    htemp.SetDirectory(0)

    htemp.Scale(scale)
    ftemp.Close()
    return htemp

def find_ratio(hist1,hist2, color_index = 1):
    ratio = hist1.Clone()
    ratio.Divide(hist2)
    return ratio

def open_file(file_name, option="READ" ):
    f = TFile(file_name,option)
    if not f:
        print "ERROR: failed to open file: ",file_name
        sys.exit()
    return f


def NormalizeHist( hist, norm=1.0):
    tot = hist.Integral()

    if tot<=0:
        print "Something is wrong with histogram"
        return hist
    else:
        hist.Scale(norm/tot)
        return hist

def NormalizeStack ( stack, scale):
    if scale <= 0:
        return stack

    histKeys = stack.GetHists()
    next = TIter(histKeys)
    hist = next()

    while hist:
        hist.Scale(1.0/scale)
        hist = next()

    return stack
