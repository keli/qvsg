'''ROOT based analysis that takes in a signal, background processes and data (S+B)
   histogram output. The following is then performed:

   1. Generates n_pe (min = 1000) Pseudo experiments by poisson fluctuating each bin
      of the input S, B and S+ B histograms

   2. Log like-lihood fit of each bin using a possion distributiuon as the
      likelihood function for both signal and background

   3. q = -2 * log of likelihood ratio of signal/backgrounddistirubtions are
      evaluated for signal and singal+background and expected CLs evaluated

TODO:
    1. Add q evaluation into multithreaded
    2. Run over a set of signal histograms
    3. Produce plots showing limit
'''
import ROOT as r
from time import time
from math import log, factorial, sqrt
from LocalFunctions import open_file, GetQuantiles,get_hist_in_range, get_hist
from multiprocessing import Pool as ThreadPool
import itertools
import array
import sys
n_pe = 1000
n_cores = 40

def multi_run_wrapper(args):
   return generate_pseudoexperiment(*args)

def generate_pseudoexperiment(seed,hist):
     pseudo_experiment = hist.Clone("toy_" + str(seed) + "_" + hist.GetName()  )
     for j in range(1,hist.GetSize()-1):
         mean              = hist.GetBinContent(j)
         if mean == 0:
            continue

         rand              = r.TRandom( int( seed )  )
         new_content       = rand.Poisson(mean)
         pseudo_experiment.SetBinContent(j,new_content)
     return pseudo_experiment

def GeneratePseudoeExperiment_MT(hist):
    pool = ThreadPool(n_cores)
    pseudo_experiments = pool.map(multi_run_wrapper, [(i, hist) for i in range(n_pe) ] )
    return pseudo_experiments

def GeneratePseudoeExperiment(hist):
    pseudo_experiments = []
    for i in range(n_pe):
        #cycle over each bin and poisson flucate,
        #i.e draw a number from a poisson distriubtion with mu = entries in that bin
        pseudo_experiment = hist.Clone(hist.GetName() + "_pe_" + str(i) )
        for j in range(1,hist.GetSize()-1):
            mean              = hist.GetBinContent(j)
            if mean == 0:
              pseudo_experiment.SetBinContent(j,0)
              continue

            rand              = TRandom( int( time()  ))
            new_content       = rand.Poisson(mean)

            pseudo_experiment.SetBinContent(j,new_content)

        pseudo_experiments.append(pseudo_experiment)

    return pseudo_experiments

if __name__ == "__main__":
    #get the histograms
    r.gStyle.SetOptStat(0)

    if len(sys.argv) < 3: 
      sys.exit("ERROR: Please run with 3 arguments specificy: input_file_name output_file_name jet_collection")
    else:
      print "Generating ", n_pe," toys."
      print "\t Input file    : \t", sys.argv[1]
      print "\t Output file   : \t", sys.argv[2]
      print "\t Jet Collection: \t", sys.argv[3]


    #get the list of histograms in the file 
    file_name = str(sys.argv[1])
    file       = r.TFile(file_name)

    #best if the histograms are stored in the file's memory 
    out_file_name = str(sys.argv[2])
    out_file = r.TFile(out_file_name,"RECREATE")
    jet_collection = str(sys.argv[3])

    if not file.IsOpen():
      print "FAILED TO OPEN FILE", file_name
      sys.exit() 

    if not out_file.IsOpen():
      print "FAILED TO OPEN OUTPUT FILE", out_file_name
      sys.exit() 

    print "Opening file: ", file_name
    hists = []
    for h in file.GetDirectory(jet_collection).GetListOfKeys():
      hists.append(h.ReadObj())
    print "Found ", len(hists), "histograms."

    #prepearet the file in anticipation....
    n = 0
    for n in range(n_pe):
      out_file.cd()
      out_file.mkdir("toy_" + str(n))
      n += 1
    out_file.Close()

    #generate the pseudo_experiments for each histogram  
    pseudo_experiments = []
    n_hist = 0    
    for h in hists:
      print "Generating pseudo experiments for hist ", str(n_hist)  +"/" + str(len(hists))
      out_file = r.TFile(out_file_name,"UPDATE")
      pseudo_experiments = GeneratePseudoeExperiment_MT(h)
      n = 0
      for h in pseudo_experiments:
        out_file.cd("toy_" + str(n))
        h.Write()
        n += 1
      n_hist += 1
      out_file.Close()

    print "Generated ", n_pe, " pseudo experiments."


    #save each histogram 
    for pe_set in pseudo_experiments:
      n = 0
      for h in pe_set:
        out_file.cd("toy_" + str(n))
        h.Write()
        n += 1


    out_file.Close()
    print "Saved to: ",out_file_name
    file.Close()
    print "Completed succesfully."
