#Python script to automate the succesive iterations of the eta-intercalibration

#imports
import os
import subprocess

#How many iterations to perform
nIterations = 9
outPutFolder = "/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/3DHistograms/IterativeTest_160428/"
inputNTuple = "/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/data15_13TeV_periodsDEFGHJ.root"
#inputNTuple = "/hepgpu1-data3/jrawling/Micro-xAODs/data15_13TeV/p2440/periodH/data15_13TeV_periodH_micro-xAOD.root"
#The starting point is our n-tuple, settings file and directory
HistogramGenerator = "/afs/hep.man.ac.uk/u/jrawling/ThirdJetExtrapolation/GenerateHistograms/histogramGenerator.exe"
HistogramGeneratorSettings = "/afs/hep.man.ac.uk/u/jrawling/ThirdJetExtrapolation/GenerateHistograms/utils/settings.config"

MatrixMinimizer = "/afs/hep.man.ac.uk/u/jrawling/ThirdJetExtrapolation/MinimiseResponseMatrix/matrix_minimization.exe"
MinimizerConfig = "/afs/hep.man.ac.uk/u/jrawling/ThirdJetExtrapolation/MinimiseResponseMatrix/utils/Dijet_xAOD.config"
JetCollection = "AntiKt4EMTopo"
productionTag = "25nsFinal"
etaBinning = "Eta2016v12"
prefix="nominal"

CalibrationTool = "/afs/hep.man.ac.uk/u/jrawling/CLEAN_INTERCALAREA/CalibrationAndUncertainties/finalize_calibration.exe"
NominalMCMinimizedResults = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercal/PowhegPythiaMMOutput/PowhegPythia_Eta2016v12_AntiKt4EMTopo.root"
SystematicMCMinimizedResults = "/afs/hep.man.ac.uk/u/jrawling/EtaIntercal/SherpaMMOutputSherpa_Eta2016v12_AntiKt4EMTopo.root"

MatrixMultiplier = "/afs/hep.man.ac.uk/u/jrawling/CLEAN_INTERCALAREA/helperScripts/MultiplyCalibrations/HistogramMultiplier.exe"
histogramName = JetCollection+"_EtaInterCalibration"

makeDirectories = True
previousCalibration = ""
for n in range(0,nIterations):
    print "============================================\n"
    print 'Performing {0}th eta-intercalibration'.format(n)
    #########Histogram Generation###########
    #Step 1a: Create the only the fly settings for 3D histogram generation.
    onTheFlySettings="JetCollections="+JetCollection+",applyCalibration="
    if n != 0:
        onTheFlySettings += "1,calibrationFilePath=" + previousCalibration
    else:
        onTheFlySettings += "0"

    #Step 1b: Create the directories we are going to use.
    HistogramPath = outPutFolder + "Level{0}Calibration".format(n)
    if makeDirectories:
        os.mkdir(HistogramPath)

    #Step 1b: Run the histogram generation
    HistogramGenerationCall = HistogramGenerator + " " + HistogramGeneratorSettings+ " " + inputNTuple +  " " + HistogramPath + " " + onTheFlySettings
    print "\n---------Generating Histograms--------------"
    print "Calling:\n" +  HistogramGenerationCall +"\n"
    print "---------------------------------------------\n"
    retcode = subprocess.call([HistogramGenerator,HistogramGeneratorSettings,inputNTuple,HistogramPath,onTheFlySettings])

    ##########Matrix Minimization###########
    #Step 2a: Create the directories
    MatrixMinimizationPath = HistogramPath + "/MMoutput"
    if makeDirectories:
        os.mkdir(MatrixMinimizationPath)

    #Step 2b: Run the minimization

    MatrixMinimizationOutput = MatrixMinimizationPath + "/" + JetCollection + "_data15_13TeV_25ns_" + etaBinning + "_level" + str(n) + "Calibration.root"
    HistogramOutputFile = HistogramPath + "/Data_AntiKt4EMTopo_nominal_3DHistograms.root"
    MinimizationCall = MatrixMinimizer + " " + " "+ MinimizerConfig+  JetCollection + " " + productionTag + " " + etaBinning + " " + HistogramOutputFile + " " + MatrixMinimizationOutput
    print "\n-----------Minimizing Matrix---------------"
    print "Calling:\n" +  MinimizationCall +"\n"
    print "---------------------------------------------\n"
    retcode = subprocess.call([MatrixMinimizer,MinimizerConfig,JetCollection,productionTag,etaBinning,HistogramOutputFile,MatrixMinimizationOutput,prefix])

    ##########Create the Calibration##########
    #Step 3a: Create the directories
    CalibrationFilePath = HistogramPath + "/Calibrations/"
    if makeDirectories:
        os.mkdir(CalibrationFilePath)

    #Step 3b: Run the calibration tool
    CalibraitonCall = CalibrationTool + " " + JetCollection + " " + MatrixMinimizationOutput + " " + NominalMCMinimizedResults + " " + SystematicMCMinimizedResults + " " + CalibrationFilePath
    print "\n---------Deriving Calibraiton-------------"
    print "Calling:\n" +  CalibraitonCall +"\n"
    print "---------------------------------------------\n"
    retcode = subprocess.call([CalibrationTool,JetCollection,"MM",MatrixMinimizationOutput,NominalMCMinimizedResults,SystematicMCMinimizedResults,CalibrationFilePath])
    CalibraitonOutput = CalibrationFilePath + "Feb16_EtaIntercalibration13TeV25ns_Eta2016_MM_" + JetCollection +"_Calibration.root"

    #Step 3c: Multiply the histogram with the previous result and store this result
    if n == 0:
        previousCalibration = CalibraitonOutput
        print 'Inter-calibariton found.'
        print "============================================\n"
        continue

    print "Multiyplying found calibraiton with previous calibraiton."

    NewCalibration = CalibrationFilePath + "Level" + str(n) + "Calibration.root"
    MultiplyCall = MatrixMultiplier + " " + " " + CalibraitonOutput + " " + previousCalibration + " " + NewCalibration + " " + histogramName + " " + histogramName

    print MultiplyCall
    print "\n--------Multiplying Calibrations----------"
    print "Calling:\n" +  MultiplyCall +"\n"
    print "------------------------v---------------------\n"
    retcode = subprocess.call([MatrixMultiplier,CalibraitonOutput,previousCalibration,NewCalibration,histogramName,histogramName])
    previousCalibration = NewCalibration

    print 'Inter-calibariton found.'
    print "============================================\n"
