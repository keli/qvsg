#!/bin/python
# -*- coding: utf-8 -*-

import math
from array import array
import sys, os
from conf import *
from ROOT import  gStyle, TList, TF1, TH1D, kBlue, kWhite, kRed, TGraphErrors, TGraphAsymmErrors, Double, TH1F, TLegend, TFile, TCanvas, TGraph, TSystem, gDirectory, TLatex, TMultiGraph,TGraph2DErrors, kOrange

def GausFitHisto(histo):
  gaus = TF1("fit","gaus",0.9,1.15)
  fitCol = kBlue-3
  gaus.SetLineColor(fitCol)
  gaus.SetLineWidth(3)
  print histo.GetEffectiveEntries()
  if (histo.GetEffectiveEntries()>10):
      histo.Fit(gaus,"RQ","P")
      mid=gaus.GetParameter(1)
      sig=gaus.GetParameter(2)
      Nsig=2.0
      gaus.SetRange(mid-sig*Nsig,mid+sig*Nsig)
      histo.Fit(gaus,"RQ","P")
 #     mid=gaus.GetParameter(1)
 #     sig=gaus.GetParameter(2)
 #     gaus.SetRange(mid-sig*Nsig,mid+sig*Nsig)
 #     histo.Fit(gaus,"RQ","P")
  print "chi squared ",gaus.GetChisquare()
  histo.SetMarkerStyle(3)
  if (abs(histo.GetMean()-gaus.GetParameter(1))>0.05 ):
     print "WARNING: Distribution and fit's mean  disagree by a signficant amount: histo's mean: ",histo.GetMean(), " and parameter mean", gaus.GetParameter(1)
     print "\t\t Using the histograms width instead"
     gaus.SetParameter(1,histo.GetMean())
     gaus.SetParameter(2,histo.GetBinWidth(22))
  histo.SetXTitle("R")
  #fileSave.cd()
  #histo.Draw("e")
  if (histo.GetEffectiveEntries()==0):
     gaus.SetParameter(1,-99)
     gaus.SetParameter(2,0)
  #R = histo.GetMean()
  #if R is 1:
   #  gaus.SetParameter(1,1)
   #  gaus.SetParameter(2,0)
  return gaus

def GetCalibration2DGraph(hist_list,jetAlgo):
  f = TFile.Open("/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4EMTopo.root")
  numInv=None
  #f.cd(jetAlgo)
  calibGraph=TGraph2DErrors()
  avg_pt = gDirectory.Get(jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+method)
  avg_eta = gDirectory.Get(jetAlgo+"_AvgEta_vs_EtaPtAvg_"+method)
  Npt=avg_pt.GetNbinsY()
  Neta=avg_pt.GetNbinsX()
  print "Neta  is ", Neta, "Npt is "
  for ipt in range(1, Npt+1):
    ptmin=avg_pt.GetYaxis().GetBinLowEdge(ipt)
    ptmax = avg_pt.GetYaxis().GetBinLowEdge(ipt+1)

    if ptmax>0 and ptmax<=ptmin:
       continue

    hR=hist_list[ipt-1]
    if hR.GetNbinsX()!=Neta:
        print "Binning problem"

    for ieta in range(1, Neta+1):
      n=calibGraph.GetN()
      R=hR.GetBinContent(ieta)

      if R < 0:
        calibGraph.SetPoint(n,-99,-99,-99)
        print "discarded point ",n, "with pt", ipt
        continue
      dR       = hR.GetBinError(ieta);
      pt       = avg_pt.GetBinContent(ieta,ipt)
      err_pt   = avg_pt.GetBinError(ieta,ipt)
      eta      = avg_eta.GetBinContent(ieta,ipt)
      err_eta  = avg_eta.GetBinError(ieta,ipt)

      if pt==0:
         continue
      etamin= avg_pt.GetXaxis().GetBinLowEdge(ieta)
      etamax = avg_pt.GetXaxis().GetBinLowEdge(ieta+1)
      if numInv:
         pt *= 2.0*R/(R+1)
         err_pt = math.sqrt( math.pow(err_pt*2.0*R/(R+1),2) + math.pow(2.0*pt/(R+1)/(R+1)*dR,2 ) )
      print "set points with n = ", n, " pt= " , pt, " eta = ", eta, " R ", R
      if R < 9.72815e-30:
         calibGraph.SetPoint(n,-pt,eta,1.005)
      calibGraph.SetPoint(n,pt,eta,R);
      calibGraph.SetPointError(n,err_pt,err_eta,dR)
  f.Close()
  return calibGraph;


gStyle.SetOptFit(0011)
jetAlgo  = sys.argv[1]
method = sys.argv[2]
nToys = int(sys.argv[3])

config = {}
#execfile("plot.conf",config)
etabins = {}
for bin in ptbins:
    etabins[bin]=maxetabins

Histograms=[]
toy_data={}
toy_data_nominal={}
toy_mc_nominal={}
toy_mc={}
Response_histograms={}
Response_histograms_nominal={}
Response_histograms_DATA={}
Response_histograms_MC={}
Response_histograms_DATA_shift={}
Response_histograms_MC_shift={}

R_nominal_MC={}
Rerr_nominal_MC={}
R_nominal_DATA={}
Rerr_nominal_DATA={}
c={}
cerr={}
NomHistograms=[]
ShiftMCHistograms=[]
ShiftDATAHistograms=[]
MCHistograms=[]
DATAHistograms=[]

toydR={}
toymR={}
toymR_nominal={}
toydR_nominal={}
for index,ptbin in enumerate(ptbins):
    for ishift in shifts:
        for i,etabin in enumerate(etabins[ptbin],1):
            toymR[str(i)+ptbin]=-99
            toydR[str(i)+ptbin]=-99
            Response_histograms["R_"+ishift+"_"+ptbin+"etabin"+str(i)]=TH1F("R_"+ishift+"_"+ptbin+"etabin"+str(i) , "R_"+ishift+"_"+ptbin+"etabin"+str(i),80,0.90, 1.15)
            Response_histograms_DATA["R_"+ishift+"_"+ptbin+"etabin"+str(i)]=TH1F("R_DATA_"+ishift+"_"+ptbin+"etabin"+str(i) , "R_DATA_"+ishift+"_"+ptbin+"etabin"+str(i),80,0.90, 1.15)
            Response_histograms_MC["R_"+ishift+"_"+ptbin+"etabin"+str(i)]=TH1F("R_MC_"+ishift+"_"+ptbin+"etabin"+str(i) , "R_MC_"+ishift+"_"+ptbin+"etabin"+str(i),80,0.90, 1.15)
            Response_histograms_DATA_shift["R_"+ishift+"_"+ptbin+"etabin"+str(i)]=TH1F("R_DATA_shift_"+ishift+"_"+ptbin+"etabin"+str(i) , "R_DATA_shift"+ishift+"_"+ptbin+"etabin"+str(i),80,0.90, 1.15)
            Response_histograms_MC_shift["R_"+ishift+"_"+ptbin+"etabin"+str(i)]=TH1F("R_MC_shift_"+ishift+"_"+ptbin+"etabin"+str(i) , "R_MC_shift"+ishift+"_"+ptbin+"etabin"+str(i),80,0.90, 1.15)
    for i,etabin in enumerate(etabins[ptbin],1):
            Response_histograms_nominal["R_Nominal_"+ptbin+"etabin"+str(i)]=TH1F("R_Nominal_"+ptbin+"etabin"+str(i) , "R_Nominal_"+ptbin+"etabin"+str(i),80,0.90, 1.15)

#retrieves the Response distributions for the MC nominal (central value)
f_mcNominal = TFile.Open("/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/MC/PowhegPythia8_mc15c/mc15_13TeV_null_Eta2016v12_AntiKt4EMTopo.root")
f_mcNominal.cd(jetAlgo)
for key in gDirectory.GetListOfKeys():
    histname = key.GetName()
    for indx,ptbin in enumerate(ptbins):
        if "pt"+ptbin+"_R_"+method in histname:
            histo = gDirectory.Get(histname)
            for ieta,etabin in enumerate(etabins[ptbin],1):
                R_nominal_MC[str(ieta)+ptbin]=histo.GetBinContent(ieta)
                Rerr_nominal_MC[str(ieta)+ptbin]=histo.GetBinError(ieta)
f_mcNominal.Close()
#retrieves the Response distributions for the DATA nominal (central value)
f_DataNominal = TFile.Open("/afs/hep.man.ac.uk/u/jrawling/2016EtaIntercal/FinalCalibration/EMTopo/MM/data16_13TeV__Eta2016v12_AntiKt4EMTopo.root")
f_DataNominal.cd(jetAlgo)
for key in gDirectory.GetListOfKeys():
    histname = key.GetName()
    for indx,ptbin in enumerate(ptbins):
        if "pt"+ptbin+"_R_"+method in histname:
             histo = gDirectory.Get(histname)
             for ieta,etabin in enumerate(etabins[ptbin],1):
                 R_nominal_DATA[str(ieta)+ptbin]=histo.GetBinContent(ieta)
                 Rerr_nominal_DATA[str(ieta)+ptbin]=histo.GetBinError(ieta)
f_DataNominal.Close()

for ishift in shifts:
     graphy=0
     graph_data=0
     graph_mc=0
     for index,ptbin in enumerate(ptbins):
         Histograms.append(TH1F(ptbins[index]+"shift","shift",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
         NomHistograms.append(TH1F(ptbins[index]+"Nominal","nom",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
         ShiftMCHistograms.append(TH1F(ptbins[index]+"MCshift","shift",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
         ShiftDATAHistograms.append(TH1F(ptbins[index]+"DATAshift","shift",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
         MCHistograms.append(TH1F(ptbins[index]+"MC","shift",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
         DATAHistograms.append(TH1F(ptbins[index]+"DATA","shift",len(etabins['25to40'])-1 , array('d',etabins['25to40'])))
     for i in range(1, nToys):
          #retrieve histograms for the shifted toys data
          f_data= TFile.Open("/hepgpu1-data3/jrawling/data16_13TeV/"+ishift+"/MMOut/mc15_13TeV_"+ishift+"_toy"+str(i)+"_Eta2016v12_25nsFinal_AntiKt4EMTopo.root")
          if not f_data:
             continue
          f_data.cd(jetAlgo)
          for key in gDirectory.GetListOfKeys():
               histname = key.GetName()
               for indx,ptbin in enumerate(ptbins):
                  if "pt"+ptbin+"_R_"+method in histname:
                     histo_datatoy = gDirectory.Get(histname)
                     toy_data[ptbin]=histo_datatoy.Clone()
                     for ieta,etabin in enumerate(etabins[ptbin],1):
                         toydR[str(ieta)+ptbin]=toy_data[ptbin].GetBinContent(ieta)
          f_data.Close()
          #retrieve histograms for the nomninal data toys
          f_data_nominal= TFile.Open("/hepgpu1-data3/jrawling/data16_13TeV/Nominal/MMOut/mc15_13TeV_nominal_toy"+str(i)+"_Eta2016v12_25nsFinal_AntiKt4EMTopo.root")
          if not f_data_nominal:
             continue
          f_data_nominal.cd(jetAlgo)
          for key in gDirectory.GetListOfKeys():
               histname = key.GetName()
               for indx,ptbin in enumerate(ptbins):
                  if "pt"+ptbin+"_R_"+method in histname:
                     histo_datatoy_nominal = gDirectory.Get(histname)
                     toy_data_nominal[ptbin]=histo_datatoy_nominal.Clone()
                     for ieta,etabin in enumerate(etabins[ptbin],1):
                         toydR_nominal[str(ieta)+ptbin]=toy_data_nominal[ptbin].GetBinContent(ieta)
          f_data_nominal.Close()
          #retrieve histograms for the shifted toys MC
          f_mc = TFile.Open("/hepgpu1-data3/jrawling/mc15_13TeV/"+ishift+"/MMOut/mc15_13TeV_"+ishift+"_toy"+str(i)+"_Eta2016v12_p1562_AntiKt4EMTopo.root")
          if not f_mc:
             continue
          f_mc.cd(jetAlgo)
          for key in gDirectory.GetListOfKeys():
              histname = key.GetName()
              for indx,ptbin in enumerate(ptbins):
                    if "pt"+ptbin+"_R_"+method in histname:
                        histo_mctoy = gDirectory.Get(histname)
                        toy_mc[ptbin]=histo_mctoy.Clone()
                        for ieta,etabin in enumerate(etabins[ptbin],1):
                            toymR[str(ieta)+ptbin]=toy_mc[ptbin].GetBinContent(ieta)
          #retrieve histograms for the nominal toys MC
          f_mc_nominal= TFile.Open("/hepgpu1-data3/jrawling/mc15_13TeV/Nominal/MMOut/mc15_13TeV_nominal_toy"+str(i)+"_Eta2016v12_p1562_AntiKt4EMTopo.root")
          if not f_mc_nominal:
             continue
          f_mc_nominal.cd(jetAlgo)
          for key in gDirectory.GetListOfKeys():
              histname = key.GetName()
              for indx,ptbin in enumerate(ptbins):
                    if "pt"+ptbin+"_R_"+method in histname:
                        histo_mctoy_nominal = gDirectory.Get(histname)
                        toy_mc_nominal[ptbin]=histo_mctoy_nominal.Clone()
                        for ieta,etabin in enumerate(etabins[ptbin],1):
                            toymR_nominal[str(ieta)+ptbin]=toy_mc_nominal[ptbin].GetBinContent(ieta)
          for indx,ptbin in enumerate(ptbins):
              for ieta,etabin in enumerate(etabins[ptbin],1):
                  if toymR[str(ieta)+ptbin]==-99:
                     continue
                  c=toymR[str(ieta)+ptbin]/toydR[str(ieta)+ptbin]
                  c_nominal=toymR_nominal[str(ieta)+ptbin]/toydR_nominal[str(ieta)+ptbin]
                  Response_histograms_nominal["R_Nominal_"+ptbin+"etabin"+str(ieta)].Fill(c_nominal)
                  Response_histograms_DATA["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Fill(toydR[str(ieta)+ptbin]/toydR_nominal[str(ieta)+ptbin])
                  Response_histograms_DATA_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Fill(toydR[str(ieta)+ptbin])
                  Response_histograms_MC_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Fill(toymR[str(ieta)+ptbin])
                  Response_histograms_MC["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Fill(toymR[str(ieta)+ptbin]/toymR_nominal[str(ieta)+ptbin])
                  Response_histograms["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Fill(c)
          f_mc.Close()
          f_mc_nominal.Close()
     for indx,ptbin in enumerate(ptbins):
         for ieta,etabin in enumerate(etabins[ptbin],1):
             fit_x=GausFitHisto(Response_histograms["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)])
             fit_nom=GausFitHisto(Response_histograms_nominal["R_Nominal_"+ptbin+"etabin"+str(ieta)])
             fit_dat=GausFitHisto(Response_histograms_DATA["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)])
             fit_mc=GausFitHisto(Response_histograms_MC["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)])
             fit_datR=GausFitHisto(Response_histograms_DATA_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)])
             fit_mcR=GausFitHisto(Response_histograms_MC_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)])
             Histograms[indx].SetBinContent(ieta,fit_mcR.GetParameter(1)/fit_datR.GetParameter(1))
             Histograms[indx].SetBinError(ieta,fit_x.GetParameter(2))
             NomHistograms[indx].SetBinContent(ieta,fit_nom.GetParameter(1))
             NomHistograms[indx].SetBinError(ieta,fit_nom.GetParameter(2))
             ShiftMCHistograms[indx].SetBinContent(ieta,fit_mc.GetParameter(1))
             ShiftMCHistograms[indx].SetBinError(ieta,fit_mc.GetParameter(2))
             ShiftDATAHistograms[indx].SetBinContent(ieta,fit_dat.GetParameter(1))
             ShiftDATAHistograms[indx].SetBinError(ieta,fit_dat.GetParameter(2))
             MCHistograms[indx].SetBinContent(ieta,fit_mcR.GetParameter(1))
             MCHistograms[indx].SetBinError(ieta,fit_mcR.GetParameter(2))
             DATAHistograms[indx].SetBinContent(ieta,fit_datR.GetParameter(1))
             DATAHistograms[indx].SetBinError(ieta,fit_datR.GetParameter(2))
             print "calibration data ", indx, " and bin ", ieta, " " , fit_mcR.GetParameter(1)/fit_datR.GetParameter(1)

     #Getting calibration graphs in the format needed by the CalibrationUncertainties package
     graphy=GetCalibration2DGraph(Histograms, jetAlgo)
     graph_data=GetCalibration2DGraph(DATAHistograms, jetAlgo)
     graph_mc=GetCalibration2DGraph(MCHistograms, jetAlgo)
     fileSave = TFile("Shifted_"+ishift+"_Eta0.3_"+method+"_"+jetAlgo+".root", "recreate")
     fileSave.cd()
     canv = TCanvas()
     for indx,ptbin in enumerate(ptbins):
         for ieta,etabin in enumerate(etabins[ptbin],1):
            Response_histograms_DATA_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Write()
            Response_histograms_MC_shift["R_"+ishift+"_"+ptbin+"etabin"+str(ieta)].Write()
     for indx,histi in enumerate(Histograms):
        histi.GetXaxis().SetRangeUser(-4.5, 4.5)
        histi.GetYaxis().SetTitle("Shifted calibration MC/DATA")
        histi.GetYaxis().SetRangeUser(0.92, 1.08)
        histi.GetXaxis().SetTitle("eta")
        histi.SetTitle(" ");
        histi.SetMarkerStyle(24)
        histi.SetLineWidth(1)
        histi.Draw()
        NomHistograms[indx].SetMarkerStyle(24)
        NomHistograms[indx].SetLineWidth(1)
        NomHistograms[indx].SetLineColor(kRed)
        NomHistograms[indx].Draw("same")
        NomHistograms[indx].Write()
        histi.Write()
        canv.Print(histi.GetName()+"_"+jetAlgo+"_"+method+"_"+ishift+".pdf")
        canv.Update()
        DATAHistograms[indx].GetXaxis().SetRangeUser(-4.5, 4.5)
        DATAHistograms[indx].GetYaxis().SetTitle("Systematic shifts")
        DATAHistograms[indx].GetXaxis().SetTitle("eta")
        DATAHistograms[indx].SetTitle(" ");
        DATAHistograms[indx].SetMarkerColor(kBlue)
        DATAHistograms[indx].SetLineColor(kBlue)
        DATAHistograms[indx].SetMarkerStyle(24)
        DATAHistograms[indx].SetLineWidth(2)
        DATAHistograms[indx].Draw()
        MCHistograms[indx].SetMarkerStyle(26)
        MCHistograms[indx].SetMarkerColor(kOrange+8)
        MCHistograms[indx].SetLineWidth(2)
        MCHistograms[indx].SetLineColor(kOrange+8)
        MCHistograms[indx].Draw("same")
        MCHistograms[indx].Write()
        DATAHistograms[indx].Write()
        leg = TLegend( 0.7,0.8,0.89,0.95)
        leg.SetFillColor(kWhite)
        leg.SetLineColor(kWhite)
        leg.AddEntry(DATAHistograms[indx], "DATA", "L")
        leg.AddEntry(MCHistograms[indx], "MC", "L")
        leg.Draw()
        canv.Print(histi.GetName()+"_DATAMC_"+jetAlgo+"_"+method+"_"+ishift+".pdf")
        canv.Update()
     graphy.SetName("Shifted_Calibration_Graph")
     graphy.Write();
     graph_data.SetName("Shifted_Calibration_Graph_Data")
     graph_mc.SetName("Shifted_Calibration_Graph_MC")
     graph_mc.Write()
     graph_data.Write();
     canv.Update()
     Histograms=[]
     NomHistograms=[]
     ShiftMCHistograms=[]
     ShiftDATAHistograms=[]
     MCHistograms=[]
     DATAHistograms=[]
     fileSave.Write()
     print "Written to file: " + "Shifted_"+ishift+"_Eta2016v12_"+method+"_"+jetAlgo+".root"
     fileSave.Close()
