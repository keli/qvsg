#!/bin/bash
USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=Data.root

##
INDS=user.jrawling.data16_13TeV.light.periodA_highTrigRate.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodA_lowTrigRate.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodB.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodC.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodD.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodE.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodF.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodG.20161005.0938_EtaInterCal_3DHistos_data.root,user.jrawling.data16_13TeV.light.periodI.20161005.0938_EtaInterCal_3DHistos_data.root
OUTDS=data16_13TeV.Nominal.3DHists

echo "Submitting to "${INDS}
echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

prun --inDS=${INDS}\
     --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
     --outputs=${OUTPUTS}\
     --exec="scripts/gridExec.sh %IN"\
     --useRootCore\
     --excludedSite=${BLACKLIST}\
     --nGBPerJob=5\
     --tmpDir=/tmp\
     --extFile=
