#include "HistogramGenerator/Histogram3DFactory.h"


int main(int argc, char **argv) {
  //Read in setting names etc. from the given parameters of this programme.
	if(argc < 4)
		error("Please provide 3 parameters to this programme: configurationFile inputFile outputFile");
	std::string configFN           = argv[1];
  std::string inputFileName      = argv[2];
  std::string outputFilePath      = argv[3];
  std::string onTheFlySettings    = (argc > 4 ? argv[4] : " ") ;

  PrintTime();
	if(outputFilePath=="null")outputFilePath="";
	vector<TString> inputFileNames = vectorise(TString(inputFileName.c_str()),",");
  Histogram3DFactory histogramFactory(configFN,inputFileNames,outputFilePath,onTheFlySettings);

  histogramFactory.Create3DHistograms();

  PrintTime();
  return 0;
} // main
