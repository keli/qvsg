#include "HistogramGenerator/Histogram3DFactory.h"
#include <algorithm>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <TRandom3.h>

Histogram3DFactory::Histogram3DFactory(std::string configFileName, vector<TString> inputFileNames, std::string outputFilePath, std::string onTheFlySettings):
  m_outputFilePath(outputFilePath),
  m_configFileName(configFileName),
  m_inputFileNames(inputFileNames),
  m_onTheFlySettings(onTheFlySettings),
  m_isMC(false)
{
  //Ignore some warnings from ROOT.
	gErrorIgnoreLevel=2000;
  m_doNominal=true;
  ReadSettings();
  DisplayWelcomeMessage();
}



void Histogram3DFactory::DisplayWelcomeMessage(){
  std::cout << "========================================================" << std::endl;
  std::cout << "=============3D histogram generation tool===============" << std::endl;
  std::cout << "========================================================" << std::endl;
  std::cout << "Input file(s): \t" << std::endl;
  for(const auto& inputFile : m_inputFileNames)
    std::cout << "\t " << inputFile << std::endl;

  std::cout << "Output folder: \t" << m_outputFilePath << std::endl;
  std::cout << "Settings file: \t" << m_configFileName << std::endl;
  std::cout << "Mode: " << std::endl;
    if(m_doNominal ) std::cout << "NOMINAL" << std::endl;
    if(m_doSystematics ) std::cout << "SYSTEAMTICS" << std::endl;
    if(m_doThirdJetExtrapolation ) std::cout << "RADIATION EXTRAPOLATION" << std::endl;
    if(m_doBootStrap ) std::cout << "BOOTSTRAP" << std::endl;
    if(m_doNPVBinning ) std::cout << "NPV" << std::endl;
    if(m_doMuBinning ) std::cout << "MU" << std::endl;
  std::cout << "========================================================" << std::endl;
}

void Histogram3DFactory::ReadSettings(){

  m_settings =OpenSettingsFile(m_configFileName);
  if(!m_settings){
    std::cerr << "Failed to open settings file: "<< m_configFileName << std::endl;
    abort();
  }
  if(m_onTheFlySettings != " "){
    vector<TString> otf_settings = vectorise(m_onTheFlySettings, ",");
    for (const auto& setting : otf_settings)
      m_settings->SetValue(setting);
  }
	m_jetCollections = vectorise(m_settings->GetValue("JetCollections",""));
  m_doNominal                = m_settings->GetValue("doNominal", false);
  m_doBootStrap              = m_settings->GetValue("doBootStrap", false);
  m_doThirdJetExtrapolation  = m_settings->GetValue("doThirdJetExtrapolation", false);
  m_doNPVBinning             = m_settings->GetValue("doNPVBinning",false);
  m_doMuBinning              = m_settings->GetValue("doMuBinning",false);
  m_doSystematics            = m_settings->GetValue("doSystematics",false);
  m_thirdJetBin  = m_settings->GetValue("thirdJetBin",0);
  if(m_doNominal){
    m_thirdJetVariable = "";
    m_thirdJetVariableRanges.push_back(" ");
  }else
  if(m_doThirdJetExtrapolation){
    //configure the ranges that we will be binning the 3d Histograms in for the zero rad extrapolation.
    m_thirdJetVariable      = m_settings->GetValue("thirdJetVariable", "pt3");
    m_thirdJetVariableBins  = vectoriseD(m_settings->GetValue(TString(m_thirdJetVariable) + ".Bins",""));

    if(m_thirdJetVariableBins.size() < 2) {
      std::cerr << "Thrid Jet variable has too few bins - check settings file: "<< m_configFileName << std::endl;
      abort();
    }

    for(int i = 0; i< m_thirdJetVariableBins.size()-1; i++)
      m_thirdJetVariableRanges.push_back(Form("%.3fTo%.3f",(float)m_thirdJetVariableBins[i],(float)m_thirdJetVariableBins[i+1]));
  }else
  if(m_doSystematics){
      m_thirdJetVariable = "";
      m_thirdJetVariableRanges.push_back(" ");
  }else
  if(m_doNPVBinning || m_doMuBinning){
    //configure the ranges that we will be binning the 3d Histograms in for the zero rad extrapolation.
    TString pileUpVar = (m_doNPVBinning? "NPV": "mu");
    m_thirdJetVariableBins  = vectoriseD(m_settings->GetValue(pileUpVar + ".Bins",""));

    if(m_thirdJetVariableBins.size() < 2) {
      std::cerr << "Pile Up variable has too few bins - check settings file: "<< m_configFileName << std::endl;
      abort();
    }

    for(int i = 0; i< m_thirdJetVariableBins.size()-1; i++)
      m_thirdJetVariableRanges.push_back(Form("%.3fTo%.3f",(float)m_thirdJetVariableBins[i],(float)m_thirdJetVariableBins[i+1]));
  }
  m_toy     = m_settings->GetValue("toy", -1);
  m_lastToy = m_settings->GetValue("lastToy", -1);
  if(m_doBootStrap){
    if(  m_toy == -1)
      error("Attempting to do a bootstrap but toy number not set. Please set 'toy' to be a value either as an on the fly setting (recommended) or in the config file");
    if( m_lastToy == -1 || m_lastToy <= m_toy)
      error("Attempting to do a bootstrap but last toy number not set. Please set 'lastToy' to be a value either as an on the fly setting (recommended) or in the config file");
  }
}

void Histogram3DFactory::InitialiseInputFile(TString fileName){
  //verify the input file exists
  m_inputFile = TFile::Open(TString(fileName));
  if (m_inputFile==NULL) error("Problem opening "+ fileName);
}
///NOTE: Since we're making a LOT of histograms, all with a lot of bins the decision was made to
///      create them one at a time - this saves on RAM but at the cost of iterating over the file multiple times.
void Histogram3DFactory::Create3DHistograms(){
  //create the output file
  TFile* outfile =0;
  TString fileName = m_outputFilePath+"/"+EvaluateName() + ".root";
  outfile= InitOutputFile(fileName);


	for (const auto& jetAlgo : m_jetCollections){
    //loop over the ranges of the third Jet variable - note this will be a vector of just " " if we are doing nominal.
    for(int i = (m_doBootStrap ? m_toy : 0 ) ;i < (m_doBootStrap ? m_lastToy : 1 );i++){
        if(m_doBootStrap){
          cout << "Before ("<<i<<"): " << m_onTheFlySettings << endl;
          std::string toRemove = std::string(Form(",toy=%d",m_toy));
          cout << "toRemove: " << toRemove << endl;
          std::string::size_type n = m_onTheFlySettings.find(toRemove);
          if (n != std::string::npos)
             m_onTheFlySettings.erase(n, toRemove.length());

          if(i > (m_doBootStrap ? m_toy : 0 ) ){
            toRemove = std::string(Form(",toy=%d",i-1));
            cout << "toRemove: " << toRemove << endl;
            n = m_onTheFlySettings.find(toRemove);
            if (n != std::string::npos)
              m_onTheFlySettings.erase(n, toRemove.length());
          }
          m_onTheFlySettings =  m_onTheFlySettings + std::string(Form(",toy=%d",i));
          cout << "after: " << m_onTheFlySettings << endl;
        }
        Histogram3DTool *histogramTool = new Histogram3DTool(m_inputFileNames,
                                                              m_isMC,0,
                                                              m_configFileName,
                                                              outfile,
                                                              m_thirdJetVariable,
                                                              std::string(m_thirdJetVariableRanges[m_thirdJetBin].Data()),
                                                              std::string(jetAlgo.Data()),
                                                              m_outputFilePath,
                                                              m_onTheFlySettings);

        histogramTool->ProcessTree();
        histogramTool->WriteOutput();

        delete histogramTool;

    }//m_thirdJetVariableRanges

  }//m_jetCollections


  outfile->Close();
  delete outfile;

}

TString Histogram3DFactory::EvaluateName(){
  TString name = (m_isMC ? "MonteCarlo": "Data");
  return name;
  //create the name
  if(m_doNominal){
    name += "nominal";
  }else if(m_doThirdJetExtrapolation){
    name = m_thirdJetVariable + "_" + m_thirdJetVariableRanges[m_thirdJetBin];

  }else if(m_doNPVBinning || m_doMuBinning){
    name = (m_doNPVBinning?"NPV_":"mu_")+m_thirdJetVariableRanges[m_thirdJetBin];
  }
  if(m_doSystematics){
    name += "syst";

    bool doDphiShiftUp       = m_settings->GetValue("doDphiShiftUp", false);
    if(doDphiShiftUp){
      name += "_dphiUp";
    }

    bool doDphiShiftDown       = m_settings->GetValue("doDphiShiftDown", false);
    if(doDphiShiftDown){
      name += "_dphiDown";
    }
    bool doJet3PtFracShiftUp   = m_settings->GetValue("doJet3PtFracShiftUp", false);
    if(doJet3PtFracShiftUp){
      name += "_j3up";
    }
    bool doJet3PtFracShiftDown = m_settings->GetValue("doJet3PtFracShiftDown", false);
    if(doJet3PtFracShiftDown){
       name += "_j3down";
    }
    bool doJVTCutTight         = m_settings->GetValue("doJVTCutTight", false);
    if(doJVTCutTight){
      name += "_jvtTight";
    }
  }
  int toy = m_settings->GetValue("toy", -1);
  if(m_doBootStrap){
    if(  m_toy == -1)
      error("Attempting to do a bootstrap but toy number not set. Please set 'toy' to be a value either as an on the fly setting (recommended) or in the config file");
    name += "bootstrap";
  }

  return name;
}
