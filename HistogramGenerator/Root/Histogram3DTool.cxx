#include "HistogramGenerator/Histogram3DTool.h"
#include <algorithm>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <TRandom3.h>

float GeV = 1000.0;
Histogram3DTool::Histogram3DTool(std::vector<TString> inputFileNames, bool isMC, int JX,std::string configFileName,TFile* outputFile,
   std::string thirdJetVariable, std::string thirdJetVariableRange, std::string jetAlgorithm, std::string outputFilePath , std::string onTheFlySettings):
  m_isMC(isMC),
  m_JX(JX),
  m_inputFileNames( inputFileNames),
  m_configFileName(configFileName),
  m_outfile(outputFile),
  m_thirdJetVariable(thirdJetVariable),
  m_thirdJetVariableRange(thirdJetVariableRange),
  m_jetAlgo(jetAlgorithm),
  m_onTheFlySettings(onTheFlySettings),
  m_outputFilePath(outputFilePath),
  m_madeControlHists(false)
{
    m_filled = 0;
    ///Read in settings
  	m_settings = new TEnv();
  	m_settings = OpenSettingsFile(m_configFileName);
  	if (!m_settings) error("Problem opening "+m_configFileName);
    if(m_onTheFlySettings != " "){
      vector<TString> otf_settings = vectorise(m_onTheFlySettings, ",");
    	for (const auto& setting : otf_settings)
        m_settings->SetValue(setting);
    }
    //binnings
  	m_eta_bins = vectoriseD(m_settings->GetValue("EtaBins.3Dhists",""));
  	m_pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));

    //trigger settings
  	m_trigMenu = m_settings->GetValue("TriggerMenu","");
  	m_trigORs = vectorise(m_settings->GetValue("Trig_OR."+m_trigMenu,""));
  	m_trigThresholds = vectoriseD(m_settings->GetValue("TrigPtThreshold."+m_trigMenu,""));
  	m_doTriggerEmulation = m_settings->GetValue("applyTriggerEmulation", true);

    // dijet selection cuts
    m_jvtCut = m_settings->GetValue("JVTCut",0.64);
    m_dphijjCut = m_settings->GetValue("DphijjCut",2.5);
    m_j3pTCut = m_settings->GetValue("Jet3PtFracCut",0.4);

    //Type of histograms to be produce
    m_testRun                  = m_settings->GetValue("testRun", false);
    m_doBootStrap              = m_settings->GetValue("doBootStrap", false);
    m_doNominal                = m_settings->GetValue("doNominal", false);
    m_doThirdJetExtrapolation  = m_settings->GetValue("doThirdJetExtrapolation", false);
    m_doNPVBinning             = m_settings->GetValue("doNPVBinning",false);
    m_doMuBinning             = m_settings->GetValue("doMuBinning",false);
    m_doSystematics            = m_settings->GetValue("doSystematics", false);
    m_makeControlPlots         = m_settings->GetValue("makeControlPlots", true);
    m_drawControlPlots         =  m_settings->GetValue("drawControlPlots", true);
    if(!m_makeControlPlots)    m_drawControlPlots = false;
    m_applyCalibration         = m_settings->GetValue("applyCalibration", false);
    m_calibrationFilePath      = m_settings->GetValue("calibrationFilePath", "");
    m_injectBias               = m_settings->GetValue("injectBias", false);

    if( m_doNominal && m_doThirdJetExtrapolation
      || m_doNominal &&  m_doSystematics
      || m_doSystematics && m_doThirdJetExtrapolation
      || m_doNPVBinning && m_doNominal
      || m_doNPVBinning && m_doSystematics
      || m_doNPVBinning && m_doThirdJetExtrapolation
      || m_doNPVBinning && m_doMuBinning){
      error("Incorrect run settings: please choise one of 'doSystematic', 'doNominal' and 'doThirdJetExtrapolation'");
    }
    //create the name
    if(m_doNominal){
      m_name = "nominal";
      m_minThirdJetVariable = 0;
      m_maxThirdJetVariable = 1000000;
    }else if(m_doThirdJetExtrapolation){
      m_name = thirdJetVariable + "_" + thirdJetVariableRange;
      vector<TString> buffer = vectorise(thirdJetVariableRange, "To");
      m_minThirdJetVariable = buffer[0].Atof();
      m_maxThirdJetVariable = buffer[1].Atof();
      std::cout << "Min: \t" << m_minThirdJetVariable << std::endl;
      std::cout << "Max: \t" << m_maxThirdJetVariable << std::endl;
    }else if(m_doNPVBinning || m_doMuBinning){
      m_name = (m_doNPVBinning?"NPV_":"mu_")+thirdJetVariableRange;
      vector<TString> buffer = vectorise(thirdJetVariableRange, "To");
      m_minPileUpVar   = buffer[0].Atof();
      m_maxPileUpVar   = buffer[1].Atof();
      std::cout << "Min: \t" << m_minPileUpVar << std::endl;
      std::cout << "Max: \t" << m_maxPileUpVar << std::endl;
    }
    if(m_doSystematics){
      m_name = "syst";

      m_doDphiShiftUp       = m_settings->GetValue("doDphiShiftUp", false);
      if(m_doDphiShiftUp){
        m_dphijjCut += 0.3;
        m_name += "_dphiUp";
      }

      m_doDphiShiftDown       = m_settings->GetValue("doDphiShiftDown", false);
      if(m_doDphiShiftDown){
        m_dphijjCut -= 0.3;
        m_name += "_dphiDown";
      }
      m_doJet3PtFracShiftUp   = m_settings->GetValue("doJet3PtFracShiftUp", false);
      if(m_doJet3PtFracShiftUp){
          m_j3pTCut += 0.1;
          m_name += "_j3up";
      }
      m_doJet3PtFracShiftDown = m_settings->GetValue("doJet3PtFracShiftDown", false);
      if(m_doJet3PtFracShiftDown){
         m_j3pTCut -= 0.1;
         m_name += "_j3down";
      }
      m_doJVTCutTight         = m_settings->GetValue("doJVTCutTight", false);
      if(m_doJVTCutTight){
        m_jvtCut = 0.92;
        m_name += "_jvtTight";
      }
    }
    m_toy = m_settings->GetValue("toy", -1);
    if(m_doBootStrap){
      if(  m_toy == -1)
        error("Attempting to do a bootstrap but toy number not set. Please set 'toy' to be a value either as an on the fly setting (recommended) or in the config file");
      m_name += Form("_toy%d",m_toy);

     m_seed = 12345678  + m_toy;
     if(m_isMC) m_seed += m_JX;
    }

    InitialiseHistograms(m_name);
    InitialiseCalibration();
    DisplayWelcomeMessage();

}


Histogram3DTool::~Histogram3DTool(){
  cout << "filled: " <<  m_filled << " times." << endl;
  //Cleanup memory.
  for( const auto& hist : m_hists3D )
    delete hist.second;
  for( const auto& hist : m_hists2D )
    delete hist.second;
  for( const auto& hist : m_controlHists )
    delete hist.second;
  if(m_applyCalibration){
    m_calibraitonFile->Close();
    delete m_calibraitonFile;
  }

}
void Histogram3DTool::DisplayWelcomeMessage(){

    std::cout << "---------------Generating 3D histograms-----------------" << std::endl;
    std::cout << "\tJet Collection: \t" << m_jetAlgo << std::endl;
    std::cout << "\tSample is: \t\t"  << (m_isMC ? "MonteCarlo" : "Data" ) << std::endl;
    std::cout << "\tMaking control plots: \t" << ( m_makeControlPlots ? "yes" : "no" )<< std::endl;
    std::cout << "\tApplying Calibration: \t" << ( m_applyCalibration ? "yes" : "no" )<< std::endl;
    std::cout << "\tPerform bootstrap: \t" << ( m_doBootStrap ? "yes" : "no" )<< std::endl;
    if(m_doBootStrap){
        std::cout << "\t\t toy = " << m_toy << std::endl;
    }
    if(m_applyCalibration){
      std::cout << "\t\tCalibration File: \t" << m_calibrationFilePath << std::endl;
    }
    if(m_doNominal){
      std::cout << "\tCreating Nominal 3D histograms with the following cuts:" << std::endl;
    }else if(m_doSystematics){
      std::cout << "\tCreating systematic 3D histograms with the following cuts:" << std::endl;
    }
    if(!m_doThirdJetExtrapolation){
      std::cout << "\t\tDPhijj > " << m_dphijjCut << std::endl;
      std::cout << "\t\tpT3/pTAvg < " << m_j3pTCut << std::endl;
      std::cout << "\t\tJVT > " << m_jvtCut << std::endl;
    }else{
      std::cout << "\tCreating third jet extrapolation 3D histograms" << std::endl;
    }
    if(m_injectBias){
      std::cout << "\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      std::cout << "\tWARNING: INJECTING A BIAS OF 0.4 INTO ASYMMETRY HISTOGRAMS FOR:" << std::endl;
      std::cout << "\t\t85 <= p_T^{Avg} < 115 GeV" <<std::endl;
      std::cout << "\t\t-1.8 <= eta_left < -1.5" <<std::endl;
      std::cout << "\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    std::cout << "\tApplying trigger emulation: " << m_doTriggerEmulation << std::endl;
    std::cout << "--------------------------------------------------------" << std::endl;
}
void Histogram3DTool::WriteOutput(){
  m_outfile->mkdir( TString(m_jetAlgo));
  m_outfile->cd(TString(m_jetAlgo));
  gDirectory->pwd();

  for( const auto& hist : m_hists3D ) {
    hist.second->Write();
  }

  for( const auto& hist : m_hists2D ) {
     hist.second->Write();
  }

for( const auto& hist : m_controlHists ) {
   hist.second->Write();
}
/*
  TCanvas*  Can = new TCanvas("Can","",800,600);
  for( const auto& hist : m_controlHists ) {
      Can->Clear();

     hist.second->Draw();
     std::string fileName = m_outputFilePath + "/"+  hist.first + ".pdf";
     cout << "Saving control plot: " << fileName << endl;
     Can->Print(fileName.c_str());
  }
  delete Can;*/

}
bool Histogram3DTool::passTrigger(TString triggerCombination){
	triggerCombination.ReplaceAll("_OR_"," ");
	TString centralTrigger = vectorise(triggerCombination).at(0);
	TString forwardTrigger = vectorise(triggerCombination).at(1);
  return ( trigMap[centralTrigger.Data()] || trigMap[forwardTrigger.Data()]);

}
void Histogram3DTool::InitialiseInputFile(TString fileName){
  //verify the input file exists
  m_inputFile = TFile::Open(TString(fileName));
  if (m_inputFile==NULL) error("Problem opening "+ fileName);
  cout << "OPENED FILE: " << fileName << endl;

}
void Histogram3DTool::InitBranches(){
  m_inputFile->cd();

  m_dijetInsituTree = (TTree*) m_inputFile->Get(Form("%s_dijet_insitu",m_jetAlgo.c_str()));
  if (  m_dijetInsituTree==0){
    cerr << "WARNING: Could not find dijet insitu tree: " << m_jetAlgo+"_dijet_insitu" << std::endl;
    return;
  }

  m_dijetInsituTree->SetBranchAddress("passTrigger",&m_passTrigger);
  m_dijetInsituTree->SetBranchAddress("Asym_MM",&m_asymMM);
  m_dijetInsituTree->SetBranchAddress("Asym_SM",&m_asymSM);
  m_dijetInsituTree->SetBranchAddress("weight",&m_weight);
  m_dijetInsituTree->SetBranchAddress("isMC",&m_isMC);
  m_dijetInsituTree->SetBranchAddress("JX",&m_JX);
  m_dijetInsituTree->SetBranchAddress("pTavg",&m_pTavg);

  m_dijetInsituTree->SetBranchAddress("pass_HLT_j15",&m_pass_HLT_j15);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j15_320eta490",&m_pass_HLT_j15_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j25",&m_pass_HLT_j25);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j25_320eta490",&m_pass_HLT_j25_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j35",&m_pass_HLT_j35);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j35_320eta490",&m_pass_HLT_j35_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j45",&m_pass_HLT_j45);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j45_320eta490",&m_pass_HLT_j45_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j60",&m_pass_HLT_j60);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j60_320eta490",&m_pass_HLT_j60_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j110",&m_pass_HLT_j110);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j110_320eta490",&m_pass_HLT_j110_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j175",&m_pass_HLT_j175);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j175_320eta490",&m_pass_HLT_j175_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j260",&m_pass_HLT_j260);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j260_320eta490",&m_pass_HLT_j260_320eta490);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j360",&m_pass_HLT_j360);
  m_dijetInsituTree->SetBranchAddress("pass_HLT_j360_320eta490",&m_pass_HLT_j360_320eta490);
  m_dijetInsituTree->SetBranchAddress("trigBitsCtrl",&m_trigBitsCtrl);
  m_dijetInsituTree->SetBranchAddress("trigBitsFwd",&m_trigBitsFwd);

  m_dijetInsituTree->SetBranchAddress("j3_eta",&m_j3_eta);
  m_dijetInsituTree->SetBranchAddress("j3_phi",&m_j3_phi);
  m_dijetInsituTree->SetBranchAddress("j3_pT",&m_j3_pT);

  m_dijetInsituTree->SetBranchAddress("j1_phi",&m_j1_phi);
  m_dijetInsituTree->SetBranchAddress("j2_phi",&m_j2_phi);

  m_dijetInsituTree->SetBranchAddress("j1_pT",&m_j1_pT);
  m_dijetInsituTree->SetBranchAddress("j2_pT",&m_j2_pT);
  m_dijetInsituTree->SetBranchAddress("j1_eta",&m_j1_eta);
  m_dijetInsituTree->SetBranchAddress("j2_eta",&m_j2_eta);
  m_dijetInsituTree->SetBranchAddress("j1_JVT",&m_j1_JVT);
  m_dijetInsituTree->SetBranchAddress("j2_JVT",&m_j2_JVT);

  m_dijetInsituTree->SetBranchAddress("pass_Nominal",&m_pass_Nominal);
  m_dijetInsituTree->SetBranchAddress("pass_DphiUp",&m_pass_DphiUp);
  m_dijetInsituTree->SetBranchAddress("pass_DphiDown",&m_pass_DphiDown);
  m_dijetInsituTree->SetBranchAddress("pass_JVTThight",&m_pass_JVTThight);
  m_dijetInsituTree->SetBranchAddress("pass_J3pTCutUp",&m_pass_J3pTCutUp);
  m_dijetInsituTree->SetBranchAddress("pass_J3pTCutDown",&m_pass_J3pTCutDown);
  m_dijetInsituTree->SetBranchAddress("NPV",&m_NPV);
  m_dijetInsituTree->SetBranchAddress("avgIntPerXing",&m_mu);
	m_dijetInsituTree->GetEntry(0);
}

void Histogram3DTool::InitialiseCalibration(){
  if(m_applyCalibration){
    m_calibraitonFile =TFile::Open(m_calibrationFilePath);
    if(!m_calibraitonFile) error(Form("Failed to open calibraiton file: %s", m_calibrationFilePath.Data()));
    m_calibrationHistogram = GetTH2F(m_calibraitonFile, "AntiKt4EMTopo_EtaInterCalibration");
  }
}

void Histogram3DTool::InitialiseHistograms( std::string histogramNamePrefix) {
	if( m_isMC ){
    for(int i = 0; i < 11;i++)
	     CreateHistograms( Form("%s_%s_J%i_",histogramNamePrefix.c_str(),m_jetAlgo.c_str(),i));
	}
	else {
		for ( const auto& trig : m_trigORs ) {
			CreateHistograms( Form("%s_%s_%s_RefFJ_FJ_", histogramNamePrefix.c_str(),m_jetAlgo.c_str(),trig.Data()));
			CreateHistograms( Form("%s_%s_%s_RefFJ_J_",histogramNamePrefix.c_str(),m_jetAlgo.c_str(),trig.Data()));

		}
	}

}
void Histogram3DTool::CreateHistograms(std::string prefix){
	// Create 3D histograms
	// Asymmetry vs. eta_right and eta_left in bins of pTavg
	std::vector<double> Asm_bins = makeUniformVec(50,-1.5,1.5);

	m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvg_vs_EtaDet", createHist2D(prefix+"PtAvg_vs_EtaDet","",m_eta_bins,m_pTavg_bins)));

	m_hists3D.insert( std::pair<std::string,TH3F*>(Form("%spTavg_etaprobe_Astandard",prefix.c_str()), createHist3D(Form("%spTavg_etaprobe_Astandard",prefix.c_str()),"",m_pTavg_bins,m_eta_bins,Asm_bins)));
	for ( uint ibin=1; ibin<m_pTavg_bins.size(); ++ibin ){
		m_hists3D.insert( std::pair<std::string,TH3F*>(Form("%sAsym3D_PtBin%d",prefix.c_str(),ibin), createHist3D(Form("%sAsym3D_PtBin%d",prefix.c_str(),ibin),"",m_eta_bins,m_eta_bins,Asm_bins)) );
	}
  std::vector<double> pTavg_finebins = makeUniformVec(3990,10,4000);
  m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvgFine_vs_EtaProbeMM", createHist2D(prefix+"PtAvgFine_vs_EtaProbeMM","",m_eta_bins,pTavg_finebins)) );
  m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvgFine_vs_EtaProbeSM", createHist2D(prefix+"PtAvgFine_vs_EtaProbeSM","",m_eta_bins,pTavg_finebins)));
  if(m_makeControlPlots && !m_madeControlHists){
    m_madeControlHists = true;
    m_controlHists.insert( std::pair<std::string,TH1F*>( std::string(m_name+"_"+ m_jetAlgo+"pt3OverPTAvg"),
     new TH1F( TString(m_name+"_"+ m_jetAlgo+"pt3OverPTAvg").Data(),";p_{T}^{3}/p_{T}^{Avg};N_{events}",80,0, 1.0) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"pt3",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"pt3").Data(),";p_{T}^{3};N_{events}",80,15, 300) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"phiStar",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"phiStar").Data(),";#phi*;N_{events}",80,0, 1) ) );
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"Dphijj",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"Dphijj").Data(),";#phi*;N_{events}",80,0, 3.14) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"eta_{left}",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"eta_{left}").Data(),";#eta_{left};N_{events}",80,4.9, -4.9) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"eta_{right}",
     new TH1F( TString(m_name+"_"+ m_jetAlgo+"eta_{right}").Data(),";#eta_{right};N_{events}",80,4.9, 4.9) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"ptLeft",
     new TH1F( TString(m_name+"_"+ m_jetAlgo+"ptLeft").Data(),";p_{T}^{left};N_{events}",120,15, 800) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"ptRight",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"ptRight").Data(),";p_{T}^{right};N_{events}",120,15, 800) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"pTAvg",
     new TH1F(TString(m_name+"_"+ m_jetAlgo+"pTAvg").Data(),";p_{T}^{Avg};N_{events}",120,15, 800) ));
    m_controlHists.insert( std::pair<std::string,TH1F*>( m_name+"_"+ m_jetAlgo+"asymMM",
     new TH1F(TString( m_name+"_"+ m_jetAlgo+"asymMM").Data(),";Asymmetry;N_{events}",120,-1.5, 1.5) ));

  }
}

bool Histogram3DTool::PassJVTCut(){
  if(TString(m_jetAlgo).Contains("HLT"))return true;

  //check eta and pT of
  bool j1Passed = true;
  bool j2Passed = true;
  if(m_j1_pT < 50 && fabs(m_j1_eta) < 2.4 && m_j1_JVT != -99 ){
    if(m_j1_JVT < m_jvtCut)
      j1Passed = false;
  }
  if(m_j2_pT < 50 && fabs(m_j2_eta) < 2.4 && m_j2_JVT != -99){
    if(m_j2_JVT < m_jvtCut  )
      j2Passed = false;
  }
  return (j1Passed && j2Passed);
}
bool Histogram3DTool::PassDeltaPhiCut(){
  return  ( fabs(TVector2::Phi_mpi_pi(m_j1_phi - m_j2_phi)) >= m_dphijjCut );
}
bool Histogram3DTool::PassPt3Cut(){
  if(m_j3_pT == -99)return true;
  return (m_j3_pT <= m_j3pTCut*m_pTavg);
}
bool Histogram3DTool::PassPileUpVarBinning(){
  if(m_doNPVBinning){
    if( m_NPV >= m_minPileUpVar && m_NPV < m_maxPileUpVar) return true;
  }
  if(m_doMuBinning){
    if( m_mu >= m_minPileUpVar && m_mu < m_maxPileUpVar) return true;
  }
  return false;

}
bool Histogram3DTool::PassCuts(){
  //pass nominal cuts
  if(m_doNominal && m_pass_Nominal) return true;
  if( (m_doNPVBinning || m_doMuBinning) && m_pass_Nominal && PassPileUpVarBinning()) return true;
  if(m_doSystematics){
    /*
    if( PassJVTCut() )
    if( PassDeltaPhiCut() )
    if( PassPt3Cut() )
      return true;

    return false;*/

    if(m_doDphiShiftUp && m_pass_DphiUp) return true;
    if(m_doDphiShiftDown && m_pass_DphiDown) return true;
    if(m_doJet3PtFracShiftUp && m_pass_J3pTCutUp) return true;
    if(m_doJet3PtFracShiftDown && m_pass_J3pTCutDown) return true;
    if(m_doJVTCutTight && m_pass_JVTThight) return true;
  }else if(m_doThirdJetExtrapolation){
    return passThirdJetVaraible();
  }

  return false;
}
double Histogram3DTool::FindThirdJetVariable(){
  if(m_thirdJetVariable.Contains( "pt3OverpTAvg")){
    return (float)( m_j3_pT/(float)m_pTavg);
  }else if(m_thirdJetVariable.Contains( "pt3") )
    return m_j3_pT;
  else if(m_thirdJetVariable.Contains( "Dphijj") ){
    float Dphijj = TMath::Pi() - fabs(TVector2::Phi_mpi_pi(m_j1_phi - m_j2_phi));
    return Dphijj;
  } else if (m_thirdJetVariable.Contains( "phiStar")){
    ///Phi Star variable defined: http://arxiv.org/abs/1410.8052
    float Dphijj = fabs(TVector2::Phi_mpi_pi(m_j1_phi - m_j2_phi));
    float phiACop = (float)(TMath::Pi()) - Dphijj;
    float thetaStar = acos(tanh((m_j1_eta - m_j2_eta)/2));
    float phiStar = tan(phiACop/2)*sin(thetaStar);
    return phiStar;
  }

  return -99;
}

void Histogram3DTool::FillControlPlot(std::string prefix){

  ///Phi Star variable defined: http://arxiv.org/abs/1410.8052
  float Dphijj = m_j1_phi - m_j2_phi;
  float phiACop = (float)(TMath::Pi()) - Dphijj;
  float thetaStar = acos(tanh((m_j1_eta - m_j2_eta)/2));
  float phiStar = tan(phiACop/2)*sin(thetaStar);
  float pt3OverPTAvg =  m_j3_pT/m_pTavg;
  float absDphijj = fabs(Dphijj);

  if(!m_makeControlPlots)return;

  m_controlHists[m_name+"_"+ prefix+"pt3OverPTAvg"]->Fill((float)( m_j3_pT/(float)m_pTavg));
  m_controlHists[m_name+"_"+ prefix+"pt3"]->Fill( m_j3_pT);
  m_controlHists[m_name+"_"+ prefix+"Dphijj"]->Fill( fabs( m_j1_phi - m_j2_phi ));
  m_controlHists[m_name+"_"+ prefix+"phiStar"]->Fill( phiStar);
  m_controlHists[m_name+"_"+ prefix+"eta_{left}"]->Fill( m_left_deta);
  m_controlHists[m_name+"_"+ prefix+"eta_{right}"]->Fill(m_right_deta);
  m_controlHists[m_name+"_"+ prefix+"ptLeft"]->Fill( m_left_pT);
  m_controlHists[m_name+"_"+ prefix+"ptRight"]->Fill( m_right_pT);
  m_controlHists[m_name+"_"+ prefix+"pTAvg"]->Fill( m_pTavg);
  m_controlHists[m_name+"_"+ prefix+"asymMM"]->Fill( m_asymMM);

}
bool  Histogram3DTool::passThirdJetVaraible(){
  if(FindThirdJetVariable() == -99) return true;
  return ( m_minThirdJetVariable <= FindThirdJetVariable() && FindThirdJetVariable() <  m_maxThirdJetVariable  );
}

void Histogram3DTool::FillHistograms( int pTbin) {
  std::string prefix = "";
	std::string jetClass ="";

  //setup for the bootstrap
	int nFill = 1;
  if (m_doBootStrap){
   TRandom3 ran;
   ran.SetSeed(m_seed);
   nFill = ran.Poisson(1);
  }

  if(!PassCuts()){
    return;
  }

	if ( m_isMC ){

		prefix = Form("%s_%s_J%d_",m_name.c_str(),m_jetAlgo.c_str(),m_JX);

    FillControlPlot(m_jetAlgo);

  	for (int i = 0; i < nFill; ++i){
      m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(m_left_deta,m_pTavg,m_weight);
      m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(m_right_deta,m_pTavg,m_weight);

      //matrix method
      m_hists3D[Form("%sAsym3D_PtBin%d",prefix.c_str(),pTbin)]->Fill(m_left_deta,m_right_deta,m_asymMM,m_weight);

      //  standard method
      if ( m_asymSM != -99){
        m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(m_probe_eta, m_pTavg,m_weight);
        m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(m_pTavg/GeV,m_probe_eta,m_asymSM,m_weight);
        if( fabs(m_ref_eta)< 0.8 && fabs(m_probe_eta)<0.8 ){ // both jets in reference region
          m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(m_pTavg,m_ref_eta,-1*m_asymSM,m_weight);
          m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(m_ref_eta,m_pTavg,m_weight);
        }
      }
    }
	}	else {
      FillControlPlot(m_jetAlgo);
		// loop over trigger OR combinations
		for ( const auto& trig : m_trigORs ) {
      //should always pass this as microXAODs should only contain events that pass at least one trigger
      if( !passTrigger(trig))continue;
			// RefFJ_FJ or RefFJ_J (or doesn't fire the triger we're looking at..)
      jetClass = TriggerClassification(trig);

    	if ( jetClass=="" ) //if event doesn't fire the triggers in m_trigORs
			   continue;

			prefix = Form("%s_%s_%s_%s_",m_name.c_str(),m_jetAlgo.c_str(),trig.Data(),jetClass.c_str());

    	for (int i = 0; i < nFill; ++i){
        m_filled++;
  		  m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(m_left_deta,m_pTavg,m_weight);
        m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(m_right_deta,m_pTavg,m_weight);
  		  m_hists2D[prefix+"PtAvgFine_vs_EtaProbeMM"]->Fill(m_left_deta,m_pTavg,m_weight);
        m_hists2D[prefix+"PtAvgFine_vs_EtaProbeMM"]->Fill(m_right_deta,m_pTavg,m_weight);

        // fill 3D histograms
        //matrix method

  	   m_hists3D[Form("%sAsym3D_PtBin%d",prefix.c_str(),pTbin)]->Fill(m_left_deta,m_right_deta,m_asymMM,m_weight);

      	//  standard method
  			if ( m_asymSM != -99) {
          m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(m_probe_eta, m_pTavg,m_weight);
  				m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(m_pTavg,m_probe_eta,m_asymSM,m_weight);
  				if( fabs(m_ref_eta)< 0.8 && fabs(m_probe_eta)<0.8 ){ // both jets in reference region
  					m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(m_pTavg,m_ref_eta,-1*m_asymSM,m_weight);
            m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(m_ref_eta,m_pTavg,m_weight);
          }
  			}
      }//close for(i<nFill)
  	}
	}
	return;
}

float Histogram3DTool::GetCalibrationFactor(float pT, float eta){
  float maxEta = 4.2f; float minPT = 25; float maxPT = 1200;
  if(pT >= maxPT || pT <= minPT || fabs(eta) >= maxEta) return 1.0f;
  return m_calibrationHistogram->Interpolate(pT,eta);

}

//Apply a found calibration
void Histogram3DTool::CalibrateJets(){
  if(!m_applyCalibration){
    return;
  }

  float j1Calibration = GetCalibrationFactor(m_j1_pT, m_j1_eta);
  float j2Calibration = GetCalibrationFactor(m_j2_pT, m_j2_eta);

  m_j1_pT *= j1Calibration;
  m_j2_pT *= j2Calibration;
}
void Histogram3DTool::SetTrigMap(){
  	trigMap["HLT_j15"] = m_pass_HLT_j15;
  	trigMap["HLT_j25"] = m_pass_HLT_j25;
  	trigMap["HLT_j35"] = m_pass_HLT_j35;
  	trigMap["HLT_j45"] = m_pass_HLT_j45;
  	trigMap["HLT_j60"] = m_pass_HLT_j60;
  	trigMap["HLT_j110"] = m_pass_HLT_j110;
  	trigMap["HLT_j175"] = m_pass_HLT_j175;
  	trigMap["HLT_j260"] = m_pass_HLT_j260;
  	trigMap["HLT_j360"] = m_pass_HLT_j360;

  	trigMap["HLT_j15_320eta490"] = m_pass_HLT_j15_320eta490;
  	trigMap["HLT_j25_320eta490"] = m_pass_HLT_j25_320eta490;
  	trigMap["HLT_j35_320eta490"] = m_pass_HLT_j35_320eta490;
  	trigMap["HLT_j45_320eta490"] = m_pass_HLT_j45_320eta490;
  	trigMap["HLT_j60_320eta490"] = m_pass_HLT_j60_320eta490;
  	trigMap["HLT_j110_320eta490"] = m_pass_HLT_j110_320eta490;
  	trigMap["HLT_j175_320eta490"] = m_pass_HLT_j175_320eta490;
  	trigMap["HLT_j260_320eta490"] = m_pass_HLT_j260_320eta490;
  	trigMap["HLT_j360_320eta490"] = m_pass_HLT_j360_320eta490;

}
void Histogram3DTool::EvaluateAsymmetry(){
  m_left_deta  = m_j1_eta < m_j2_eta ? m_j1_eta : m_j2_eta;
  m_left_pT    = m_j1_eta < m_j2_eta ? m_j1_pT : m_j2_pT;
  m_right_deta = m_j1_eta > m_j2_eta ? m_j1_eta : m_j2_eta;
  m_right_pT   = m_j1_eta > m_j2_eta ? m_j1_pT : m_j2_pT;

  //recalculates the Asymmetry and pTavg (in GeV)
  m_pTavg = (m_left_pT + m_right_pT)/2.0;
  m_asymMM = (m_left_pT - m_right_pT)/m_pTavg;

  if(fabs(m_j1_eta) <= 0.8 || fabs(m_j2_eta)){
    if(fabs(m_j1_eta) <= 0.8){
      m_ref_eta = m_j1_eta;
      m_ref_pT  = m_j1_pT;
      m_probe_eta = m_j2_eta;
      m_probe_pT  = m_j2_pT;
    }else{
      m_ref_eta = m_j2_eta;
      m_ref_pT  = m_j2_pT;
      m_probe_eta = m_j1_eta;
      m_probe_pT  = m_j1_pT;
    }
    m_asymSM = (m_probe_pT - m_ref_pT)/m_pTavg;
  }else{
    m_asymSM = -99;
  }

  if(m_injectBias){
    //-1.8 -1.5
    if(m_left_deta >= -1.8 & m_left_deta <= -1.5)
    if(m_pTavg >= 85  && m_pTavg < 115){
      m_asymMM += 0.5;
      if(m_asymSM != -99) m_asymSM += 0.5;
    }
  }
}
void Histogram3DTool::ProcessTree()
{
  for( const auto& inputFile : m_inputFileNames){
    InitialiseInputFile(inputFile);
    InitBranches();

  	Long64_t nentries = (m_testRun ? 10000 : m_dijetInsituTree->GetEntries()) ;
  	printf("\nWill loop over %d events\n\n",int(nentries));
  	for (Long64_t jentry=0; jentry<nentries; jentry++) {
  		if (jentry&&(jentry%100000)==0) {
  			cout<<Form("Processed %5dk / %6.2fM events ( %7.1f%% )",int(jentry)/1000,float(nentries)/1e6,100.0*jentry/nentries);
        PrintTime();
  		}

  		m_dijetInsituTree->GetEntry(jentry);
      m_seed+=jentry;
      SetTrigMap();
      CalibrateJets();
      EvaluateAsymmetry();

      //find the pTBin that pTAvg is in
  		uint pTbin=0;
  		for (uint i=0;i<m_pTavg_bins.size();++i)
  			if (m_pTavg >= m_pTavg_bins[i]) pTbin=i+1;

      //don't fill if its not pT is less than that of the first bin, or greater than that of the alst.
      if (pTbin==0 || pTbin>(m_pTavg_bins.size()-1)) continue;

      //Theshold needs implementin
      FillHistograms(pTbin);

  	}
  }
}//event loop

std::string Histogram3DTool::TriggerClassification(TString trig) {

	bool passEmulFwd=false;

	for ( uint itrig=0; itrig<m_trigORs.size(); ++itrig ) {
		if ( m_trigORs[itrig]!=trig ) continue;
		passEmulFwd  = m_trigBitsFwd  & int(pow(2,itrig));
	}

	trig.ReplaceAll("_OR_"," ");
	ctrl_trig = vectorise(trig).at(0);
	fwd_trig = vectorise(trig).at(1);

  std::string classification = "";
	if(m_doTriggerEmulation){
    if (trigMap[fwd_trig.Data()] && !trigMap[ctrl_trig.Data()] ) classification = "RefFJ_FJ";
		if (trigMap[ctrl_trig.Data()]  && !trigMap[fwd_trig.Data() ] ) classification = "RefFJ_J";

		if (trigMap[fwd_trig.Data() ] &&trigMap[ctrl_trig.Data()])
			if(passEmulFwd) classification="RefFJ_FJ";
	}else{
		if (trigMap[fwd_trig.Data()] && !trigMap[ ctrl_trig.Data() ]) classification = "RefFJ_FJ";
		if (trigMap[ctrl_trig.Data()] && !trigMap[fwd_trig.Data()]  ) classification = "RefFJ_J";
	}
  return classification;
}
