#include <TCanvas.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TGraph2DErrors.h>

typedef TGraphErrors Graph;
typedef TGraph2DErrors Graph2D;

TLatex *_tex;
double TEXTSIZE=0.04;
int cols[] = {kRed, kBlue, kGreen+1, kOrange+7, kViolet+2, kGray+1};

int _jetR;
Str _calibration, _methodDesc, _name;

TFile *Open(Str fn) { TFile *f=TFile::Open(fn); if (f==NULL) error("Can't open "+fn); return f; }
void FormatHisto(TH1 *h, int mstyle, int col, double msize);
void FormatHisto(TH1 *h, int style);
void FormatGraph(Graph *h, int mstyle, int col, double msize);
void FormatGraph(Graph *h, int style);

void DrawText(TString txt, int col=kBlack, double y=0.88, double x=0.145);
void DrawTextR(TString txt, int col=kBlack, double y=0.88, double x=0.92);
void DrawText(TLatex *tex, TString txt, int col=kBlack, double y=0.88, double x=0.145);
void DrawLabel(TString txt, int style, double x, double y);
void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize);


void FormatHisto(TH1 *h, int style)
{
  if      (style==1) FormatHisto(h,20,kBlack,0.8); // data
  else if (style==2) FormatHisto(h,22,kRed,0.6);
  else if (style==3) FormatHisto(h,23,kBlue,0.8);
  else if (style==4) FormatHisto(h,24,kOrange+7,1.2);
  else if (style==5) FormatHisto(h,25,kGreen+2,1.6);
  else if (style==6) FormatHisto(h,26,kGray+2,0.8);
  else if (style==7) FormatHisto(h,27,kBlue-2+1,0.8);
  else if (style==8) FormatHisto(h,28,kCyan+1,0.8);
  else if (style==9) FormatHisto(h,29,kMagenta+2,0.8);
  else if (style==10) FormatHisto(h,30,kPink+1,1.2);

  h->SetXTitle("");
  h->SetYTitle("Relative jet response, 1/c");
  h->GetXaxis()->SetTitleOffset(0.95);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetYaxis()->SetNdivisions(505);
}

void FormatHisto(TH1 *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col);
}
/*
void DrawText(TString txt, int col=kBlack, double y=0.91, double x=0.15, int align=12, bool NDC0=false, float textsize=TEXTSIZE){
static TLatex *tex = new TLatex();
if(NDC0) tex->SetNDC(0);
else     tex->SetNDC();
tex->SetTextSize(textsize);
tex->SetTextAlign(align);
tex->SetTextColor(col);
tex->DrawLatex(x,y,txt);

}

void DrawLabel(TString txt, double x, double y, int style, bool style2=false, float textsize=TEXTSIZE) {
TMarker *m = new TMarker(x,y,mark[style]);
m->SetNDC();
m->SetMarkerColor(cols[style]);
m->SetMarkerSize(1.4);
m->SetMarkerStyle(mark[style]);
if(style2) m->SetMarkerStyle(mark2[style]);
static TLine *l = new TLine();
l->SetLineWidth(1);
l->SetLineColor(cols[style]);
l->DrawLineNDC(x-0.02,y,x-0.005,y);
l->DrawLineNDC(x+0.005,y,x+0.02,y);
m->Draw();
DrawText(txt,kBlack,y,x+0.032,12,false,textsize);
}
*/
// white space between figures
double SUBFIGURE_MARGIN = 0.06;
void DrawSubPad(TCanvas* can, double FIGURE2_RATIO = 0.36) {
  can->SetBottomMargin(FIGURE2_RATIO);
  // create new pad, fullsize to have equal font-sizes in both plots
  TPad *p = new TPad( "p_test", "", 0, 0, 1, 1.0 - SUBFIGURE_MARGIN, 0, 0, 0);
  p->SetFillStyle(0);
  p->SetMargin(0.12,0.04,0.125,1.0 - FIGURE2_RATIO);
  p->SetGridy();
  p->Draw();
  p->cd();
  }

void FormatGraph(Graph *h, int style)
{
  if      (style==1) FormatGraph(h,20,kBlack,0.8); // data
  else if (style==2) FormatGraph(h,21,kRed,0.6);
  else if (style==3) FormatGraph(h,23,kBlue,0.8);
  else if (style==4) FormatGraph(h,26,kOrange+7,1.2);
  else if (style==5) FormatGraph(h,33,kGreen+2,1.6);
  else if (style==6) FormatGraph(h,28,kGray+2,1.2);
  else if (style==10) FormatGraph(h,24,kBlack,1.2);
  else if (style==11) FormatGraph(h,24,kBlack,1.2);
  else if (style==12) FormatGraph(h,25,kRed,0.8);
  else if (style==13) FormatGraph(h,27,kBlue,0.8);

  // styles for SM vs. MM comparisons
  else if (style==21) FormatGraph(h,20,kBlack,1.0);
  else if (style==22) FormatGraph(h,24,kBlack,1.0);
  else if (style==23) FormatGraph(h,22,kBlue+1,1.0);
  else if (style==24) FormatGraph(h,26,kBlue+1,1.0);
  else if (style==25) FormatGraph(h,34,kRed+1,1.0);
  else if (style==26) FormatGraph(h,28,kRed+1,1.0);


}

void FormatGraph(Graph *h, int mstyle, int col, double msize) {
  h->SetMarkerStyle(mstyle); h->SetMarkerSize(msize);
  h->SetMarkerColor(col);
  h->SetLineColor(col); h->SetLineWidth(2);
}

void PrintInfo() {
  DrawText(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+_calibration+", "+_methodDesc,kBlack,0.97,0.12);
  DrawTextR(_name,kBlack,0.97);
}

void PrintEtaBinInfo(double ptmin, double ptmax, Str Calibration = _calibration) {
  DrawText(Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+Calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void PrintPtBinInfo(double etamin, double etamax, Str Calibration = _calibration) {
  DrawText(Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),1,0.875);
  DrawTextR(Form("Anti-k_{t} #font[52]{R} = 0.%d, ",_jetR)+Calibration,kBlack,0.93);
  DrawTextR(_methodDesc,kBlack,0.875);
  DrawTextR(_name,kBlack,0.82);
}

void DrawText(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(11); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt); }

void DrawText(TLatex *tex, TString txt, int col, double y, double x)
{ tex->SetTextColor(col); tex->DrawLatex(x,y,txt); }

void DrawTextR(TString txt, int col, double y, double x)
{ _tex->SetTextAlign(31); _tex->SetTextColor(col); _tex->DrawLatex(x,y,txt);
  _tex->SetTextAlign(11); _tex->SetTextColor(kBlack); }

  void DrawGuideLineSyst(double min, double max) {
    static TLine *line = new TLine();
    line->SetLineStyle(2);
    line->SetLineWidth(1); line->SetLineColor(kRed);
    line->DrawLine(min,1.4,max,1.4);
    line->SetLineColor(kBlue);
    line->DrawLine(min,0.6,max,0.6);
  }

  void DrawGuideLines(double min, double max) {
    static TLine *line = new TLine();
    line->SetLineStyle(2);
    line->SetLineWidth(1);
    if ( max<1000 ) {
      line->SetLineColor(kGray+1);
      line->DrawLine(-0.8,0.25,-0.8,0.35);
      line->DrawLine(0.8,0.25,0.8,0.35);
    }
    line->SetLineWidth(1); line->SetLineColor(kBlack);
    line->DrawLine(min,0.3,max,0.3);
    line->SetLineWidth(1);
    line->SetLineColor(kRed);
    line->DrawLine(min,0.4,max,0.4);
    line->DrawLine(min,0.2,max,0.2);
    line->SetLineColor(kGreen+1);
    line->DrawLine(min,0.35,max,0.35);
    line->DrawLine(min,0.25,max,0.25);
  }

  void DrawCenterLine(double min, double max) {
    static TLine *line = new TLine();
    line->SetLineStyle(2); line->SetLineWidth(1);
    line->SetLineColor(kBlack);
    line->DrawLine(min,0.3,max,0.3);
  }

  void DrawGuideLinesVsEta(double etaMin=-4.5, double etaMax=4.5) {
    DrawGuideLines(etaMin,etaMax);
  }

  void DrawGuideLinesVsPt() {
    DrawGuideLines(25,1200);
  }

  void DrawLabel(TString txt, double x, double y, int mstyle, int col, double msize) {
    TMarker *m = new TMarker(x,y,mstyle);
    m->SetNDC(); m->SetMarkerSize(msize); m->SetMarkerColor(col);
    TLine *l = new TLine(); l->SetLineWidth(2); l->SetLineColor(col);
    l->DrawLineNDC(x-0.02,y,x-0.005,y); l->DrawLineNDC(x+0.005,y,x+0.02,y); m->Draw();
    _tex->SetTextSize(0.04); _tex->SetTextAlign(12); _tex->SetTextColor(col);
    _tex->DrawLatex(x+0.04,y,txt);
    _tex->SetTextSize(TEXTSIZE);
  }

  void DrawLabel(TLatex* tex, TString txt, double x, double y, int mstyle, int col, double msize) {
    TMarker *m = new TMarker(x,y,mstyle);
    m->SetNDC(); m->SetMarkerSize(msize); m->SetMarkerColor(col);
    TLine *l = new TLine(); l->SetLineWidth(2); l->SetLineColor(col);
    l->DrawLineNDC(x-0.02,y,x-0.005,y); l->DrawLineNDC(x+0.005,y,x+0.02,y); m->Draw();
    tex->SetTextSize(0.04);  tex->SetTextColor(col);
    tex->DrawLatex(x+0.04,y,txt);
  }

  void DrawLabel(TLatex* tex, TString txt, int style, double x, double y) {
    if (style==1) DrawLabel(tex,txt,x,y,20,kBlack,1.2);
    else if (style==2) DrawLabel(tex,txt,x,y,21,kRed,1.0);
    else if (style==3) DrawLabel(tex,txt,x,y,23,kBlue,1.2);
    else if (style==4) DrawLabel(tex,txt,x,y,26,kOrange+7,1.2);
    else if (style==5) DrawLabel(tex,txt,x,y,33,kGreen+2,1.6);
    else if (style==6) DrawLabel(tex,txt,x,y,28,kGray+2,1.2);
    else if (style==10) DrawLabel(tex,txt,x,y,24,kBlack,1.2);
    else if (style==11) DrawLabel(tex,txt,x,y,24,kBlack,1.2);
    else if (style==12) DrawLabel(tex,txt,x,y,25,kRed,0.8);
    else if (style==13) DrawLabel(tex,txt,x,y,27,kBlue,0.8);
  }


  void DrawLabel(TString txt, int style, double x, double y) {
    if (style==1) DrawLabel(txt,x,y,20,kBlack,1.2);
    else if (style==2) DrawLabel(txt,x,y,21,kRed,1.0);
    else if (style==3) DrawLabel(txt,x,y,23,kBlue,1.2);
    else if (style==4) DrawLabel(txt,x,y,26,kOrange+7,1.2);
    else if (style==5) DrawLabel(txt,x,y,33,kGreen+2,1.6);
    else if (style==6) DrawLabel(txt,x,y,28,kGray+2,1.2);
    else if (style==10) DrawLabel(txt,x,y,24,kBlack,1.2);
    else if (style==11) DrawLabel(txt,x,y,24,kBlack,1.2);
    else if (style==12) DrawLabel(txt,x,y,25,kRed,0.8);
    else if (style==13) DrawLabel(txt,x,y,27,kBlue,0.8);
  }


  void PrintIntegratedLumi(float lumi){
    DrawText(Form("#int #it{L dt} = %.0f fb^{-1}", lumi ),kBlack,0.94,0.14);
  }

  void PrintDataMC(Str mc1, Str mc2, float y=0.55, float x= 0.42) {
    float dy = 0.07;
    DrawLabel("Data 2015: 13 TeV",1,x,y);
    DrawLabel(mc1,2,x,y-=dy);
    DrawLabel(mc2,3,x,y-=dy);
  }



  void DrawPtEtaPoints(Graph2D *graph, VecD ptBins, VecD etaBins, double freezeCalibEta = 4.5, double minPt = 20.0,  double maxPt = 1500.0 ) {

    TLine *line = new TLine();
    line->SetLineColor(kGray);
    Graph *bin_xy = new Graph(), *avg_xy = new Graph();
    static TH2F *h = new TH2F("a","",1,17.5,2000,1,etaBins[0],etaBins[etaBins.size()-1]);
    h->GetXaxis()->SetMoreLogLabels();
    h->GetYaxis()->SetTitleOffset(0.8);
    h->SetXTitle("p_{T}^{avg}");
    h->SetYTitle("#eta_{det}");
    h->SetXTitle("p_{T}^{probe} [GeV]");
    h->Draw();

    for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
      double etal = etaBins[ieta];
      double eta = 0.5*(etal+etaBins[ieta+1]);
      line->DrawLine(25,etal,1500,etal);
      if (ieta==etaBins.size()-2) line->DrawLine(minPt,etaBins[ieta+1],1500,etaBins[ieta+1]);
      for (int ipt=0;ipt<ptBins.size()-1;++ipt) {
        double ptl = ptBins[ipt];
        double pt  = 0.5*(ptl+ptBins[ipt+1]);
        line->DrawLine(ptl,etaBins[0],ptl,etaBins[etaBins.size()-1]);
        if (ipt==ptBins.size()-2) line->DrawLine(ptBins[ipt+1],etaBins[0],ptBins[ipt+1],4.5);
        int n=bin_xy->GetN();
        bin_xy->SetPoint(n,pt,eta);
      }
    }
    for (int i=0;i<graph->GetN();++i) {
      avg_xy->SetPoint(i,graph->GetX()[i],graph->GetY()[i]);
      avg_xy->SetPointError(i,graph->GetEX()[i],graph->GetEY()[i]);
    }
    avg_xy->SetMarkerSize(0.4);
    avg_xy->Draw("P");


    float minEta = -freezeCalibEta;
    if(etaBins[0]>=0) minEta = 0;

    line->SetLineColor(kRed+1);
    line->SetLineWidth(2);

    line->DrawLine(minPt,minEta,minPt,freezeCalibEta);
    line->DrawLine(minPt,minEta,maxPt,minEta);
    line->DrawLine(maxPt,minEta,maxPt,freezeCalibEta);
    line->DrawLine(minPt,freezeCalibEta,maxPt,freezeCalibEta);
    h->Draw("sameaxis");

  }
