#!/bin/sh                                                                                                                                             

#code=DrawDijetBalance.C
#code=DijetResponsePlotting.C
code=DrawDijetWidth.C
exec=draw_dijet_response.exe
config=${1:-"Difference.config"}
#config=${1:-"MCvsData.config"}
plotType=${2:-"Difference"} #MCvsData SMvsMM PowhegPythiavsSherpa
#plotType=${2:-"MCvsData"}

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore"
rm -f $exec
gcc -std=c++11 $flagsNlibs -o $exec $code && {

    echo ; echo "Compilation successful" ; echo
    ./$exec $config $plotType

}
#rm -f $exec
