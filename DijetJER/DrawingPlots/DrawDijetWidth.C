#include "Utils_W.h"
#include "DrawingUtils.h"
#include "atlasstyle/AtlasStyle.C"
#include "atlasstyle/AtlasLabels.C"

float textsize = 0.045;
double canX  = 800;
double canY  = 600;

Str outfn = "DijetWidth";

bool SMvsMM=false;
bool MCvsData=false;
bool PowhegPythiavsSherpa=false;
bool etaVsnoEta=false;
bool ScaledVsNot=false;
bool HLTvsOffline=false;
bool Closure=false;
bool CrossChecks=false;
bool Difference=false;
bool MC=false;
bool Data=false;
Str plotType="";

void GetPtEtaBins(Str jetAlgo, Str file, VecD &etaBins, VecD& ptBins);
void TrimGraphs(vector< Graph2D* > &graphs, double maxPt=1500);
void DrawGuideBands(const VecD &etaBins);
TCanvas* CreateCanvas(){
  TCanvas* can  = new TCanvas("can","",canX,canY);
  can->SetMargin(0.12,0.04,0.125,0.04);
  return can;
}
void InitialiseStyleSettings(){
  gErrorIgnoreLevel=2000;
  SetAtlasStyle();
  gStyle->SetPalette(1);
  gStyle->SetNumberContours(100);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadTopMargin(0.04);
  gStyle->SetPadBottomMargin(0.1);
  return;
}
void WritePDF(){
  Str pdf = outfn; pdf.ReplaceAll("ps","pdf");
  int stat = gSystem->Exec(Form("ps2pdf %s %s",outfn.Data(),pdf.Data()));
  if (stat==0)  gSystem->Exec(Form("rm %s",outfn.Data()));
  else { printf("\nWARNING: could not convert %s to %s\n",outfn.Data(),pdf.Data()); }
  if (stat==0) printf("\nProduced\n  %s\n\n",pdf.Data());
  return;
}

int main(int argc, char **argv) {

  InitialiseStyleSettings();
  TCanvas* can  = CreateCanvas();

  Str config;
  StrV jetAlgos;

  if ( argc<2 ) {
    error("Need to provide two arguments, e.g. plots.config SMvsMM");
  }

  for (int i=1;i<=argc;++i) {
    Str arg=argv[i];
    if (arg.Contains("config")) {
      config = arg;
    }
    else if (arg=="") continue;
    else {
      plotType=arg;
      if ( plotType=="SMvsMM" ) SMvsMM=true;
      else if ( plotType=="MCvsData" ) MCvsData=true;
      else if ( plotType=="PowhegPythiavsSherpa" ) PowhegPythiavsSherpa=true;
      else if ( plotType=="ScaledVsNot" ) ScaledVsNot=true;
      else if ( plotType=="etaVsnoEta" ) etaVsnoEta=true;
      else if ( plotType=="HLTvsOffline" ) HLTvsOffline=true;
      else if ( plotType=="Closure" ) Closure=true;
      else if ( plotType=="CrossChecks" ) CrossChecks=true;
      else if ( plotType=="Difference" ) Difference=true;
      else if ( plotType=="Data" ) Data=true;
      else if ( plotType=="MC" ) MC=true;
      else {
        error("Don't recognise plot type "+plotType);
      }
    }
  }
  cout << "Config file:  " << config << endl;

  outfn += "_"+plotType+".ps";
  can->Print(outfn+"[");

  TEnv *settings = OpenSettingsFile(config);
  jetAlgos =  Vectorize(settings->GetValue(plotType+".JetAlgos","") );
  StrV files = Vectorize(settings->GetValue(plotType+".InputFiles",""));
  if ( files.size()==0 ) error("Failed to get input files.");
  StrV methods = Vectorize(settings->GetValue(plotType+".Methods",""));

  int jetR=4; if (jetAlgos[0].Contains("Kt6")) jetR=6;
  Str jetAlgo = jetAlgos[0];

  // get pTavg and eta bins from input files
  VecD etaBins, ptBins;
  GetPtEtaBins(jetAlgo,files[0],etaBins,ptBins);

  // get 2D Width graphs
  bool numInv = true;
  vector< Graph2D* > graphs2D;
  TFile* f;
  if ( Difference ){
    TFile* fTruth = Open(files[0]);
    TFile* fReco = Open(files[1]);
    graphs2D.push_back( GetMCResolution(fReco, fTruth, methods[0]) );
  }

  for ( int fi=0; fi<files.size(); fi++ ) {
    f = Open(files[fi]);
    graphs2D.push_back( GetCalibration2DGraph(f,jetAlgos[fi],methods[fi],numInv) );
  }

  //TrimGraphs(graphs2D);


  TH1F *heta = new TH1F("heta","",1,etaBins[0],etaBins[etaBins.size()-1]);
  FormatHisto(heta,1);

  TH1F *hratio = new TH1F("hratio","",1,etaBins[0],etaBins[etaBins.size()-1]);
  FormatHisto(hratio,1);

  heta->SetYTitle("#sigma(p_{T})_{unsubtracted}/p_{T}");
  heta->SetXTitle("#eta_{det}");
  heta->SetMinimum(0.001);
  heta->SetMaximum(0.7);
  heta->GetXaxis()->SetLabelColor(0);
  heta->GetXaxis()->SetTitleOffset(1.1);
  heta->Draw();

  if( !Data && !MC ){  
  hratio->SetYTitle("Ratio");
  hratio->SetXTitle("#eta_{det}");
  hratio->SetMinimum(0.);
  hratio->SetMaximum(2.0);
  hratio->GetXaxis()->SetTitleOffset(1.1);
  }
  
  TLatex* tex = new TLatex();
  tex->SetNDC();
  tex->SetTextColor(kBlack);
  tex->SetTextSize(0.04);
  tex->SetTextAlign(12);

  if ( CrossChecks ) {


    std::vector<int> pt_bins_trig = {3};
    for ( int ipt : pt_bins_trig ) {
      float ptmin=ptBins[ipt]; float ptmax=ptBins[ipt+1];
      can->Clear();
      heta->Draw();
      //DrawGuideBands(etaBins);
      //DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);

      heta->SetMinimum(0.01);
      heta->SetMaximum(1.2);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"j25 trigger",0.6,0.85,24,kBlack,1);
      DrawLabel(tex,"j60 trigger",0.6,0.8,20,kBlack,1);

      VecI points = GetWvsEtaPoints(graphs2D[0],ptBins[ipt],ptBins[ipt+1]);
      vector< Graph* > eta_graphs;
      for ( int ig=0; ig<graphs2D.size(); ++ig ) {
        Graph *g = GetWvsEtaGraph(graphs2D[ig],points);
        FormatGraph(g,20+ig+1);
        eta_graphs.push_back(g);
        g->Draw("PL same");
      }
      heta->Draw("same axis");
      //DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);

      if( !Data && !MC ){
	DrawSubPad(can);
	hratio->Draw();
      
	for ( int ig=1; ig<eta_graphs.size(); ++ig ) {
	  Graph* r = Ratio(eta_graphs[0],eta_graphs[ig]);
	  FormatGraph(r,21);
	  r->Draw("PL same");
	}
      }
    }
    can->Print(outfn);

    std::vector<int> pt_bins_plot = {3,4};
    vector< Graph* > eta_graphs;

    for ( int ipt : pt_bins_plot ) {
      VecI points = GetWvsEtaPoints(graphs2D[1],ptBins[ipt],ptBins[ipt+1]);
      Graph *g = GetWvsEtaGraph(graphs2D[1],points);
      FormatGraph(g,ipt-1);
      eta_graphs.push_back(g);
    }

    can->Clear();
    heta->Draw();
    //DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);

    heta->SetMinimum(0.01);
    heta->SetMaximum(1.2);

    ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
    DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
    DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

    //DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

    DrawLabel(tex,"55 #leq p_{T}^{avg} < 65 GeV",1,0.6,0.90);
    DrawLabel(tex,"65 #leq p_{T}^{avg} < 85 GeV",2,0.6,0.85);
    DrawLabel(tex,"85 #leq p_{T}^{avg} < 115 GeV",3,0.6,0.80);

    for ( int ig=0; ig<eta_graphs.size(); ++ig ) {
      eta_graphs[ig]->Draw("PL same");
      heta->Draw("same axis");
    }

    DrawSubPad(can);
    hratio->Draw();

    //DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);
    
    for ( int ig=1; ig<eta_graphs.size(); ++ig ) {
      Graph* r = Ratio(eta_graphs[0],eta_graphs[ig]);
      FormatGraph(r,ig+1);
      r->Draw("PL same");
    }
    
    can->Print(outfn);

    can->Print(outfn+"]");
    WritePDF();
    return 1;
  } // end if CrossChecks

  // loop over pT bins
  for (int ipt=0;ipt<ptBins.size()-1;++ipt) {
    float ptmin=ptBins[ipt]; float ptmax=ptBins[ipt+1];
    can->Clear();
    heta->Draw();
    //DrawGuideBands(etaBins);
    //    DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);

    if ( SMvsMM ) {

      heta->SetMaximum(1.36);
      hratio->SetYTitle("SM / MM");

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.71,0.15);

      DrawText(tex,"Central reference method",kBlack,0.9,0.55);
      //void DrawLabel(TLatex* tex, TString txt, double x, double y, int mstyle, int col, double msize) {
      DrawLabel(tex,"Data",0.53,0.86,24,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.71,0.86,26,kBlue+1,1);
     // DrawLabel(tex,"Forward ",0.37,0.86,28,kRed+1,1);
      DrawText(tex,"Matrix method",kBlack,0.8,0.55);
      DrawLabel(tex,"Data",0.53,0.76,20,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.71,0.76,22,kBlue+1,1);
    //  DrawLabel(tex,"Forward ",0.37,0.76,34,kRed+1,1);
    }
    else if ( etaVsnoEta ) {

      heta->SetMaximum(1.36);
      hratio->SetYTitle("MM / SM");

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.71,0.15);

      DrawText(tex,"Central reference method",kBlack,0.9,0.55);
      DrawLabel(tex,"Central",0.57,0.86,24,kBlack,1);
      DrawLabel(tex,"Forward",0.71,0.86,26,kBlue+1,1);

      DrawText(tex,"Matrix Method",kBlack,0.8,0.55);
      DrawLabel(tex,"Central",0.57,0.76,20,kBlack,1);
      DrawLabel(tex,"Forward",0.71,0.76,22,kBlue+1,1);

    }
    else if ( MCvsData ){
      hratio->SetYTitle("MC / Data");
      heta->SetMinimum(-0.01);
      heta->SetMaximum(1.0);

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"Data",1,0.6,0.85);
      DrawLabel(tex,"Sherpa EM+JES",2,0.6,0.8);
      DrawLabel(tex,"Sherpa Truth",3,0.6,0.75);

    }

    else if ( PowhegPythiavsSherpa ){
      hratio->SetYTitle("EM+JES / Truth");
      heta->SetMinimum(0.01);
      heta->SetMaximum(0.8);

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"Powheg+Pythia8 EM+JES",1,0.6,0.8);
      DrawLabel(tex,"Powheg+Pythia8 Truth",2,0.6,0.75);
      DrawLabel(tex,"Sherpa EM+JES",3,0.6,0.7);
      DrawLabel(tex,"Sherpa Truth",4,0.6,0.65);

    }
	
    else if ( ScaledVsNot ){
      hratio->SetYTitle("Scaled / Not");
      heta->SetMinimum(0.88);
      heta->SetMaximum(1.29);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"Luminosity Scaled",1,0.6,0.85);
      DrawLabel(tex,"No scaling",2,0.6,0.8);

    }
    else if ( HLTvsOffline ){
      hratio->SetYTitle("HLT / Offline");
      heta->SetMinimum(0.83);
      heta->SetMaximum(1.28);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"Central offline jets",1,0.6,0.85);
      DrawLabel(tex,"Central HLT jets",2,0.6,0.8);
    }
    else if ( Closure ){
      heta->SetMinimum(0.88);
      heta->SetMaximum(1.25);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"Central",0.6,0.85,24,kBlack,1);
      DrawLabel(tex,"Central with calibration",0.6,0.8,20,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.6,0.75,26,kBlue+1,1);
    }
    else if ( Difference ) {
      heta->SetYTitle("#sigma(p_{T})/p_{T}");
      heta->SetMinimum(0.);
      heta->SetMaximum(0.6);
      hratio->SetYTitle("Data/MC");

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);
      DrawText(tex,"#sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}}",kBlack,0.65,0.15);
      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);

      DrawLabel(tex,"(p_{T}^{Reco}-p_{T}^{Truth})/p_{T}^{Reco}",1,0.6,0.83);
      DrawLabel(tex,"Sherpa",2,0.6,0.76);
      DrawLabel(tex,"Data",3,0.6,0.7); 
    }
    else if ( Data ){

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      
      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);
      DrawLabel(tex,"Data",1,0.6,0.85);

    }
    else if ( MC ){
      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.0f #leq p_{T}^{avg} < %.0f GeV",ptmin,ptmax),kBlack,0.9,0.58);
      DrawLabel(tex,"Powheg+Pythia8",1,0.6,0.85);
    }

    VecI points = GetWvsEtaPoints(graphs2D[0],ptBins[ipt],ptBins[ipt+1]);
    vector< Graph* > eta_graphs;
    for ( int ig=0; ig<graphs2D.size(); ++ig ) {
      Graph *g = GetWvsEtaGraph(graphs2D[ig],points);
      if ( SMvsMM ) { FormatGraph(g,20+ig+1); }
      else  if ( etaVsnoEta ) { FormatGraph(g,20+ig+1); }
      else if ( Closure ) { FormatGraph(g,20+ig+1); }
      else { FormatGraph(g,ig+1); }

      eta_graphs.push_back(g);

      if ( !Difference ) {
	g->Draw("PL same");
      }
    }

    vector< Graph* > eta_subtraction_graphs;
    if ( Difference ) {
      Graph *G = eta_graphs[0];
      FormatGraph(G,1);
      eta_subtraction_graphs.push_back(G);
      G->Draw("PL same");
      for ( int ig=2; ig<eta_graphs.size(); ++ig ) {
   	Graph *g_Diff = squareDifference(eta_graphs[ig],eta_graphs[1]);
	FormatGraph(g_Diff,ig);
	eta_subtraction_graphs.push_back(g_Diff);
	g_Diff->Draw("PL same");
      }
    }

    heta->Draw("same axis");

    if( !Data && !MC ){
    DrawSubPad(can);
    hratio->Draw();
    }
    //DrawGuideLinesVsEta(etaBins[0],etaBins[etaBins.size()-1]);
    
    if ( SMvsMM || etaVsnoEta ) {
      Graph* r = Ratio(eta_graphs[0],eta_graphs[1]);
      FormatGraph(r,1);
      r->Draw("PL same");

      r = Ratio(eta_graphs[2],eta_graphs[3]);
      FormatGraph(r,3);
      r->Draw("PL same");
    }
    else if ( PowhegPythiavsSherpa ) {
      Graph* r = Ratio(eta_graphs[1],eta_graphs[0]);
      FormatGraph(r,1);
      r->Draw("PL same");

      r = Ratio(eta_graphs[3],eta_graphs[2]);
      FormatGraph(r,3);
      r->Draw("PL same");
    }
    else if ( Closure ) {
      Graph* r = Ratio(eta_graphs[2],eta_graphs[0]);
      FormatGraph(r,21);
      r->Draw("PL same");

      r = Ratio(eta_graphs[2],eta_graphs[1]);
      FormatGraph(r,22);
      r->Draw("PL same");
    }
    else if ( Difference ) {
      Graph* r = Ratio(eta_subtraction_graphs[1],eta_subtraction_graphs[2]);
      FormatGraph(r,1);
      r->Draw("PL same");
    }
    else if ( !MC && !Data ) {
      for ( int ig=1; ig<eta_graphs.size(); ++ig ) {
        Graph* r = Ratio(eta_graphs[0],eta_graphs[ig]);
        FormatGraph(r,ig+1);
        r->Draw("PL same");
      }
    }
  
    can->Print(outfn);
    if ( SMvsMM ) can->Print(Form("SMvsMM/%1.0fpTavg%1.0f_SMvsMM.pdf",ptmin,ptmax));
    if ( etaVsnoEta ) can->Print(Form("etaVsnoEta/%1.0fpTavg%1.0f_etaVsnoEta.pdf",ptmin,ptmax));
    if ( MCvsData ) can->Print(Form("MCvsData/%1.0fpTavg%1.0f_MCvsData.pdf",ptmin,ptmax));
    if ( PowhegPythiavsSherpa ) can->Print(Form("PowhegPythiavsSherpa/%1.0fpTavg%1.0f_PowhegPythiavsSherpa.pdf",ptmin,ptmax));
    if ( ScaledVsNot ) can->Print(Form("ScaledVsNot/%1.0fpTavg%1.0f_ScaledVsNot.pdf",ptmin,ptmax));
    if ( HLTvsOffline ) can->Print(Form("HLTvsOffline/%1.0fpTavg%1.0f_HLTvsOffline.pdf",ptmin,ptmax));
    if ( Difference ) can->Print(Form("Difference/%1.0fpTavg%1.0f_Difference.pdf",ptmin,ptmax));
    if ( Data ) can->Print(Form("Data/%1.0fpTavg%1.0f_Data.pdf",ptmin,ptmax));
    if ( MC ) can->Print(Form("MC/%1.0fpTavg%1.0f_MC.pdf",ptmin,ptmax));
  }

  TH1F *hpt = new TH1F("hpt","",1,ptBins[0],ptBins[ptBins.size()-1]);
  FormatHisto(hpt,1);

  hratio = new TH1F("hratio_pt","",1,ptBins[0],ptBins[ptBins.size()-1]);
  FormatHisto(hratio,1);

  hpt->SetYTitle("#sigma(p_{T})_{unsubtracted}/p_{T}");
  hpt->SetXTitle("p_{T} [GeV]");
  hpt->SetMinimum(0.001);
  hpt->SetMaximum(0.7);
  hpt->GetXaxis()->SetLabelColor(0);
  hpt->GetXaxis()->SetTitleOffset(1.1);
  hpt->Draw();

  if( !Data && !MC ){  
  hratio->SetYTitle("Ratio");
  hratio->SetXTitle("p_{T} [GeV]");
  hratio->SetMinimum(0.0);
  hratio->SetMaximum(2.0);
  hratio->GetXaxis()->SetTitleOffset(1.1);
  }
  
  can->SetLogx(1);

  // loop over eta bins
  for (int ieta=0;ieta<etaBins.size()-1;++ieta) {
    float etamin=etaBins[ieta]; float etamax=etaBins[ieta+1];
    can->Clear();
    hpt->Draw();
    //DrawGuideLinesVsPt();
    
    if ( SMvsMM ) {
      hratio->SetYTitle("MM / SM");
      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.71,0.15);


      DrawText(tex,"Central reference method",kBlack,0.9,0.55);
      //void DrawLabel(TLatex* tex, TString txt, double x, double y, int mstyle, int col, double msize) {
      DrawLabel(tex,"Data",0.53,0.86,24,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.71,0.86,26,kBlue+1,1);
     // DrawLabel(tex,"Forward ",0.37,0.86,28,kRed+1,1);
      DrawText(tex,"Matrix method",kBlack,0.8,0.55);
      DrawLabel(tex,"Data",0.53,0.76,20,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.71,0.76,22,kBlue+1,1);
    }
    else if ( MCvsData ){
      hpt->SetMinimum(-0.01);
      hpt->SetMaximum(1.0);
      hratio->SetYTitle("MC / Data");

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);

      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"Data",1,0.6,0.85);
      DrawLabel(tex,"Sherpa EM+JES",2,0.6,0.8);
      DrawLabel(tex,"Sherpa Truth",3,0.6,0.75);
    }
    else if ( PowhegPythiavsSherpa ){
      hpt->SetMinimum(0.01);
      hpt->SetMaximum(0.8);
      hratio->SetYTitle("EM+JES / Truth");

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);

      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"Powheg+Pythia8 EM+JES",1,0.6,0.8);
      DrawLabel(tex,"Powheg+Pythia8 Truth",2,0.6,0.75);
      DrawLabel(tex,"Sherpa EM+JES",3,0.6,0.7);
      DrawLabel(tex,"Sherpa Truth",4,0.6,0.65);

    }

    else if ( ScaledVsNot ){
      hpt->SetMinimum(0.88);
      hpt->SetMaximum(1.25);
      hratio->SetYTitle("MC / Data");

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"Luminosity Scaled",1,0.6,0.85);
      DrawLabel(tex,"No Scaling",2,0.6,0.8);
    }
    else if ( HLTvsOffline ){
      hpt->SetMinimum(0.83);
      hpt->SetMaximum(1.28);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"Central offline jets",1,0.6,0.85);
      DrawLabel(tex,"Central HLT jets",2,0.6,0.8);
    }
    else if ( Closure ){
      hpt->SetMinimum(0.88);
      hpt->SetMaximum(1.25);

      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);

      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"Central",0.6,0.85,24,kBlack,1);
      DrawLabel(tex,"Central with calibration",0.6,0.8,20,kBlack,1);
      DrawLabel(tex,"Powheg+Pythia8",0.6,0.75,26,kBlue+1,1);
    }
    else if ( Difference ) {
      hpt->SetYTitle("#sigma(p_{T})/p_{T}");
      hpt->SetMinimum(0.);
      hpt->SetMaximum(0.6);
      hratio->SetYTitle("Data/MC");

      ATLASLabel(0.15,0.89,(char*)"Work in Progress",kBlack);
      DrawText(tex,"Dijet Balance Method",kBlack,0.83,0.15);
      DrawText(tex,"#sqrt{s} = 13 TeV, 3.2 fb^{-1}",kBlack,0.77,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4",kBlack,0.71,0.15);
      DrawText(tex,"#sqrt{[#sigma_{jet}^{EM+JES}]^{2} - [#sigma_{Truth}^{MC}]^{2}}",kBlack,0.65,0.15);
      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);

      DrawLabel(tex,"(p_{T}^{Reco}-p_{T}^{Truth})/p_{T}^{Reco}",1,0.6,0.83);
      DrawLabel(tex,"Sherpa",2,0.6,0.76);
      DrawLabel(tex,"Data",3,0.6,0.7);
    }

    else if ( Data ){
      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);
      DrawLabel(tex,"Data",1,0.6,0.85);
    }
    else if ( MC ){
      ATLASLabel(0.15,0.89,(char*)"Internal",kBlack);
      DrawText(tex,"#sqrt{s} = 13 TeV",kBlack,0.83,0.15);
      DrawText(tex,"anti-k_{t} #font[52]{R} = 0.4, EM+JES",kBlack,0.77,0.15);
      DrawText(tex,Form("%.1f #leq #eta_{det} < %.1f",etamin,etamax),kBlack,0.9,0.58);
      DrawLabel(tex,"Powheg+Pythia8",1,0.6,0.85);
    }

    VecI points = GetWvsPtPoints(graphs2D[0],etaBins[ieta],etaBins[ieta+1]);
    vector< Graph* > eta_graphs;
    for ( int ig=0; ig<graphs2D.size(); ++ig ) {
      Graph *g = GetWvsPtGraph(graphs2D[ig],points);
      if ( SMvsMM ) { FormatGraph(g,20+ig+1); }
      else if ( Closure ) { FormatGraph(g,20+ig+1); }
      else { FormatGraph(g,ig+1); }

      eta_graphs.push_back(g);
      
      if ( !Difference ) {
       g->Draw("PL same");
      }
    }

    vector< Graph* > eta_subtraction_graphs;
    if ( Difference ) {
      Graph *G = eta_graphs[0];
      FormatGraph(G,1);
      eta_subtraction_graphs.push_back(G);
      G->Draw("PL same");
      for ( int ig=2; ig<eta_graphs.size(); ++ig ) {
	Graph *g_Diff = squareDifference(eta_graphs[ig],eta_graphs[1]);
	FormatGraph(g_Diff,ig);
	eta_subtraction_graphs.push_back(g_Diff);
	g_Diff->Draw("PL same");
      }
    }

    hpt->Draw("same axis");

    if( !Data && !MC ){
    DrawSubPad(can);
    gPad->SetLogx(1);
    hratio->Draw();
    }

    //DrawGuideLinesVsPt();
    
    if ( SMvsMM ) {
      Graph* r = Ratio(eta_graphs[0],eta_graphs[2]);
      FormatGraph(r,21);
      r->Draw("PL same");

      r = Ratio(eta_graphs[1],eta_graphs[3]);
      FormatGraph(r,22);
      r->Draw("PL same");
    }
    if ( PowhegPythiavsSherpa ) {
      Graph* r = Ratio(eta_graphs[1],eta_graphs[0]);
      FormatGraph(r,1);
      r->Draw("PL same");

      r = Ratio(eta_graphs[3],eta_graphs[2]);
      FormatGraph(r,3);
      r->Draw("PL same");
      }
    else if ( Closure ) {
      Graph* r = Ratio(eta_graphs[2],eta_graphs[0]);
      FormatGraph(r,21);
      r->Draw("PL same");

      r = Ratio(eta_graphs[2],eta_graphs[1]);
      FormatGraph(r,22);
      r->Draw("PL same");
    }
    else if ( Difference ) {
      Graph* r = Ratio(eta_subtraction_graphs[1],eta_subtraction_graphs[2]);
      FormatGraph(r,1);
      r->Draw("PL same");
    }
    else if ( !MC && !Data ) {
      for ( int ig=1; ig<eta_graphs.size(); ++ig ) {
        Graph* r = Ratio(eta_graphs[0],eta_graphs[ig]);
        FormatGraph(r,ig+1);
        r->Draw("PL same");
      }
    }

    can->Print(outfn);
    if ( SMvsMM ) can->Print(Form("SMvsMM/etaBin%d_SMvsMM.pdf",ieta+1));
    if ( MCvsData ) can->Print(Form("MCvsData/etaBin%d_MCvsData.pdf",ieta+1));
    if ( PowhegPythiavsSherpa ) can->Print(Form("PowhegPythiavsSherpa/etaBin%d_PowhegPythiavsSherpa.pdf",ieta+1));
    if ( ScaledVsNot ) can->Print(Form("ScaledVsNot/etaBin%d_ScaledVsNot.pdf",ieta+1));
    if ( HLTvsOffline ) can->Print(Form("HLTvsOffline/etaBin%d_HLTvsOffline.pdf",ieta+1));
    if ( Difference ) can->Print(Form("Difference/etaBin%d_Difference.pdf",ieta+1));
    if ( Data ) can->Print(Form("Width/etaBin%d_Data.pdf",ieta+1));
    if ( MC ) can->Print(Form("Width/etaBin%d_MC.pdf",ieta+1));

  }

  std::cout<<"graphs2d size = "<<graphs2D.size()<<std::endl;
  TFile * outfile = new TFile("Resolution2DHist.root", "RECREATE");
  outfile->cd();

  for ( int fi=0; fi<graphs2D.size(); fi++ ) {
    graphs2D[fi]->Write();
  }


  can->SetLogx(0);
  can->Print(outfn+"]");
  WritePDF();

  return 0;
}

void DrawGuideBands(const VecD &etaBins) {
  static TBox b4 = TBox(etaBins[0],0.2,etaBins[etaBins.size()-1],0.4);
  b4.SetFillColor(kGray);
  b4.Draw("F");
  static TBox b3 = TBox(etaBins[0],0.2,etaBins[etaBins.size()-1],0.4);
  b3.SetFillColor(kOrange);
  b3.SetFillStyle(1001);
  b3.Draw("F");
  static TBox b2 = TBox(etaBins[0],0.2,etaBins[etaBins.size()-1],0.4);
  b2.SetFillColor(kOrange-2);
  b2.SetFillStyle(1001);
  b2.Draw("F");
  static TBox b1 = TBox(etaBins[0],0.2,etaBins[etaBins.size()-1],0.4);
  b1.SetFillColor(kOrange-4);
  b1.SetFillStyle(1001);
  b1.SetFillColorAlpha(kOrange-4, 0.35);
  b1.Draw("F");
  return;
}

void GetPtEtaBins(Str jetAlgo, Str file, VecD &etaBins, VecD& ptBins) {

  TFile *f_data_temp = Open(file);
  TH2D *h_pT_temp  = Get2DHisto(f_data_temp,jetAlgo+"_AvgPtAvg_vs_EtaPtAvg_"+"MM");
  for( int ipt=0; ipt<=h_pT_temp->GetNbinsY(); ipt++) ptBins.push_back(h_pT_temp->GetYaxis()->GetBinLowEdge(ipt+1) );
  int NptBins=ptBins.size()-1;
  for( int ieta=0; ieta<=h_pT_temp->GetNbinsX(); ieta++) etaBins.push_back(h_pT_temp->GetXaxis()->GetBinLowEdge(ieta+1) );
  int NetaBins=etaBins.size()-1;
  delete h_pT_temp;
  f_data_temp->Close();

  return;
}


// trim the 2d response graphs so that they have the same number of points corresponding to the same x/y values
void TrimGraphs(vector< Graph2D* > &graphs, double maxPt) {

  double maxErr =  0.04;

  // check that number of points in all graphs are equal
  for ( int ig=1; ig<graphs.size(); ++ig ) {
    if ( graphs[0]->GetN()!=graphs[ig]->GetN() ) {
      printf(" Npoints g1 = %d, g%d = %d",graphs[0]->GetN(),ig+2,graphs[ig]->GetN());
      error("Trimming issue!");
    }
  }

  int n=graphs[0]->GetN();
  // loop over bins
  for ( int i=0; i<n; ++i ) {
    // skip bins where response is less than or equal to zero
    for ( const auto &g : graphs ) {
      if ( g->GetZ()[i]<=0 ) continue;
    }

    bool bad_point = false;
    for ( const auto &g : graphs ) {
      if ( g->GetX()[i]>100 ) {
        for ( const auto &g2 : graphs ) {
          if ( g2->GetEZ()[i]>maxErr ) {
            bad_point=true;
          }
        }
      }
    }

    if ( bad_point ) {
      for ( auto &g : graphs ) {
        g->RemovePoint(i+1);
      }
    }
  }

  return;
}








/*
vector<TH2D*> MakeVsEtaPlots(TCanvas* can, Str outfn, StrV &files, Str jetAlgo, VecVecD &pTranges, Str method){

vector< vector<TH1D*> > ResponseHistosTot;

for (int i=0;i<pTranges.size();++i) {
float pTmin = pTranges[i][0];  float pTmax = pTranges[i][1];
Str name = Form("%s_pt%.0fto%.0f_R_%s", jetAlgo.Data(), pTmin, pTmax, method.Data());
vector<TH1D*> HistV;

if( method!="MMSMcomp" &&  method!="Truthcomp"){
for(int fi=0; fi<files.size(); fi++){
TFile *f = TFile::Open(files[fi]);
if (f==NULL) error("Cannot open "+files[fi]);
Str jetR="4";
if(files[fi].Contains("Kt2")) jetR="2";
if(files[fi].Contains("Kt3")) jetR="3";
if(files[fi].Contains("Kt4")) jetR="4";
if(files[fi].Contains("Kt5")) jetR="5";
if(files[fi].Contains("Kt6")) jetR="6";
if(files[fi].Contains("Kt7")) jetR="7";
if(files[fi].Contains("Kt8")) jetR="8";


HistV.push_back( GetHisto(f, jetAlgo, name) );
}
}


VecD etaBins;
for( int ieta=1; ieta<=HistV[0]->GetNbinsX()+1; ieta++) etaBins.push_back( HistV[0]->GetBinLowEdge( ieta ) );

ResponseHistosTot.push_back(HistV);
DrawPtBalance(can,HistV,jetAlgo);

can->Print(outfn);

} // loop over pTavg bins

return Get2dResponseHistos(ResponseHistosTot);
}


void DrawPtBalance(TCanvas* can, vector<TH1D*> Histos, Str calibration) {

can->clear();

double Max = 0, Min = 0;
GetMax(Histos,0,Histos.size(),Max,Min);
FIGURE1MAX=FIGUREMAX; FIGURE1MIN=FIGUREMIN;
double figMAX;
if(fabs(Min) > fabs(Max) ) figMAX = fabs(Min);
else                       figMAX = fabs(Max);

if( (figMAX>0.8 && figMAX<0.9) ||  (figMAX>1.1 && figMAX<1.2) )       { FIGURE1MAX=1.23; FIGURE1MIN=0.77; }
else if( (figMAX>0.9 && figMAX<0.95) || (figMAX>1.05 && figMAX<1.1) ) { FIGURE1MAX=1.15; FIGURE1MIN=0.85; }
else if( (figMAX>0.95 && figMAX<1.0) || (figMAX>1.0 && figMAX<1.05) ) { FIGURE1MAX=1.10; FIGURE1MIN=0.90; }
else if( (figMAX>0.7 && figMAX<0.8)  || (figMAX>1.2 && figMAX<1.3) )  { FIGURE1MAX=1.33; FIGURE1MIN=0.67; }

if(overRideFig){FIGURE1MAX=FIGUREMAX; FIGURE1MIN=FIGUREMIN; }

FormatH1(Histos[0]);
if(_comp2011to2012 )   FormatH(Histos[0], MARKERstyle, 2, markerSize);

if(!isVsEta) {Can->GetPad(0)->SetLogx(); _CAN->GetPad(0)->SetLogx();  }

Histos[0]->Draw("");
if( _RATIO ) Histos[0]->GetXaxis()->SetLabelOffset(20);
//if(_RATIO && isVsEta ) Histos[0]->GetXaxis()->SetLabelOffset(20);
for(int i=1; i<Histos.size(); i++){
FormatH(Histos[i],MARKERstyle+i,i,markerSize); Histos[i]->Draw("same");
if(_comp2011to2012 ) {
if(i==1) FormatH(Histos[i],MARKERstyle+1,8,markerSize); Histos[i]->Draw("same");
if(i==2) FormatH(Histos[i],MARKERstyle,1,markerSize); Histos[i]->Draw("same");
if(i==3) FormatH(Histos[i],MARKERstyle+1,7,markerSize); Histos[i]->Draw("same");
}
}


//for( int ipt=0; ipt<Histos[0]->GetNbinsX(); ipt++){
//  cout<<"**************************"<<endl;
//  cout<<" pT bin1  : "<<Histos[0]->GetBinLowEdge(ipt+1)<<endl;
//  cout<<" pT bin2  : "<<Histos[1]->GetBinLowEdge(ipt+1)<<endl;
//}


Str JetLabel;
if(calibration.Contains("AntiKt4EMTopo"))      JetLabel = "anti-k_{T} R=0.4 EM+"+_CALIBRATION;
else if(calibration.Contains("AntiKt4LCTopo")) JetLabel = "anti-k_{T} R=0.4 LC+"+_CALIBRATION;
else if(calibration.Contains("AntiKt6EMTopo")) JetLabel = "anti-k_{T} R=0.6 EM+"+_CALIBRATION;
else if(calibration.Contains("AntiKt6LCTopo")) JetLabel = "anti-k_{T} R=0.6 LC+"+_CALIBRATION;
else if(jetAlgo.Contains("AntiKt4Truth")) JetLabel = "anti-k_{T} R=0.4 Truth";
else if(jetAlgo.Contains("AntiKt6Truth")) JetLabel = "anti-k_{T} R=0.6 Truth";


DrawText(JetLabel,kBlack,0.9,TEXTSIZE);
//DrawText(Form("%.0f < p_{T}^{avg} < %.0f GeV",PtMin,PtMax), 1, 0.84, TEXTSIZE);
DrawTextR(LABEL_2, kBlack, 0.83, TEXTSIZE);
if(isVsEta){
DrawText(Form("%.0f < p_{T}^{avg} < %.0f GeV",PtMin,PtMax), 1, 0.84, TEXTSIZE);
//if(_comp2011to2012)
//DrawText(Form("%.0f < p_{T}^{avg} < %.0f GeV",_pTmin2011,_pTmax2011), GetColor(1) , 0.4, TEXTSIZE);
//DrawTextR(LABEL_2, kBlack, 0.83, TEXTSIZE);
}else{
Str ETAdesc;
if(_ABS) ETAdesc = Form("%.1f < |#eta| < %.1f",etamin,etamax);
else ETAdesc = Form("%.1f < #eta < %.1f",etamin,etamax);
DrawTextL(ETAdesc,kBlack,0.83,TEXTSIZE);
//DrawTextR(LABEL_2, kBlack, 0.82, TEXTSIZE);
}


double labely = labelY, dy = 0.05, dx = 0.26, labelx = labelX;
if(_NumFiles>3 && isVsEta) { labely = 0.6; dy=0.035; }
if(!isVsEta) { labely = 0.8; labelx = 0.7; }
if(_method=="MMSMcomp") {dx = 0.25; labelx = 0.45; }
if(_method=="Truthcomp") {dx = 0.25; labelx = 0.28; }
if(_method=="Truthcomp") {labely=0.79; }
if(_method=="Truthcomp" && FILES[0].Contains("erwig")) {labely=0.55; }

for(int fi=0; fi<_LABELS.size(); fi++){

tex->SetTextSize(0.03);
DrawplotLabel(_LABELS[fi].Data(),labelx,labely,MARKERstyle+fi,fi);
tex->SetTextSize(TEXTSIZE);

labely =  labely-dy;
}

// Draw the ATLAS label
double x=0.145, y=0.89;
DrawTextR(LABEL_1, kBlack, 0.88, LabelSize);
DrawTextR(_Method, kBlack, 0.4, LabelSize);
//PrintIntegratedLumi();

Histos[0]->Draw("same");

//if( _comp2011to2012 && isVsEta)
if(_RATIO) DrawRatio(Histos);
else {
if(isVsEta)
if(_ABS) Histos[0]->GetXaxis()->SetTitle("Jet |#eta|");
else Histos[0]->GetXaxis()->SetTitle("Jet #eta");
else Histos[0]->GetXaxis()->SetTitle("pT_{avg} [GeV]");
}
//else{
//Histos[0]->GetXaxis()->SetTitle("pT_{avg} [GeV]");
//Histos[0]->GetXaxis()->SetTitleOffset(1.2);
//}

}
*/
