#!/bin/sh

exec=matrix_minimization.exe

configFile="utils/Dijet_xAOD.config"
eta="Eta0.3"
inFile="Data_nominal.root" #"user.jrawling.10276340._000001.EtaInterCal_3DHistos_data.root" #"data15_13TeV_periodsDEFGHJ_triggerEmulation.root"
jetAlgo="AntiKt4EMTopo"
productionTag="25nsFinal"
syst=""
outDir="./"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/Data/EtaInterCal_3DHists_Period_DandE.root"
#inDir="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/triggerEmulation/data15_13TeV_periodsDEFGHJ_triggerEmulation.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Data/user.jrawling.data15_13TeV.Rerun.r7600_p2521_p2566.20161220.1617_EtaInterCal_3DHistos_data.root/user.jrawling.10276340._000001.EtaInterCal_3DHistos_data.root"
#inDir="Data_nominal_toy1.root"
inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/data/Data_nominal.root"

#configFile="utils/Dijet_xAOD.config"
#configFile="utils/Dijet_xAOD_j3545test.config"
#productionTag="25nsFinal"
#syst=""

#eta="Eta0.3"


./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data15_NewCodeTest_${eta}_${jetAlgo}.root

##Forward as default 


#inDir="/pc2012-data2/jrawling/EtaInetrcalibrationAnalysis/data15_13TeV_FixedTrigger/triggerEmulation/data15_13TeV_periodsDEFGHJ_triggerEmulation.root"
#outDir="~/doubleCloneData/"

#jetAlgo="AntiKt4EMTopo"
#./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data_13TeV_doubleCloneBadFitTest_periodsDEFGHJ_${eta}_${jetAlgo}.root
#jetAlgo="AntiKt4EMPFlow"
#./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data_13TeV_finalFinal_periodsDEFGHJ_${eta}_${jetAlgo}.root
#jetAlgo="AntiKt4LCTopo"
#./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir} ${outDir}data_13TeV_finalFinal_periodsDEFGHJ_${eta}_${jetAlgo}.root


#inFile="periodC_all.root"
#jetAlgo="AntiKt4EMTopo"
#productionTag="highpT"
#syst="missingRun"
#outDir="periodC"
#inDir=${ETA_INTERCAL_RESULTS}"/systematics/data/"
#eta="Eta0.3"
#./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir}${inFile} ${outDir}data_13TeV_periodC_${syst}_${eta}_${jetAlgo}.root


# for eta in "Eta0.3"; do
# #    for jetAlgo in HLT_a4tcemsubjesFS AntiKt4EMTopo; do
#     for jetAlgo in AntiKt4EMTopo; do
# #	for syst in beforeReproc afterReproc; do
# #	for syst in EMscale Closure; do
# 	for syst in mergedBins; do
# #	for syst in 70GeV; do
# #	    inDir="/pc2012-data2/mqueitsch/jetetmiss/dijet-intercalibration/data/periodA_"${syst}"/"
# 	    inDir="/pc2012-data2/mqueitsch/jetetmiss/dijet-intercalibration/systematics/data/"
# 	    inFile="periodC_"${syst}".root"
# 	    productionTag="highpT"
# #	    syst="70pTavg85_j25"
# 	    nohup ./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir}${inFile} ${outDir}data_13TeV_periodC_${syst}_${eta}_${jetAlgo}.root &> ${outDir}data_${jetAlgo}_periodC_${syst}_${eta}.log &
# 	done
#     done
# done

#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_periodC_highPtTriggers_${ieta}_${jetAlgo}.root

#for eta in "Eta0.3"; do
##    for jetAlgo in AntiKt4EMTopo; do
#    for jetAlgo in HLT_a4tcemsubjesFS; do#
#	for syst in "nominal"; do
##	for syst in "nominal" "JVTtight" "Dphijj_up" "Dphijj_dn" "j3pT_up" "j3pT_dn" ; do
##	for syst in "noCalib"; do
#	    inFile="periodC_"${syst}".root"
#	    nohup ./$exec $configFile $jetAlgo $productionTag ${eta} ${inDir}${inFile} ${outDir}data_13TeV_periodC_${syst}_${eta}_${jetAlgo}.root &> ${outDir}data_${jetAlgo}_periodC_${syst}_${eta}.log &
#	done
#    done
#done

#inDir=${ETA_INTERCAL_RESULTS}"/data/periodA_vetoEventsInBadEta/"
#ieta="Eta2011"
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_periodA_r6848_vetoEventsInBadEta_Eta2011_${jetAlgo}.root &> data_${jetAlgo}_periodA_r6848_vetoEventsInBadEta_Eta2011.log & 

# Full period A
# ieta="Eta2011"
# inDir=${ETA_INTERCAL_RESULTS}"/ForwardTriggers/periodA/"
# nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_periodA_r6848_ForwardTriggers_CentralBins_${jetAlgo}.root &> data_${jetAlgo}_periodA_r6848_ForwardTriggers_CentralBins.log &

# for run in 00267638 00267639 00267073 00267167
# do
#     inDir=${ETA_INTERCAL_RESULTS}"/ForwardTriggers/"${run}"/"
#     nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${run}_r6848_ForwardTriggers_CentralBins_${jetAlgo}.root &> data_${jetAlgo}_${run}_r6848_ForwardTriggers_CentralBins.log &
# done

# for period in periodA4 periodA1 periodA3
# do
#     inDir=${ETA_INTERCAL_RESULTS}"/ForwardTriggers/"${period}"/"
#     nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${period}_r6848_ForwardTriggers_CentralBins_${jetAlgo}.root &> data_${jetAlgo}_${period}_r6848_ForwardTriggers_CentralBins.log &
# done

#jetAlgo="AntiKt4EMTopo"
#for run in 00267638 00267639
#do
#for suffix in tightJet3Cut #noGSC noJet3Cut 
#do
#    productionTag="p1562"
#    inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/data/00267638_00267639_"${suffix}"/"
#    nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${suffix}_r6848_${jetAlgo}.root &> data_${jetAlgo}_${suffix}_r6848.log &
#done

#productionTag="p1562"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/reprocessed_data/00267638_00267639/"
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_00267638_00267639_r6848_${jetAlgo}.root &> data_${jetAlgo}_00267638_00267639_r6848.log &


#suffix="00267073_00267152"
#suffix="00267167"
#suffix="00267639"

#for jetAlgo in AntiKt4EMTopo HLT_a4tcemsubjesFS
#do 
#    nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${suffix}_${jetAlgo}.root &> data_${jetAlgo}_suffix.log &
#done

#jetAlgo="AntiKt4EMTopo"
#jetAlgo="HLT_a4tcemsubjesFS"
#productionTag="p1562"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/reprocessed_data/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/data/"
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_LumiWeights_${jetAlgo}.root &> data_${jetAlgo}_LumiWeights.log &
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_periodA_r6848_${jetAlgo}.root &> data_${jetAlgo}_periodA_r6848.log &
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_periodA_LumiWeights_${jetAlgo}.root &> data_${jetAlgo}_periodA_LumiWeights.log &

#jetAlgo="AntiKt4EMTopo"
#for suffix in 00267073 00267167 00267638 00267639
#for suffix in 00267167
#do
#    productionTag=${suffix}
#    inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/reprocessed_data/"${suffix}"/"
#    nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${suffix}_r6848_${jetAlgo}.root &> data_${jetAlgo}_${suffix}_r6848.log &
#done

# for jetAlgo in AntiKt4EMTopo #HLT_a4tcemsubjesFS
# do 
#     for suffix in 00267073 00267167 00267638 00267639 
#     do
#  	productionTag=${suffix}
#  	inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/reprocessed_data/"${suffix}"/"
#  	nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${suffix}_r6848_${jetAlgo}.root &> data_${jetAlgo}_${suffix}_r6848.log &
#      done
#  done

#productionTag=${suffix}
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/data/"${suffix}"/"
#nohup ./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}data_13TeV_${suffix}_LumiWeights_${jetAlgo}.root &> data_${jetAlgo}_LumiWeights_${suffix}.log &

