#!/bin/sh

#jetAlgo=$1
gen="PowhegPythia" #"Sherpa"
#gen="Sherpa"
#suffix=$3

exec=matrix_minimization.exe
configFile="utils/Dijet_xAOD.config"
productionTag="p1562" #p2440 p1562
ieta="Eta0.3"

#inFile="PowhegPythia8_EtaInterCal_3DHistos.root"
#inFile="PowhegPythia_MCAsym_nominal.root"
inFile="PowhegPythia_Truth_nominal.root"
outDir="./"

#productionTag="25ns"
#gen="Sherpa"
#inDir="$PWD/mc15_13TeV_PowhegPythiaFinal.root"
#inDir="/higgs-data4/jrawling/EtaIntercalibration/Pythia83DHists/mc15_13TeV_Pythia8_3DHistograms.root"
#inDir="/pc2013-data5/jrawling/EtaIntercal/mc15_13TeV/SherpaFinal/mc15_13TeV_sherpa_final.root"
#inDir="/pc2013-data5/jrawling/EtaIntercal/mc15_13TeV/PowhegPythiaFinal/mc15_13TeV_PowhegPythiaFinal.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Sherpa/mc15_13tev_Sherpa.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/PowhegPythia/mc15_13tev_PowhegPythia.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/PowhegPythia/PowhegPythia8_EtaInterCal_3DHistos.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/PowhegPythia/PowhegPythia_MCAsym_nominal.root"
inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/PowhegPythia/PowhegPythia_Truth_nominal.root"
#inDir="/pc2012-data2/rpickles/EtaInetrcalibrationAnalysis/data15_13TeV/MC/AntiKt4EMTopo/3Dhists/Grid/Pythia8/Pythia_MCAsym_nominal.root"

#jetAlgo="AntiKt4EMTopo"
jetAlgo="AntiKt4Truth"
#./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_finalFinal_${ieta}_${jetAlgo}.root ${gen}

#jetAlgo="AntiKt4LCTopo"
#./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_MCAsym_${ieta}_${jetAlgo}.root ${gen}
./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_NewCodeTest_${ieta}_${jetAlgo}.root ${gen}
#jetAlgo="AntiKt4EMPFlow"
#./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir} ${outDir}${gen}_finalFinal_${ieta}_${jetAlgo}.root ${gen}


#ieta="Eta2011"

#for settings in 50ns week1
#do
#    jetAlgo="AntiKt4EMTopo"
#    inDir=${ETA_INTERCAL_RESULTS}"/mc/PowhegPythia8_"${settings}"/"
#    inFile="PowhegPythia.root"
#    nohup ./$exec ${configFile} ${jetAlgo} ${productionTag} ${ieta} ${inDir}${inFile} ${outDir}PowhegPythia8_${settings}_CentralTriggers_${jetAlgo}.root PowhegPythia &> PowhegPythia_${jetAlgo}_CentralTriggers.log &
#done

#jetAlgo="AntiKt4EMTopo"
#jetAlgo="HLT_a4tcemsubjesFS"
#inDir="/tony-data2/mqueitsch/DijetEtaInterCalibration/PowhegPythia8_JETM1/"
#inDir="/tony-data1/mqueitsch/DijetEtaInterCalibration/No_JVF_JetCleaning/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/PowhegPythia8_"${suffix}"/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/PowhegPythia_"${suffix}"/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/mc/PowhegPythia8_"${suffix}"/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/mc/"${gen}"8_"${suffix}"/"
#inDir="/higgs-data1/mqueitsch/jetetmiss/dijet-intercalibration/Pythia_"${suffix}"/"
#inFile="PowhegPythia_"${suffix}".root"
#inFile="Pythia8.root"
#inFile="PowhegPythia8.root"
#inFile="PowhegPythia.root"
#inFile=${gen}".root"
#inFile="EtaInterCal_3DHistos.root"

#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}Pythia8_13TeV_MC15_nominal_${jetAlgo}.root Pythia
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}PowhegPythia8_13TeV_MC15_nominal_${jetAlgo}.root PowhegPythia
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}PowhegPythia8_13TeV_MC15_nominal_noJVFCleaning_${jetAlgo}.root PowhegPythia
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}PowhegPythia8_13TeV_MC15_${suffix}_${jetAlgo}.root PowhegPythia
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}PowhegPythia8_13TeV_MC15_${suffix}_${jetAlgo}.root PowhegPythia
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}${gen}8_13TeV_MC15_${suffix}_${jetAlgo}.root ${gen}
#./$exec $configFile $jetAlgo $productionTag ${ieta} ${inDir}${inFile} ${outDir}Pythia8_13TeV_MC15_${suffix}_${jetAlgo}.root Pythia

