/* 
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#ifndef HelperFunctions_h
#define HelperFunctions_h

// ignore warnings about unused local typedefs from trigger decision tool
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"

// C++ includes
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "xAODCore/ShallowCopy.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
///////////////////////for gamma
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"
#include "PathResolver/PathResolver.h"
#include <AsgTools/MessageCheck.h>

//#include "EventShapeTools/EventShapeCopier.h"

// ROOT includes
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "TEnv.h"
#include "TError.h"
#include "TRandom3.h"

// Tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetInterface/IJetUpdateJvt.h"  // updateJvt through AnaToolHandle
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "AsgTools/ToolHandle.h"

static const float GeV = 1000.;

bool descendingPt(xAOD::Jet* a, xAOD::Jet* b);

float deltaPhi(const xAOD::Jet& j1, const xAOD::Jet& j2);
float deltaR(const xAOD::Jet &j1,const xAOD::Jet &j2);
float deltaDetecR(const xAOD::Jet &j1,const xAOD::Jet &j2);
float deltaR(const xAOD::Jet &j1,const xAOD::JetRoI &j2);

TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh);
TH1F* createHist1D(TString hname, TString title, std::vector<double> bins);
TH2F* createHist2D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh);
TH2F* createHist2D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins);
TH3F* createHist3D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh, int znbins, float zlow, float zhigh);
TH3F* createHist3D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins, std::vector<double> zbins);

std::vector<TString> vectorise(TString str, TString sep=" ");
std::vector<double> vectoriseD(TString str, TString sep=" ");
std::vector<double> makeUniformVec(int N, double min, double max);

#endif
