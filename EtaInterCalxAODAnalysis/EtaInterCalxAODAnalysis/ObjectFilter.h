#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#ifndef OBJECTFILTER_H
#define OBJECTFILTER_H


#ifndef __CINT__
#include "xAODJet/Jet.h"
#include "xAODEgamma/PhotonContainer.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"

#endif /* __CINT __ */

#include "TEnv.h"
#include "TLorentzVector.h"
#include <vector>
#include <string> 
#include <math.h>


//class JetCleaningTool;
class egammaEnergyCorrectionTool;

class ObjectFilter
{

   protected:

   public:
      // Constructor
      ObjectFilter();

      // Destructor
      ~ObjectFilter();

      std::string Mode;
      
      // ///////////
      // Photon cuts
      // ///////////
      double MinPt_Photon;
      double MaxEta_Photon;
      std::string PhQual;
      // Isolation energy and isLoose saved for purity stupies;
      double IsolE;
      double isTight;
      double isIsol;

      double photon_id_sf;
      double photon_iso_sf;

      bool PassNomID;
      bool PassNomIsol;
      bool isMC;

      int FirstFailed;

      #ifndef __CINT__
      ////////
      // Tools
      ////////
      CP::IsolationSelectionTool *m_EgammaIsolationSelectionTool=0;
      CP::IsolationCorrectionTool* isoCorr_tool; //!
      ElectronPhotonShowerShapeFudgeTool* m_fudgeMCTool=0;
      AsgPhotonIsEMSelector* m_photonTightIsEMSelector=0;
      AsgPhotonIsEMSelector* m_photonLooseIsEMSelector=0;
      AsgPhotonIsEMSelector* m_photonLoosePIsEMSelector=0;
      AsgPhotonEfficiencyCorrectionTool* m_photonIDTightEfficiencySF=0;
      AsgPhotonEfficiencyCorrectionTool* m_photonISOTightEfficiencySF=0;

      // /////////
      // Functions
      // /////////
      bool LeadPhoton(xAOD::Photon *photon);
      bool PassPhoton(xAOD::Photon *photon, std::vector<int> *failed, bool isNom);
      #endif

      // /////////
      // Functions
      // /////////
      bool Init(TString Config, std::vector<std::string> *cuts, bool isMC);
      int GetFirstFailed() {return FirstFailed;};
      void Clean();
      bool isLoosePrime(xAOD::Photon *photon);

      // ///////////////////////////////
      // A few quick functions to access
      // information about the cutflow
      // ///////////////////////////////
      std::vector<std::string> *Cuts;
      int FindCode(std::string String){ if ((find(Cuts->begin(), Cuts->end(), String) - Cuts->begin()) == 24) std::cout << "problem " << String << std::endl; return find(Cuts->begin(), Cuts->end(), String) - Cuts->begin();};
      int NumCuts(){ return sizeof(*Cuts);};
      std::string GetCuts(int cut){return (*Cuts)[cut];};

      double GeV = 1000.;
};
#endif
