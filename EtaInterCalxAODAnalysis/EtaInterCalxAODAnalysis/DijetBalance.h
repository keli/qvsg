/* 
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#ifndef DijetBalance_h
#define DijetBalance_h

#include "EtaInterCalxAODAnalysis/HelperFunctions.h"

namespace DijetInSitu {
  
  class DijetBalance {
    
  public: 
    
    // Constructor
    DijetBalance(const xAOD::JetContainer& jets, float refRegion=0.8, bool truthJets=false);
    // Destructor
    ~DijetBalance();
    
    // Calculate dijet balance variables
    void Balance(const xAOD::JetContainer& jets, float refRegion=0.8, bool truthJets=false);

    float pTavg();

    float Asym_MM();
    float left_eta();
		float left_phi();
		float	left_pT();

    float right_eta();
		float right_phi();
		float right_pT();

		float thirdJet_phi();
		float	thirdJet_eta();
		float thirdJet_pT();

    float Asym_SM();
    float probe_eta();
		float probe_phi();
		float probe_pT();
    float ref_eta();
		float ref_pT();
		float ref_phi();

    bool  jetInRefRegion();
    bool	thirdJetExists();
		

  private:

    TString name;

    bool m_isData;
    bool m_isMC;
    bool m_isTruth;

    bool m_jetInRef, m_thirdJetExists;
    float m_Asym_MM; float m_Asym_SM;
    float m_pTavg;
    float m_probe_pT, m_probe_eta, m_probe_phi, m_probe_E;
    float m_ref_pT, m_ref_eta, m_ref_phi, m_ref_E;
    float m_right_pT, m_right_eta, m_right_phi, m_right_E;
    float m_left_pT, m_left_eta, m_left_phi, m_left_E;
    float m_j1_pT, m_j1_eta, m_j1_phi, m_j1_E;
    float m_j2_pT, m_j2_eta, m_j2_phi, m_j2_E;
    float m_j3_pT, m_j3_eta, m_j3_phi, m_j3_E;

  };
}

#endif
