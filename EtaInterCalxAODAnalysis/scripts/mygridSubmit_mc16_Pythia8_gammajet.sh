#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root


i=0

#((i++)); v_period[i]="09";  v_INDS[i]="mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="00";  v_INDS[i]="mc16_13TeV.423100.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP17_35.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="1";  v_INDS[i]="mc16_13TeV.423101.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP35_50.deriv.DAOD_JETM4.e3904_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="2";  v_INDS[i]="mc16_13TeV.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="3";  v_INDS[i]="mc16_13TeV.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="4";  v_INDS[i]="mc16_13TeV.423104.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP140_280.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="5";  v_INDS[i]="mc16_13TeV.423105.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP280_500.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="6";  v_INDS[i]="mc16_13TeV.423106.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP500_800.deriv.DAOD_JETM4.e3791_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="7";  v_INDS[i]="mc16_13TeV.423107.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP800_1000.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="8";  v_INDS[i]="mc16_13TeV.423108.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1000_1500.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="9";  v_INDS[i]="mc16_13TeV.423109.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1500_2000.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="10";  v_INDS[i]="mc16_13TeV.423110.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2000_2500.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
#((i++)); v_period[i]="11";  v_INDS[i]="mc16_13TeV.423111.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2500_3000.deriv.DAOD_JETM4.e4453_s3126_r9781_r9778_p3260"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Pythia8.${period}.p3260

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=3\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged

done
