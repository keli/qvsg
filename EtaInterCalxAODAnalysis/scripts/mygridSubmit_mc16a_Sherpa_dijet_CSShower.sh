#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root
# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="77";  v_INDS[i]="mc16_13TeV.364677.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ1W.deriv.DAOD_JETM1.e6997_s3126_r10455_p4133"
((i++)); v_period[i]="78";  v_INDS[i]="mc16_13TeV.364678.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ2W.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="79";  v_INDS[i]="mc16_13TeV.364679.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ3W.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="80";  v_INDS[i]="mc16_13TeV.364680.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ4W.deriv.DAOD_JETM1.e6929_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="81";  v_INDS[i]="mc16_13TeV.364681.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ5.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="82";  v_INDS[i]="mc16_13TeV.364682.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ6.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="83";  v_INDS[i]="mc16_13TeV.364683.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ7.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="84";  v_INDS[i]="mc16_13TeV.364684.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ8.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
((i++)); v_period[i]="85";  v_INDS[i]="mc16_13TeV.364685.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ9plus.deriv.DAOD_JETM1.e6929_e5984_s3126_r10455_r10210_p4133"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Sherpa.${period}.CSShower.p4133

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
