#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root


i=0

((i++)); v_period[i]="1";  v_INDS[i]="mc16_13TeV.361281.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ1.deriv.DAOD_JETM1.e7530_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="2";  v_INDS[i]="mc16_13TeV.361282.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ2.deriv.DAOD_JETM1.e7530_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="3";  v_INDS[i]="mc16_13TeV.361283.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ3.deriv.DAOD_JETM1.e7530_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="4";  v_INDS[i]="mc16_13TeV.361284.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ4.deriv.DAOD_JETM1.e7481_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="5";  v_INDS[i]="mc16_13TeV.361285.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ5.deriv.DAOD_JETM1.e7481_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="6";  v_INDS[i]="mc16_13TeV.361286.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ6.deriv.DAOD_JETM1.e7481_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="7";  v_INDS[i]="mc16_13TeV.361287.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ7.deriv.DAOD_JETM1.e7481_e5984_s3126_r10455_r10210_p3596"
((i++)); v_period[i]="8";  v_INDS[i]="mc16_13TeV.361288.PowhegPythia8EvtGen_A14NNPDF23_jj_JZ8.deriv.DAOD_JETM1.e7481_e5984_s3126_r10455_r10210_p3596"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_PowPht.${period}.a.p3596

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
