USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=
OUTPUTS=EtaInterCal_3DHistos.root
#SITE=
#ANALY_MANC_SL6_SHORT
#UKI-NORTHGRID-MAN-HEP_LOCALGROUPDISK

for i in 1 2 3 4 5 6 7 8 9
do

	INDS=mc15_13TeV.42600${i}.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ${i}.merge.DAOD_JETM1.e3788_s2608_s2183_r7725_r7676_p2666/
    OUTDS=PowhegPythia8.JZ${i}.LCTopo.r7676_p2666
    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=10\
		 --tmpDir=/tmp\
         --extFile=JetCalibTools/*,EventShapeTools/*\
         --mergeOutput

done
