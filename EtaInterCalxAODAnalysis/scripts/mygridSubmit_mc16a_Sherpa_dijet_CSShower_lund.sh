#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root
# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="86";  v_INDS[i]="mc16_13TeV.364686.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ1W.deriv.DAOD_JETM1.e6997_s3126_r10201_p4049"
((i++)); v_period[i]="87";  v_INDS[i]="mc16_13TeV.364687.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ2W.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="88";  v_INDS[i]="mc16_13TeV.364688.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ3W.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="89";  v_INDS[i]="mc16_13TeV.364689.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ4W.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="90";  v_INDS[i]="mc16_13TeV.364690.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ5.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="91";  v_INDS[i]="mc16_13TeV.364691.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ6.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="92";  v_INDS[i]="mc16_13TeV.364692.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ7.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="93";  v_INDS[i]="mc16_13TeV.364693.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ8.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
((i++)); v_period[i]="94";  v_INDS[i]="mc16_13TeV.364694.Sherpa_CT10_CT14nnlo_CSShower_Lund_2to2jets_JZ9plus.deriv.DAOD_JETM1.e6929_s3126_r10201_p4049"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Sherpa.${period}.Lund.p4049

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
