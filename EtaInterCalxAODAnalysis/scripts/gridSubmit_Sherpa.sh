USERNAME=jrawling
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=SFU-LCG2_SCRATCHDISK,SFU-LCG2_LOCALGROUPDISK
OUTPUTS=EtaInterCal_3DHistos.root
SITE=ANALY_MANC_SL6_SHORT
#UKI-NORTHGRID-MAN-HEP_LOCALGROUPDISK

for i in 1 2 3 4 5 6
do
	INDS=mc15_13TeV.42613${i}.Sherpa_CT10_jets_JZ${i}.merge.DAOD_JETM1.*r7676_p2666
	OUTDS=Sherpa.PRW.JZ${i}.r7676_p2613
    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
         --exec="EtaInterCalxAODAnalysis/scripts/gridExec.sh %IN"\
         --useRootCore\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=10\
			   --tmpDir=/tmp\
         --extFile=JetCalibTools/*,EventShapeTools/*\
         --mergeOutput

done
