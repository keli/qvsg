#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos_data.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="B";  v_INDS[i]="data18_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="C";  v_INDS[i]="data18_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="D";  v_INDS[i]="data18_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="F";  v_INDS[i]="data18_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="L";  v_INDS[i]="data18_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="I";  v_INDS[i]="data18_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="K";  v_INDS[i]="data18_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="M";  v_INDS[i]="data18_13TeV.periodM.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="O";  v_INDS[i]="data18_13TeV.periodO.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"
((i++)); v_period[i]="Q";  v_INDS[i]="data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_JETM1.grp18_v01_p3704"

for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=data18_R21.${period}.p3704

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
