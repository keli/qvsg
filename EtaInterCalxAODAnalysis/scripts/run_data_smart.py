#!/bin/python

from subprocess import call,Popen
from sys import argv
import fnmatch
import os
import re
from math import ceil
import argparse

max_n_jobs=20
settings=""

parser = argparse.ArgumentParser(description='Script for splitting jobs to run over many input files')
parser.add_argument('-i','--input', help='Input files',  nargs='*', required=True)
parser.add_argument('-s','--set', help='Settings to be changed in config',required=False)
parser.add_argument('-n','--njobs', help='Maximum number of jobs',required=False)
args = parser.parse_args()
argsdict = vars(args)

if argsdict['njobs'] :
    max_n_jobs=int(args.njobs)

if argsdict['set'] :
    settings=args.set

# remove input files that are not root files
for f in range(0,len(args.input)) :
    fn = args.input[f]
    if fnmatch.fnmatch(fn, '*.txt') or fnmatch.fnmatch(fn, '*.log') :
        del args.input[f]

print max_n_jobs

n_input_files=len(args.input)
n_files_per_subjob=1
if n_input_files > max_n_jobs :
    n_files_per_subjob=ceil(float(n_input_files)/float(max_n_jobs))

print n_input_files
print n_files_per_subjob

n_files_in_job=0
n_files_processed=0
job_count=0
infiles=[]

for fn in args.input :
    n_files_in_job+=1
    n_files_processed+=1

    infiles.append(fn)

#    print "n_files_in_job "+str(n_files_in_job)
#    print "n_files_processed "+str(n_files_processed)

    if n_files_in_job >= n_files_per_subjob :
#        print infiles
        log_file = open("job_"+str(job_count)+".log","wb")
        Popen(["nice","-n","0","EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_"+str(job_count)+".root","--in"]+infiles,stdout=log_file,stderr=log_file) 
#        Popen(["EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_"+str(job_count)+".root","--in"]+infiles,stdout=log_file,stderr=log_file) 
        infiles=[]
        n_files_in_job=0
        job_count+=1
    elif n_files_processed == n_input_files :
#        print "Reached last file!"
#        print infiles
        log_file = open("job_"+str(job_count)+".log","wb")
        Popen(["nice","-n","0","EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_"+str(job_count)+".root","--in"]+infiles,stdout=log_file,stderr=log_file) 
 #       Popen(["EtaInterCalAnalysis","--set",settings,"--o","EtaInterCal_3DHists_"+str(job_count)+".root","--in"]+infiles,stdout=log_file,stderr=log_file) 
        infiles=[]
        n_files_in_job=0
        job_count+=1

