#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="2";  v_INDS[i]="mc16_13TeV.364902.H7EG_Matchbox_dipole_jetjetNLO_JZ2WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="3";  v_INDS[i]="mc16_13TeV.364903.H7EG_Matchbox_dipole_jetjetNLO_JZ3WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="4";  v_INDS[i]="mc16_13TeV.364904.H7EG_Matchbox_dipole_jetjetNLO_JZ4WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="5";  v_INDS[i]="mc16_13TeV.364905.H7EG_Matchbox_dipole_jetjetNLO_JZ5WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="6";  v_INDS[i]="mc16_13TeV.364906.H7EG_Matchbox_dipole_jetjetNLO_JZ6WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="7";  v_INDS[i]="mc16_13TeV.364907.H7EG_Matchbox_dipole_jetjetNLO_JZ7WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="8";  v_INDS[i]="mc16_13TeV.364908.H7EG_Matchbox_dipole_jetjetNLO_JZ8WithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
((i++)); v_period[i]="9";  v_INDS[i]="mc16_13TeV.364909.H7EG_Matchbox_dipole_jetjetNLO_JZ9plusWithSW.deriv.DAOD_JETM1.e7482_s3126_r9364_p4049"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_HegDipo.a.${period}.p4049

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
