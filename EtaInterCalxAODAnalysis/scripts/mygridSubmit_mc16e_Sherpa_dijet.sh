#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="31";  v_INDS[i]="mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="32";  v_INDS[i]="mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="33";  v_INDS[i]="mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="34";  v_INDS[i]="mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="35";  v_INDS[i]="mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="36";  v_INDS[i]="mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.deriv.DAOD_JETM1.e4355_s3126_r10724_p3864"
((i++)); v_period[i]="37";  v_INDS[i]="mc16_13TeV.426137.Sherpa_CT10_jets_JZ7.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
((i++)); v_period[i]="38";  v_INDS[i]="mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
((i++)); v_period[i]="39";  v_INDS[i]="mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
((i++)); v_period[i]="40";  v_INDS[i]="mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
((i++)); v_period[i]="41";  v_INDS[i]="mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
((i++)); v_period[i]="42";  v_INDS[i]="mc16_13TeV.426142.Sherpa_CT10_jets_JZ12.deriv.DAOD_JETM1.e4635_s3126_r10724_p3864"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Sherpa.${period}.e.p3864

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
