#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

((i++)); v_period[i]="20";  v_INDS[i]="mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="21";  v_INDS[i]="mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="22";  v_INDS[i]="mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="23";  v_INDS[i]="mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="24";  v_INDS[i]="mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="25";  v_INDS[i]="mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="26";  v_INDS[i]="mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_JETM1.e3569_e5984_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="27";  v_INDS[i]="mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="28";  v_INDS[i]="mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="29";  v_INDS[i]="mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="30";  v_INDS[i]="mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="31";  v_INDS[i]="mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_JETM1.e3569_s3126_r9781_r9778_p3260"
((i++)); v_period[i]="32";  v_INDS[i]="mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_JETM1.e3668_s3126_r9781_r9778_p3260"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Pythia8.${period}.p3260

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
