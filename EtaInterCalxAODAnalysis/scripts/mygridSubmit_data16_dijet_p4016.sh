#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos_data.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

#((i++)); v_period[i]="Z";  v_INDS[i]="data16_13TeV:data16_13TeV.periodZ.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p3601"
((i++)); v_period[i]="A";  v_INDS[i]="data16_13TeV.periodA.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="B";  v_INDS[i]="data16_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="C";  v_INDS[i]="data16_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="D";  v_INDS[i]="data16_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="E";  v_INDS[i]="data16_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="F";  v_INDS[i]="data16_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="G";  v_INDS[i]="data16_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="I";  v_INDS[i]="data16_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="K";  v_INDS[i]="data16_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
((i++)); v_period[i]="L";  v_INDS[i]="data16_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p4016"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=data16_R21.${period}.p4016

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=100\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
