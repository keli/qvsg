#!/bin/bash

USERNAME=rhankach
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos_data.root

# used with: data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml

i=0

((i++)); v_period[i]="D";  v_INDS[i]="data15_13TeV:data15_13TeV.00276262.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276329.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276336.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276416.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276511.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276689.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276778.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276790.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276952.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00276954.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319"
((i++)); v_period[i]="E";  v_INDS[i]="data15_13TeV:data15_13TeV.00278880.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00278912.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00278968.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279169.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279259.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279279.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279284.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279345.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279515.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279598.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279685.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279764.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279813.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279867.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279928.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319"
((i++)); v_period[i]="FG"; v_INDS[i]="data15_13TeV:data15_13TeV.00279932.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00279984.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280231.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280273.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280319.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280368.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280423.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280464.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280500.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280520.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280614.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280673.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280753.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280853.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280862.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280950.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00280977.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00281070.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00281074.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00281075.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319"
((i++)); v_period[i]="HJ"; v_INDS[i]="data15_13TeV:data15_13TeV.00281317.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00281385.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00281411.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00282625.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00282631.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00282712.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00282784.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00282992.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283074.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283155.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283270.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283429.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283608.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00283780.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284006.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284154.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284213.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284285.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284420.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284427.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319,data15_13TeV:data15_13TeV.00284484.physics_Main.deriv.DAOD_JETM1.r9264_p3083_p3319"

for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=data15_R21.${period}.p3319

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=10\
       --tmpDir=/tmp\
       --extFile=JetCalibTools/*,EventShapeTools/*\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
