#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos_data.root

# used with: data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml

i=0

#((i++)); v_period[i]="B";  v_INDS[i]="data17_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p3601"
((i++)); v_period[i]="B";  v_INDS[i]="data17_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="C";  v_INDS[i]="data17_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="D";  v_INDS[i]="data17_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="E";  v_INDS[i]="data17_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="F";  v_INDS[i]="data17_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="H";  v_INDS[i]="data17_13TeV.periodH.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="I";  v_INDS[i]="data17_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
((i++)); v_period[i]="K";  v_INDS[i]="data17_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM1.grp17_v01_p4016"
for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=data17_R21.${period}.p4016

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=100\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged
done
#to specify root version : --cmtConfig=x86_64-slc6-gcc49-opt
