#!/bin/bash

USERNAME=wasu
DATE=`date --utc '+20%y%m%d.%H%M'`
BLACKLIST=

OUTPUTS=EtaInterCal_3DHistos.root


i=((i++)); v_period[i]="39";  v_INDS[i]="mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="40";  v_INDS[i]="mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="41";  v_INDS[i]="mc16_13TeV.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="42";  v_INDS[i]="mc16_13TeV.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="43";  v_INDS[i]="mc16_13TeV.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="44";  v_INDS[i]="mc16_13TeV.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="45";  v_INDS[i]="mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="46";  v_INDS[i]="mc16_13TeV.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="47";  v_INDS[i]="mc16_13TeV.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="48";  v_INDS[i]="mc16_13TeV.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="49";  v_INDS[i]="mc16_13TeV.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="50";  v_INDS[i]="mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="51";  v_INDS[i]="mc16_13TeV.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="52";  v_INDS[i]="mc16_13TeV.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="53";  v_INDS[i]="mc16_13TeV.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="54";  v_INDS[i]="mc16_13TeV.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="55";  v_INDS[i]="mc16_13TeV.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="56";  v_INDS[i]="mc16_13TeV.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="57";  v_INDS[i]="mc16_13TeV.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="58";  v_INDS[i]="mc16_13TeV.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="59";  v_INDS[i]="mc16_13TeV.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="60";  v_INDS[i]="mc16_13TeV.361060.Sherpa_CT10_SinglePhotonPt4000_CVetoBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="61";  v_INDS[i]="mc16_13TeV.361061.Sherpa_CT10_SinglePhotonPt4000_CFilterBVeto.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"
((i++)); v_period[i]="62";  v_INDS[i]="mc16_13TeV.361062.Sherpa_CT10_SinglePhotonPt4000_BFilter.deriv.DAOD_JETM4.e3587_s3126_r9364_p3600"

for (( j = 1; j <= $i; j++ )); do
  period=${v_period[j]}
  INDS=${v_INDS[j]}
  OUTDS=mc16_Sherpa.${period}.p3600

  echo "Submitting to "${INDS}
  echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}

  prun --useAthenaPackages\
       --inDS=${INDS}\
       --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
       --outputs=${OUTPUTS}\
       --exec="EtaInterCalxAODAnalysis/scripts/gridExec_cmake.sh %IN"\
       --excludedSite=${BLACKLIST}\
       --nGBPerJob=3\
       --tmpDir=/tmp\
       --mergeOutput\
       --forceStaged

done
