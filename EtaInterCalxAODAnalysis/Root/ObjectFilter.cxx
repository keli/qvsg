#include "EtaInterCalxAODAnalysis/ObjectFilter.h"
#include "xAODEventInfo/EventInfo.h"
#include <vector>
#include "xAODEgamma/EgammaDefs.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
// Tools
//#include "JetSelectorTools/JetCleaningTool.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"

// Egamma EDM
#include "xAODEgamma/PhotonxAODHelpers.h"
// Needed for messages
#include <AsgTools/MessageCheck.h>

#define CHECK( ARG )                                  \
do {                                                  \
    const bool result = ARG;                          \
  if( ! result ) {                                    \
      ::Error( APP_NAME, "Failed to execute: \"%s\"", \
#ARG );                                               \
      return 1;                                       \
  }                                                   \
 } while( false )

using namespace asg::msgUserCode; // needed for messages
using std::cout;
using std::endl;

ObjectFilter::ObjectFilter()
{
}

ObjectFilter::~ObjectFilter()
{
}

bool ObjectFilter::Init(TString Config,  std::vector<std::string> *cuts, bool ismc)
{
  const char* APP_NAME = "Init";
  isMC = ismc;
  TEnv * settings = new TEnv();
  int status = settings->ReadFile(Config,EEnvLevel(0));
  if (status!=0) {
     cout<<"cannot read config file in ObjectFilter"<< endl;
     cout << Config << endl;
     cout<<"******************************************\n"<< endl;
     return false;
  }

  Mode      		= settings->GetValue("Mode", "None");

  // ///////////////
  // Get Photon cuts
  // ///////////////
  MinPt_Photon          = settings->GetValue("leadingPhoton_minPt", 25); // 25 GeV
  MaxEta_Photon		= settings->GetValue("leadingPhoton_maxEta", 1.37); // 1.37 
  PhQual 		= settings->GetValue("leadingPhoton_ID", "Tight"); // Tight

  
  // /////////////////////
  // Isolation tool
  // ///////////////////// 
  m_EgammaIsolationSelectionTool = new CP::IsolationSelectionTool("IsolationTool");
  // //////////////////
  // Set isolation cuts
  // //////////////////

  isoCorr_tool = new CP::IsolationCorrectionTool("isoCorr_tool");
  //isoCorr_tool->setProperty( "IsMC", isMC); //if MC, else false
  if (! isoCorr_tool->initialize().isSuccess() ){
    Fatal("ObjectFilter::Init", "Failed to properly initialize the isoCorr_tool. Exiting." );
    return false;
  }
  else Info("ObjectFilter::Init","Succesfully initialized the isoCorr_tool.");

  ANA_CHECK( m_EgammaIsolationSelectionTool->setProperty("PhotonWP", "FixedCutTight") ); //Cone20") );
  if (! m_EgammaIsolationSelectionTool->initialize().isSuccess()) {
    Fatal("ObjectFilter::Init", "Failed to properly initialize EgammaIsolationSelectionTool. Exiting." );
    return false;
  }
  else Info("ObjectFilter::Init","Succesfully initialized the EgammaIsolationSelectionTool.");


  // ////////////////////
  // photon scale factors
  // ////////////////////
  //
  // 1 -> IDEfficiency
  //
  m_photonIDTightEfficiencySF = new AsgPhotonEfficiencyCorrectionTool("AsgPhotonEfficiencyCorrectionTool");
  ANA_CHECK(m_photonIDTightEfficiencySF->setProperty("MapFilePath","PhotonEfficiencyCorrection/2015_2017/rel21.2/Winter2018_Prerec_v1/map0.txt"));
  ANA_CHECK(m_photonIDTightEfficiencySF->setProperty("ForceDataType",1));
  if(isMC){ANA_CHECK(m_photonIDTightEfficiencySF->initialize());}
  //
  // 2 -> ISOEfficiency
  //
  m_photonISOTightEfficiencySF = new AsgPhotonEfficiencyCorrectionTool("PhotonISOTightEfficiencyScaleFactors");
  ANA_CHECK(m_photonISOTightEfficiencySF->setProperty("MapFilePath","PhotonEfficiencyCorrection/2015_2017/rel21.2/Winter2018_Prerec_v1/map0.txt"));
  ANA_CHECK(m_photonISOTightEfficiencySF->setProperty("IsoKey","Tight"));    // Set isolation WP
  ANA_CHECK(m_photonISOTightEfficiencySF->setProperty("ForceDataType",1)); // 0-Data, 1-FULLSIM, 3-AF2
  if(isMC){ANA_CHECK(m_photonISOTightEfficiencySF->initialize());}
    
  // /////////////////
  // Photon Fudge Tool
  // ///////////////// 
  if (Mode == "Photon"){
    m_fudgeMCTool = new ElectronPhotonShowerShapeFudgeTool ( "FudgeMCTool" );
    int FFset = 22; // for MC15 samples, which are based on a geometry derived from GEO-21
    m_fudgeMCTool->setProperty("Preselection", FFset);
    m_fudgeMCTool->setProperty("FFCalibFile","ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root"); // Rel 21 

    if (!m_fudgeMCTool->initialize().isSuccess()) {
      Fatal("ObjectFilter::Init", "Failed to initialize FudgeMCTool ");
    }
    else Info("ObjectFilter::Init","Succesfully initialized the FudgeMCTool.");

    // ////////////////
    // Photon IsEM Tool
    // ////////////////

    // Tight initialization
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    m_photonTightIsEMSelector->setProperty("isEMMask",egammaPID::PhotonTight).ignore();
    m_photonTightIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf").ignore();
    if (!m_photonTightIsEMSelector->initialize().isSuccess()) {
      Fatal("ObjectFilter::Init", "Failed to properly initialize PhotonTightIsEMSelector. Exiting." );
      return false;
    }
    else Info("ObjectFilter::Init","Succesfully initialized the PhotonTightIsEMSelector.");

    // Loose initialization
    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
    m_photonLooseIsEMSelector->setProperty("isEMMask",egammaPID::PhotonLoose).ignore();
    m_photonLooseIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20151012/PhotonIsEMLooseSelectorCutDefs.conf").ignore();
    if (!m_photonLooseIsEMSelector->initialize().isSuccess()) {
      Fatal("ObjectFilter::Init", "Failed to properly initialize PhotonLooseIsEMSelector. Exiting." );
      return false;
    }
    else Info("ObjectFilter::Init","Succesfully initialized the PhotonLooseIsEMSelector.");

    // LoosePrime definition (updated by Daniel)
    int isem = m_photonTightIsEMSelector->IsemValue();
    const unsigned int  LOOSEPRIME =  egammaPID::PhotonLoose |
      0x1 << egammaPID::ClusterStripsWtot_Photon | 
      0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon |
      0x1 << egammaPID::ClusterStripsEratio_Photon; 
    const unsigned int PhotonLoosePrime =  LOOSEPRIME;
    bool isLooseP = (( isem & PhotonLoosePrime )==0); // loose'    
    // control couts (recommended mask: 392193
    cout << "PhotonLoosePrime criteria defined " << endl;
    cout << "PhotonLoosePrime operating with mask: " << PhotonLoosePrime << endl;
    
    /////////////////////////////////////////////
    // Identification criteria masks //
    /////////////////////////////////////////////
    //cout << " PhotonLoose operating mask: " << egammaPID::PhotonLoose << endl;
    //cout << "PhotonLoosePrime operating mask: " << PhotonLoosePrime << endl;
    //cout << " PhotonTight operating mask: " << egammaPID::PhotonTight << endl;
    
  } // mode Photon loop
  
     
  Cuts = cuts;
  return true;
}


//bool ObjectFilter::LeadPhoton(xAOD::Photon *photon)
//{
//  int pass = true;
//
//  // Photon fuge factors 
//  if (isMC){
//    if (m_fudgeMCTool->applyCorrection(*photon) == CP::CorrectionCode::Error){
//      Error("LeadPhoton()", "Failed to apply photon fudge factors. Exiting." );
//      return false;
//    }
//  }
//
//  // Photon Author 
//  uint16_t author =  photon->author();
//  if(not ((author & xAOD::EgammaParameters::AuthorPhoton) or (author & xAOD::EgammaParameters::AuthorAmbiguous))){
//    //cout<<"Fail photon author"<<endl;
//    return false;
//  }
// 
//  // Photon Pt cut 
//  if((photon)->pt()/GeV < MinPt_Photon){
//    //cout<<" Fail photon pT "<<endl;
//    return false;
//  }
//
//  // Photon Eta 
//  if(fabs(photon->eta()) > MaxEta_Photon){
//    //cout<<" Fail photon eta cut "<<endl;
//    return false;
//  }
//
//  // Photon crack
//  if (fabs(photon->eta()) > 1.37 and fabs(photon->eta()) < 1.52){
//    //cout<<" Fail photon crack "<<endl;
//    return false;
//  }
//  
//  // Photon identification
//  isTight = -1;
//  bool isLoose = m_photonLooseIsEMSelector->accept(photon);
//  if(! photon->passSelection(isLoose, "Loose"))
//    {
//      cout<<"WARNING: Loose decision is not available" <<endl;
//    }
//
//  bool tight = m_photonTightIsEMSelector->accept(photon);
//  bool isLooseP  = isLoosePrime(photon);
//  isLoose = isLooseP; 
//   
//  if(isLoose){isTight = 0;}
//  if(not isLoose){
//    return false;
//  }
//
//  // Photon isolation 
//  if (isoCorr_tool->applyCorrection((*photon)) == CP::CorrectionCode::Error){
//    Error("LeadPhoton()", "Failed to apply photon isolation. Exiting." );
//    return false;
//  }
//  static SG::AuxElement::Accessor<float> Isol( "topoetcone40" );
//  IsolE = Isol(*photon); 
//
//  isIsol=1;
//  if (!m_EgammaIsolationSelectionTool->accept( *photon )){
//    isIsol=0;
//    //cout<<" Fail photon isolation "<<endl;
//  }
//  //else PassNomIsol = true;
//
//  // Photon conversion 
//  bool passEoverP = true;
//  // E/p cut for converted photons
//  if (xAOD::EgammaHelpers::conversionType(photon) > 0){
//    double clusterEt = photon->caloCluster()->e() / cosh(photon->caloCluster()->eta());
//    double trackPt  = xAOD::EgammaHelpers::momentumAtVertex(photon).perp();
//    if (xAOD::EgammaHelpers::conversionType(photon) > 2){
//      if (clusterEt / trackPt > 1.5 or clusterEt / trackPt < 0.5) passEoverP = false;
//    }
//    else if (clusterEt / trackPt > 2.0) passEoverP = false;
//  }
//  //
//  if (not passEoverP){ 
//    //cout<<" Fail photon conversion "<<endl;
//    return false;
//   }
//
//  return pass;
//}

bool ObjectFilter::PassPhoton(xAOD::Photon *photon, std::vector<int> *failed, bool isNom)
{
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Don't return false right away if the photon fails the nominal    //
  // selection.  Photon may still be useful in measuring the purity. //
  /////////////////////////////////////////////////////////////////////////////////////////////

  // isNom flag is not used anymore
  
  int pass = true;  

  PassNomID   = false;
  PassNomIsol = false;
  if (isMC){
    if (m_fudgeMCTool->applyCorrection(*photon) == CP::CorrectionCode::Error){
        Error("PassPhoton()", "Failed to apply photon fudge factors. Exiting." );
        return false;
    }
  }

  //////////////////////////
  // Photon Author //
  //////////////////////////
  uint16_t author =  photon->author();
  if (not ((author & xAOD::EgammaParameters::AuthorPhoton) or (author & xAOD::EgammaParameters::AuthorAmbiguous))){
    failed->push_back(FindCode("Photon: author"));
    //cout<<"Fail photon author"<<endl;
    return false;
  }

  ///////////////////////
  // Photon isEM //
  ///////////////////////
  isTight = -1;
  bool isLoose = m_photonLooseIsEMSelector->accept(photon);
  if(! photon->passSelection(isLoose, "Loose"))
    {
      cout<<"WARNING: Loose decision is not available" <<endl;
    }
  
  bool tight = m_photonTightIsEMSelector->accept(photon);
  bool isLooseP  = isLoosePrime(photon);
  isLoose = isLooseP; 
   
  if(isLoose){isTight = 0;}
  if(not isLoose){
    /*
      Daniel 28-03-2018: here I found something that is clearly wrong. loose prime non-tight photons 
      must be used to determine the purity and its uncertainty. It seems that even non-loose or 
      non-loose prime  photons have been used in the past for the background control regions.
    */
    //cout<<"Fail photon is loose'"<<endl;
    //failed->push_back(FindCode("Photon: tight")); This allows non-loose photons for purity estimations
    //
    return false;
  }

  if(tight){isTight = 1;}
  else if(not tight){
    // Daniel 28-03-2018: loose prime non-tight photons used for the purity measurements NOW OK!
    failed->push_back(FindCode("Photon: tight"));
  }
  if(isTight == 1){PassNomID = true;}

  ////////////////////////
  // Photon Pt cut //
  ////////////////////////
  if ((photon)->pt()/GeV < MinPt_Photon){
    failed->push_back(FindCode("Photon: Pt"));
    return false;
  }


  /////////////////////
  // Photon Eta //
  /////////////////////
  if (fabs(photon->eta()) > MaxEta_Photon){
    failed->push_back(FindCode("Photon: Eta"));
    //cout<<" Fail photon eta cut "<<endl;
    return false;
  }

  ////////////////////////
  // Photon crack //
  ////////////////////////
  if (fabs(photon->eta()) > 1.37 and fabs(photon->eta()) < 1.52){
    failed->push_back(FindCode("Photon: crack"));
    //cout<<" Fail photon crack "<<endl;
    return false;
  }

  ////////////////////////////
  // Photon isolation //
  ////////////////////////////
  //  if (isoCorr_tool->CorrectLeakage((*photon)) == CP::CorrectionCode::Error){
  if (isoCorr_tool->applyCorrection((*photon)) == CP::CorrectionCode::Error){
    Error("PassPhoton()", "Failed to apply photon isolation. Exiting." );
    return false;
  }
  static SG::AuxElement::Accessor<float> Isol( "topoetcone40" );
  IsolE = Isol(*photon); 

  isIsol=1;
  if (m_EgammaIsolationSelectionTool->accept( *photon )){
    isIsol=0;
    PassNomIsol = true;
    failed->push_back(FindCode("Photon: Isolation"));
    //cout<<" Fail photon isolation "<<endl;
  }
//  else PassNomIsol = true;

  ///////////////////////////////
  // Photon conversion //
  ///////////////////////////////
  bool passEoverP = true;
  // E/p cut for converted photons
  if (xAOD::EgammaHelpers::conversionType(photon) > 0){
    double clusterEt = photon->caloCluster()->e() / cosh(photon->caloCluster()->eta());
    double trackPt  = xAOD::EgammaHelpers::momentumAtVertex(photon).perp();
    if (xAOD::EgammaHelpers::conversionType(photon) > 2){
      if (clusterEt / trackPt > 1.5 or clusterEt / trackPt < 0.5) passEoverP = false;
    }
    else if (clusterEt / trackPt > 2.0) passEoverP = false;
  }

  if (not passEoverP){ 
    failed->push_back(FindCode("Photon: Conversion"));
    //cout<<" Fail photon conversion "<<endl;
    return false;
   }

  ////////////////////////////
  // Ph. Scale factors //
  ////////////////////////////
  if(isMC)
    {
      m_photonIDTightEfficiencySF->getEfficiencyScaleFactor(*photon,photon_id_sf);
      m_photonISOTightEfficiencySF->getEfficiencyScaleFactor(*photon,photon_iso_sf);
      //cout<<" id sf "<<photon_id_sf<<" iso sf "<<photon_iso_sf<<endl;
    }
  
  return pass;
}


void ObjectFilter::Clean()
{
  if (Mode == "Photon") {
    delete m_photonTightIsEMSelector;
    delete m_photonLooseIsEMSelector;
  }

  if(! m_photonIDTightEfficiencySF->finalize().isSuccess()){    
    Error("Clean()", "Failed to properly finalize ID  AsgPhotonEfficiencyCorrectionTool. Exiting." );
  }
  delete m_photonIDTightEfficiencySF;
  
  if(! m_photonISOTightEfficiencySF->finalize().isSuccess()){   
    Error("Clean()", "Failed to properly finalize ISO AsgPhotonEfficiencyCorrectionTool. Exiting." );
  }
  delete m_photonISOTightEfficiencySF;
  
  delete isoCorr_tool;

  
  if(! m_EgammaIsolationSelectionTool->finalize().isSuccess()){
    Error("Clean()", "Failed to properly finalize the EgammaIsolationSelectionTool. Exiting." );
  }
  delete m_EgammaIsolationSelectionTool;

}

bool ObjectFilter::isLoosePrime(xAOD::Photon *photon)
{
  // loose
  bool isLoose = m_photonLooseIsEMSelector->accept(photon);

  // loose prime
  int isem = m_photonTightIsEMSelector->IsemValue();
  const unsigned int  LOOSEPRIME =  egammaPID::PhotonLoose |
    0x1 << egammaPID::ClusterStripsWtot_Photon | 
    0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon |
    0x1 << egammaPID::ClusterStripsEratio_Photon;
  const unsigned int PhotonLoosePrime =  LOOSEPRIME;
  bool isLooseP = (( isem & PhotonLoosePrime )==0); // loose'    

  // tight
  bool isTight = m_photonTightIsEMSelector->accept(photon);

  
  if(isLooseP){
    return true;
  }else{
    return false;
  }
  
}
