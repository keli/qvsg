/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#include "EtaInterCalxAODAnalysis/EtaInterCalxAODAnalysis.h"

// Helper macro for checking xAOD::TReturnCode return values
#define CHECK_STATUS( CONTEXT, EXP )                   \
  do {                                                 \
  if( ! EXP.isSuccess() ) {                            \
  Error( CONTEXT,                                      \
    XAOD_MESSAGE( "Failed to execute: %s" ),           \
	 #EXP );                                       \
  abort();                                             \
  }                                                    \
  } while( false )

// Constructor
DijetInSitu::EtaInterCalxAODAnalysis::EtaInterCalxAODAnalysis() {
// StatusCode::enableFailure(); Found unchecked status codes
// xAOD::TReturnCode::enableFailure(); Found unchecked status codes
}

// Destructor
DijetInSitu::EtaInterCalxAODAnalysis::~EtaInterCalxAODAnalysis() {

}

// Initialise
void DijetInSitu::EtaInterCalxAODAnalysis::Initialise(xAOD::TEvent& event, std::vector<TString> otf_settings, TString ofn) {



	m_random = new TRandom3();
	m_random->SetSeed(123456);

	//  m_event = event;

	//  m_eventShapeCopier = new EventShapeCopier("copier");
	//  m_eventShapeCopier->initialize().isSuccess();

	// get the event information
	const xAOD::EventInfo* eventInfo = 0;
	CHECK_STATUS( "Initialise()", event.retrieve( eventInfo, "EventInfo") );
	//const xAOD::FileMetaData* fmd = nullptr;
	//ANA_CHECK( event.retrieve( fmd, "FileMetaData" ) );

	// check if input dataset is MC
	m_isMC = false;
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
//	if ( fmd->MetaDataType( xAOD::MetaData::dataType ) )
		m_isMC = true;
	//  std::cout << " isMC? " << m_isMC << std::endl;

	// if MC, determine which dataset and jet slice we're running over
	if (m_isMC) {
		m_mcChannelNumber = eventInfo->mcChannelNumber();
		m_JX = m_mcChannelNumber % 10;
		std:: cout << " mc_channel_number = " <<  m_mcChannelNumber << std::endl;
		std:: cout << " JX = " <<  m_JX << std::endl;
	}
	  else{
		m_mcChannelNumber = 0;
		m_JX = -1;
	  }

	/// for BDT
	std::cout<<"Initializing QG BDT Tagger"<<std::endl;
	m_Tagger.setType("CP::JetQGTaggerBDT");
	m_Tagger.setName("MyTagger");
	if( ! m_Tagger.setProperty( "ConfigFile",   "JetQGTaggerBDT/JetQGTaggerBDT90Gluon.dat").isSuccess())        //other WPs in BoostedJetTaggers/share/JetQGTaggerBDT
		     return;

//	if(m_isMC){
//	if(! m_Tagger.setProperty( "UseJetVars", 1) )  return;
//	}
//	else {
    if(! m_Tagger.setProperty( "UseJetVars", 1) )  return;
//	}
	if( ! m_Tagger.retrieve().isSuccess())	     return ;
	
	//
 	ct10_set = new LHAPDF::PDFSet("CT10nlo");
	nnpdf23_set = new LHAPDF::PDFSet("NNPDF23_lo_as_0119_qed");
	pdfs = nnpdf23_set->mkPDFs(); // pointers to PDF set members
	pdfs1 = ct10_set->mkPDFs(); // pointers to PDF set members

	//setup the PMGTruthWeightTool
	if (m_isMC){
	  m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
	  CHECK_STATUS("Initialization",m_weightTool.retrieve());
	}

  // get path to $TestArea if cmake or $ROOTCOREBIN if rc
	if(getenv("EtaInterCal_cmake")!=NULL && std::string(getenv("EtaInterCal_cmake"))=="yes") { m_MainDirectory = std::string(getenv("TestArea")); std::cout << "\ncmake used" << '\n';}
  else { m_MainDirectory = std::string(getenv("ROOTCOREBIN"))+"/../"; std::cout << "\nrc used" << '\n';}

  std::string settings_file = m_MainDirectory + "/EtaInterCalxAODAnalysis/share/settings.config";
	std::cout << "Reading settings file " << settings_file << std::endl;
	m_settings = new TEnv();
	m_settings->ReadFile(settings_file.c_str(),EEnvLevel(0));

	// override some settings in the config file on-the-fly
	for (const auto& setting : otf_settings)
		m_settings->SetValue(setting);

	m_debug = m_settings->GetValue("Debug",false);
	
	m_mode = m_settings->GetValue("Mode","");
	m_jetCollections = vectorise(m_settings->GetValue("JetCollections",""));
	m_truthJetCollections = vectorise(m_settings->GetValue("TruthJetCollections",""));
  	m_HLTJetContainer = m_settings->GetValue("HLTJetContainer","a4tcemsubjesISFS"); //default name for data 2017 and above

	// dijet selection cuts
	m_enforceRefFJ_FJEta = m_settings->GetValue("enforceRefFJ_FJEta", true);
	m_jvfCut = m_settings->GetValue("JVFCut",0.25);
	m_dphijjCut = m_settings->GetValue("DphijjCut",2.5);
	m_j3pTCut = m_settings->GetValue("Jet3PtFracCut",0.4);
	m_pileupSuppMethod = m_settings->GetValue("PileupSuppressionMethod","JVF");
  for (const auto& jetAlgo : m_jetCollections) {
    m_jvtCut[jetAlgo] =  m_settings->GetValue(Form("JVTCut.%s",jetAlgo.Data()),0.59);
  }

	//cuts for syst shifts
	m_dphijjUpCut = m_settings->GetValue("DphijjUpCut",2.8);
	m_dphijjDownCut = m_settings->GetValue("DphijjDownCut",2.3);

  for (const auto& jetAlgo : m_jetCollections) {
    m_jvtCutTight[jetAlgo] =  m_settings->GetValue(Form("JVTCutTight.%s",jetAlgo.Data()),0.91);
    m_jvtCutLoose[jetAlgo] =  m_settings->GetValue(Form("JVTCutLoose.%s",jetAlgo.Data()),0.11);
  }

	m_j3pTCutUp = m_settings->GetValue("Jet3PtFracUpCut",0.5);
	m_j3pTCutDown = m_settings->GetValue( "Jet3PtFracDownCut",0.3);

	// cutflow flags
	m_tagAndProbe = m_settings->GetValue("TagAndProbe",false);
	m_tagAndProbeTrigs = vectorise(m_settings->GetValue("TagAndProbeTrigs",""));
	m_DRcut = m_settings->GetValue("TagAndProbeDRCut",0.3);
	m_truth_daod = m_settings->GetValue("DAOD_TRUTH",false);
	m_controlPlots = m_settings->GetValue("ControlPlots",true);
	m_applyGRL =  m_settings->GetValue("applyGRL",true);
	m_applyTrigger =  m_settings->GetValue("applyTrigger",true);
	m_applyPileupReweighting = m_settings->GetValue("applyPileupReweighting",false);
	m_applyJetCalib =  m_settings->GetValue("applyJetCalibration",true);
	m_EMscale =  m_settings->GetValue("EMscale",false);
	m_applyJetCleaning =  m_settings->GetValue("applyJetCleaning",true);
	m_applyJ3Cleaning = m_settings->GetValue("applyJ3Cleaning",false);
	m_applyJVF =  m_settings->GetValue("applyJVF",true);
	m_applyJVT =  m_settings->GetValue("applyJVT",true);
	m_applyDphijj = m_settings->GetValue("applyDphijj",true);
	m_applyJ3pT = m_settings->GetValue("applyJet3pT",true);
	m_applyMCCleaning = m_settings->GetValue("applyMCCleaning",false);
	m_calibDataAsMC = m_settings->GetValue("calibDataAsMC",false);
	m_requireOneJetForward = m_settings->GetValue("requireOneJetForward", true);
	m_discardTriggerOverlapEvents = m_settings->GetValue("discardTriggerOverlapEvents", true);

	m_doNominal = m_settings->GetValue("doNominal",true);
	m_doSystematics = m_settings->GetValue("doSystematics",true);
	m_doDphiShiftUp = m_settings->GetValue("doDphiShiftUp",true);
	m_doDphiShiftDown = m_settings->GetValue("doDphiShiftDown",true);
	m_doJet3PtFracShiftUp = m_settings->GetValue("doJet3PtFracShiftUp",true);
	m_doJet3PtFracShiftDown = m_settings->GetValue("doJet3PtFracShiftDown",true);
	m_doJVTCutTight = m_settings->GetValue("doJVTCutTight",true);
	m_vetoJetsInBadEta = m_settings->GetValue("vetoJetsInBadEta",false);
	m_vetoEventsInBadEta = m_settings->GetValue("vetoEventsInBadEta",false);
	m_badEtaRange = vectoriseD(m_settings->GetValue("badEtaRange",""));
	if ( m_vetoJetsInBadEta && m_badEtaRange.size() < 2 ) {
		std::cout << "ERROR: Check input to bad jet eta range." << std::endl;
		abort();
	}
	m_vetoPathEvnts = m_settings->GetValue("vetoPathalogicalEvents",true);
	m_applyDijetSelection = m_settings->GetValue("applyDijetSelection",false);
	m_doTriggerEmulation = m_settings->GetValue("applyTriggerEmulation", true);

	m_refRegion = m_settings->GetValue("RefRegion",0.8);
	m_centralRegion = m_settings->GetValue("RefRegion",2.4);

  m_runLightWeight = m_settings->GetValue("runLightWeight",false);
	// check JVF/JVT cut logic
	if ( (m_applyJVF && m_applyJVT) || (m_pileupSuppMethod=="JVF" && m_applyJVT) || (m_pileupSuppMethod=="JVT" && m_applyJVF) ) {
		std::cout << "ERROR: Can't apply both JVF and JVT! Check configuration file." << std::endl;
		abort();
	}

	// check jet calibration logic
	if ( m_applyJetCalib && m_EMscale ) {
		std::cout << "ERROR: Can't apply jet calibration and use EM scale jets! Check configuration file." << std::endl;
		abort();
	}

	// trigger information
	m_trigMenu = m_settings->GetValue("TriggerMenu","");
	m_trigs = vectorise(m_settings->GetValue("TriggersRun2."+m_trigMenu,""));
	m_trigsCtrl = vectorise(m_settings->GetValue("CentralTriggersRun2."+m_trigMenu,""));
	m_trigsFwd = vectorise(m_settings->GetValue("ForwardTriggersRun2."+m_trigMenu,""));
	m_trigORs = vectorise(m_settings->GetValue("Trig_OR."+m_trigMenu,""));
	m_trigThresholds = vectoriseD(m_settings->GetValue("TrigPtThreshold."+m_trigMenu,""));
	m_ctrlTrigsOnly = m_settings->GetValue("UseCentralTriggersOnly",false);
	m_fwdTrigsOnly = m_settings->GetValue("UseForwardTriggersOnly",false);
	m_forceMCTrigs = m_settings->GetValue("forceMCTrig", false);
	m_centralAsDefaultTriggerPrefrence = m_settings->GetValue("centralAsDefaultTriggerPrefrence", false);

	if ( m_fwdTrigsOnly && m_ctrlTrigsOnly ) {
		std::cout << "ERROR: Can't apply only central triggers and only forward triggers (did you mean trigger OR combination?)!  Check logic in configuration file." << std::endl;
		abort();
	}

  // is samples release >= 21
  m_isR21 = m_settings->GetValue("isR21",true);


	printf("\n\nRUN SETTINGS\n");
	printf("Tag and probe:         %s\n", m_tagAndProbe ? "yes" : "no");
	if ( m_tagAndProbe ) {
		printf("DR matching:            %1.1f\n", m_DRcut);
		printf("Tag and probe trigs:   %s\n", m_settings->GetValue("TagAndProbeTrigs",""));
	}
	printf("Input:                 %s\n", m_isMC ? "MC" : "data");
	printf("DAOD_TRUTH:            %s\n", m_truth_daod ? "yes" : "no");
	printf("Control plots?         %s\n", m_controlPlots ? "yes" : "no");
	if ( !m_isMC ) {
		printf("Apply trigger:         %s\n",m_applyTrigger ? "yes" : "no");

		if ( m_applyTrigger ) {
			printf("Trigger menu:          %s\n",m_trigMenu.Data());

			printf("Central triggers:      %s\n",m_settings->GetValue("CentralTriggersRun2."+m_trigMenu,""));
			printf("Forward triggers:      %s\n",m_settings->GetValue("ForwardTriggersRun2."+m_trigMenu,""));

			if ( !m_ctrlTrigsOnly && !m_fwdTrigsOnly )
				printf("Trigger strategy:       central OR forward\n");
			else
				printf("Trigger strategy:       %s\n", m_ctrlTrigsOnly ? "central" : "forward");
		}

		printf("Apply GRL:             %s\n",m_applyGRL ? "yes" : "no");
		if ( m_applyGRL )
			printf("   GRL xml:            %s\n",m_settings->GetValue("GRL.xml",""));
	}
	if ( m_isMC ) {
		printf("Apply pileup reweighting: %s\n",m_applyPileupReweighting ? "yes" : "no");
		printf("Veto pathalogical events: %s\n",m_vetoPathEvnts ? "yes" : "no");
		printf("Apply MC cleaning:        %s\n",m_applyMCCleaning ? "yes" : "no");
	}
  printf("samples release >= 21  %s\n", m_isR21 ? "yes" : "no");
	printf("Jet collections:       %s\n",m_settings->GetValue("JetCollections",""));
	printf("Calibrate jets:        %s\n",m_applyJetCalib ? "yes" : "no");
	if ( m_applyJetCalib ) {
		for( const auto& jetCollection : m_jetCollections){
			printf("   %s.Config:             %s\n", jetCollection.Data(),m_settings->GetValue("JetCalibTool."+jetCollection+".config",""));
			printf("   %s.CalibSeq:           %s\n", jetCollection.Data(),m_settings->GetValue("JetCalibTool."+jetCollection+".calibSeq",""));
		}
		printf("Calibrate data as MC?  %s\n", m_calibDataAsMC ? "yes" : "no");
	}
	else {
		printf("Use EM scale jets:       %s\n",m_EMscale ? "yes" : "no");
	}
	printf("Apply jet cleaning:    %s\n",m_applyJetCleaning ? "yes" : "no");
	if ( m_applyJetCleaning ) {
		printf("   CutLevel:           %s\n", m_settings->GetValue("JetCleaningTool.cutLevel",""));
		printf("   DoUgly:             %s\n", m_settings->GetValue("JetCleaningTool.doUgly",false) ? "yes" : "no");
		printf("Apply cleaning to 3rd jets? %s\n", m_applyJ3Cleaning ? "yes" : "no");

	}
	if ( m_pileupSuppMethod=="JVF") {
		printf("Apply JVF:             %s\n",m_applyJVF ? "yes" : "no");
		if ( m_applyJVF )
			printf("   JVF cut:            %1.2f\n", m_jvfCut);
	}
	else if ( m_pileupSuppMethod=="JVT" ) {
		printf("Apply JVT:             %s\n",m_applyJVT ? "yes" : "no");
    for (const auto& jetAlgo : m_jetCollections) {
      if ( m_applyJVT )
      {
        printf("   %s\n", jetAlgo.Data());
        printf("      JVT cut:            %1.2f\n", m_jvtCut[jetAlgo]);
  		  if( m_doSystematics){
  			  std::cout << "      JVT up,down:        " << m_jvtCutTight[jetAlgo] << "," << m_jvtCutLoose[jetAlgo] << std::endl;
  		}
      }
		}
	}
	printf("Veto bad eta jets:         %s\n",m_vetoJetsInBadEta ? "yes" : "no");
	printf("Veto events with bad jets: %s\n",m_vetoEventsInBadEta ? "yes" : "no");
	if ( m_vetoJetsInBadEta || m_vetoEventsInBadEta ) {
		printf("Bad eta range:         %1.2f < η < %1.2f\n", m_badEtaRange[0], m_badEtaRange[1]);
	}
	printf("Apply dijet selection: %s\n",m_applyDijetSelection ? "yes" : "no");
	printf("Apply Trigger emulation: %s\n",m_doTriggerEmulation ? "yes" : "no");
	if ( m_applyDijetSelection ) {
		printf("Apply Dphijj:          %s\n",m_applyDphijj ? "yes" : "no");
		if ( m_applyDphijj )
			printf("   Dphijj cut:         %1.2f\n", m_dphijjCut);
		printf("Apply J3pT:            %s\n",m_applyJ3pT ? "yes" : "no");
		if ( m_applyJ3pT )
			printf("   J3pT cut:           %1.2f\n", m_j3pTCut);
	}
	printf("Force MC to use triggers: %s\n", m_forceMCTrigs ? "yes" : "no");
	printf("Enforce RefFJ_FJ Eta: %s\n", m_enforceRefFJ_FJEta ? "yes" : "no");
	printf("Central-as-default Trigger Pref: %s\n", 	m_centralAsDefaultTriggerPrefrence ? "yes" : "no");
	printf("requireOneJetForward: %s\n", m_requireOneJetForward ? "yes" : "no");
	printf("discardTriggerOverlapEvents: %s\n", m_discardTriggerOverlapEvents  ?"yes" : "no");
  if(m_runLightWeight)
    std::cout << "====WARNING: SAVING NTUPLES ONLY=====" << std::endl;
  else
    std::cout << "Light weight running: no" << std::endl;
	printf("\n\n");

	if(m_requireOneJetForward && m_centralAsDefaultTriggerPrefrence){
		std::cout << "ERROR: Check config. Cannot have both default central and require one jet forward" << std::endl;
		abort();
	}

	if(m_discardTriggerOverlapEvents && m_requireOneJetForward){
		std::cout << "ERROR: Check config. Cannot have both discarding overlap trigger events and require one jet forward" << std::endl;
		abort();
	}
	if(m_discardTriggerOverlapEvents && m_centralAsDefaultTriggerPrefrence){
		std::cout << "ERROR: Check config. Cannot have both discarding overlap trigger events and have both default central" << std::endl;
		abort();
	}
	// switch on sumw2 for all histograms
	TH1::SetDefaultSumw2(kTRUE);

	// Initialise the tools, cutflow, histograms
	if ( ofn=="" ) {
		if ( m_isMC ) m_fout = TFile::Open("EtaInterCal_3DHistos.root","recreate");

		//    if ( m_isMC ) m_fout = TFile::Open(Form("EtaInterCal_3DHistos_J%d.root",m_JX),"recreate");
		else m_fout = TFile::Open("EtaInterCal_3DHistos_data.root","recreate");
	}
	else
		m_fout = TFile::Open(ofn,"recreate");

	//  m_fout = TFile::Open(Form("EtaInterCal_3DHistos.root",m_JX),"recreate");
	InitialiseTools();

	
	if ( m_tagAndProbe ) {
		InitialiseTagAndProbeHists();
	}
	else {
		for (const auto& jetAlgo : m_jetCollections)
			InitialiseCutflow(std::string(jetAlgo.Data()));

		if ( m_isMC ) {
			for (const auto& jetAlgo : m_truthJetCollections)
				InitialiseCutflow(std::string(jetAlgo.Data()));
		}

		for (const auto& jetAlgo : m_jetCollections)
			InitialiseHistograms(std::string(jetAlgo.Data()));

		if ( m_isMC ) {
			for (const auto& jetAlgo : m_truthJetCollections)
				InitialiseHistograms(std::string(jetAlgo.Data()));
		}

		for (const auto& jetAlgo : m_jetCollections)
			InitialiseJetCollectionTrees(std::string(jetAlgo.Data()));
	}

	m_test2DHist = new TH2D("m_test2DHist","test",50,-3,3,50,-3,3);
}


// Fill histograms for trigger jets
void DijetInSitu::EtaInterCalxAODAnalysis::FillTrigHists(xAOD::TEvent& event, std::string trig){
	const xAOD::JetContainer* trig_jets = 0;
	if( !event.retrieve( trig_jets, "HLT_xAOD__JetContainer_"+trig ).isSuccess() )
		std::cout << "WARNING: failed to retrieve trigger jets " << trig << std::endl;

	for ( xAOD::JetContainer::const_iterator jet = trig_jets->begin(); jet < trig_jets->end(); ++jet ) {
		// xAODJet/JetAttributes.h
		m_trigHists[trig+"_ET"]->Fill((*jet)->p4().Et()/1000.,m_weight);
		m_trigHists[trig+"_pT"]->Fill((*jet)->pt()/1000.,m_weight);
		m_trigHists[trig+"_eta"]->Fill((*jet)->eta(),m_weight);
		m_trigHists[trig+"_phi"]->Fill((*jet)->phi(),m_weight);
		m_trigHists[trig+"_E"]->Fill((*jet)->e()/1000.,m_weight);
	}

}

// Process event
void DijetInSitu::EtaInterCalxAODAnalysis::ProcessEvent(xAOD::TEvent& event) {

	debug("Processing event");

	if ( m_tagAndProbe ) {
		TagAndProbeAnalysis(event);
		return;
	}

	const xAOD::JetContainer* truthJets = NULL;
	if ( m_isMC ) {
		for (const auto& jetAlgo : m_truthJetCollections) {
			AnalyseTruthEvent(event, std::string(jetAlgo.Data())); //jetAlgo.Data():AntiKt4Truth
			TString jetCollectionName = TString(std::string(jetAlgo.Data()))+"Jets";
			if ( !event.retrieve( truthJets, std::string(jetCollectionName.Data()) ).isSuccess() )  //jetCollectionName.Data()=AntiKt4TruthJets
				std::cout << " Failed to retrieve truthJets." << std::endl;
			}		//      m_truth_tree->Fill();
		}

	if ( !m_truth_daod ) {
		for (const auto& jetAlgo : m_jetCollections) {
				AnalyseEvent(event, std::string(jetAlgo.Data()),truthJets);
			}
	}
	return;
}

void DijetInSitu::EtaInterCalxAODAnalysis::TagAndProbeAnalysis(xAOD::TEvent& event) {

	xAOD::TStore store;

	const xAOD::EventInfo* eventInfo = 0;
	CHECK_STATUS( "AnalyseEvent()", event.retrieve( eventInfo, "EventInfo") );

	m_weight = m_isMC ? eventInfo->mcEventWeight() : 1.0;

	// CUT: GRL for data only
	if ( !m_isMC && m_applyGRL && !m_grlTool->passRunLB(*eventInfo) ) return;

	// CUT: Errors in LAr/Tile/Core (corrupted events)
	// Event cleaning (for data only) to remove events with problematic regions of the detector
	if (!m_isMC && ( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)
			|| (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)
			|| (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) ) ) return;

	// CUT: PV
	// Primary vertex with at least two tracks
	const xAOD::VertexContainer* pVertices = 0;
	if( !event.retrieve(pVertices,"PrimaryVertices").isSuccess() ) {
		std::cout << " Failed to retrieve primary vertices." << std::endl;
	}

	const xAOD::Vertex* PV = GetPV(pVertices);
  m_NPV = (float)pVertices->size();
	if( pVertices->size() < 1 || !PV ) {
		// no primary vertices
		std::cout << "WARNING: no primary vertex found! Skipping event." << std::endl;
		return;
	}

	if ( PV->nTrackParticles()<2 ) return;



	bool badEvent = false;

	xAOD::JetContainer offlineJets = xAOD::JetContainer(SG::VIEW_ELEMENTS);
	SelectOfflineJets(event,store,offlineJets,badEvent);

	// require at least two jets which satisfy the event cleaning criteria
	if ( (offlineJets.size() < 2) || badEvent )
		return;

	const xAOD::Jet& jet1 = *offlineJets.at(0);
	const xAOD::Jet& jet2 = *offlineJets.at(1);

	// ensure leading and subleading jets sufficiently well separated
	if ( deltaR(jet1,jet2) < 0.8 )
		return;

	const xAOD::JetRoIContainer* L1RoIs;
	CHECK_STATUS( "AnalyseEvent()", event.retrieve( L1RoIs, "LVL1JetRoIs") );
	xAOD::JetContainer onlineJets = xAOD::JetContainer(SG::VIEW_ELEMENTS);

	// loop over triggers
	for ( const auto& trig : m_tagAndProbeTrigs ) {

		onlineJets.clear();
		TString trig_name = trig;
		trig_name.ReplaceAll("HLT_j","");
		float ET_cut = std::atoi( trig_name.Data() );

		SelectOnlineJets(event,store,onlineJets,ET_cut, true);

		// require at least one HLT jet, and trigger to pass
		if ( onlineJets.size()<1 || !m_trigDecTool->isPassed( std::string(trig.Data()) ) )
			continue;

		float minDR=999;
		for ( auto hlt_jet : onlineJets ) {
			if ( deltaR(jet1,*hlt_jet) < minDR ) {
				minDR = deltaR(jet1,*hlt_jet);
			}
		}

		m_hists1D[ Form("%s_DeltaR_offline_HLT",trig.Data()) ]->Fill(minDR,m_weight);

		const xAOD::Jet& hlt_jet1 = *onlineJets.at(0);
		minDR=999;
		for ( xAOD::JetRoIContainer::const_iterator jet = L1RoIs->begin(); jet < L1RoIs->end(); ++jet ) {
			if ( deltaR(hlt_jet1,**jet) < minDR ) {
				minDR = deltaR(hlt_jet1,**jet);
			}
		}
		m_hists1D[ Form("%s_DeltaR_HLT_L1RoI",trig.Data()) ]->Fill(minDR,m_weight);

		minDR=999;
		for ( xAOD::JetRoIContainer::const_iterator jet = L1RoIs->begin(); jet < L1RoIs->end(); ++jet ) {
			if ( deltaR(jet1,**jet) < minDR ) {
				minDR = deltaR(jet1,**jet);
			}
		}
		m_hists1D[ Form("%s_DeltaR_offline_L1RoI",trig.Data()) ]->Fill(minDR,m_weight);

		// fill histograms based on leading jet being tag jet
		if ( matchHLTJet(jet1,onlineJets,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_HLT_pT_lead",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_HLT_pT_lead",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

			if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_HLT_pT_match_lead",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			}
		}

		// fill histograms based on either jet being tag jet
		if ( matchHLTJet(jet1,onlineJets,m_DRcut) && matchHLTJet(jet2,onlineJets,m_DRcut) ) {

			if ( m_random->Rndm() <= 0.5 ) {

				m_hists1D[ Form("%s_tag_HLT_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
				m_hists1D[ Form("%s_probe_HLT_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

				if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
					m_hists1D[ Form("%s_probe_HLT_pT_match_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
				}

			}
			else {

				m_hists1D[ Form("%s_tag_HLT_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
				m_hists1D[ Form("%s_probe_HLT_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);

				if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
					m_hists1D[ Form("%s_probe_HLT_pT_match_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
				}

			}
		}
		else if ( matchHLTJet(jet1,onlineJets,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_HLT_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_HLT_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

			if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_HLT_pT_match_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			}
		}
		else if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_HLT_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_HLT_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);

			if ( matchHLTJet(jet2,onlineJets,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_HLT_pT_match_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			}
		}

		// L1 RoI tag and probe
		// fill histograms based on leading jet being tag jet
		if ( matchL1RoI(jet1,L1RoIs,100.0,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_L1RoI_pT_lead",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_L1RoI_pT_lead",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

			if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_L1RoI_pT_match_lead",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			}
		}


		// fill histograms based on either jet being tag jet
		if ( matchL1RoI(jet1,L1RoIs,100.0,m_DRcut) && matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {

			if ( m_random->Rndm() <= 0.5 ) {

				m_hists1D[ Form("%s_tag_L1RoI_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
				m_hists1D[ Form("%s_probe_L1RoI_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

				if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
					m_hists1D[ Form("%s_probe_L1RoI_pT_match_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
				}

			}
			else {

				m_hists1D[ Form("%s_tag_L1RoI_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
				m_hists1D[ Form("%s_probe_L1RoI_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);

				if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
					m_hists1D[ Form("%s_probe_L1RoI_pT_match_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
				}

			}
		}
		else if ( matchL1RoI(jet1,L1RoIs,100.0,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_L1RoI_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_L1RoI_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);

			if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_L1RoI_pT_match_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			}
		}
		else if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
			m_hists1D[ Form("%s_tag_L1RoI_pT_random",trig.Data()) ]->Fill(jet2.pt()/GeV,m_weight);
			m_hists1D[ Form("%s_probe_L1RoI_pT_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);

			if ( matchL1RoI(jet2,L1RoIs,100.0,m_DRcut) ) {
				m_hists1D[ Form("%s_probe_L1RoI_pT_match_random",trig.Data()) ]->Fill(jet1.pt()/GeV,m_weight);
			}
		}


	}

	return;
}








//match jet to HLT jet,argument :reco jet,onlineJet=truth jet ,DRcut =0.3
bool DijetInSitu::EtaInterCalxAODAnalysis::matchHLTJet(const xAOD::Jet& jet,const xAOD::JetContainer onlineJets,float DRcut){

	for ( const auto& hlt : onlineJets ) {
		if ( deltaR(jet,*hlt) < DRcut ) {
			return true;
		}
	}

	return false;
}

const xAOD::Jet *DijetInSitu::EtaInterCalxAODAnalysis::matchTruthJet(const xAOD::Jet& jet,const xAOD::JetContainer onlineJets,float DRcut){

	for ( const auto& hlt : onlineJets ) {
		if ( deltaDetecR(jet,*hlt) < DRcut ) {
			return hlt;
		}
	}

	return NULL;
}


bool DijetInSitu::EtaInterCalxAODAnalysis::matchL1RoI(const xAOD::Jet& jet,const xAOD::JetRoIContainer* L1RoIs,float ETcut, float DRcut){

	for ( xAOD::JetRoIContainer::const_iterator L1 = L1RoIs->begin(); L1 < L1RoIs->end(); ++L1 ) {
		if ( (*L1)->et8x8()/GeV < ETcut )
			continue;
		if ( deltaR(jet,**L1) < DRcut ) {
			return true;
		}
	}

	return false;
}

void DijetInSitu::EtaInterCalxAODAnalysis::SelectOnlineJets(xAOD::TEvent& event, xAOD::TStore &store, xAOD::JetContainer &selJets, float ET_cut, bool forceCentral){

	const xAOD::JetContainer* jets;
	CHECK_STATUS( "SelectOnlineJets()", event.retrieve( jets, Form("HLT_xAOD__JetContainer_%s",m_HLTJetContainer.Data()) ) );

	std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );

	// Add shallow copy to TStore, so it handles memory cleanup
	CHECK_STATUS ( "SelectOnlineJets()", store.record(jets_shallowCopy.first,  Form("shallowCopy_HLT_xAOD__JetContainer_%s_%1.0f",m_HLTJetContainer.Data(),ET_cut)) );
	CHECK_STATUS ( "SelectOnlineJets()", store.record(jets_shallowCopy.second, Form("shallowCopy_HLT_xAOD__JetContainer_%s_%1.0fAux",m_HLTJetContainer.Data(),ET_cut)) );

	xAOD::JetContainer calibJets = xAOD::JetContainer(jets_shallowCopy.first->begin(),
			jets_shallowCopy.first->end(),
			SG::VIEW_ELEMENTS);

	//  for ( xAOD::JetContainer::const_iterator jet = jets->begin(); jet < jets->end(); ++jet ) {
	for ( auto jet : calibJets ) {
		float etaDet = jet->eta(); float ET = jet->e()/1000/cosh(etaDet);

		jet->auxdata< float >( "ET" ) = ET;


		if ( ET > ET_cut){
			if(forceCentral){
				if(fabs(etaDet) <= 3.2)
					selJets.push_back( jet );
			}else{
				if(fabs(etaDet) <= 4.9)
					selJets.push_back( jet );
			}
		}
	}

	return;
}

void DijetInSitu::EtaInterCalxAODAnalysis::SelectOfflineJets(xAOD::TEvent& event, xAOD::TStore &store, xAOD::JetContainer &selJets, bool &badEvent){

	// Get the jets
	const xAOD::JetContainer* jets = 0;
	CHECK_STATUS ( "SelectOfflineJets()", event.retrieve( jets, "AntiKt4EMTopoJets" ) );

	std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );

	// Add shallow copy to TStore, so it handles memory cleanup
	CHECK_STATUS ( "SelectOfflineJets()", store.record(jets_shallowCopy.first, "shallowCopy_AntiKt4EMTopoJets") );
	CHECK_STATUS ( "SelectOfflineJets()", store.record(jets_shallowCopy.second, "shallowCopy_AntiKt4EMTopoJetsAux") );

	xAOD::JetContainer calibJets = xAOD::JetContainer(jets_shallowCopy.first->begin(),
			jets_shallowCopy.first->end(),
			SG::VIEW_ELEMENTS);

	for (auto jet : calibJets) {

		float E_orig = jet->e();
		// calibrate jet
		if ( m_applyJetCalib ) {
			if( m_jetCalibTools["AntiKt4EMTopo"]->applyCorrection(*jet) == CP::CorrectionCode::Error )
				std::cout << "WARNING: error while calibrating jet" << std::endl;
		}

		// correct JVT of jet
		if ( m_applyJVT ) {

			if ( m_applyJetCalib ) {
                float corrJVT = m_updateJvt->updateJvt(*jet);
				jet->auxdata< float >( "corrJVT" ) = corrJVT;
			}
			else{
				jet->auxdata< float >( "corrJVT" ) = m_jvtTool->evaluateJvt(jet->getAttribute<float>("JvtRpt"), jet->getAttribute<float>("JvtJvfcorr"));

			}
		}

		// decorate with original jet energy
		jet->auxdata< float >( "E_orig" ) = E_orig;

		// decorate with detector-level eta and phi
		xAOD::JetFourMom_t jetconstitP4 = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
    if(!m_isR21) jet->auxdata< float >("DetectorEta") = jetconstitP4.eta(); //for R21, JetConstitScaleMomentum has the origin correction and shouldn't be used, and DetectorEta is already stored with the proper value.
		jet->auxdata< float >("DetectorPhi") = jetconstitP4.phi();

		if ( fabs( jet->eta() ) > 0.8 )
			continue;

		if ( !passJVT( *jet, m_jvtCut["AntiKt4EMTopo"] ) )
			continue;

		if ( !m_jetCleaningTool->accept( *jet ) ) {
			// event cleaning: reject event if jet with pT > 20 GeV that passes JVT is flagged as bad by loose criteria
			if ( jet->pt()/GeV > 20. )
				badEvent = true;
			continue;
		}

		selJets.push_back(jet);
	}

	// Sort the jets descending calibrated pT
	std::sort(selJets.begin(),selJets.end(),descendingPt);

	return;
}

// Per event analysis at truth level
void DijetInSitu::EtaInterCalxAODAnalysis::AnalyseTruthEvent(xAOD::TEvent& event, std::string jetAlgo) {
	m_pTavg_truth = -99;
	m_j1_pT_truth = -99; m_j1_eta_truth = -99; m_j1_phi_truth = -99;
	m_j2_pT_truth = -99; m_j2_eta_truth = -99; m_j2_phi_truth = -99;
	m_j3_pT_truth = -99; m_j3_eta_truth = -99; m_j3_phi_truth = -99;
	m_j4_pT_truth = -99; m_j4_eta_truth = -99; m_j4_phi_truth = -99;
	m_Dphijj_truth = -99;
	m_passDijetCuts_truth = false;

	xAOD::TStore store;

	const xAOD::EventInfo* eventInfo = 0;
	CHECK_STATUS( "AnalyseEvent()", event.retrieve( eventInfo, "EventInfo") );


	////pdf
//	const xAOD::TruthEventContainer * truthEvents(nullptr);
//  	event.retrieve(truthEvents, "TruthEvents");
//
//    for (int i=0; i<nPdf; i++){
//		    pdfWeights[i] = 0;
//	  }
//	if(m_isMC){
//		for( const auto *truthEvent : *truthEvents){
//			xAOD::TruthEvent::PdfInfo pdf = truthEvent->pdfInfo();
//
//			//const LHAPDF::PDFSet nnpdf23_set("NNPDF23_lo_as_0119_qed");
//			//      //const std::vector<LHAPDF::PDF*> pdfs = nnpdf23_set.mkPDFs(); // pointers to PDF set members
//			if(nPdf != nnpdf23_set->size()){
//				std::cout << "Error: number of PDF sets doesn't equal nPdf in the code! Ending process!" << std::endl;
//				//return EL::StatusCode::FAILURE;
//			}
//			for (size_t imem = 0; imem <= nnpdf23_set->size()-1; imem++) {
//				double weight =  LHAPDF::weightxxQ2( pdf.pdgId1, pdf.pdgId2, pdf.x1, pdf.x2, pdf.Q, pdfs[0], pdfs[imem] );//modified by ws
//				pdfWeights[imem] = weight ; // one event weight for each error in the set including the nominal
//			}
//		}
//	}
							  


	if ( m_isMC && m_vetoPathEvnts && isPathalogicalEvent(eventInfo) )
		return ;

	// update MC channel number/JX slice
	if ( m_isMC && eventInfo->mcChannelNumber() != m_mcChannelNumber ) {
		m_mcChannelNumber = eventInfo->mcChannelNumber();
		m_JX = m_mcChannelNumber % 10;
		std:: cout << " mc_channel_number = " <<  m_mcChannelNumber << std::endl;
		std:: cout << " JX = " <<  m_JX << std::endl;
	}

	m_weight = m_isMC ? eventInfo->mcEventWeight() : 1.0;
	//if ( m_isMC ) m_hists1D[Form("%s_sumOfWeights",jetAlgo.c_str())]->Fill(0.5,m_weight);
	if ( m_isMC ) m_hists1D[Form("%s_J%d_sumOfWeights",jetAlgo.c_str(),m_JX)]->Fill(0.5,m_weight);

	if (m_isMC){
	      m_mcEventWeightsString=m_weightTool->getWeightNames();
	      m_mcEventWeightsVector.clear();
	      for (auto weight : m_mcEventWeightsString) {
		m_mcEventWeightsVector.push_back(m_weightTool->getWeight(weight));
	      }
	      eventInfo->auxdecor< std::vector< std::string > >("mcEventWeightsString") = m_mcEventWeightsString;
	      eventInfo->auxdecor< std::vector< float> >("mcEventWeightsVector")= m_mcEventWeightsVector; 
	}


	m_cutFlow[jetAlgo]->Fill(0.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(1.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(2.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(3.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(4.0,m_weight);

	// Get the jets
	const xAOD::JetContainer* jets = 0;
	TString jetCollectionName = TString(jetAlgo)+"Jets";

	if ( !event.retrieve( jets, std::string(jetCollectionName.Data()) ).isSuccess() )
		std::cout << " Failed to retrieve the jets." << std::endl;

	std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );

	// Add shallow copy to TStore, so it handles memory cleanup
	if (!store.record(jets_shallowCopy.first, Form("shallowCopy_%s",jetAlgo.c_str()))) {
		std::cout << "Could not record shallow copy of " << jetAlgo << " jets to TStore." << std::endl;
		abort();
	}
	if (!store.record(jets_shallowCopy.second, Form("shallowCopy_%sAux",jetAlgo.c_str()))) {
		std::cout << "Could not record aux shallow copy of " << jetAlgo << " jets to TStore." << std::endl;
		abort();
	}

	xAOD::JetContainer truthJets = xAOD::JetContainer(jets_shallowCopy.first->begin(),
			jets_shallowCopy.first->end(),
			SG::VIEW_ELEMENTS);

	if ( truthJets.size() < 1 )
		return;

	const xAOD::Jet& jet1 = *truthJets.at(0);
	m_j1_pT_truth = jet1.pt(); m_j1_eta_truth = jet1.eta(); m_j1_phi_truth = jet1.phi();


	if ( truthJets.size() < 2 )
		return;
	m_cutFlow[jetAlgo]->Fill(5.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(6.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(7.0,m_weight);

	// Sort the jets descending calibrated pT
	std::sort(truthJets.begin(),truthJets.end(),descendingPt);

	const xAOD::Jet& jet2 = *truthJets.at(1);

	// third jet
	xAOD::Jet* jet3 = 0;
	if(truthJets.size()>=3) { jet3 = truthJets.at(2); }
	// 4th jet
	xAOD::Jet* jet4 = 0;
	if(truthJets.size()>=4) { jet4 = truthJets.at(3); }

	float _j3_pT = jet3 ? jet3->pt() : 0.0;

	float _deltaPhi = fabs( deltaPhi(*truthJets.at(0),*truthJets.at(1)) );
	float _pTavg = (jet1.pt()+jet2.pt())/2.0;

	m_pTavg_truth = _pTavg;
	m_j2_pT_truth = jet2.pt(); m_j2_eta_truth = jet2.eta(); m_j2_phi_truth = jet2.phi();
	m_j3_pT_truth = jet3 ? jet3->pt() : -99; m_j3_eta_truth = jet3 ? jet3->eta() : -99; m_j3_phi_truth = jet3 ? jet3->phi() : -99;
	m_j4_pT_truth = jet4 ? jet4->pt() : -99; m_j4_eta_truth = jet4 ? jet4->eta() : -99; m_j4_phi_truth = jet4 ? jet4->phi() : -99;
	m_Dphijj_truth = _deltaPhi;

	// CUT: delta phi between leading jets
	if ( m_applyDphijj && _deltaPhi < m_dphijjCut ) 
		return;
	m_cutFlow[jetAlgo]->Fill(8.0,m_weight);

	// CUT: third jet pT cut
	if ( m_applyJ3pT && jet3 && jet3->pt()/_pTavg > m_j3pTCut )
		return;
	m_cutFlow[jetAlgo]->Fill(9.0,m_weight);
	m_cutFlow[jetAlgo]->Fill(10.0,m_weight);

	DijetBalance dijet(truthJets,m_refRegion,true);

	// fill histograms
  if(!m_runLightWeight){
  	m_hists1D[jetAlgo+"_jet1_pT"]->Fill(jet1.pt()/GeV,m_weight);
  	m_hists1D[jetAlgo+"_jet2_pT"]->Fill(jet2.pt()/GeV,m_weight);
  	m_hists1D[jetAlgo+"_jet3_pT"]->Fill(_j3_pT/GeV,m_weight);
  	m_hists1D[jetAlgo+"_pTavg"]->Fill(_pTavg/GeV,m_weight);
  }
	m_passDijetCuts_truth=true;

	// fill histograms in pTavg bins
	std::vector<double> pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));

	uint pTbin=0;
	for (uint i=0;i<pTavg_bins.size();++i)
		if (_pTavg/GeV > pTavg_bins[i]) pTbin=i+1;

	// skip event if pTavg outside bin range
	if (pTbin==0 || pTbin>(pTavg_bins.size()-1)) 
		return;

	FillHistograms( Form("%s_J%d_",jetAlgo.c_str(),m_JX), dijet, pTbin );

	return;
}

// Per event analysis
void DijetInSitu::EtaInterCalxAODAnalysis::AnalyseEvent(xAOD::TEvent& event, std::string jetAlgo, const xAOD::JetContainer *truthJets) {


	m_pass_Nominal = true;
	m_pass_DphiUp = true;
	m_pass_DphiDown = true;
	m_pass_JVTThight = true;
	m_pass_JVTLoose = true;
	m_pass_J3pTCutUp = true;
	m_pass_J3pTCutDown = true;
	//  bool m_truthAOD = false;

	xAOD::TStore store;

	//  m_eventShapeCopier->renameEventDensities();

	const xAOD::EventInfo* eventInfo = 0;
	CHECK_STATUS( "AnalyseEvent()", event.retrieve( eventInfo, "EventInfo") );

	if ( m_isMC && m_applyPileupReweighting )
		m_pileupReweightingTool->apply( *eventInfo, true );

	/*
  auto chainGroup = m_trigDecTool->getChainGroup("HLT_j.*320eta490");
  std::cout << chainGroup->getListOfTriggers().size() << std::endl;
  for(auto &trig : chainGroup->getListOfTriggers()) {
    std::cout << trig << std::endl;
  }

  abort();
	 */

	if ( m_isMC && m_vetoPathEvnts && isPathalogicalEvent(eventInfo) )
		return;
	if ( !m_isMC && isBadDataLumiBlock(eventInfo) )
		return;

	// update MC channel number/JX slice
	if ( m_isMC && eventInfo->mcChannelNumber() != m_mcChannelNumber ) {
		m_mcChannelNumber = eventInfo->mcChannelNumber();
		m_JX = m_mcChannelNumber % 10;
		std:: cout << " mc_channel_number = " <<  m_mcChannelNumber << std::endl;
		std:: cout << " JX = " <<  m_JX << std::endl;
	}

	m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();

	m_weight = m_isMC ? eventInfo->mcEventWeight() : 1.0;

	if ( m_isMC && m_applyPileupReweighting ){
	  m_weight_pileup = m_pileupReweightingTool->getCombinedWeight( *eventInfo );
	  CP::SystematicSet s; s.insert( CP::SystematicVariation("PRW_DATASF",1) );
	  m_pileupReweightingTool->applySystematicVariation( s );
	  m_weight_pileup_up = m_pileupReweightingTool->getCombinedWeight( *eventInfo);
	  s.clear(); s.insert(CP::SystematicVariation("PRW_DATASF",-1) );
	  m_weight_pileup_down = m_pileupReweightingTool->getCombinedWeight( *eventInfo );
  }
  else {m_weight_pileup = -1.0;}

	// for each slice   modified by WS
	if ( m_isMC ) m_hists1D[Form("%s_J%d_sumOfWeights",jetAlgo.c_str(),m_JX)]->Fill(0.5,m_weight);
	//if ( m_isMC ) m_hists1D[Form("%s_sumOfWeights",jetAlgo.c_str())]->Fill(0.5,m_weight);

	m_cutFlow[jetAlgo]->Fill(0.0,m_weight);

//////////////meta data reading
//const xAOD::CutBookkeeperContainer* completeCBC = 0;
//if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
//	   Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
//		return EL::StatusCode::FAILURE;
//		  }

// CHECK_STATUS( "AnalyseEvent()", event.retrieve( completeCBC, "CutBookkeepers") );

//Get some of event weights before derivation
//double initialSumOfWeightsInThisFile = 0;
//for (const auto& cbk: *completeCBC) {
//	double sumOfEventWeights = cbk->sumOfEventWeights();
//		// presume that the largest number is the num events in the primary xAOD
//	if( sumOfEventWeights > initialSumOfWeightsInThisFile ) 
//		 initialSumOfWeightsInThisFile = sumOfEventWeights; 
//		 }
//
//totalEventsWeighted += initialSumOfWeightsInThisFile;



	// CUT: GRL for data only
	if ( !m_isMC && m_applyGRL && !m_grlTool->passRunLB(*eventInfo) ) return;
	m_cutFlow[jetAlgo]->Fill(1.0,m_weight);

	// CUT: Errors in LAr/Tile/Core (corrupted events)
	// Event cleaning (for data only) to remove events with problematic regions of the detector
	if (!m_isMC && ( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)
			|| (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)
			|| (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) ) ) return;
	m_cutFlow[jetAlgo]->Fill(2.0,m_weight);

	// CUT: PV
	// Primary vertex with at least two tracks
	const xAOD::VertexContainer* pVertices = 0;
	if( !event.retrieve(pVertices,"PrimaryVertices").isSuccess() ) {
		std::cout << " Failed to retrieve primary vertices." << std::endl;
	}

	const xAOD::Vertex* PV = GetPV(pVertices);
	if( pVertices->size() < 1 || !PV ) {
		// no primary vertices
		std::cout << "WARNING: no primary vertex found! Skipping event." << std::endl;
		return;
	}

	if ( PV->nTrackParticles()<2 ) return;
  m_NPV = pVertices->size();
	m_cutFlow[jetAlgo]->Fill(3.0,m_weight);
  double rhoLC = -99, rhoEM = -99;
  const xAOD::EventShape* eventShapeLC = 0;

  //from R21, use origin event shape
  TString LCEventShape, EMEventShape;
  if(m_isR21) { LCEventShape="Kt4LCTopoOriginEventShape"; EMEventShape="Kt4EMTopoOriginEventShape"; }
  else { LCEventShape="Kt4LCTopoEventShape"; EMEventShape="Kt4EMTopoEventShape"; }

  CHECK_STATUS("execute",event.retrieve( eventShapeLC, LCEventShape.Data()));//Kt4LCTopoOriginEventShape
  if(eventShapeLC)eventShapeLC->getDensity( xAOD::EventShape::Density, rhoLC );
  m_rhoLC = rhoLC/GeV;

  const xAOD::EventShape* eventShapeEM = 0;
  CHECK_STATUS("execute",event.retrieve( eventShapeEM, EMEventShape.Data()));//Kt4EMTopoOriginEventShape
  if(eventShapeEM)eventShapeEM->getDensity( xAOD::EventShape::Density, rhoEM );
  m_rhoEM = rhoEM/GeV;

	if ( !m_isMC ) {
		std::vector<TString> trigs = vectorise(m_settings->GetValue("TriggersForOverlap",""));
		for ( uint xtrig = 0; xtrig<trigs.size(); ++xtrig ) {
			for ( uint ytrig = 0; ytrig<trigs.size(); ++ytrig ) {
				if ( !m_runLightWeight && m_trigDecTool->isPassed( std::string(trigs[xtrig].Data()) ) && m_trigDecTool->isPassed( std::string(trigs[ytrig].Data()) ) )
					m_hists2D[jetAlgo+"_Triggers_All"]->Fill(xtrig,ytrig,m_weight);
			}
		}
	}

	// CUT: Any trigger (for data only)
	if ( !m_isMC && m_applyTrigger && !PassAnyTrigger(m_trigs) )
		return;
	if( m_isMC && m_forceMCTrigs && !PassAnyTrigger(m_trigs))
		return;
	m_cutFlow[jetAlgo]->Fill(4.0,m_weight);

	// Trigger emulation
	if ( !m_isMC && m_doTriggerEmulation ) {
		const xAOD::JetContainer* trig_jets;
		CHECK_STATUS( "AnalyseEvent()", event.retrieve( trig_jets, Form("HLT_xAOD__JetContainer_%s",m_HLTJetContainer.Data()) ) );
		const xAOD::JetRoIContainer* trig_jets_L1;
		CHECK_STATUS( "AnalyseEvent()", event.retrieve( trig_jets_L1, "LVL1JetRoIs") );
		std::pair<int,int> trigBits = EmulateTriggerDecisionsRun2(trig_jets,trig_jets_L1, true);
		m_trigBitsCtrl = trigBits.first;
		m_trigBitsFwd = trigBits.second;
    // emulate L1_J15
    m_passRaw_L1_J15 = PassL1(15,(*trig_jets_L1), true);
    m_passRaw_L1_J15_31ETA49 = PassL1(15,(*trig_jets_L1), false);
	}
	if ( m_isMC && m_doTriggerEmulation ) {
		const xAOD::JetContainer* trig_jets;
		CHECK_STATUS( "AnalyseEvent()", event.retrieve( trig_jets, Form("HLT_xAOD__JetContainer_%s",m_HLTJetContainer.Data()) ) );

		std::pair<int,int> trigBits = EmulateTriggerDecisionsRun2(trig_jets, 0, false);
		m_trigBitsCtrl = trigBits.first;
		m_trigBitsFwd = trigBits.second;
	}

	// Get the jets
	const xAOD::JetContainer* jets = 0;

	TString jetCollectionName = TString(jetAlgo); //jetCollectionName:AntiKt4EMTopos AntiKt4LCTopo AntiKt4EMPFlow
	if ( TString(jetAlgo).Contains("HLT") ) {
		jetCollectionName.ReplaceAll("HLT_","");
		jetCollectionName = "HLT_xAOD__JetContainer_"+jetCollectionName;
	}
	else {
		jetCollectionName = jetCollectionName + "Jets";
	}

	if ( !event.retrieve( jets, std::string(jetCollectionName.Data()) ).isSuccess() )
		std::cout << " Failed to retrieve the jets." << std::endl;

	std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );

	// Add shallow copy to TStore, so it handles memory cleanup
	if (!store.record(jets_shallowCopy.first, Form("shallowCopy_%s",jetAlgo.c_str()))) {
		std::cout << "Could not record shallow copy of " << jetAlgo << " jets to TStore." << std::endl;
		abort();
	}
	if (!store.record(jets_shallowCopy.second, Form("shallowCopy_%sAux",jetAlgo.c_str()))) {
		std::cout << "Could not record aux shallow copy of " << jetAlgo << " jets to TStore." << std::endl;
		abort();
	}

	xAOD::JetContainer calibJets = xAOD::JetContainer(jets_shallowCopy.first->begin(),
			jets_shallowCopy.first->end(),
			SG::VIEW_ELEMENTS);

	xAOD::JetContainer selJets = xAOD::JetContainer(SG::VIEW_ELEMENTS);


	bool jetInBadEtaRegion = false;

	for (auto jet : calibJets) {  

		if( TString(jetAlgo).Contains("HLT") ) {  
			jet->auxdata< float >("DetectorEta") = jet->eta();
			jet->auxdata< float >("DetectorPhi") = jet->phi();

			jet->auxdata< bool >("TriggerJet") = true;
		} 
		else { 
			float E_orig = jet->e();

			//      std::cout << "before: " << jet->pt() << "\t" << jet->eta() << "\t" << jet->phi() << "\t" << jet->e() << std::endl;

			// calibrate jet
			if ( m_applyJetCalib ) { 
				if( m_jetCalibTools[jetAlgo]->applyCorrection(*jet) == CP::CorrectionCode::Error )
					std::cout << "WARNING: error while calibrating jet" << std::endl;
			}
			else if ( m_EMscale ) {
				xAOD::JetFourMom_t jetconstitP4 = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
				//	std::cout << "emscale: " << jetconstitP4.pt() << "\t" << jetconstitP4.eta() << "\t" << jetconstitP4.phi() << "\t" << jetconstitP4.e() << std::endl;
				jet->setJetP4( jetconstitP4 );
				/*
	jet->setAttribute<float>("pt") = jetconstitP4.pt();
	jet->eta() = jetconstitP4.eta();
	jet->phi() = jetconstitP4.phi();
	jet->E() = jetconstitP4.E();
				 */
			} 

			//      std::cout << "after: " << jet->pt() << "\t" << jet->eta() << "\t" << jet->phi() << "\t" << jet->e() << std::endl;

			// correct JVT of jet
			if ( m_applyJVT ) { 
				if ( m_applyJetCalib ) { 
                    float corrJVT = m_updateJvt->updateJvt(*jet);
					jet->auxdata< float >( "corrJVT" ) = corrJVT;
				} 
				else
					jet->auxdata< float >( "corrJVT" ) = m_jvtTool->evaluateJvt(jet->getAttribute<float>("JvtRpt"), jet->getAttribute<float>("JvtJvfcorr"));
			} 

			// decorate with original jet energy
			jet->auxdata< float >( "E_orig" ) = E_orig;

			// decorate with detector-level eta and phi
			xAOD::JetFourMom_t jetconstitP4 = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
			if(!m_isR21) jet->auxdata< float >("DetectorEta") = jetconstitP4.eta(); //for R21, JetConstitScaleMomentum has the origin correction and shouldn't be used, and DetectorEta is already stored with the proper value.
			jet->auxdata< float >("DetectorPhi") = jetconstitP4.phi();

			jet->auxdata< bool >("TriggerJet") = false;

			// decorate with JVF for selected PV
			if ( m_applyJVF ) {
			  std::vector<float> jvf = jet->getAttribute< std::vector<float> >( "JVF" );
			  if(jvf.size()>0){ 
			    int ivtx = PV->auxdata< int >("index");
			    if(ivtx!=0) std::cout << "JVF = " << jvf[ivtx] << " for vertex " << ivtx << std::endl;
			    jet->auxdata< float >("JVF_PV") = jvf[ivtx];
			  } 
			  else{ 
			    std::cout << " WARNING: JVF vector empty?" << std::endl;
			  } 
			} 
		} 

		if ( m_vetoJetsInBadEta || m_vetoEventsInBadEta ) { 
			if ( fabs(jet->auxdata<float>("DetectorEta")) > m_badEtaRange[0] && fabs(jet->auxdata<float>("DetectorEta")) < m_badEtaRange[1] ) { 
				jetInBadEtaRegion = true;
				continue;
			}
		}

		if ( TString(jetAlgo).Contains("AntiKt4EMTopo") && m_applyJ3Cleaning ) { 
			if ( !m_jetCleaningTool->accept( *jet ) )
				continue;
		}

		selJets.push_back(jet);

	} 

	if ( m_vetoEventsInBadEta && jetInBadEtaRegion )
		return;

	// Sort the jets descending calibrated pT
	std::sort(selJets.begin(),selJets.end(),descendingPt);

		m_weight_ptslice = -1;  
		m_j1_is_truth_jet = false;  
		m_j2_is_truth_jet = false;  
		m_j3_is_truth_jet = false;  
		m_j4_is_truth_jet = false;  
		m_j1_match_pT_truth = -99;
		m_j2_match_pT_truth = -99;
		m_j3_match_pT_truth = -99;
		m_j4_match_pT_truth = -99;
		m_j1_NumTrkPt500 = -99;  
		m_j2_NumTrkPt500 = -99;  
		m_j3_NumTrkPt500 = -99;  
		m_j4_NumTrkPt500 = -99;  
		m_j1_trackWidth = -99;  
		m_j2_trackWidth = -99;  
		m_j3_trackWidth = -99;  
		m_j1_trackC10 = -99;  
		m_j2_trackC10 = -99;  
		m_j3_trackC10 = -99;  
		m_trackC1 = -99;
		m_j1_bdt_resp = -99;  
		m_j2_bdt_resp = -99;  
		m_j3_bdt_resp = -99;  
		m_j1_NTracks = -99;  
		m_j2_NTracks = -99;  
		m_j3_NTracks = -99;  
		m_j1_partonLabel = -99;   // branch for parton label
		m_j2_partonLabel = -99;
		m_j3_partonLabel = -99;
		m_j4_partonLabel = -99;

	if ( !m_applyDijetSelection ){ 

		m_pass_HLT_j15 = m_trigDecTool->isPassed( "HLT_j15" );
		m_pass_HLT_j15_320eta490 = m_trigDecTool->isPassed( "HLT_j15_320eta490" );
		m_pass_HLT_j25 = m_trigDecTool->isPassed( "HLT_j25" );
		m_pass_HLT_j25_320eta490 = m_trigDecTool->isPassed( "HLT_j25_320eta490" );
		m_pass_HLT_j35 = m_trigDecTool->isPassed( "HLT_j35" );
		m_pass_HLT_j35_320eta490 = m_trigDecTool->isPassed( "HLT_j35_320eta490" );
		m_pass_HLT_j45 = m_trigDecTool->isPassed( "HLT_j45" );
		m_pass_HLT_j45_320eta490 = m_trigDecTool->isPassed( "HLT_j45_320eta490" );
		m_pass_HLT_j60 = m_trigDecTool->isPassed( "HLT_j60" );
		m_pass_HLT_j60_320eta490 = m_trigDecTool->isPassed( "HLT_j60_320eta490" );
		m_pass_HLT_j110 = m_trigDecTool->isPassed( "HLT_j110" );
		m_pass_HLT_j110_320eta490 = m_trigDecTool->isPassed( "HLT_j110_320eta490" );
		m_pass_HLT_j175 = m_trigDecTool->isPassed( "HLT_j175" );
		m_pass_HLT_j175_320eta490 = m_trigDecTool->isPassed( "HLT_j175_320eta490" );
		m_pass_HLT_j260 = m_trigDecTool->isPassed( "HLT_j260" );
		m_pass_HLT_j260_320eta490 = m_trigDecTool->isPassed( "HLT_j260_320eta490" );
		m_pass_HLT_j360 = m_trigDecTool->isPassed( "HLT_j360" );
		m_pass_HLT_j360_320eta490 = m_trigDecTool->isPassed( "HLT_j360_320eta490" ); // lowest unprescaled single jet triggerks(forward)
		m_pass_HLT_j400 = m_trigDecTool->isPassed( "HLT_j400" );// lowest unprescaled single jet triggers(central)
		m_pass_HLT_j400_320eta490 = 0;
		m_pass_HLT_j0_perf_L1RD0_FILLED = m_trigDecTool->isPassed( "HLT_j0_perf_L1RD0_FILLED" );

		m_j1_pT = -99; m_j1_eta = -99; m_j1_phi = -99; 
		m_j2_pT = -99; m_j2_eta = -99; m_j2_phi = -99; m_j2_passCleaning=false; m_j2_passJVF=false; m_j2_passJVT=false;
		m_j3_pT = -99; m_j3_eta = -99; m_j3_phi = -99; m_j3_passCleaning=false; m_j3_passJVF=false; m_j3_passJVT=false;
		m_Dphijj = -99; m_pTavg = -99; m_pT3OverpTAvg = -1;

		if ( selJets.size()<1 )
			return;
  	       
	
		const xAOD::Jet& jet1 = *selJets.at(0);

		if(m_isMC){
			if(truthJets != NULL && truthJets->size() >= 1){
				if (matchTruthJet(jet1,*truthJets,0.4)!= NULL){
					m_j1_is_truth_jet = true;
					m_j1_match_pT_truth = matchTruthJet(jet1,*truthJets,0.4)->pt();
				}
			}
		}
		

		if ( jet1.pt() < 15000 )
			return;



		m_j1_pT = jet1.pt()/GeV;
		m_j1_E = jet1.e()/GeV;
		m_j1_eta = jet1.auxdata<float>("DetectorEta");
		m_j1_phi = jet1.auxdata<float>("DetectorPhi");
   	m_j1_JVT = -1;m_j2_JVT=-1;m_j3_JVT=-1;
		if ( !TString(jetAlgo).Contains("HLT") ) { 
			m_j1_passCleaning = m_jetCleaningTool->accept( jet1 );
			m_j1_passJVF = passJVF(jet1);
			m_j1_passJVT = passJVT(jet1, m_jvtCut[TString(jetAlgo)] );
		  m_j1_JVT = getJVT(jet1);//jet1.getAttribute< float >( "corrJVT" ) ;
		  if(m_isMC){
			  m_j1_partonLabel = jet1.auxdata< int >("PartonTruthLabelID"); 
		  }
		  std::vector<int>  j1_ntrackVector = jet1.auxdata< std::vector< int> >("NumTrkPt500");
		  m_j1_NumTrkPt500 = j1_ntrackVector[0]; 
		 
//		std::vector<const xAOD::IParticle*> trackVector1 = jet1.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack);
//		m_j1_NTracks=trackVector1.size();
//		  if(m_j1_JVT != -1)
//			  std::cout << "m_j1_JVT = " << m_j1_JVT << std::endl;
		} 

		if (selJets.size() < 2) return ;

			const xAOD::Jet& jet2 = *selJets.at(1);

			if(m_isMC){
				if(truthJets->size() >= 1){
					if (matchTruthJet(jet2,*truthJets,0.4)){
						m_j2_is_truth_jet = true;
						m_j2_match_pT_truth = matchTruthJet(jet2,*truthJets,0.4)->pt();
					}
				}
			}
		
			m_j2_pT = jet2.pt()/GeV;
			m_j2_E = jet2.e()/GeV;
			m_j2_eta = jet2.auxdata<float>("DetectorEta");
			m_j2_phi = jet2.auxdata<float>("DetectorPhi");

			if ( !TString(jetAlgo).Contains("HLT") ) { 
//				std::cout << "Reading in JVT ..." << std::endl;
				m_j2_passCleaning = m_jetCleaningTool->accept( jet2 );
				m_j2_passJVF = passJVF(jet2);
				m_j2_passJVT = passJVT(jet2, m_jvtCut[TString(jetAlgo)]);
                        m_j2_JVT =getJVT(jet2);//jet2.getAttribute< float >( "corrJVT" );
			if(m_isMC){
				m_j2_partonLabel = jet2.auxdata< int >("PartonTruthLabelID"); 
			}
		  	std::vector<int>  j2_ntrackVector = jet2.auxdata< std::vector< int> >("NumTrkPt500");
		  	m_j2_NumTrkPt500 = j2_ntrackVector[0]; 
		
			std::vector<const xAOD::IParticle*> trackVector2 = jet2.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack);
			m_j2_NTracks=trackVector2.size();
			} 

			m_Dphijj = deltaPhi(jet1,jet2);
			m_pTavg = (jet1.pt()+jet2.pt())/2.0;


			if ( selJets.size()>2 ) { 
				const xAOD::Jet& jet3 = *selJets.at(2);

			if(m_isMC){
				if(truthJets->size() >= 1){
					if (matchTruthJet(jet3,*truthJets,0.4)){
						m_j3_is_truth_jet = true;
						m_j3_match_pT_truth =matchTruthJet(jet3,*truthJets,0.4)->pt();
					}
				}
			}


				m_j3_pT = jet3.pt()/GeV;
				m_j3_E = jet3.e()/GeV;
			        m_pT3OverpTAvg = jet3.pt()/((jet1.pt()+jet2.pt())/2.0);
				m_j3_eta = jet3.auxdata<float>("DetectorEta");
				m_j3_phi = jet3.auxdata<float>("DetectorPhi");
				if ( !TString(jetAlgo).Contains("HLT") ) { 
					m_j3_passCleaning = m_jetCleaningTool->accept( jet3 );
					m_j3_passJVF = passJVF(jet3);
					m_j3_passJVT = passJVT(jet3, m_jvtCut[TString(jetAlgo)]);
					m_j3_JVT =getJVT(jet3);
					if(m_isMC){
						m_j3_partonLabel = jet3.auxdata< int >("PartonTruthLabelID"); 
					}
					std::vector<int>  j3_ntrackVector = jet3.auxdata< std::vector< int> >("NumTrkPt500");
			  	m_j3_NumTrkPt500 = j3_ntrackVector[0]; 
		
				std::vector<const xAOD::IParticle*> trackVector3 = jet3.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack);
				m_j3_NTracks=trackVector3.size();
				} 
			} 
//		} 


		
		m_bcid = eventInfo->bcid();
		m_actualIntPerXing = eventInfo->actualInteractionsPerCrossing();
		m_avgIntPerXing = eventInfo->averageInteractionsPerCrossing();


		std::vector<double> pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));
		uint pTbin=0;
		for (uint i=0;i<pTavg_bins.size();++i)
			if (m_pTavg/GeV > pTavg_bins[i]) pTbin=i+1;

		m_passTrigger=false;
		m_trigClass="";
		m_trigHLT="";

		// skip event if pTavg outside bin range
		if (pTbin==0 || pTbin>(pTavg_bins.size()-1)) { 
			m_passTrigger=false;
		} 
		else { 
			int itrig=0;
//			std::cout << "current itrig = " << itrig << std::endl;
			// loop over trigger OR combinations
//			std::cout << " trigORs size = " << m_trigORs.size() << std::endl;
			for ( const auto& trig : m_trigORs ) { 
				TString trig_or = trig;
				trig_or.ReplaceAll("_OR_"," ");
				TString ctrl_trig = vectorise(trig_or).at(0);
				TString fwd_trig = vectorise(trig_or).at(1);

				bool passTrig = m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) || m_trigDecTool->isPassed( std::string(fwd_trig.Data()) );

				//std::cout<<"trig "<<ctrl_trig.Data()<<" | "<<fwd_trig.Data()<<std::endl;
				if ( !passTrig )
					continue;

				// pass pTavg threshold requirement
				//ASSUMES THAT
				if ( m_pTavg/GeV < m_trigThresholds[itrig] )
					continue;

				// RefFJ_FJ or RefFJ_J
				std::string jetClass = TriggerClassification(trig);
				if ( jetClass=="" )
					continue;

				m_passTrigger=true;
				if ( m_trigClass!="" )
					m_trigClass += "_";

				m_trigClass += TString(jetClass);
//				std::cout<<"trig set = "<<m_trigClass.Data() << std::endl;
				ctrl_trig.ReplaceAll("HLT_","");

				if ( m_trigHLT!="" )
					m_trigHLT += "_";

				m_trigHLT += ctrl_trig;

				++itrig;
			} 
		} 

		if ( !m_isMC && TString(jetAlgo).Contains("HLT") ) { 

			m_HLT_pT = jet1.pt();
			m_HLT_eta = jet1.eta();
			m_HLT_phi = jet1.phi();

			const xAOD::JetRoIContainer* trig_jets_L1;
			CHECK_STATUS( "AnalyseEvent()", event.retrieve( trig_jets_L1, "LVL1JetRoIs") );
			float etmax=0;
			for ( xAOD::JetRoIContainer::const_iterator jet = trig_jets_L1->begin(); jet < trig_jets_L1->end(); ++jet ) {
				if ( (*jet)->et8x8()>etmax ) { 
					etmax = (*jet)->et8x8();
					m_L1_et8x8 = (*jet)->et8x8();
					m_L1_eta = (*jet)->eta();
					m_L1_phi = (*jet)->phi();
					m_L1_word = (*jet)->roiWord();
				} 
			} 

		      m_trig_mini_tree->Fill();

		}
//		const xAOD::Jet& tmpjet1 = *selJets.at(0);
//                m_test2DHist->Fill(tmpjet1.auxdata<float>("DetectorEta"),tmpjet1.auxdata<float>("DetectorPhi"));   
		m_jetCollectionTrees[jetAlgo]->Fill();
		return;
	}

	// CUT: at least 2 jets  ( applyDijetSelection )
	
	if ( selJets.size()<2 )
		return;
	m_cutFlow[jetAlgo]->Fill(5.0,m_weight);

	///modified by ws, trigger for dijet
	m_pass_HLT_j15 = m_trigDecTool->isPassed( "HLT_j15" );
	m_pass_HLT_j15_320eta490 = m_trigDecTool->isPassed( "HLT_j15_320eta490" );
	m_pass_HLT_j25 = m_trigDecTool->isPassed( "HLT_j25" );
	m_pass_HLT_j25_320eta490 = m_trigDecTool->isPassed( "HLT_j25_320eta490" );
	m_pass_HLT_j35 = m_trigDecTool->isPassed( "HLT_j35" );
	m_pass_HLT_j35_320eta490 = m_trigDecTool->isPassed( "HLT_j35_320eta490" );
	m_pass_HLT_j45 = m_trigDecTool->isPassed( "HLT_j45" );
	m_pass_HLT_j45_320eta490 = m_trigDecTool->isPassed( "HLT_j45_320eta490" );
	m_pass_HLT_j60 = m_trigDecTool->isPassed( "HLT_j60" );
	m_pass_HLT_j60_320eta490 = m_trigDecTool->isPassed( "HLT_j60_320eta490" );
	m_pass_HLT_j110 = m_trigDecTool->isPassed( "HLT_j110" );
	m_pass_HLT_j110_320eta490 = m_trigDecTool->isPassed( "HLT_j110_320eta490" );
	m_pass_HLT_j175 = m_trigDecTool->isPassed( "HLT_j175" );
	m_pass_HLT_j175_320eta490 = m_trigDecTool->isPassed( "HLT_j175_320eta490" );
	m_pass_HLT_j260 = m_trigDecTool->isPassed( "HLT_j260" );
	m_pass_HLT_j260_320eta490 = m_trigDecTool->isPassed( "HLT_j260_320eta490" );
	m_pass_HLT_j360 = m_trigDecTool->isPassed( "HLT_j360" );
	m_pass_HLT_j360_320eta490 = m_trigDecTool->isPassed( "HLT_j360_320eta490" ); // lowest unprescaled single jet triggerks(forward)
	m_pass_HLT_j400 = m_trigDecTool->isPassed( "HLT_j400" );	// lowest unprescaled single jet triggers (central)
	m_pass_HLT_j400_320eta490 = 0; // HLT_j400_320eta490 does not exist in the trigDecTool, but the variable is only needed for the other scripts of the package
	m_pass_HLT_j0_perf_L1RD0_FILLED = m_trigDecTool->isPassed( "HLT_j0_perf_L1RD0_FILLED" );

	if ( !(m_pass_HLT_j15 || m_pass_HLT_j25 || m_pass_HLT_j35 || m_pass_HLT_j45 || m_pass_HLT_j60 || m_pass_HLT_j110 || m_pass_HLT_j175 || m_pass_HLT_j260 || m_pass_HLT_j360 || m_pass_HLT_j400) )
		return;

	m_j1_pT = -99; m_j1_eta = -99; m_j1_phi = -99; 
	m_j4_pT = -99; m_j4_eta = -99; m_j4_phi = -99; 
	m_j2_pT = -99; m_j2_eta = -99; m_j2_phi = -99; m_j2_passCleaning=false; m_j2_passJVF=false; m_j2_passJVT=false;
	m_j3_pT = -99; m_j3_eta = -99; m_j3_phi = -99; m_j3_passCleaning=false; m_j3_passJVF=false; m_j3_passJVT=false;
	m_Dphijj = -99; m_pTavg = -99; m_pT3OverpTAvg = -1;

	const xAOD::Jet& jet1 = *selJets.at(0);
	const xAOD::Jet& jet2 = *selJets.at(1);

	if(m_mcChannelNumber == 426131){
		m_weight_ptslice = 6.08E+01;
	}
	else if(m_mcChannelNumber == 426132){m_weight_ptslice = 8.46E-02;}
	else if(m_mcChannelNumber == 426133){m_weight_ptslice = 3.69E-05;}
	else if(m_mcChannelNumber == 426134){m_weight_ptslice = 3.35E-07;}
	else if(m_mcChannelNumber == 426135){m_weight_ptslice = 6.28E-09;}
	else if(m_mcChannelNumber == 426136){m_weight_ptslice = 9.12E-10;}
	else if(m_mcChannelNumber == 426137){m_weight_ptslice = 6.82E-11;}
	else if(m_mcChannelNumber == 426138){m_weight_ptslice = 2.43E-12;}
	else if(m_mcChannelNumber == 426139){m_weight_ptslice = 9.23E-14;}
	else if(m_mcChannelNumber == 426140){m_weight_ptslice = 2.72E-15;}
	else if(m_mcChannelNumber == 426141){m_weight_ptslice = 4.56E-17;}
	else if(m_mcChannelNumber == 426142){m_weight_ptslice = 2.63E-19;}
        
	// third jet
	xAOD::Jet* jet3 = 0;
	for (uint ijet=2; ijet<selJets.size(); ++ijet) {
		if ( m_applyJVF && !TString(jetAlgo).Contains("HLT") && !passJVF( *selJets.at(ijet) ) ) {
			continue;
		}
		else if ( m_applyJVT && !TString(jetAlgo).Contains("HLT") && !passJVT( *selJets.at(ijet), m_jvtCut[TString(jetAlgo)] ) ) {
			continue;
		}
		if ( m_applyJ3Cleaning && TString(jetAlgo).Contains("AntiKt4EMTopo") ) {
			if ( !m_jetCleaningTool->accept( *selJets.at(ijet) ) )
				continue;
		}

		jet3 = selJets.at(ijet);
		break;
	}
	// 4th jet
	xAOD::Jet* jet4 = 0;
	if(selJets.size()>=4) { jet4 = selJets.at(3); }

	float _j3_pT = jet3 ? jet3->pt() : 0.0;

	// dijet quantities
	float _deltaPhi = fabs( deltaPhi(jet1,jet2) );
	float _pTavg = (jet1.pt()+jet2.pt())/2.0;

	if ( m_controlPlots ) FillControlHistograms("2jets",jetAlgo,_pTavg,_deltaPhi,jet1,jet2,jet3);

	// CUT: JVF/JVT
	if(!m_doSystematics) {
		if ( m_applyJVF && !TString(jetAlgo).Contains("HLT") && (!passJVF(jet1) || !passJVF(jet2)) )
			return;
		if ( m_applyJVT && !TString(jetAlgo).Contains("HLT") && (!passJVT(jet1, m_jvtCut[TString(jetAlgo)]) || !passJVT(jet2, m_jvtCut[TString(jetAlgo)])) )
			return;
	}
	else if (m_doSystematics){
//		std::cout<<"m_doSystematics"<<std::endl;
		if ( m_applyJVT && !TString(jetAlgo).Contains("HLT") && (!passJVT(jet1, m_jvtCut[TString(jetAlgo)]) || !passJVT(jet2, m_jvtCut[TString(jetAlgo)])) ) {m_pass_Nominal =false;}
		if ( m_applyJVT && !TString(jetAlgo).Contains("HLT") && (!passJVT(jet1, m_jvtCutTight[TString(jetAlgo)]) || !passJVT(jet2, m_jvtCutTight[TString(jetAlgo)])) ) m_pass_JVTThight =false;
		if ( m_applyJVT && !TString(jetAlgo).Contains("HLT") && (!passJVT(jet1, m_jvtCutLoose[TString(jetAlgo)]) || !passJVT(jet2, m_jvtCutLoose[TString(jetAlgo)])) ) m_pass_JVTLoose =false;
	}
	if (!m_pass_Nominal && !m_pass_JVTThight && !m_pass_JVTLoose) return;
	if (m_pass_Nominal){ m_cutFlow[jetAlgo]->Fill(7.0,m_weight);
	if ( m_controlPlots ) FillControlHistograms("JVT",jetAlgo,_pTavg,_deltaPhi,jet1,jet2,jet3);
	}

	// CUT: leading jets are not bad
	if ( m_applyJetCleaning && TString(jetAlgo).Contains("AntiKt4EMTopo") && (!m_jetCleaningTool->accept( jet1 ) || !m_jetCleaningTool->accept( jet2 )) )
		return;
	if (m_pass_Nominal){m_cutFlow[jetAlgo]->Fill(6.0,m_weight);

	if ( m_controlPlots ) FillControlHistograms("cleaning",jetAlgo,_pTavg,_deltaPhi,jet1,jet2,jet3);
	}

	float eta1 = jet1.auxdata<float>("DetectorEta"), eta2 = jet2.auxdata<float>("DetectorEta");

	// calculate eta-intercalibration quantities
	bool _jetInRefRegion = false; // at least one jet in reference region?
	float _probe_eta=-99., _ref_eta = -99.;
	float _probe_pT=-99., _ref_pT = -99.;
	float _probe_phi=-99., _ref_phi = -99.;

	if ( fabs(eta1)<m_refRegion ) {
		_jetInRefRegion = true; _ref_eta = eta1; _ref_pT = jet1.pt(); _probe_eta = eta2; _probe_pT = jet2.pt(); _ref_phi = jet1.phi(); _probe_phi = jet2.phi();
	}
	else if ( fabs(eta2)<m_refRegion ) {
		_jetInRefRegion = true; _ref_eta = eta2; _ref_pT = jet2.pt(); _probe_eta = eta1; _probe_pT = jet1.pt(); _ref_phi = jet2.phi(); _probe_phi = jet1.phi();
	}

	// matrix method
	float _right_eta = eta1 > eta2 ? eta1 : eta2;
	float _right_pT = eta1 > eta2 ? jet1.pt() : jet2.pt();
	float _left_eta = eta1 < eta2 ? eta1 : eta2;
	float _left_pT = eta1 < eta2 ? jet1.pt() : jet2.pt();
	float _right_phi = eta1 > eta2 ? jet1.phi() : jet2.phi();
	float _left_phi = eta1 < eta2 ? jet1.phi() : jet2.phi();

	//  m_hists1D["AntiKt4EMTopo_deltaPhi"]->Fill(_deltaPhi,m_weight);
	if (m_pass_Nominal && m_controlPlots && !m_runLightWeight) m_hists1D[jetAlgo+"_deltaPhi"]->Fill(_deltaPhi,m_weight);

	// CUT: delta phi between leading jets
	//  if ( m_applyDphijj && _deltaPhi < m_dphijjCut ) return;
	//  m_cutFlow[jetAlgo]->Fill(8.0,m_weight);

	if(!m_doSystematics) {
		if ( m_applyDphijj && _deltaPhi < m_dphijjCut ) return;
	}
	if (m_doSystematics){
		if ( m_applyDphijj && _deltaPhi < m_dphijjCut ) {
			m_pass_Nominal =false;
			m_pass_JVTThight = false;
			m_pass_JVTLoose = false;
		}
		if ( m_applyDphijj && m_doDphiShiftDown && _deltaPhi < m_dphijjDownCut )
			m_pass_DphiDown =false;
		if ( m_applyDphijj && m_doDphiShiftUp && _deltaPhi < m_dphijjUpCut )
			m_pass_DphiUp =false;
	}
	if(!m_pass_Nominal && !m_pass_DphiDown && !m_pass_DphiUp ) return;
	if (m_pass_Nominal){ m_cutFlow[jetAlgo]->Fill(8.0,m_weight);

	if ( m_controlPlots ) FillControlHistograms("Dphijj",jetAlgo,_pTavg,_deltaPhi,jet1,jet2,jet3);
	}

	// CUT: third jet pT cut
	//  if ( m_applyJ3pT && jet3 && jet3->pt()/_pTavg > m_j3pTCut )
	//    return;

	if(!m_doSystematics) {
		if ( m_applyJ3pT && jet3 && jet3->pt()/_pTavg > m_j3pTCut ) return;
	}
	if (m_doSystematics && m_applyJ3pT ){
		if ( jet3 && jet3->pt()/_pTavg > m_j3pTCut) {m_pass_Nominal =false; m_pass_JVTThight =false; m_pass_JVTLoose=false;m_pass_DphiDown =false; m_pass_DphiUp =false;}
		if ( m_doJet3PtFracShiftDown && jet3 &&  jet3->pt()/_pTavg > m_j3pTCutDown) m_pass_J3pTCutDown =false;
		if ( m_doJet3PtFracShiftUp  && jet3 &&  jet3->pt()/_pTavg > m_j3pTCutUp) m_pass_J3pTCutUp =false;
	}
	if(!m_pass_Nominal && !m_pass_J3pTCutDown && !m_pass_J3pTCutUp ) return;
	if (m_pass_Nominal) {
		if ( m_controlPlots ) FillControlHistograms("j3pT",jetAlgo,_pTavg,_deltaPhi,jet1,jet2,jet3);

		m_cutFlow[jetAlgo]->Fill(9.0,m_weight);
	}

	// CUT: MC cleaning
	if ( m_isMC ) {
		const xAOD::JetContainer *truthJets = 0;
		// TO DO: change AntiKt4TruthJets to match radius of reco jet collection
		if ( !event.retrieve( truthJets, "AntiKt4TruthJets" ).isSuccess() )
			std::cout << "Faild to retrieve the truth jets" << std::endl;

		// must be at least one truth jet
		if (truthJets->size()<1) {
			std::cout << "WARNING: no truth jets! Skipping event." << std::endl;
			return;
		}
		const xAOD::Jet& leadingTruthJet = *truthJets->at(0);

		// require pTavg to be within 40% of leading truth jet pT
		if( m_applyMCCleaning && _pTavg/leadingTruthJet.pt() > 1.4 ) {
		std::cout<<"m_applyMCCleaning and _pTavg/leadingTruthJet.pt"<<std::endl;
			return;
		}

	}
	if (m_pass_Nominal) m_cutFlow[jetAlgo]->Fill(10.0,m_weight);
	// asymmetries
	float _Asm_SM = _jetInRefRegion ? ( _probe_pT - _ref_pT )/_pTavg : -99;
	float _Asm_MM = ( _left_pT - _right_pT )/_pTavg;

	DijetBalance dijet(selJets,m_refRegion);
	// fill mini-tree
	m_pTavg = _pTavg;
	m_Asym_SM = _Asm_SM;
	m_Asym_MM = _Asm_MM;
	m_j1_pT = jet1.pt()/GeV;m_j1_eta = jet1.auxdata<float>("DetectorEta"); m_j1_phi = jet1.auxdata<float>("DetectorPhi");
	m_j2_pT = jet2.pt()/GeV; m_j2_eta = jet2.auxdata<float>("DetectorEta"); m_j2_phi = jet2.auxdata<float>("DetectorPhi");

	///////////////////////////
	//modified by ws
	///////////////////////////////////////
// for BDT
	if(m_isMC){
		if(truthJets != NULL && truthJets->size() >= 1){
			if (matchTruthJet(jet1,*truthJets,0.4)!= NULL){
				m_j1_is_truth_jet = true;
				m_j1_match_pT_truth = matchTruthJet(jet1,*truthJets,0.4)->pt();
			}
		}
		if(truthJets != NULL && truthJets->size() > 1){
				if (matchTruthJet(jet2,*truthJets,0.4)!= NULL){
					m_j2_is_truth_jet = true;
					m_j2_match_pT_truth = matchTruthJet(jet2,*truthJets,0.4)->pt();
				}
		}
		if(jet3 && truthJets != NULL && truthJets->size() > 1){
				if (matchTruthJet(*jet3,*truthJets,0.4)!= NULL){
					m_j3_is_truth_jet = true;
					m_j3_match_pT_truth = matchTruthJet(*jet3,*truthJets,0.4)->pt();
				}
		}
		if(jet4 && truthJets != NULL && truthJets->size() > 1){
				if (matchTruthJet(*jet4,*truthJets,0.4)!= NULL){
					m_j4_is_truth_jet = true;
					m_j4_match_pT_truth = matchTruthJet(*jet4,*truthJets,0.4)->pt();
				}
		}

		if(!m_j1_is_truth_jet || !m_j2_is_truth_jet ) return;

		if(jet3 && m_j3_is_truth_jet) {
			m_j3_bdt_resp = m_Tagger->getScore(*jet3);
			m_j3_trackC10 = jet3->auxdata<float>("DFCommonJets_QGTagger_TracksC1");
			m_j3_trackWidth = jet3->auxdata<float>("DFCommonJets_QGTagger_TracksWidth");
			std::vector<int>  j3_ntrackVector = jet3->auxdata< std::vector< int> >("NumTrkPt500");
			m_j3_NumTrkPt500 = j3_ntrackVector[0]; 
			m_j3_partonLabel = jet3->auxdata< int >("PartonTruthLabelID");
		}


		m_j1_partonLabel = jet1.auxdata< int >("PartonTruthLabelID"); 
		m_j2_partonLabel = jet2.auxdata< int >("PartonTruthLabelID"); 


		if(jet4){
			m_j4_partonLabel = jet4->auxdata< int >("PartonTruthLabelID");
		}
	} //end for is_MC
//		if(m_isMC){
//		}
//		else{
//		m_j1_trackWidth = jet1.auxdata<float>("TrackWidthPt500");
/*
		//TrackC1
		float beta = 0.2;
		float weightedwidth = 0.;
		float weightedwidth2 = 0.;
		float sumPt = 0.;
		float sumPt2 = 0.;
		std::vector<const xAOD::TrackParticle*> trackParttmp;
		std::vector<const xAOD::TrackParticle*> trackParttmp2;

		if(!jet1.getAssociatedObjects("GhostTrack",trackParttmp)){
			std::cout<<"This jet has no associated objects"<<std::endl;	
			//ATH_MSG_ERROR("This jet has no associated objects");
		}
		 //track selection
		 for(unsigned i=trackParttmp.size();i>0; i--){
			 if(!trackParttmp[i-1]){
				 trackParttmp.erase(trackParttmp.begin()+i-1);
				 continue;
			 }
		
		 const xAOD::TrackParticle* trk = static_cast<const xAOD::TrackParticle*>(trackParttmp[i-1]);
		 bool accept = (trk->pt()>500 &&
		 m_trkSelectionTool->accept(*trk) &&
		 (trk->vertex()==primvertex || (!trk->vertex() &&
		 fabs((trk->z0()+trk->vz()-primvertex->z())*sin(trk->theta()))<3.))
		 );
		 if (!accept){
		 trackParttmp.erase(trackParttmp.begin()+i-1);
		 }
		 }


		//calculate TrackC1 (and TrackWidth if necessary)
		for(unsigned i=0; i<trackParttmp.size(); i++){
			double ipt = trackParttmp.at(i)->pt();
			double ieta = trackParttmp.at(i)->eta();
			double iphi = trackParttmp.at(i)->phi();
			sumPt += ipt;
			for(unsigned j=i+1; j<trackParttmp.size(); j++){
				double deta = ieta - trackParttmp.at(j)->eta();
				double dphi = TVector2::Phi_mpi_pi(iphi - trackParttmp.at(j)->phi());
				double dR = sqrt( deta*deta + dphi*dphi );
				weightedwidth += ipt * trackParttmp.at(j)->pt() * pow(dR,beta);
			}
		}
		m_j1_trackC10 = sumPt>0 ? weightedwidth/(sumPt*sumPt) : -99;

		if(!jet2.getAssociatedObjects("GhostTrack",trackParttmp2)){
			std::cout<<"This jet has no associated objects"<<std::endl;	
			//ATH_MSG_ERROR("This jet has no associated objects");
		}
		 //track selection
		 for(unsigned i=trackParttmp2.size();i>0; i--){
			 if(!trackParttmp2[i-1]){
				 trackParttmp2.erase(trackParttmp2.begin()+i-1);
				 continue;
			 }
		 const xAOD::TrackParticle* trk2 = static_cast<const xAOD::TrackParticle*>(trackParttmp2[i-1]);
		 bool accept = (trk2->pt()>500 &&
		 m_trk2SelectionTool->accept(*trk2) &&
		 (trk2->vertex()==primvertex || (!trk2->vertex() &&
		 fabs((trk2->z0()+trk2->vz()-primvertex->z())*sin(trk2->theta()))<3.))
		 );
		 if (!accept){
		 trackParttmp2.erase(trackParttmp2.begin()+i-1);
		 }
		 }


		//calculate TrackC1 (and TrackWidth if necessary)
		for(unsigned i=0; i<trackParttmp2.size(); i++){
			double ipt = trackParttmp2.at(i)->pt();
			double ieta = trackParttmp2.at(i)->eta();
			double iphi = trackParttmp2.at(i)->phi();
			sumPt2 += ipt;
			for(unsigned j=i+1; j<trackParttmp2.size(); j++){
				double deta = ieta - trackParttmp2.at(j)->eta();
				double dphi = TVector2::Phi_mpi_pi(iphi - trackParttmp2.at(j)->phi());
				double dR = sqrt( deta*deta + dphi*dphi );
				weightedwidth2 += ipt * trackParttmp2.at(j)->pt() * pow(dR,beta);
			}
		}
		m_j2_trackC10 = sumPt2>0 ? weightedwidth2/(sumPt2*sumPt2) : -99;

//		}
	std::vector<float>  j1_WtrkVector = jet1.auxdata< std::vector<float> >("TrackWidthPt500");
	m_j1_trackWidth = j1_WtrkVector[0]; 
	std::vector<float>  j2_WtrkVector = jet2.auxdata< std::vector<float> >("TrackWidthPt500");
	m_j2_trackWidth = j2_WtrkVector[0]; 
*/

	if (TString(jetAlgo).Contains("AntiKt4EMTopo") || TString(jetAlgo).Contains("AntiKt4EMPFlow") ){
	m_j1_bdt_resp = m_Tagger->getScore(jet1);
	m_j2_bdt_resp = m_Tagger->getScore(jet2);
	m_j1_trackC10 = jet1.auxdata<float>("DFCommonJets_QGTagger_TracksC1");
	m_j2_trackC10 = jet2.auxdata<float>("DFCommonJets_QGTagger_TracksC1");
	m_j1_trackWidth = jet1.auxdata<float>("DFCommonJets_QGTagger_TracksWidth");
	m_j2_trackWidth = jet2.auxdata<float>("DFCommonJets_QGTagger_TracksWidth");
	if(jet3) {
		m_j3_bdt_resp = m_Tagger->getScore(*jet3);
		m_j3_trackC10 = jet3->auxdata<float>("DFCommonJets_QGTagger_TracksC1");
		m_j3_trackWidth = jet3->auxdata<float>("DFCommonJets_QGTagger_TracksWidth");
		std::vector<int>  j3_ntrackVector = jet3->auxdata< std::vector< int> >("NumTrkPt500");
		m_j3_NumTrkPt500 = j3_ntrackVector[0]; 
		}
	}	

	std::vector<int>  j1_ntrackVector = jet1.auxdata< std::vector< int> >("NumTrkPt500");
	m_j1_NumTrkPt500 = j1_ntrackVector[0]; 
	std::vector<int>  j2_ntrackVector = jet2.auxdata< std::vector< int> >("NumTrkPt500");
	m_j2_NumTrkPt500 = j2_ntrackVector[0]; 
//	std::cout<<"vector: "<<m_j1_trackWidth<<std::endl;

//	if(jet4){
//		std::vector<int>  j4_ntrackVector = jet4->auxdata< std::vector< int> >("NumTrkPt500");
//		m_j4_NumTrkPt500 = j4_ntrackVector[0]; 
//	}

	if ((m_j1_trackWidth<0) || (m_j1_NumTrkPt500<0) || (m_j2_trackWidth<0) || (m_j2_NumTrkPt500<0) || (m_j1_pT<0) || (m_j2_pT)<0 )
	return;



	m_j1_E = jet1.e()/GeV;
	m_j1_E = jet1.e()/GeV;
	m_j2_E = jet2.e()/GeV;
	m_j3_E = jet3 ? jet3->e()/GeV : -99;
	m_j3_pT = (jet3 && m_j3_is_truth_jet) ? jet3->pt()/GeV : -99; m_j3_eta = (jet3 && m_j3_is_truth_jet)? jet3->auxdata<float>("DetectorEta") : -99; m_j3_phi = jet3 ? jet3->auxdata<float>("DetectorPhi") : -99;
	m_j4_pT = jet4 ? jet4->pt()/GeV : -99; m_j4_eta = jet4 ? jet4->auxdata<float>("DetectorEta") : -99; m_j4_phi = jet4 ? jet4->auxdata<float>("DetectorPhi") : -99;
	m_pT3OverpTAvg = jet3? jet3->pt()/((jet1.pt()+jet2.pt())/2.0) :-1;
	m_Dphijj = fabs(TVector2::Phi_mpi_pi(m_j1_phi - m_j2_phi));
	m_ref_pT = _ref_pT; m_ref_eta = _ref_eta; m_ref_phi = _ref_phi;
	m_probe_pT = _probe_pT; m_probe_eta = _probe_eta; m_probe_phi = _probe_phi;
	m_left_pT = _left_pT; m_left_eta = _left_eta; m_left_phi = _left_phi;
	m_right_pT = _right_pT; m_right_eta = _right_eta; m_right_phi = _right_phi;

	m_bcid = eventInfo->bcid();
	m_actualIntPerXing = eventInfo->actualInteractionsPerCrossing();
	m_avgIntPerXing = eventInfo->averageInteractionsPerCrossing();
	// get the corrected mu for this event
  if(!m_isMC && m_applyPileupReweighting) { m_correct_mu = m_pileupReweightingTool->getCorrectedAverageInteractionsPerCrossing(*eventInfo, true); }//currently set to true. The scale factor should be set to that used by most analyses
  // if -1 is returned, it indicates that the lcalcFiles are not set correctly
  else { m_correct_mu=0; }

        m_lastUnpairedBunchCrossing = 0;
        m_lastEmptyBunchCrossing = 0;

        //Calculate distance to previous empty BCID and previous unpaired BCID
        if( !m_isMC ){
          //Distance to previous empty BCID
          for (int i = eventInfo->bcid() - 1; i >= 0; i--){
          //get the bunch group pattern for bunch crossing i
            uint16_t bgPattern = m_trigConfTool->bunchGroupSet()->bgPattern()[i];
            bool isLast = (bgPattern >> 3) & 0x1;
            if (isLast){
              m_lastEmptyBunchCrossing = eventInfo->bcid()-i;
              break;
            }
          }//  for each bcid
          //Distance to previous unpaired crossing
          for (int i = eventInfo->bcid() - 1; i >= 0; i--){
            //get the bunch group pattern for bunch crossing i
            uint16_t bgPattern = m_trigConfTool->bunchGroupSet()->bgPattern()[i];
            bool isLast = !((bgPattern >> 1) & 0x1);
            if (isLast){
              m_lastUnpairedBunchCrossing = eventInfo->bcid()-i;
              break;
            }
          }//  for each bcid
        }//  if data


	if(m_applyJetCleaning)
	  {
	    m_j1_passCleaning = m_jetCleaningTool->accept( jet1 );
	    m_j2_passCleaning = m_jetCleaningTool->accept( jet2 );
	    m_j3_passCleaning = jet3 ? m_jetCleaningTool->accept( *jet3 ) : -99;
	  }
	else
	  {
	    m_j1_passCleaning = -99;
	    m_j2_passCleaning = -99;
	    m_j2_passCleaning = -99;
	  }
	m_j1_passJVF = passJVF(jet1);
	m_j1_passJVT = passJVT(jet1, m_jvtCut[TString(jetAlgo)] );
	m_j2_passJVF = passJVF(jet2);
	m_j2_passJVT = passJVT(jet2, m_jvtCut[TString(jetAlgo)] );
	m_j3_passJVF = jet3 ? passJVF(*jet3) : -99;
	m_j3_passJVT = jet3 ? passJVT(*jet3, m_jvtCut[TString(jetAlgo)] ) : -99;
	m_j1_JVT = getJVT(jet1);//jet1.getAttribute< float >( "corrJVT" ) ;
	m_j2_JVT = getJVT(jet1);//jet1.getAttribute< float >( "corrJVT" ) ;

//	if(jetAlgo == "AntiKt4EMTopo")
	m_jetCollectionTrees[jetAlgo]->Fill();

	if(!m_pass_Nominal) return;
	// fill histograms
	  if(!m_runLightWeight){
	  	m_hists1D[jetAlgo+"_jet1_pT"]->Fill(jet1.pt()/GeV,m_weight);
	  	m_hists1D[jetAlgo+"_jet2_pT"]->Fill(jet2.pt()/GeV,m_weight);
	  	m_hists1D[jetAlgo+"_jet3_pT"]->Fill(_j3_pT/GeV,m_weight);
	  	m_hists1D[jetAlgo+"_pTavg"]->Fill(_pTavg/GeV,m_weight);
	  	m_hists1D[jetAlgo+"_eta_right"]->Fill(_right_eta,m_weight);
	  	m_hists1D[jetAlgo+"_pT_right"]->Fill(_right_pT/GeV,m_weight);
	  	m_hists1D[jetAlgo+"_eta_left"]->Fill(_left_eta,m_weight);
	  	m_hists1D[jetAlgo+"_pT_left"]->Fill(_left_pT/GeV,m_weight);
	  	if ( _jetInRefRegion ) {
	  		m_hists1D[jetAlgo+"_eta_ref"]->Fill(_ref_eta,m_weight);
	  		m_hists1D[jetAlgo+"_pT_ref"]->Fill(_ref_pT/GeV,m_weight);
	  		m_hists1D[jetAlgo+"_eta_probe"]->Fill(_probe_eta,m_weight);
	  		m_hists1D[jetAlgo+"_pT_probe"]->Fill(_probe_pT/GeV,m_weight);
	  		m_hists1D[jetAlgo+"_Asm_SM"]->Fill(_Asm_SM,m_weight);
	  		if ( fabs(eta1)<m_refRegion && fabs(eta2)<m_refRegion ) // both jets in reference region
	  			m_hists1D[jetAlgo+"_Asm_SM"]->Fill(-_Asm_SM,m_weight);
	  	}
	  	m_hists1D[jetAlgo+"_Asm_MM"]->Fill(_Asm_MM,m_weight);
	  }
	if ( !m_isMC ) {
		std::vector<TString> trigs = vectorise(m_settings->GetValue("TriggersForOverlap",""));
		for ( uint xtrig = 0; xtrig<trigs.size(); ++xtrig ) {
			for ( uint ytrig = 0; ytrig<trigs.size(); ++ytrig ) {
				if (!m_runLightWeight &&  m_trigDecTool->isPassed( std::string(trigs[xtrig].Data()) ) && m_trigDecTool->isPassed( std::string(trigs[ytrig].Data()) ) )
					m_hists2D[jetAlgo+"_Triggers_PassSel"]->Fill(xtrig,ytrig,m_weight);
			}
		}
	}

	// fill histograms in pTavg bins
	std::vector<double> pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));

	uint pTbin=0;
	for (uint i=0;i<pTavg_bins.size();++i)
		if (_pTavg/GeV > pTavg_bins[i]) pTbin=i+1;

	// skip event if pTavg outside bin range
	if (pTbin==0 || pTbin>(pTavg_bins.size()-1)) return;

	if ( m_isMC ){
		if(! m_forceMCTrigs){
			FillHistograms( Form("%s_J%d_",jetAlgo.c_str(),m_JX), dijet, pTbin );
		}else{
			int itrig=0;
			for ( const auto& trig : m_trigORs ) {

				TString trig_or = trig;
				trig_or.ReplaceAll("_OR_"," ");
//				auto vec = vectorise(trig_or);
//				for (auto& v: vec)
//					std::cout << v << ' ';  //v: HLT_j15 HLT_j15_360e450 etc
//				std::cout << std::endl;
				TString ctrl_trig = vectorise(trig_or).at(0);
				TString fwd_trig = vectorise(trig_or).at(1);

				bool passTrig = m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) || m_trigDecTool->isPassed( std::string(fwd_trig.Data()) );

				if ( !passTrig )
					continue;

				// pass pTavg threshold requirement
				if ( _pTavg/GeV < m_trigThresholds[itrig] )
					continue;

				// RefFJ_FJ or RefFJ_J
				std::string jetClass = TriggerClassification(trig);
				if ( jetClass=="" )
					continue;

				//if RefFJ_FJ only fill if there is at least one jet in 3.5 to 4.5
				if(m_enforceRefFJ_FJEta){
					if(TString(jetClass).Contains("RefFJ_FJ")){
						if( CheckAtLeastOneJetInEtaRange(3.5,4.5,dijet ))
							FillHistograms( Form("%s_J%d_",jetAlgo.c_str(),m_JX), dijet, pTbin );

					}else
						FillHistograms( Form("%s_J%d_",jetAlgo.c_str(),m_JX), dijet, pTbin );
				}else
					FillHistograms( Form("%s_J%d_",jetAlgo.c_str(),m_JX), dijet, pTbin );


				++itrig;
			}

		}
	} else {
		int itrig=0;
		// loop over trigger OR combinations
		for ( const auto& trig : m_trigORs ) {
			TString trig_or = trig;
			trig_or.ReplaceAll("_OR_"," ");
			TString ctrl_trig = vectorise(trig_or).at(0);
			TString fwd_trig = vectorise(trig_or).at(1);

			bool passTrig = m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) || m_trigDecTool->isPassed( std::string(fwd_trig.Data()) );

			if ( !passTrig )
				continue;

			// pass pTavg threshold requirement
			if ( _pTavg/GeV < m_trigThresholds[itrig] )
				continue;

			// RefFJ_FJ or RefFJ_J
			std::string jetClass = TriggerClassification(trig);
			if ( jetClass=="" )
				continue;


			//if RefFJ_FJ only fill if there is at least one jet in 3.5 to 4.5
			if(m_enforceRefFJ_FJEta){
				if(TString(jetClass).Contains("RefFJ_FJ")){
					if( CheckAtLeastOneJetInEtaRange(3.5,4.5,dijet ))
						FillHistograms( Form("%s_%s_%s_",jetAlgo.c_str(),trig.Data(),jetClass.c_str()), dijet, pTbin );

				}else
					FillHistograms( Form("%s_%s_%s_",jetAlgo.c_str(),trig.Data(),jetClass.c_str()), dijet, pTbin );
			}else
				FillHistograms( Form("%s_%s_%s_",jetAlgo.c_str(),trig.Data(),jetClass.c_str()), dijet, pTbin );



			++itrig;
		}
	}
}
bool DijetInSitu::EtaInterCalxAODAnalysis::DoesTriggerMatch(xAOD::TEvent& event, xAOD::TStore &store,TString centralTriggerName, int itrig){
	//get offlineline jets
	xAOD::JetContainer offlineJets = xAOD::JetContainer(SG::VIEW_ELEMENTS);
	bool badEvent = false;
	SelectOfflineJets(event,store,offlineJets,badEvent);
	if(badEvent) return false;
	xAOD::Jet selectedJets[2];
	const xAOD::Jet& jet1 = *(offlineJets.at(0));
	const xAOD::Jet& jet2 = *(offlineJets.at(1));

	selectedJets[0] = jet1;
	selectedJets[1] = jet2;

	//get online jets
	TString trig_name = centralTriggerName;
	trig_name.ReplaceAll("HLT_j","");
	float ET_cut = std::atoi( trig_name.Data() );
	xAOD::JetContainer triggerJets = xAOD::JetContainer(SG::VIEW_ELEMENTS);
	SelectOnlineJets(event,store,triggerJets,ET_cut, false);

	std::vector<int> noMatchedForward(2);
	std::vector<int> noMatchedCentral(2);

	//based on trigger classification attempt to match jets
	for(unsigned int i =0; i<2;i++){
		for(const auto& triggerJet : triggerJets){
			//for selected jet find DR between the trigger jets
			double dR = deltaR(selectedJets[0], *triggerJet);
			bool 	 isTriggerJetForward = (fabs(triggerJet->eta()) >= 3.2);

			if(dR < m_DRcut){
				if(isTriggerJetForward)
					noMatchedForward[i]++;
				else
					noMatchedCentral[i]++;
			}
			//	m_hists1D[Form( (isTriggerJetForward ? "forward_%d" : "central_%d") , (int)ET_cut)]->Fill(deltaR);
			//	m_triggerMatchingHists[Form( (isTriggerJetForward ? "forward_%d" : "central_%d") , (int)ET_cut)]->Fill(deltaR);
		}
	}


	bool passEmulCtrl= m_trigBitsCtrl & int(pow(2,itrig));
	bool passEmulFwd = m_trigBitsFwd  & int(pow(2,itrig));


	//now return true/false based on
	if(passEmulCtrl && ! passEmulFwd)
		if(noMatchedCentral[0] > 0 || noMatchedCentral[1] > 0)
			return true;

	if(!passEmulCtrl && passEmulFwd)
		if(noMatchedForward[0] > 0 || noMatchedForward[1] > 0)
			return true;

	//if one central and one foward jet -
	if(passEmulCtrl && passEmulFwd)
		if( (noMatchedForward[0] > 0 || noMatchedCentral[1] > 0) ||
				(noMatchedCentral[0] > 0 || noMatchedForward[1] > 0) )
			return true;

	return false;
}

// convert string of trigger OR combination and convert to FJ/J classification
std::string DijetInSitu::EtaInterCalxAODAnalysis::TriggerClassification(TString trig) {

	bool passEmulFwd=false;
	bool passEmulCtrl=false;
	m_atLeasteOneForwardJet = ( fabs(m_j1_eta) >= 3.2 || fabs(m_j2_eta) >= 3.2);


	for ( uint itrig=0; itrig<m_trigORs.size(); ++itrig ) {
		if ( m_trigORs[itrig]!=trig ) continue;

		passEmulCtrl = m_trigBitsCtrl & int(pow(2,itrig));
		passEmulFwd  = m_trigBitsFwd  & int(pow(2,itrig));
	}

	trig.ReplaceAll("_OR_"," ");
	TString ctrl_trig = vectorise(trig).at(0);
	TString fwd_trig = vectorise(trig).at(1);

	if ( m_ctrlTrigsOnly ) {
		if ( m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) ) return "RefFJ_J";
		else return "";
	}

	if ( m_fwdTrigsOnly ) {
		if ( m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) ) return "RefFJ_FJ";
		else return "";
	}

	if(m_doTriggerEmulation){
		if ( m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) && !m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) )) return "RefFJ_FJ";
		if ( m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) && ! m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) ) return "RefFJ_J";

		if ( m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) && m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) )
			if(passEmulFwd) return "RefFJ_FJ";
		return "";

	}else{
		if ( m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) && m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) )
			return "";

		if ( m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) && !m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) )) return "RefFJ_FJ";
		if ( m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) && ! m_trigDecTool->isPassed( std::string(fwd_trig.Data()) ) ) return "RefFJ_J";
	}

  return "";
}

bool	DijetInSitu::EtaInterCalxAODAnalysis::CheckAtLeastOneJetInEtaRange(float etaMin,float etaMax, DijetBalance dijet){


	if( fabs(dijet.left_eta()) > etaMin && fabs(dijet.left_eta()) < etaMax)
		return true;

	if( fabs(dijet.right_eta()) > etaMin && fabs(dijet.right_eta()) < etaMax)
		return true;

	return false;
}

void DijetInSitu::EtaInterCalxAODAnalysis::FillHistograms(std::string prefix, DijetBalance dijet, int pTbin) {
  if(m_runLightWeight)return;
	// fill 2D histograms
	m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(dijet.left_eta(),dijet.pTavg()/GeV,m_weight);
	m_hists2D[prefix+"PtAvg_vs_EtaDet"]->Fill(dijet.right_eta(),dijet.pTavg()/GeV,m_weight);

	if ( dijet.jetInRefRegion() ) {
		m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(dijet.probe_eta(),dijet.pTavg()/GeV,m_weight);
		if ( fabs(dijet.probe_eta())<m_refRegion ) m_hists2D[prefix+"PtAvgFine_vs_EtaProbeSM"]->Fill(dijet.ref_eta(),dijet.pTavg()/GeV,m_weight);
	}

	m_hists2D[prefix+"PtAvgFine_vs_EtaProbeMM"]->Fill(dijet.right_eta(),dijet.pTavg()/GeV,m_weight);
	m_hists2D[prefix+"PtAvgFine_vs_EtaProbeMM"]->Fill(dijet.left_eta(),dijet.pTavg()/GeV,m_weight);

	// fill 3D histograms

	// standard method
	if ( dijet.jetInRefRegion() ) {
		m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(dijet.pTavg()/GeV,dijet.probe_eta(),dijet.Asym_SM(),m_weight);
		if( fabs(dijet.ref_eta())<m_refRegion && fabs(dijet.probe_eta())<m_refRegion ) // both jets in reference region
			m_hists3D[prefix+"pTavg_etaprobe_Astandard"]->Fill(dijet.pTavg()/GeV,dijet.ref_eta(),-1*dijet.Asym_SM(),m_weight);
	}

	// matrix method
	m_hists3D[Form("%sAsym3D_PtBin%d",prefix.c_str(),pTbin)]->Fill(dijet.left_eta(),dijet.right_eta(),dijet.Asym_MM(),m_weight);

	return;
}

void DijetInSitu::EtaInterCalxAODAnalysis::WriteOutput() {

	m_fout->cd();
	//m_test2DHist->Write();
	//m_testTProfile->Write();
	for ( const auto& hist : m_cutFlow )
		hist.second->Write();

	//  m_cutFlow->Write();
	//  m_triggerCounts->Write();

	WriteHistograms();
	if ( !m_tagAndProbe )
		WriteMiniTree();

	m_fout->Close();

}

void DijetInSitu::EtaInterCalxAODAnalysis::WriteMiniTree() {

	m_fout->cd();
	for(const auto& jetCollectionTrees : m_jetCollectionTrees){
		jetCollectionTrees.second->Write();
	}
	//  m_truth_tree->Write();
	//  m_trig_mini_tree->Write();
}

void DijetInSitu::EtaInterCalxAODAnalysis::WriteHistograms() {

	//  TFile* fout = TFile::Open("EtaInterCal_3DHistos.root","recreate");
	//  m_cutFlow->Write();
	m_fout->cd();

	std::cout << "Writing histograms..." << std::endl;

	for ( const auto& hist : m_hists1D ) {
		if ( TString(hist.first).Contains("Control") ) continue;
		hist.second->Write();
	}

	if ( m_tagAndProbe )
		return;

	if ( m_controlPlots ) {
		m_fout->mkdir("ControlPlots");
		m_fout->cd("ControlPlots");

		for ( const auto& hist : m_hists1D ) {
			if ( TString(hist.first).Contains("Control") )
				hist.second->Write();
		}

		m_fout->cd("..");
	}

	//  m_fout->mkdir("TriggerJets");
	//  m_fout->cd("TriggerJets");
	//  for ( const auto&hist : m_trigHists )
	//    hist.second->Write();
	//  m_fout->cd("../");

	for (const auto &jetAlgo : m_jetCollections) {

		m_fout->mkdir(jetAlgo);
		m_fout->cd(jetAlgo);

		for ( const auto& hist : m_hists2D ) {
			if ( TString(hist.first).Contains(jetAlgo) ) hist.second->Write();
		}

		for ( const auto& hist : m_hists3D ) {
			if ( TString(hist.first).Contains(jetAlgo) ) hist.second->Write();
		}

	}

	if ( m_isMC ) {
		for (const auto &jetAlgo : m_truthJetCollections) {

			m_fout->mkdir(jetAlgo);
			m_fout->cd(jetAlgo);

			for ( const auto& hist : m_hists2D ) {
				if ( TString(hist.first).Contains(jetAlgo) ) hist.second->Write();
			}

			for ( const auto& hist : m_hists3D ) {
				if ( TString(hist.first).Contains(jetAlgo) ) hist.second->Write();
			}

		}
	}


	//  m_fout->mkdir("AntiKt4EMTopo");
	//  m_fout->cd("AntiKt4EMTopo");

	//  for ( const auto& hist : m_hists2D )
	//    hist.second->Write();

	//  for ( const auto& hist : m_hists3D )
	//    hist.second->Write();


	//  fout->Close();
	//  delete fout;
}

// Finalise
void DijetInSitu::EtaInterCalxAODAnalysis::Finalise() {
//	m_testTProfile=m_test2DHist->ProfileX();
	WriteOutput();
	//  WriteHistograms();

	// delete tools
	if( m_grlTool ) {
		delete m_grlTool;
		m_grlTool = 0;
	}
	for ( auto calibTool : m_jetCalibTools ) {
		if( calibTool.second ) {
			delete calibTool.second;
			calibTool.second = 0;
		}
	}
	if( m_jetCleaningTool ) {
		delete m_jetCleaningTool;
		m_jetCleaningTool = 0;
	}
//	if ( m_pileupReweightingTool ) {
//		delete m_pileupReweightingTool;
//		m_pileupReweightingTool = 0;
//	}

	/*
  if (m_eventShapeCopier) {
    delete m_eventShapeCopier;
    m_eventShapeCopier = 0;
  }
	 */
}

// Initialise the tools
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseTools() {

	// Trigger configuration tool
	m_trigConfTool = new TrigConf::xAODConfigTool("xAODConfigTool");
	ToolHandle<TrigConf::ITrigConfigTool> configHandle(m_trigConfTool);
	CHECK_STATUS("InitialiseTools()",configHandle->initialize());

	// Trigger decision tool
	m_trigDecTool = new Trig::TrigDecisionTool("TrigDecTool");
	CHECK_STATUS("InitialiseTools()",m_trigDecTool->setProperty("ConfigTool",configHandle));
	//m_trigDecTool->setProperty("OutputLevel", MsetSG::VERBOSE);
 	CHECK_STATUS("InitialiseTools()",m_trigDecTool->setProperty("TrigDecisionKey","xTrigDecision"));
	CHECK_STATUS("InitialiseTools()",m_trigDecTool->initialize());

	// Pileup reweighting tool
	if(m_applyPileupReweighting)
	{
		//m_pileupReweightingTool = asg::AnaToolHandle<CP::IPileupReweightingTool>("CP::PileupReweightingTool/tool");
		m_pileupReweightingTool = new CP::PileupReweightingTool("PileupReweightingTool");
		std::vector<std::string> confFiles;
		std::vector<std::string> lcalcFiles;

		for ( const auto& f : vectorise(m_settings->GetValue("PileupReweightingTool.confFiles","")) )
			confFiles.push_back(std::string(f.Data()));
		for ( const auto& f : vectorise(m_settings->GetValue("PileupReweightingTool.lumiCalcFiles","")) )
			lcalcFiles.push_back(std::string(f.Data()));

		CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool->setProperty( "ConfigFilesPathPrefix", "/afs/cern.ch/work/w/wasu/QvsG-Gselection/EtaInterCalxAODAnalysis/PileupReweighting/") );
		CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool->setProperty( "ConfigFiles", confFiles) );
		CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool->setProperty( "LumiCalcFiles", lcalcFiles) );
		CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool->setProperty( "DataScaleFactor", 1.0) );//Default value, correction is applied
		//CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool.retrieve());
		CHECK_STATUS("InitialiseTools()",m_pileupReweightingTool->initialize());
	}

	InitialiseGRL();
	InitialiseJetTools();
}

// Initialise the GRL tool
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseGRL() {
	m_grlTool = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  std::vector<std::string> vecStringGRL;
  for ( const auto& f : vectorise(m_settings->GetValue("GRL.xml","")) )
    vecStringGRL.push_back( m_MainDirectory+"/EtaInterCalxAODAnalysis/GRLs/"+std::string(f.Data()) );
    //vecStringGRL.push_back(std::string(f.Data()) );
	CHECK_STATUS("InitialiseGRL()", m_grlTool->setProperty( "GoodRunsListVec", vecStringGRL));
	CHECK_STATUS("InitialiseGRL()", m_grlTool->setProperty("PassThrough", false));
	CHECK_STATUS("InitialiseGRL()",m_grlTool->initialize());
}

// Initialise the jet tools (cleaning, calibration)
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseJetTools() {

	// initialise jet calibration tools
	for (const auto &jetAlgo : m_jetCollections) {
		if ( jetAlgo.Contains("HLT") ) { continue; }

        const std::string name = std::string("CalibTools_")+std::string(jetAlgo.Data());
		TString config = m_settings->GetValue("JetCalibTool."+jetAlgo+".config","");
		TString calibSeq = m_settings->GetValue("JetCalibTool."+jetAlgo+".calibSeq","");
    if(m_isR21) calibSeq.ReplaceAll("Origin",""); // origin correction already applied for samples R21
		m_isData = m_isMC ? false : true;
		m_jetCalibTools[jetAlgo] = new JetCalibrationTool(name);

    CHECK_STATUS("InitialiseJetTools", 	m_jetCalibTools[jetAlgo] ->setProperty("JetCollection",TString(jetAlgo).Data() ) );
    CHECK_STATUS("InitialiseJetTools", 	m_jetCalibTools[jetAlgo] ->setProperty("ConfigFile",TString(config).Data()  ) );
    CHECK_STATUS("InitialiseJetTools", 	m_jetCalibTools[jetAlgo] ->setProperty("CalibSequence",TString(calibSeq).Data()  ) );
    CHECK_STATUS("InitialiseJetTools", 	m_jetCalibTools[jetAlgo] ->setProperty("IsData",m_calibDataAsMC ? false : m_isData ) );
    // if using cmake, need to set calibArea property
    if(getenv("EtaInterCal_cmake")!=NULL && std::string(getenv("EtaInterCal_cmake"))=="yes")
    {
      TString calibArea = m_settings->GetValue("JetCalibTool."+jetAlgo+".calibArea","");
      CHECK_STATUS("InitialiseJetTools", m_jetCalibTools[jetAlgo]->setProperty("CalibArea",calibArea.Data() ) );
    }
		CHECK_STATUS("InitialiseJetTools()",m_jetCalibTools[jetAlgo]->initializeTool(name));
	}

	// initialise jet cleaning tool
	TString cutLevel = m_settings->GetValue("JetCleaningTool.cutLevel","");
	bool doUgly = bool(m_settings->GetValue("JetCleaningTool.doUgly",0));
	m_jetCleaningTool = new JetCleaningTool("JetCleaning");
	CHECK_STATUS( "InitialiseJetTools()", m_jetCleaningTool->setProperty( "CutLevel", std::string(cutLevel.Data()) ) );
	CHECK_STATUS( "InitialiseJetTools()", m_jetCleaningTool->setProperty( "DoUgly", doUgly ) );
	CHECK_STATUS( "InitialiseJetTools()", m_jetCleaningTool->initialize() );
	// initialise the jet vertex tagger tool
	m_jvtTool = new JetVertexTaggerTool("JVTTool");
	CHECK_STATUS( "InitialiseJetTools()", m_jvtTool->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root") );
	CHECK_STATUS( "InitialiseJetTools()", m_jvtTool->initialize() );
  CHECK_STATUS( "InitialiseJetTools()", m_updateJvt.setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root") );
  CHECK_STATUS( "InitialiseJetTools()", m_updateJvt.retrieve() );
}

bool DijetInSitu::EtaInterCalxAODAnalysis::passJVF(const xAOD::Jet& j) {
	if ( j.pt()/GeV < 50 && fabs(j.getAttribute< float >( "DetectorEta" )) < 2.4 && m_applyJVF) {
		if ( j.getAttribute< float >( "JVF_PV" ) > m_jvfCut )
			return true;
		else return false;
	}
	else
		return true;
}

bool DijetInSitu::EtaInterCalxAODAnalysis::passJVT(const xAOD::Jet& j, float cut) {
	if ( j.pt()/GeV < 50 && fabs(j.getAttribute< float >( "DetectorEta" )) < 2.4 && m_applyJVT) {
		if ( j.getAttribute< float >( "corrJVT" ) > cut )
			return true;
		else return false;
	}
	else
		return true;
}

float DijetInSitu::EtaInterCalxAODAnalysis::getJVT(const xAOD::Jet& j) {
	// std::cout << "Getting JVT for jet "<<std::endl
	// 		  << "\teta:" << j.getAttribute< float >( "DetectorEta" ) << std::endl
	// 		  <<  ( j.pt()/GeV < 50 && fabs(j.getAttribute< float >( "DetectorEta" )) < 2.4 ) ? j.getAttribute< float >( "corrJVT" ) : - 1<< std::endl;
	if ( j.pt()/GeV <60 && fabs(j.getAttribute< float >( "DetectorEta" )) < 2.4 && m_applyJVT)
	   return j.getAttribute< float >( "corrJVT" );
  return -1;
}


const xAOD::Vertex* DijetInSitu::EtaInterCalxAODAnalysis::GetPV(const xAOD::VertexContainer* pVertices) {
	const xAOD::Vertex* PV = 0;
	int ivtx = 0;
	for ( xAOD::VertexContainer::const_iterator v = pVertices->begin(); v < pVertices->end(); ++v ) {
		//  for ( const auto& v : pVertices ) {
		//    if ( (*v)->vertexType() == xAOD::VxType::VertexType::PriVtx ) {
		PV = (*v);
		PV->auxdecor<int>("index") = ivtx;
		break;
		//    }
		ivtx++;
	}
	return PV;
}

// Initialise the cutflow
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseCutflow(std::string prefix) {

	m_cutFlow[prefix] = new TH1F(Form("%s_CutFlow",prefix.c_str()),Form("Cut flow for %s jets",prefix.c_str()),13,0,13);
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(1,"All events");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(2,"GRL");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(3,"LAr error");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(4,"PV");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(5,"Any Trigger");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(6,"2 jets");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(7,m_applyJVF ? "JVF" : "JVT");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(8,"Jet cleaning");
	//  m_cutFlow[prefix]->GetXaxis()->SetBinLabel(8,"JVF");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(9,"Dphi(jj)");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(10,"pT jet3");
	m_cutFlow[prefix]->GetXaxis()->SetBinLabel(11,"MC cleaning");

}

//add function here !
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseMiniTree(){
}


void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseJetCollectionTrees(std::string jetCollection){
//	if (jetCollection.compare("AntiKt4EMTopo") == 0){
	std::string treeName = jetCollection + std::string("_dijet_insitu");
	m_jetCollectionTrees[jetCollection] = new TTree(treeName.c_str(),treeName.c_str());
	m_jetCollectionTrees[jetCollection]->SetDirectory(m_fout);
	m_jetCollectionTrees[jetCollection]->SetAutoFlush(100000);

	// bcid and pileup information
//	m_jetCollectionTrees[jetCollection]->Branch("bcid", &m_bcid);
//	m_jetCollectionTrees[jetCollection]->Branch("DistEmptyBCID", &m_lastEmptyBunchCrossing);
//	m_jetCollectionTrees[jetCollection]->Branch("DistUnpairedBCID", &m_lastUnpairedBunchCrossing);
//	m_jetCollectionTrees[jetCollection]->Branch("actualIntPerXing",&m_actualIntPerXing);
//	m_jetCollectionTrees[jetCollection]->Branch("avgIntPerXing",&m_avgIntPerXing);
//	m_jetCollectionTrees[jetCollection]->Branch("correct_mu",&m_correct_mu);
//	m_jetCollectionTrees[jetCollection]->Branch("NPV",&m_NPV);
//	m_jetCollectionTrees[jetCollection]->Branch("rhoEM",&m_rhoEM);
//	m_jetCollectionTrees[jetCollection]->Branch("rhoLC",&m_rhoLC);
//	m_jetCollectionTrees[jetCollection]->Branch("pT3OverpTAvg",&m_pT3OverpTAvg);

	// weight and global flags
	m_jetCollectionTrees[jetCollection]->Branch("weight",&m_weight);
	m_jetCollectionTrees[jetCollection]->Branch("weight_pileup",&m_weight_pileup);
	m_jetCollectionTrees[jetCollection]->Branch("weight_pileup_up",&m_weight_pileup_up);
	m_jetCollectionTrees[jetCollection]->Branch("weight_pileup_down",&m_weight_pileup_down); 
	m_jetCollectionTrees[jetCollection]->Branch("weight_ptslice",&m_weight_ptslice); 
	m_jetCollectionTrees[jetCollection]->Branch("pdfWeights",&pdfWeights,"pdfWeights[101]/F"); 

	m_jetCollectionTrees[jetCollection]->Branch("m_mcEventWeightsString",&m_mcEventWeightsString);
	m_jetCollectionTrees[jetCollection]->Branch("m_mcEventWeightsVector",&m_mcEventWeightsVector);

//	m_jetCollectionTrees[jetCollection]->Branch("JX",&m_JX);
	m_jetCollectionTrees[jetCollection]->Branch("isMC",&m_isMC);
	m_jetCollectionTrees[jetCollection]->Branch("runNumber",&m_runNumber);
    m_jetCollectionTrees[jetCollection]->Branch("eventNumber",&m_eventNumber);
    m_jetCollectionTrees[jetCollection]->Branch("mcChannelNumber",&m_mcChannelNumber);
//	m_jetCollectionTrees[jetCollection]->Branch("passTrigger",&m_passTrigger);
//	m_jetCollectionTrees[jetCollection]->Branch("trigClass",&m_trigClass);
//	m_jetCollectionTrees[jetCollection]->Branch("trigHLT",&m_trigHLT);

	// trigger information
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j15",&m_pass_HLT_j15);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j15_320eta490",&m_pass_HLT_j15_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j25",&m_pass_HLT_j25);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j25_320eta490",&m_pass_HLT_j25_320eta490);
//  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j35",&m_pass_HLT_j35);
////  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j35_320eta490",&m_pass_HLT_j35_320eta490);
//  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j45",&m_pass_HLT_j45);
////  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j45_320eta490",&m_pass_HLT_j45_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j60",&m_pass_HLT_j60);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j60_320eta490",&m_pass_HLT_j60_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j110",&m_pass_HLT_j110);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j110_320eta490",&m_pass_HLT_j110_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j175",&m_pass_HLT_j175);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j175_320eta490",&m_pass_HLT_j175_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j260",&m_pass_HLT_j260);
////	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j260_320eta490",&m_pass_HLT_j260_320eta490);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j360",&m_pass_HLT_j360);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j360_320eta490",&m_pass_HLT_j360_320eta490);
  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j400",&m_pass_HLT_j400);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j400_320eta490",&m_pass_HLT_j400_320eta490);
//  m_jetCollectionTrees[jetCollection]->Branch("passRaw_L1_J15",&m_passRaw_L1_J15);
//  m_jetCollectionTrees[jetCollection]->Branch("passRaw_L1_J15_31ETA49",&m_passRaw_L1_J15_31ETA49);
//  m_jetCollectionTrees[jetCollection]->Branch("pass_HLT_j0_perf_L1RD0_FILLED",&m_pass_HLT_j0_perf_L1RD0_FILLED);
//	m_jetCollectionTrees[jetCollection]->Branch("trigBitsCtrl",&m_trigBitsCtrl);
//	m_jetCollectionTrees[jetCollection]->Branch("trigBitsFwd",&m_trigBitsFwd);

	// truth jets
//	m_jetCollectionTrees[jetCollection]->Branch("pTavg_truth",&m_pTavg_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_pT_truth",&m_j1_pT_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_pT_truth",&m_j2_pT_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_pT_truth",&m_j3_pT_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_pT_truth",&m_j4_pT_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_eta_truth",&m_j1_eta_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_eta_truth",&m_j2_eta_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_eta_truth",&m_j3_eta_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_eta_truth",&m_j4_eta_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_phi_truth",&m_j1_phi_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_phi_truth",&m_j2_phi_truth);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_phi_truth",&m_j3_phi_truth);

	// pTavg, SM and MM asymmetries
//	m_jetCollectionTrees[jetCollection]->Branch("Dphijj",&m_Dphijj);
//	m_jetCollectionTrees[jetCollection]->Branch("pTavg",&m_pTavg);
//	m_jetCollectionTrees[jetCollection]->Branch("Asym_MM",&m_Asym_MM);
//	m_jetCollectionTrees[jetCollection]->Branch("Asym_SM",&m_Asym_SM);

	// leading, subleading, subsubleading jet
//	m_jetCollectionTrees[jetCollection]->Branch("j1_is_truth_jet",&m_j1_is_truth_jet); 
//	m_jetCollectionTrees[jetCollection]->Branch("j2_is_truth_jet",&m_j2_is_truth_jet); 
//	m_jetCollectionTrees[jetCollection]->Branch("j3_is_truth_jet",&m_j3_is_truth_jet); 
//	m_jetCollectionTrees[jetCollection]->Branch("j4_is_truth_jet",&m_j4_is_truth_jet); 
//	m_jetCollectionTrees[jetCollection]->Branch("j1_match_pT_truth",&m_j1_match_pT_truth); 
//	m_jetCollectionTrees[jetCollection]->Branch("j2_match_pT_truth",&m_j2_match_pT_truth); 
//	m_jetCollectionTrees[jetCollection]->Branch("j3_match_pT_truth",&m_j3_match_pT_truth); 
//	m_jetCollectionTrees[jetCollection]->Branch("j4_match_pT_truth",&m_j4_match_pT_truth); 
	m_jetCollectionTrees[jetCollection]->Branch("j1_NumTrkPt500",&m_j1_NumTrkPt500); 
	m_jetCollectionTrees[jetCollection]->Branch("j2_NumTrkPt500",&m_j2_NumTrkPt500); 
	m_jetCollectionTrees[jetCollection]->Branch("j3_NumTrkPt500",&m_j3_NumTrkPt500); 
//	m_jetCollectionTrees[jetCollection]->Branch("j4_NumTrkPt500",&m_j4_NumTrkPt500); 
//	m_jetCollectionTrees[jetCollection]->Branch("j1_NTracks",&m_j1_NTracks); 
//	m_jetCollectionTrees[jetCollection]->Branch("j2_NTracks",&m_j2_NTracks); 
//	m_jetCollectionTrees[jetCollection]->Branch("j3_NTracks",&m_j3_NTracks); 
 
	m_jetCollectionTrees[jetCollection]->Branch("j1_trackWidth",&m_j1_trackWidth);  
	m_jetCollectionTrees[jetCollection]->Branch("j2_trackWidth",&m_j2_trackWidth); 
	m_jetCollectionTrees[jetCollection]->Branch("j3_trackWidth",&m_j3_trackWidth); 
	m_jetCollectionTrees[jetCollection]->Branch("j1_trackC1",&m_j1_trackC10); 
	m_jetCollectionTrees[jetCollection]->Branch("j2_trackC1",&m_j2_trackC10); 
	m_jetCollectionTrees[jetCollection]->Branch("j3_trackC1",&m_j3_trackC10); 
	m_jetCollectionTrees[jetCollection]->Branch("j1_bdt_resp",&m_j1_bdt_resp); //for BDT 
	m_jetCollectionTrees[jetCollection]->Branch("j2_bdt_resp",&m_j2_bdt_resp); 
	m_jetCollectionTrees[jetCollection]->Branch("j3_bdt_resp",&m_j3_bdt_resp); 
	m_jetCollectionTrees[jetCollection]->Branch("j1_partonLabel",&m_j1_partonLabel); //new branch for parton label
	m_jetCollectionTrees[jetCollection]->Branch("j2_partonLabel",&m_j2_partonLabel);
	m_jetCollectionTrees[jetCollection]->Branch("j3_partonLabel",&m_j3_partonLabel);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_partonLabel",&m_j4_partonLabel);
	
	m_jetCollectionTrees[jetCollection]->Branch("j1_pT",&m_j1_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_E",&m_j1_E);
	m_jetCollectionTrees[jetCollection]->Branch("j1_eta",&m_j1_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_phi",&m_j1_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_passCleaning",&m_j1_passCleaning);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_passJVT",&m_j1_passJVT);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_passJVF",&m_j1_passJVF);
//	m_jetCollectionTrees[jetCollection]->Branch("j1_JVT",&m_j1_JVT);
	m_jetCollectionTrees[jetCollection]->Branch("j2_pT",&m_j2_pT);
	m_jetCollectionTrees[jetCollection]->Branch("j2_eta",&m_j2_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_E",&m_j2_E);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_phi",&m_j2_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_passCleaning",&m_j2_passCleaning);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_passJVT",&m_j2_passJVT);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_passJVF",&m_j2_passJVF);
//	m_jetCollectionTrees[jetCollection]->Branch("j2_JVT",&m_j2_JVT);
	m_jetCollectionTrees[jetCollection]->Branch("j3_pT",&m_j3_pT);
	m_jetCollectionTrees[jetCollection]->Branch("j3_eta",&m_j3_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_phi",&m_j3_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_E",&m_j3_E);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_pT",&m_j4_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_eta",&m_j4_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("j4_phi",&m_j4_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_passCleaning",&m_j3_passCleaning);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_passJVT",&m_j3_passJVT);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_passJVF",&m_j3_passJVF);
//	m_jetCollectionTrees[jetCollection]->Branch("j3_JVT",&m_j3_JVT);
//
//	// SM ref/probe jets
//	m_jetCollectionTrees[jetCollection]->Branch("ref_pT",&m_ref_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("ref_eta",&m_ref_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("ref_phi",&m_ref_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("probe_pT",&m_probe_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("probe_eta",&m_probe_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("probe_phi",&m_probe_phi);
//
//	// MM left/right jets
//	m_jetCollectionTrees[jetCollection]->Branch("left_pT",&m_left_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("left_eta",&m_left_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("left_phi",&m_left_phi);
//	m_jetCollectionTrees[jetCollection]->Branch("right_pT",&m_right_pT);
//	m_jetCollectionTrees[jetCollection]->Branch("right_eta",&m_right_eta);
//	m_jetCollectionTrees[jetCollection]->Branch("right_phi",&m_right_phi);
//
//	m_jetCollectionTrees[jetCollection]->Branch("pass_Nominal",&m_pass_Nominal);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_DphiUp",&m_pass_DphiUp);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_DphiDown",&m_pass_DphiDown);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_J3pTCutDown",&m_pass_J3pTCutDown);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_J3pTCutUp",&m_pass_J3pTCutUp);
//	m_jetCollectionTrees[jetCollection]->Branch("pass_JVTThight",&m_pass_JVTThight);

//	}
}

// Initialise jet trigger histograms
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseTrigHists(std::string prefix) {
	prefix = prefix+"_";
	m_trigHists.insert( std::pair<std::string,TH1F*>(prefix+"ET",  createHist1D(prefix+"ET",";E_{T} [GeV];",1000,0,1000)) );
	m_trigHists.insert( std::pair<std::string,TH1F*>(prefix+"pT",  createHist1D(prefix+"pT",";p_{T} [GeV];",1000,0,1000)) );
	m_trigHists.insert( std::pair<std::string,TH1F*>(prefix+"eta", createHist1D(prefix+"eta",";#eta;",100,-5,5)) );
	m_trigHists.insert( std::pair<std::string,TH1F*>(prefix+"phi", createHist1D(prefix+"phi",";#phi;",80,-4,4)) );
	m_trigHists.insert( std::pair<std::string,TH1F*>(prefix+"E",   createHist1D(prefix+"E",";E [GeV];",1000,0,1000)) );
}

void DijetInSitu::EtaInterCalxAODAnalysis::FillControlHistograms(std::string plot, std::string jetAlgo,float pTavg, float Dphijj, const xAOD::Jet& jet1,const xAOD::Jet& jet2, xAOD::Jet* jet3){
	std::string suffix="";
	if ( m_isMC ) {
		if ( pTavg/1000. < 25 )
			return;

		suffix = Form("J%d",m_JX);
	}
	else {
		std::vector<double> pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));
		uint pTbin=0;
		for (uint i=0;i<pTavg_bins.size();++i)
			if (pTavg/GeV > pTavg_bins[i]) pTbin=i+1;

		// skip event if pTavg outside bin range
		if (pTbin==0 || pTbin>(pTavg_bins.size()-1))
			return;

		for ( uint itrig=0; itrig < m_trigORs.size(); ++itrig ) {

			TString trig_or = m_trigORs[itrig];
			trig_or.ReplaceAll("_OR_"," ");
			TString ctrl_trig = vectorise(trig_or).at(0);
			TString fwd_trig = vectorise(trig_or).at(1);

			bool passTrig = m_trigDecTool->isPassed( std::string(ctrl_trig.Data()) ) || m_trigDecTool->isPassed( std::string(fwd_trig.Data()) );

			if ( !passTrig )
				continue;
			/*
      std::cout << m_trigORs[itrig] << std::endl;
      std::cout << pTavg/GeV << std::endl;
      std::cout << m_trigThresholds[itrig] << "\t" << m_trigThresholds[itrig+1] << std::endl;

      float min_pT = m_trigThresholds[itrig];
      float max_pT = m_trigThresholds[itrig+1];
      bool stop=false;

      if ( itrig==(m_trigORs.size()-1) )
	max_pT = 10e10;

      std::cout << itrig << "\t" << m_trigORs.size()-1 << std::endl;
      std::cout << (pTavg/GeV < m_trigThresholds[itrig]) << std::endl;
      std::cout << (itrig<(m_trigORs.size()-1)) << std::endl;
      std::cout << (pTavg/GeV > m_trigThresholds[itrig+1]) << std::endl;

      if ( pTavg/GeV > 40 )
	stop = true;
			 */
			// pass pTavg threshold requirement
			if ( pTavg/GeV < m_trigThresholds[itrig] || (itrig<(m_trigORs.size()-1) && pTavg/GeV > m_trigThresholds[itrig+1])  )
				continue;
			//std::cout << "FAIL" << std::endl;

			//	std::cout << "pass pTavg" << std::endl;

			//      if (stop) abort();

			// RefFJ_FJ or RefFJ_J
			std::string jetClass = TriggerClassification(m_trigORs[itrig]);
			if ( jetClass=="" )
				continue;

			//      std::cout << jetClass << std::endl;

			//      suffix = Form( "%s_%s",m_trigORs[itrig].Data(),jetClass.c_str() );
			ctrl_trig.ReplaceAll("HLT_","");
			suffix = Form( "%s_%s",ctrl_trig.Data(),jetClass.c_str() );

		}
	}

	// did not pass trigger/pTavg requirements
	if ( suffix=="" ) return;

	//  std::cout << suffix << std::endl;
	std::string prefix = Form("ControlPlots_%s_%s_%s_",plot.c_str(),jetAlgo.c_str(),suffix.c_str());
	//  std::cout << prefix << std::endl;

	m_hists1D[ prefix+"pTavg" ]->Fill(pTavg/GeV,m_weight);
	m_hists1D[ prefix+"Dphijj" ]->Fill(Dphijj,m_weight);

	if ( jet3 && jet3->pt() > 0 )
		m_hists1D[ prefix+"j3pT_pTavg_ratio" ]->Fill(jet3->pt()/pTavg,m_weight);
	else
		m_hists1D[ prefix+"j3pT_pTavg_ratio" ]->Fill(0.0,m_weight);

	m_hists1D[ prefix+"j1_pT" ]->Fill(jet1.pt()/GeV,m_weight);
	m_hists1D[ prefix+"j1_eta" ]->Fill(jet1.auxdata<float>("DetectorEta"),m_weight);
	m_hists1D[ prefix+"j1_phi" ]->Fill(jet1.phi(),m_weight);

	m_hists1D[ prefix+"j2_pT" ]->Fill(jet2.pt()/GeV,m_weight);
	m_hists1D[ prefix+"j2_eta" ]->Fill(jet2.auxdata<float>("DetectorEta"),m_weight);
	m_hists1D[ prefix+"j2_phi" ]->Fill(jet2.phi(),m_weight);

	if ( jet3 && jet3->pt() > 0 ) {
		m_hists1D[ prefix+"j3_pT" ]->Fill(jet3->pt()/GeV,m_weight);
		m_hists1D[ prefix+"j3_eta" ]->Fill(jet3->auxdata<float>("DetectorEta"),m_weight);
		m_hists1D[ prefix+"j3_phi" ]->Fill(jet3->phi(),m_weight);
	}

	return;
}



void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseControlHistograms(std::string prefix) {

	m_leadingTruthPT.insert( std::pair<std::string,TH1F*>(prefix+"leading truth PT",createHist1D(prefix+"leading truth PT",";my leading truth p_{T} [GeV];",2500,0,2500)) );

	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pTavg",createHist1D(prefix+"pTavg",";p_{T}^{avg} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"Dphijj",createHist1D(prefix+"Dphijj",";#Delta#phi_{jj};",100,0,TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j3pT_pTavg_ratio",createHist1D(prefix+"j3pT_pTavg_ratio",";p_{T}^{j3}/p_{T}^{avg};",500,0,5)) );

	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j1_pT",createHist1D(prefix+"j1_pT",";Leading jet p_{T} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j2_pT",createHist1D(prefix+"j2_pT",";Subleading jet p_{T} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j3_pT",createHist1D(prefix+"j3_pT",";Third jet p_{T} [GeV];",2500,0,2500)) );

	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j1_eta",createHist1D(prefix+"j1_eta",";Leading jet #eta;",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j2_eta",createHist1D(prefix+"j2_eta",";Subleading jet #eta;",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j3_eta",createHist1D(prefix+"j3_eta",";Third jet #eta;",100,-5,5)) );

	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j1_phi",createHist1D(prefix+"j1_phi",";Leading jet #phi;",100,-TMath::Pi(),TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j2_phi",createHist1D(prefix+"j2_phi",";Subleading jet #phi;",100,-TMath::Pi(),TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"j3_phi",createHist1D(prefix+"j3_phi",";Third jet #phi;",100,-TMath::Pi(),TMath::Pi())) );

	return;
}

void DijetInSitu::EtaInterCalxAODAnalysis::CreateHistograms(std::string prefix) {
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"sumOfWeights", createHist1D(prefix+"sumOfWeights",";Sum of weights;",1,0,1)) );
  if(m_runLightWeight)return;
	std::vector<double> Asm_bins = makeUniformVec(50,-1.5,1.5);
	std::vector<double> pTavg_bins = vectoriseD(m_settings->GetValue("AntiKt4EMTopo.pTavg.Bins",""));
	std::cout << "pTavg bins" << std::endl;
	for ( auto bin : pTavg_bins )
		std::cout << bin << " ";
	std::vector<double> eta_bins = vectoriseD(m_settings->GetValue("EtaBins.3Dhists",""));
	std::vector<double> pTavg_finebins = makeUniformVec(3990,10,4000);

	// 2D histograms
	m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvg_vs_EtaDet", createHist2D(prefix+"PtAvg_vs_EtaDet","",eta_bins,pTavg_bins)) );
	m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvgFine_vs_EtaProbeSM", createHist2D(prefix+"PtAvgFine_vs_EtaProbeSM","",eta_bins,pTavg_finebins)) );
	m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"PtAvgFine_vs_EtaProbeMM", createHist2D(prefix+"PtAvgFine_vs_EtaProbeMM","",eta_bins,pTavg_finebins)) );

	// 3D histograms

	// standard method
	// Asymmetry vs. pTavg and eta_probe
	m_hists3D.insert( std::pair<std::string,TH3F*>(prefix+"pTavg_etaprobe_Astandard", createHist3D(prefix+"pTavg_etaprobe_Astandard","",pTavg_bins,eta_bins,Asm_bins)) );

	// matrix method
	// Asymmetry vs. eta_right and eta_left in bins of pTavg

	//  for (const auto& bin : pTavg_bins ) {
	for ( uint ibin=1; ibin<pTavg_bins.size(); ++ibin )
		m_hists3D.insert( std::pair<std::string,TH3F*>(Form("%sAsym3D_PtBin%d",prefix.c_str(),ibin), createHist3D(Form("%sAsym3D_PtBin%d",prefix.c_str(),ibin),"",eta_bins,eta_bins,Asm_bins)) );
	for ( uint ibin=1; ibin<pTavg_bins.size(); ++ibin )
		m_hists3D.insert( std::pair<std::string,TH3F*>(Form("eta320350_%sAsym3D_PtBin%d",prefix.c_str(),ibin), createHist3D(Form("eta320350_%sAsym3D_PtBin%d",prefix.c_str(),ibin),"",eta_bins,eta_bins,Asm_bins)) );

}

// Initialise the histograms
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseTagAndProbeHists() {

	for ( const auto& trig : m_tagAndProbeTrigs ) {

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_tag_HLT_pT_lead",trig.Data()), createHist1D(Form("%s_tag_HLT_pT_lead",trig.Data()),";Tag jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_HLT_pT_lead",trig.Data()), createHist1D(Form("%s_probe_HLT_pT_lead",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_HLT_pT_match_lead",trig.Data()), createHist1D(Form("%s_probe_HLT_pT_match_lead",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_tag_HLT_pT_random",trig.Data()), createHist1D(Form("%s_tag_HLT_pT_random",trig.Data()),";Tag jet p_{T} [GeV];",2500,0,2500)) );

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_HLT_pT_random",trig.Data()), createHist1D(Form("%s_probe_HLT_pT_random",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_HLT_pT_match_random",trig.Data()), createHist1D(Form("%s_probe_HLT_pT_match_random",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_tag_L1RoI_pT_lead",trig.Data()), createHist1D(Form("%s_tag_L1RoI_pT_lead",trig.Data()),";Tag jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_L1RoI_pT_lead",trig.Data()), createHist1D(Form("%s_probe_L1RoI_pT_lead",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_L1RoI_pT_match_lead",trig.Data()), createHist1D(Form("%s_probe_L1RoI_pT_match_lead",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_tag_L1RoI_pT_random",trig.Data()), createHist1D(Form("%s_tag_L1RoI_pT_random",trig.Data()),";Tag jet p_{T} [GeV];",2500,0,2500)) );

		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_L1RoI_pT_random",trig.Data()), createHist1D(Form("%s_probe_L1RoI_pT_random",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_probe_L1RoI_pT_match_random",trig.Data()), createHist1D(Form("%s_probe_L1RoI_pT_match_random",trig.Data()),";Probe jet p_{T} [GeV];",2500,0,2500)) );


		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_DeltaR_offline_HLT",trig.Data()), createHist1D(Form("%s_DeltaR_offline_HLT",trig.Data()),";#Delta R (HLT,offline) jets;",50,0,5)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_DeltaR_HLT_L1RoI",trig.Data()), createHist1D(Form("%s_DeltaR_HLT_L1RoI",trig.Data()),";#Delta R (HLT,L1RoI) jets;",50,0,5)) );
		m_hists1D.insert( std::pair<std::string,TH1F*>(Form("%s_DeltaR_offline_L1RoI",trig.Data()), createHist1D(Form("%s_DeltaR_offline_L1RoI",trig.Data()),";#Delta R (HLT,L1RoI) jets;",50,0,5)) );

	}

}

// Initialise the histograms
void DijetInSitu::EtaInterCalxAODAnalysis::InitialiseHistograms(std::string jetAlgo) {



	std::vector<TString> plots = {"2jets","cleaning","JVT","Dphijj","j3pT"};

	if ( m_isMC ) {
		for ( int jx=0; jx<10; ++jx ) {
			CreateHistograms( Form("%s_J%d_",jetAlgo.c_str(),jx) );

			if ( m_controlPlots&& !m_runLightWeight )  {
				for ( const auto& plot : plots ) {
					InitialiseControlHistograms( Form("ControlPlots_%s_%s_J%d_",plot.Data(),jetAlgo.c_str(),jx) );
				}
			}
		}
	}
	else {
		std::vector<TString> trigs = vectorise(m_settings->GetValue("Trig_OR."+m_trigMenu,""));
		for ( const auto& trig : trigs ) {
			CreateHistograms( Form("%s_%s_RefFJ_FJ_",jetAlgo.c_str(),trig.Data()) );
			CreateHistograms( Form("%s_%s_RefFJ_J_",jetAlgo.c_str(),trig.Data()) );
		}

		for ( const auto& ctrl_trig : m_trigsCtrl ) {
			TString trig = ctrl_trig;
			trig.ReplaceAll("HLT_","");

			if ( m_controlPlots && !m_runLightWeight)  {
				for ( const auto&plot : plots ) {
					InitialiseControlHistograms( Form("ControlPlots_%s_%s_%s_RefFJ_J_",plot.Data(),jetAlgo.c_str(),trig.Data()) );
					InitialiseControlHistograms( Form("ControlPlots_%s_%s_%s_RefFJ_FJ_",plot.Data(),jetAlgo.c_str(),trig.Data()) );
				}
			}
		}
	}
  if(m_runLightWeight)return;
	std::string prefix = jetAlgo+"_";
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet1_pT", createHist1D(prefix+"jet1_pT",";Leading jet p_{T} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet2_pT", createHist1D(prefix+"jet2_pT",";Subleading jet p_{T} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet3_pT", createHist1D(prefix+"jet3_pT",";Third jet p_{T} [GeV];",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet1_eta", createHist1D(prefix+"jet1_eta",";Leading jet#eta;",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet2_eta", createHist1D(prefix+"jet2_eta",";Subleading jet#eta;",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet3_eta", createHist1D(prefix+"jet3_eta",";Third jet#eta;",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet1_phi", createHist1D(prefix+"jet1_phi",";Leading jet #phi;",100,-TMath::Pi(),TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet2_phi", createHist1D(prefix+"jet2_phi",";Subleading jet #phi;",100,-TMath::Pi(),TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"jet3_phi", createHist1D(prefix+"jet3_phi",";Third jet #phi;",100,-TMath::Pi(),TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pTavg", createHist1D(prefix+"pTavg",";p_{T}^{avg};",2500,0,2500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"deltaPhi", createHist1D(prefix+"deltaPhi",";#Delta#phi(j1,j2);",100,0,TMath::Pi())) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"eta_right", createHist1D(prefix+"eta_right",";#eta_{right};",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"eta_left", createHist1D(prefix+"eta_left",";#eta_{left};",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pT_right", createHist1D(prefix+"pT_right",";p_{T}^{right};",100,0,500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pT_left", createHist1D(prefix+"pT_left",";p_{T}^{left};",100,0,500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"eta_ref", createHist1D(prefix+"eta_ref",";#eta_{ref};",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"eta_probe", createHist1D(prefix+"eta_probe",";#eta_{probe};",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pT_ref", createHist1D(prefix+"pT_ref",";p_{T}^{ref};",100,0,500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"pT_probe", createHist1D(prefix+"pT_probe",";p_{T}^{probe};",100,0,500)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"Asm_SM", createHist1D(prefix+"Asm_SM",";A_{SM};",100,-5,5)) );
	m_hists1D.insert( std::pair<std::string,TH1F*>(prefix+"Asm_MM", createHist1D(prefix+"Asm_MM",";A_{MM};",100,-5,5)) );

	if ( !m_isMC ) {
		std::vector<TString> trigs = vectorise(m_settings->GetValue("TriggersForOverlap",""));
		int nbins = trigs.size();
		m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"Triggers_All", createHist2D(prefix+"Triggers_All","",nbins,0,nbins,nbins,0,nbins)) );
		m_hists2D.insert( std::pair<std::string,TH2F*>( prefix+"Triggers_PassSel", createHist2D(prefix+"Triggers_PassSel","",nbins,0,nbins,nbins,0,nbins)) );
		for ( int i=0; i<nbins; ++i) {
			m_hists2D[ prefix+"Triggers_All" ]->GetXaxis()->SetBinLabel(i+1,trigs[i]);
			m_hists2D[ prefix+"Triggers_All" ]->GetYaxis()->SetBinLabel(i+1,trigs[i]);
			m_hists2D[ prefix+"Triggers_PassSel" ]->GetXaxis()->SetBinLabel(i+1,trigs[i]);
			m_hists2D[ prefix+"Triggers_PassSel" ]->GetYaxis()->SetBinLabel(i+1,trigs[i]);
		}
	}
}

bool DijetInSitu::EtaInterCalxAODAnalysis::isPathalogicalEvent(const xAOD::EventInfo* eventInfo) {
	if ( !m_isMC )
		return false;

	// Powheg+Pythia8
	if ( eventInfo->mcChannelNumber()==426005 && (eventInfo->mcEventWeight()>1e8) ) {
		std::cout << "WARNING: excluding event with abnormally high weight (" << eventInfo->mcEventWeight() << ")" << std::endl;
		return true;
	}
	// Powheg+fHerwig
	else if ( (eventInfo->mcChannelNumber()==426101 && fabs(eventInfo->mcEventWeight())>1e8) ||
			(eventInfo->mcChannelNumber()==426102 && fabs(eventInfo->mcEventWeight())>1e8) ||
			(eventInfo->mcChannelNumber()==426103 && fabs(eventInfo->mcEventWeight())>1e6) ) {
		std::cout << "WARNING: excluding event with abnormally high weight (" << eventInfo->mcEventWeight() << ")" << std::endl;
		return true;
	}
	/*
  else if ( (eventInfo->mcChannelNumber()==426101 && fabs(eventInfo->mcEventWeight())>400e9) ||
	    (eventInfo->mcChannelNumber()==426102 && fabs(eventInfo->mcEventWeight())>20e9) ||
	    (eventInfo->mcChannelNumber()==426103 && fabs(eventInfo->mcEventWeight())>200e3) ) {
    std::cout << "WARNING: excluding event with abnormally high weight (" << eventInfo->mcEventWeight() << ")" << std::endl;
    return true;
    }*/
	return false;
}

bool DijetInSitu::EtaInterCalxAODAnalysis::isBadDataLumiBlock(const xAOD::EventInfo* eventInfo) {

	// run 267162
	// solenoid ramping until until LB 81
	if ( eventInfo->runNumber() == 267162 && eventInfo->lumiBlock() < 81 ) return true;

	return false;
}

bool DijetInSitu::EtaInterCalxAODAnalysis::PassAnyTrigger(const std::vector<TString> &trigs) {
	for ( const auto& trig : trigs ) {
		if ( m_trigDecTool->isPassed( std::string(trig.Data()) ) ) return true;
	}
	return false;
}

// Trigger emulation
// Fabrice Balli <fabrice.balli@cern.ch>
std::pair<int,int> DijetInSitu::EtaInterCalxAODAnalysis::EmulateTriggerDecisions2015(const xAOD::JetContainer* trig_jets_HLT, const xAOD::JetRoIContainer* trig_jets_L1, bool useL1Seed) {
	// Week 1 menu

	// Central jet triggers
	// Triggers that are random at L1
	bool j15 = PassHLT(15,(*trig_jets_HLT),true);
	bool j25 = PassHLT(25,(*trig_jets_HLT),true);
	bool j35 = PassHLT(35,(*trig_jets_HLT),true);
	bool j45 = PassHLT(45,(*trig_jets_HLT),true);

	// Triggers seeded from L1
	bool j60  = ( useL1Seed ? PassL1(20,(*trig_jets_L1), true) : useL1Seed) &&  PassHLT(60,(*trig_jets_HLT),true);
	//bool j80  = PassL1(20,(*trig_jets_L1), true) &&  PassHLT(80,(*trig_jets_HLT),true);

	//high pT trigger menu:  HLT_j15 HLT_j25 HLT_j60 HLT_j110 HLT_j175 HLT_j260 HLT_j360
	//also seed from L1

	bool j110 = ( useL1Seed ? PassL1(30,(*trig_jets_L1), true) : useL1Seed) &&PassHLT(110,(*trig_jets_HLT),true);
	bool j175 = ( useL1Seed ? PassL1(50,(*trig_jets_L1), true) : useL1Seed) &&  PassHLT(175,(*trig_jets_HLT),true);
	bool j260 = ( useL1Seed ? PassL1(75,(*trig_jets_L1), true) : useL1Seed) &&  PassHLT(260,(*trig_jets_HLT),true);
	bool j360 = ( useL1Seed ? PassL1(100,(*trig_jets_L1), true) : useL1Seed) &&  PassHLT(360,(*trig_jets_HLT),true);
	// Forward jet triggers
	// Triggers that are random at L1
	bool fj15 = PassHLT(15,(*trig_jets_HLT),false);
	bool fj25 = PassHLT(25,(*trig_jets_HLT),false);
	bool fj35 = PassHLT(35,(*trig_jets_HLT),false);
	bool fj45 = PassHLT(45,(*trig_jets_HLT),false);

	// Triggers seeded from L1
	bool fj60  = ( useL1Seed ? PassL1(20,(*trig_jets_L1), false) : useL1Seed) &&  PassHLT(60,(*trig_jets_HLT),false);
	//bool fj80  = PassL1(20,(*trig_jets_L1), false) &&  PassHLT(80,(*trig_jets_HLT),false);
	bool fj110 = ( useL1Seed ? PassL1(30,(*trig_jets_L1), false) : useL1Seed) && PassHLT(110,(*trig_jets_HLT),false);
	bool fj175 = ( useL1Seed ? PassL1(50,(*trig_jets_L1), false) : useL1Seed) &&  PassHLT(175,(*trig_jets_HLT),false);
	bool fj260 = ( useL1Seed ? PassL1(75,(*trig_jets_L1), false) : useL1Seed) &&  PassHLT(260,(*trig_jets_HLT),false);
	bool fj360 = ( useL1Seed ? PassL1(100,(*trig_jets_L1), false) : useL1Seed) &&  PassHLT(360,(*trig_jets_HLT),false);

	// Store trigger bits
	std::pair<int,int> TrigBit;

	//TrigBit.first = j15 | 2*j25 | 4*j35 | 8*j45 | 16*j55 | 32*j80 | 64*j110 | 128*j145 | 256*j180 | 512*j220 | 1024*j360 | 2048*j45L1J15;
	//TrigBit.second = fj15 | 2*fj25 | 4*fj35 | 8*fj45 | 16*fj55 | 32*fj80 | 64*fj110 | 128*fj145 | 256*fj180 | 512*fj220;

	TrigBit.first = j15 | 2*j25 |4*j35 | 8*j45| 16*j60 | 32*j110 | 64*j110 | 128*j175 | 256*j260 | 512*j360  ;
	TrigBit.second =  fj15 | 2*fj25 |4*fj35 | 8*fj45| 16*fj60 | 32*fj110 | 64*fj110 | 128*fj175 | 256*fj260 | 512*fj360  ;

	return TrigBit;
}

std::pair<int,int> DijetInSitu::EtaInterCalxAODAnalysis::EmulateTriggerDecisionsRun2(const xAOD::JetContainer* trig_jets_HLT, const xAOD::JetRoIContainer* trig_jets_L1, bool useL1Seed) {
	// Central jet triggers
	// Triggers that are random at L1
	bool j15 = PassHLT(15,(*trig_jets_HLT),true);
	bool j25 = PassHLT(25,(*trig_jets_HLT),true);
	bool j35 = PassHLT(35,(*trig_jets_HLT),true);

	// Triggers seeded from L1
  bool j45  = ( useL1Seed ? PassL1(15,(*trig_jets_L1), true) : true) &&  PassHLT(45,(*trig_jets_HLT),true);
	bool j60  = ( useL1Seed ? PassL1(20,(*trig_jets_L1), true) : true) &&  PassHLT(60,(*trig_jets_HLT),true);
	bool j110 = ( useL1Seed ? PassL1(30,(*trig_jets_L1), true) : true) &&  PassHLT(110,(*trig_jets_HLT),true);
	bool j175 = ( useL1Seed ? PassL1(50,(*trig_jets_L1), true) : true) &&  PassHLT(175,(*trig_jets_HLT),true);
	bool j260 = ( useL1Seed ? PassL1(75,(*trig_jets_L1), true) : true) &&  PassHLT(260,(*trig_jets_HLT),true);
	bool j360 = ( useL1Seed ? PassL1(100,(*trig_jets_L1), true) : true) &&  PassHLT(360,(*trig_jets_HLT),true);
  bool j400 = ( useL1Seed ? PassL1(100,(*trig_jets_L1), true) : true) &&  PassHLT(400,(*trig_jets_HLT),true);

  // Forward jet triggers
	// Triggers that are random at L1
	bool fj15 = PassHLT(15,(*trig_jets_HLT),false);
	bool fj25 = PassHLT(25,(*trig_jets_HLT),false);
	bool fj35 = PassHLT(35,(*trig_jets_HLT),false);

	// Triggers seeded from L1
  bool fj45  = ( useL1Seed ? PassL1(15,(*trig_jets_L1), false) : true) &&  PassHLT(45,(*trig_jets_HLT),false);
	bool fj60  = ( useL1Seed ? PassL1(20,(*trig_jets_L1), false) : true) &&  PassHLT(60,(*trig_jets_HLT),false);
	bool fj110 = ( useL1Seed ? PassL1(30,(*trig_jets_L1), false) : true) &&  PassHLT(110,(*trig_jets_HLT),false);
	bool fj175 = ( useL1Seed ? PassL1(50,(*trig_jets_L1), false) : true) &&  PassHLT(175,(*trig_jets_HLT),false);
	bool fj260 = ( useL1Seed ? PassL1(75,(*trig_jets_L1), false) : true) &&  PassHLT(260,(*trig_jets_HLT),false);
	bool fj360 = ( useL1Seed ? PassL1(100,(*trig_jets_L1), false) : true) &&  PassHLT(360,(*trig_jets_HLT),false);
  bool fj400 = 0; // HLT_j400_320eta490 does not exist in the trigDecTool, but the variable is only needed for the other scripts of the package

	// Store trigger bits
	std::pair<int,int> TrigBit;

	TrigBit.first  =  j15  | 2*j25  |4*j35  | 8*j45  | 16*j60  | 32*j110  | 64*j175  | 128*j260  | 256*j360  | 512*j400  ;
	TrigBit.second =  fj15 | 2*fj25 |4*fj35 | 8*fj45 | 16*fj60 | 32*fj110 | 64*fj175 | 128*fj260 | 256*fj360 | 512*fj400  ;

	return TrigBit;
}

bool DijetInSitu::EtaInterCalxAODAnalysis::PassHLT(float ptcut,const xAOD::JetContainer &trig_jets_HLT, bool isCentral){
	for ( xAOD::JetContainer::const_iterator jet = trig_jets_HLT.begin(); jet < trig_jets_HLT.end(); ++jet ) {
		if( (*jet)->p4().Et()/1000. > ptcut && fabs((*jet)->eta()) < 3.2 && isCentral) return true;
		else if( (*jet)->p4().Et()/1000. > ptcut && fabs((*jet)->eta()) > 3.2 && !isCentral) return true;
	}
	return false;
}

bool DijetInSitu::EtaInterCalxAODAnalysis::PassL1(float ptcut,const xAOD::JetRoIContainer &trig_jets_L1, bool isCentral){
	for ( xAOD::JetRoIContainer::const_iterator jet = trig_jets_L1.begin(); jet < trig_jets_L1.end(); ++jet ) {
		if( (*jet)->et8x8()/1000. > ptcut && fabs((*jet)->eta()) < 3.1 && isCentral) return true;
		else if( (*jet)->et8x8()/1000. > ptcut && fabs((*jet)->eta()) > 3.1 && !isCentral) return true;
	}
	return false;
}
