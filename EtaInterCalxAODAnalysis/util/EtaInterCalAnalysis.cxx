/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */

#include "EtaInterCalxAODAnalysis/EtaInterCalxAODAnalysis.h"

int main(int argc, char** argv) {

  xAOD::Init();
  xAOD::TStore store; // create a transient object store (needed for the tools)
  // xAOD access mode
  // //branch access is available for the xAOD->xAOD reprocessing,
  // derivations and 13 TeV DC14 MC samples (and needed to access trigger information)

  //WE should always be using Class access from now non
  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kClassAccess;
  xAOD::TEvent event(mode);

  //  StatusCode::enableFailure();
  //  xAOD::TReturnCode::enableFailure();

  std::vector<TString> infiles;
  std::vector<TString> otf_settings;
  TString ofn="";

  // loop over arguments
  for(int i=1; i<argc; ++i){
    TString arg = TString(argv[i]);
    if(arg.BeginsWith("-")){
      arg.ReplaceAll("-","");
      if(arg=="input"||arg=="in"){
	infiles = vectorise(TString(argv[i+1]),",");
	if(!TString(argv[i+1]).Contains(",")) {
	  infiles.clear();
	  for(int j=i+1; j<argc; ++j) {
	    if(arg.BeginsWith("-")) break;
	    infiles.push_back(argv[j]);
	    ++i;
	  }
	}
	continue;
      }
      if(arg=="set"||arg=="s"){ otf_settings = vectorise(TString(argv[i+1]),","); ++i; continue; }
      if(arg=="out"||arg=="o"){ ofn = argv[i+1]; ++i; continue; }
      std::cout << "Don't understand input argument " << arg << std::endl;
      abort();
    }

  }

  if(infiles.size()<1) {
    infiles = vectorise(TString(argv[1]),",");
    if(!TString(argv[1]).Contains(",")) {
      infiles.clear();
      for(int i=1; i<argc; ++i)
	      infiles.push_back(argv[i]);
    }
  }

  if (infiles.size()<1) {
    std::cout << "Couldn't find any input files!" << std::endl;
    abort();
  }

  for ( TString str : infiles )
    std::cout << str << std::endl;

  char buf[kMAXPATHLEN+1]={0};

  TFile *f;
  if ( readlink(infiles[0].Data(),buf, kMAXPATHLEN) >=0) { f = TFile::Open(buf); }
  else { f = TFile::Open(infiles[0].Data()); }
  memset(buf,0,sizeof(buf));

  if (f==NULL)
  {
    std::cout << "File not found: " << infiles[0] << std::endl;
    abort();
  }

  if ( !f->IsOpen() ) {
    std::cout << "Failed to open file " << infiles[0] << std::endl;
    abort();
  }

  if ( !event.readFrom(f).isSuccess() )
    std::cout << "Failed to read from file " << infiles[0] << std::endl;

  // for BDT
  if( event.getEntry(0) <= 0) {
	   std::cout << "Couldn't load the first event from the input!" << std::endl;
	//      return 1;
  }   


  std::cout << "\n\n  Initialising EtaInterCalxAODAnalysis... \n\n" << std::endl;

  DijetInSitu::EtaInterCalxAODAnalysis analysis;
  analysis.Initialise(event,otf_settings,ofn);

  int eventCounter = 0;
  int tot_events = 0;

  for (const TString& filename : infiles) {
    TFile* f = TFile::Open(filename.Data());
    event.readFrom(f);
    tot_events += event.getEntries();
    f->Close();
  }

  for (const TString& filename : infiles) {
    std::cout << "Opening " << filename << std::endl;

    TFile* f;

    if ( readlink(filename.Data(),buf, kMAXPATHLEN) >=0) { f = TFile::Open(buf); }
    else { f = TFile::Open(filename.Data()); }
    memset(buf,0,sizeof(buf));

    event.readFrom(f);
    const unsigned int n_entries = event.getEntries();
		//std::cout << "WARNING ONLY RUNNING ON FIRST 1000 ENTRIES" << std::endl;

    std::cout << " Processing file: " << filename << std::endl;
    std::cout << " Entries in file: " << n_entries << std::endl;

    for (unsigned int ientry = 0; ientry < n_entries; ++ientry) {


      if (ientry % 1000 == 0)
	std::cout << " Processing event " << ientry << " / " << n_entries << std::endl;

	//      	std::cout << " Processing event " << ientry << " / " << n_entries << "(" << 100*(eventCounter/tot_events) << "% )" << std::endl;

      event.getEntry(ientry);
      analysis.ProcessEvent(event);
      eventCounter++;
    }

    f->Close();
  }

  std::cout << eventCounter << " events processed in total." << std::endl;
  analysis.Finalise();

  return 0;
}
